<?php
if (!defined('ROOTPATH')) define('ROOTPATH', dirname(__FILE__));
date_default_timezone_set('Asia/Jakarta');
require_once 'config/config.php';
require_once 'config/session.php';
require_once 'libraries/loader/Library.class.php';
require_once MODULE_PATH.'core/model.core.php';

$module = isset($_GET['m']) ? $_GET['m'] : DEFAULT_MODULE;
$controller = isset($_GET['c']) ? $_GET['c'] : DEFAULT_CONTROLLER;
$action = isset($_GET['a']) ? strtoupper($_GET['a']) : 'VIEW';
/* Check module is protected ? */
if (!isset($_SESSION['isLoggedIn']) && !empty($configModule['private']) && in_array($module, $configModule['private'])){
	$module = 'login';
	$controller = 'viewLogin';
	$action = 'VIEW';
}
/* end check module is protected */

$modulePage = MODULE_PATH.$module.'/'.$controller.EXT;


if ($module == '' && $controller == '' && $action == 'VIEW'){
	$modulePage = MODULE_PATH.DEFAULT_MODULE.'/'.DEFAULT_CONTROLLER.EXT;
	include 'header.php';
	?>
	<div id="content-center">
		<div class="content-isi">
	    	<?php include $modulePage;?>
		</div>	
	</div>
	<?php
	include 'footer.php';
}else
if ($module != '' && $controller != '' && file_exists($modulePage) && $action == 'VIEW'){
	include 'header.php';
	?>
	<div id="content-center">
		<div class="content-isi">	
	    	<?php include $modulePage;?>
		</div>	
	</div>
	<?php
	include 'footer.php';
}else
if ($module != '' && $controller != '' && file_exists($modulePage) && $action == 'DO'){
	include $modulePage;
}else{
	include 'header.php';
	?>
	<div id="content-center">
		<div class="content-isi">	
	    	<strong>404, Module tidak ditemukan</strong>
		</div>	
	</div>
	<?php
	include 'footer.php';
}
?>