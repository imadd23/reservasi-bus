<?php
Class Library{

	function __construct(){

	}

	static function load($className = '', $folderName = ''){
		$path = Self::getDir($folderName).'/'.$className;
		if (file_exists($path)){
			require_once $path;
		}else{
			die('File library tidak ditemukan');
		}
	}

	static function getDir($folderName){
		$basedir = str_replace('loader', '', dirname(__FILE__));
		$result = $basedir.$folderName;
		return $result;
	}
}
?>