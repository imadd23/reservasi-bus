<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<title><?php echo ucwords(str_replace('_', ' ', $module));?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap.min.css';?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/font-awesome.min.css';?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/AdminLTE.css';?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/style.css';?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datatables/jquery.dataTables.css';?>">
	<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datatables/dataTables.bootstrap.css';?>">
	<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/jquery-1.11.1.min.js';?>"></script>
	<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap.min.js';?>"></script>
	<script src="<?php echo BASE_ADDRESS.'js/datatables/jquery.dataTables.min.js';?>"></script>
	<script src="<?php echo BASE_ADDRESS.'js/jqBootstrapValidation.js';?>"></script>
</head>
<body>
	<div id="wrp">
		<div id="header">
			<div id="logo"><img src="<?php echo BASE_ADDRESS.'images/12.jpg';?>" width="100%" height="100%"></div>
			<div id="judul-web" style="text-align:center"><h1>PO. Family  Raya Ceria</h1></div>
			<div id="subjudul-web" style="text-align:center"><h1>Jl. Perintis Kemerdekaan no.228 Yogyakarta</h1></div>
		</div>
			
		<div style="clear:both"></div>

		<div class="td_konten">
			<div id="menu">
				<nav class="navbar navbar-default navbar-no-space">
				  <div class="container-fluid container-no-padding">
				    <!-- <div class="navbar-header">
				      <a class="navbar-brand" href="<?php echo BASE_URL.'?m=dashboard&c=viewDashboard&a=view';?>">Beranda</a>
				    </div> -->
				    <ul class="nav navbar-nav">
				    	<li <?php if (!isset($_GET['m']) || ((isset($_GET['m']) && $_GET['m'] == 'beranda'))) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=beranda&c=beranda&a=view';?>">Beranda</a></li>
				      	<li <?php if ((isset($_GET['m']) && in_array($_GET['m'], array('pemesanan_tiket', 'pembatalan', 'pengiriman_barang', 'pariwisata')))) echo 'class="dropdown active"'; else echo 'class="dropdown"';?>>
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Reservasi
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					        	<li><a href="<?php echo BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view';?>">Pemesanan Tiket</a></li>
					        	<li><a href="<?php echo BASE_URL.'?m=pembatalan&c=viewPembatalan&a=view';?>">Pembatalan Tiket</a></li>
					        	<li><a href="<?php echo BASE_URL.'?m=pemesanan_tiket&c=viewKonfirmasiTiket&a=view';?>">Konfirmasi Pemesanan Tiket</a></li>
					        	<li><a href="<?php echo BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view';?>">Cek Harga Pengiriman</a></li>
					        	<li><a href="<?php echo BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view';?>">Pariwisata</a></li>
					        </ul>
				      	</li>
				      	<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'galeri')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=galeri&c=viewGaleri&a=view';?>">Galeri</a></li>
				   		<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'tentang')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=tentang&c=viewTentang&a=view';?>">Tentang</a></li>
				   		<!-- <li <?php if ((isset($_GET['m']) && $_GET['m'] == 'konfirmasi_pembayaran')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=konfirmasi_pembayaran&c=viewKonfirmasiPembayaran&a=view';?>">Konfirmasi Pembayaran</a></li> -->
				    </ul>
				    <?php
				    if (isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == true){
				    	?>
				    	<ul class="nav navbar-nav navbar-right">
				    		<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'profil')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=profil&c=viewProfil&a=view';?>">Profil</a></li>
					      	<li><a href="<?php echo BASE_URL.'?m=login&c=doLogout&a=do';?>">Logout</a></li>
					    </ul>
				    	<?php
				    }else{
				    	?>
				    	<ul class="nav navbar-nav navbar-right">
				    		<li><a href="<?php echo BASE_URL.'?m=register&c=viewRegister&a=view';?>">Daftar</a></li>
					      	<li><a href="<?php echo BASE_URL.'?m=login&c=viewLogin&a=view';?>">Login</a></li>
					    </ul>
				    	<?php
				    }
				    ?>
				  </div>
				</nav>
			</div>
		</div>

		<div id="content">