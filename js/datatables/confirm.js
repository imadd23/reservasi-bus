$(".confirmation").confirm({
    text: "Apakah anda yakin ingin menghapus data tersebut?",
    title: "Konfirmasi",
    confirm: function(button)
    {
        var target = button.prop("href");

        $.ajax({
            url: target,
            type: "POST",
            data: { _method:"DELETE" },
            success: function()
            {
                location.reload();
            },
            error: function()
            {
                alert("Failed to delete data.");
            }
        });
    },
    confirmButton: "Ya",
    cancelButton: "Tidak",
    post: true,
    confirmButtonClass: "btn-danger"
});