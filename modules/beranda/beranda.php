<?php
$message = '';
if (isset($_GET['registerSuccess']) && $_GET['registerSuccess'] == '1'){
	$message = '<div class="alert alert-success">Pendaftaran akun berhasil</div>';
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/flexslider.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/slider.css';?>">
<div class="cont-Beranda">
	<!-- slider -->
	<div id="box-wrapper">
		<div id="wrapper">
			<div id="wrapper-footer">
		    	<br>
		    	<marquee>PO. FAMILY RAYA CERIA  PO. FAMILY RAYA CERIA</marquee>
		    	<br>
		    </div>

		    <div id="wrapper-header">
		        <div id="slider-container">
		            <div class="flexslider">
		              <ul class="slides">
		                    <li>
		                        <img src="<?php echo BASE_ADDRESS.'images/galery/1.jpg';?>" />
	                        </li>
	                        <li>
	                        	<img src="<?php echo BASE_ADDRESS.'images/galery/2.jpg';?>" />
	                        </li>
	                        <li>
		                        <img src="<?php echo BASE_ADDRESS.'images/galery/3.jpg';?>" />
		                    </li>
		                </ul>
		            </div>
		        </div>
		    </div>
		    
		    <div id="wrapper-content">
		    	<div class="col-sm-12 no-padding">
		    		<?php echo $message;?>
		    	</div>
		    </div>
		    
		    <div id="wrapper-beforefooter">
		    </div>
		</div>
	</div>
	<!-- end slider -->
	<div class="cont-judul">PO. FAMILY RAYA CERIA</div>
    <div class="cont-text">
    	<p>Selamat datang di Official Website PO. FAMILY RAYA CERIA</p>
    </div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/jquery.flexslider.js';?>"></script>
<script type="text/javascript" >
$(document).ready(function(){
    $('.flexslider').flexslider({
        animation:"slide"
    });
});
</script>
    