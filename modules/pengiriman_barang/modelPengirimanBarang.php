<?php
Class modelPengirimanBarang{

	protected $wheres;

	public function __construct(){
		$this->wheres = '';
	}

	public function where($whereData = array()){
		$where = '';
		if (!empty($whereData) && is_array($whereData)){
			$where .= ' WHERE ';
			foreach ($whereData as $key => $value) {
				$where .= $key." = '".$value."' AND ";
			}
			$where .= '1 = 1';
		}else
		if (!empty($whereData) && is_string($whereData)){
			$where .= ' WHERE '.$whereData;
		}
		$this->wheres = $where;
	}

	private function setData($newData){
		$_newData = '';
		if (!empty($newData)){
			$maxKey = max(array_keys(array_values($newData)));
			$i = 0;
			foreach ($newData as $key => $value) {
				if ($i === $maxKey){
					$_newData .= $key." = '".$value."'";
				}else{
					$_newData .= $key." = '".$value."', ";
				}
				$i++;				
			}
		}
		return $_newData;
	}

	public function getPaket(){
		$query = mysql_query("SELECT * FROM paket_pengiriman WHERE status = '1'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getHarga(){

		$query = mysql_query("SELECT * FROM harga_pengiriman ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getKota(){
		$query = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_tujuan
			FROM kota
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi".
			$this->wheres."
			ORDER BY CONCAT(nama_kota, ' (', nama_provinsi, ')') ASC ");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function add($newData = array()){
		$_newData = $this->setData($newData);
		$query = "INSERT INTO pengiriman SET ".$_newData;
		$query = mysql_query($query);
		return $query;
	}

	public function update($data = array()){
		$newData = $this->setData($data);
		$query = "UPDATE pengiriman SET ".$newData." ".$this->wheres;
		return $query;
		$update = mysql_query($query);
		return $update;
	}

	public function delete(){
		$query = mysql_query("DELETE FROM pengiriman ".$this->wheres);
		return $query;
	}

	public function getDataPengiriman(){
		$query = mysql_query("SELECT * FROM pengiriman ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPengiriman(){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			harga_pengiriman.id_paket_pengiriman, harga_pengiriman.id_kota_asal,  harga_pengiriman.id_kota_tujuan
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi` ".
			$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}
}
?>