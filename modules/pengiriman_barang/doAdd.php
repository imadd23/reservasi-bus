<?php
require_once 'modelPengirimanBarang.php';
$model = new modelPengirimanBarang();

$p = array(
	'user_id' => $_SESSION['user_id'],
	'alamat_tujuan' =>  $_POST['alamat_tujuan'],
	'id_harga_pengiriman' => $_POST['id_harga_pengiriman'],
	'berat' => $_POST['berat'],
	'harga_per_kilo' => $_POST['harga_per_kilo'],
	'total_harga' => $_POST['total_harga']
   );

if (isset($_POST['save'])){
    if (empty($p['user_id']) || empty($p['alamat_tujuan']) || empty($p['id_harga_pengiriman']) || empty($p['berat']) || empty($p['harga_per_kilo']) || empty($p['total_harga'])){
    	header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&statusAdd=0&'.http_build_query($_POST));
    }else
    if (empty($_POST['id_pengiriman'])){   	
    	$save = $model->add($p);
	    if ($save){
	    	header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&statusAdd=1');
	    }else{
	    	header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&statusAdd=0&'.http_build_query($_POST));
	    }
	}else
	if (!empty($_POST['id_pengiriman'])){
		$model->where(array('id_pengiriman' => $_POST['id_pengiriman']));
		$save = $model->update($p);
	    if ($save){
	    	header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&statusUpdate=1');
	    }else{
	    	print_r($_POST);
	    	header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&statusUpdate=0&'.http_build_query($_POST));
	    }
	}		
}
?>