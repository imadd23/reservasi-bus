<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$id_pemberangkatan = $_POST['ref_id'];
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$arrayNoKursi = $model->getNoKursi($id_pemberangkatan, $tanggal_pemberangkatan);
$harga = $model->getHarga($id_pemberangkatan);
$harga = (empty($harga)) ? 0 : $harga[0]['harga'];
echo json_encode(array('success' => true, 'data' => $arrayNoKursi, 'harga' => $harga));
?>