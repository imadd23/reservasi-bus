<?php
require_once 'modelPengirimanBarang.php';
$model = new modelPengirimanBarang();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$message = '<div class="alert alert-success">Data pengiriman berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data pengiriman gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data pengiriman berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data pengiriman gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}
$id_pengiriman = isset($_GET['id_pengiriman']) ? $_GET['id_pengiriman'] : '';
$alamat_tujuan = isset($_GET['alamat_tujuan']) ? $_GET['alamat_tujuan'] : '';
$id_harga_pengiriman = isset($_GET['id_harga_pengiriman']) ? $_GET['id_harga_pengiriman'] : '';
$berat = isset($_GET['berat']) ? $_GET['berat'] : '';
$harga_per_kilo = isset($_GET['harga_per_kilo']) ? $_GET['harga_per_kilo'] : '';
$total_harga = isset($_GET['total_harga']) ? $_GET['total_harga'] : '';
$estimasi_hari = isset($_GET['estimasi_hari']) ? $_GET['estimasi_hari'] : '';
$tanggal_kirim = isset($_GET['tanggal_kirim']) ? date('d-m-Y', strtotime($_GET['tanggal_kirim'])) : '';
$estimasi_tanggal_sampai = isset($_GET['estimasi_tanggal_sampai']) ? date('d-m-Y', strtotime($_GET['estimasi_tanggal_sampai'])) : '';
$tanggal_sampai = isset($_GET['tanggal_sampai']) ? date('d-m-Y', strtotime($_GET['tanggal_sampai'])) : '';

?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-8">
		<h3 class="text-center">Cek Harga Pengiriman Barang</h3>
		<?php echo $message;?>
		<div class="form-horizontal">
			<input type="hidden" name="id_pengiriman" value="<?php echo $id_pengiriman;?>">
			<div class="form-group">
				<label class="control-label col-sm-4">Berat Barang *</label>
				<div class="col-sm-8">
					<input type="text" name="berat" class="form-control input-sm" value="<?php echo $berat;?>" required>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Paket *</label>
				<div class="col-sm-8">
					<select name="id_paket_pengiriman" class="form-control input-sm" required>
						<option value="">Pilih Paket</option>
						<?php
						if (!empty($model->getPaket())){
							foreach ($model->getPaket() as $key => $value) {
								if (isset($_GET['id_paket_pengiriman']) && $value['id_paket_pengiriman'] == $_GET['id_paket_pengiriman']){
									echo '<option value="'.$value['id_paket_pengiriman'].'" selected>'.$value['nama_paket'].'</option>';
								}else{
									echo '<option value="'.$value['id_paket_pengiriman'].'">'.$value['nama_paket'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Lokasi Asal *</label>
				<div class="col-sm-8">
					<select name="id_kota_asal" class="form-control input-sm" required>
						<option value="">Pilih Kota</option>
						<?php
						if (!empty($model->getKota())){
							foreach ($model->getKota() as $key => $value) {
								if (isset($_GET['id_kota_asal']) && $value['id_kota'] == $_GET['id_kota_asal']){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Lokasi Tujuan *</label>
				<div class="col-sm-8">
					<select name="id_kota_tujuan" class="form-control input-sm" required>
						<option value="">Pilih Kota</option>
						<?php
						if (!empty($model->getKota())){
							foreach ($model->getKota() as $key => $value) {
								if (isset($_GET['id_kota_tujuan']) && $value['id_kota'] == $_GET['id_kota_tujuan']){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<!-- <div class="form-group">
				<label class="control-label col-sm-4">Alamat Tujuan *</label>
				<div class="col-sm-8">
					<textarea name="alamat_tujuan" class="form-control"><?php echo $alamat_tujuan;?></textarea>
				</div>
			</div> -->
			<div class="form-group">
				<label class="control-label col-sm-4">Harga Per Kilo </label>
				<div class="col-sm-6">
					<input type="text" name="harga_per_kilo" class="form-control input-sm" value="<?php echo $harga_per_kilo;?>" readonly>
					<input type="hidden" name="id_harga_pengiriman" value="">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Total Harga </label>
				<div class="col-sm-6">
					<input type="text" name="total_harga" class="form-control input-sm" value="<?php echo $total_harga;?>" readonly>
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Estimasi Hari </label>
				<div class="col-sm-6">
					<input type="text" name="estimasi_hari" class="form-control input-sm" value="<?php echo $estimasi_hari;?>" readonly>
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<!-- <div class="form-group">
				<label class="col-sm-4"></label>
				<div class="col-sm-8">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div> -->
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
			format : 'dd-mm-yyyy',
			autoClose : true
		}).on('changeDate', function(ev){
			$(this).datepicker('hide');
		});
	});

	$(document).on('change', 'select[name=id_paket], select[name=id_kota_asal], select[name=id_kota_tujuan], input[name=berat]', function(){
        getHarga();
    });

	function getHarga(){
		var _id_paket = $('select[name=id_paket_pengiriman]').val();
		var _id_kota_asal = $('select[name=id_kota_asal]').val();
		var _id_kota_tujuan = $('select[name=id_kota_tujuan]').val();
		var _berat = $('input[name=berat]').val();
        var imgLoader = $('.imgLoader');
        if (_id_paket != '' && _id_kota_asal != '' && _id_kota_tujuan != '' && _berat != ''){
        	imgLoader.show();
	        $.ajax({
	            url : "<?php echo BASE_URL.'?m=pengiriman_barang&c=getHargaPaketJson&a=do';?>",
	            type : 'post', 
	            dataType : 'jSON',
	            data : {
	                id_paket : _id_paket,
	                id_kota_asal : _id_kota_asal,
	                id_kota_tujuan : _id_kota_tujuan,
	                berat : _berat
	            },
	            success : function(response){
	                imgLoader.hide();
	                if (response.success){
	                    $('input[name=estimasi_hari]').val(response.estimasi_hari);
	                    $('input[name=total_harga]').val(response.total_harga);
	                    $('input[name=harga_per_kilo]').val(response.harga);
	                    $('input[name=id_harga_pengiriman]').val(response.id_harga_pengiriman);
	                }
	            },
	            error : function(){
	                imgLoader.hide();
	                alert('Gagal Mengambil data nomer kursi kosong');
	            }
	        });
        }else{
        	$('input[name=estimasi_hari]').val('');
	        $('input[name=total_harga]').val('');
        }
        return;        
	}
</script>