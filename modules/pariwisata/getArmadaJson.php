<?php
ob_start();
require_once 'modelPariwisata.php';
$model = new modelPariwisata();
$tanggal_pemakaian = date('Y-m-d', strtotime($_POST['tanggal_pemakaian']));
$tanggal_kembali = date('Y-m-d', strtotime($_POST['tanggal_kembali']));
$data = $model->getArmada($tanggal_pemakaian, $tanggal_kembali);
echo json_encode(array('success' => true, 'data' => $data));
?>