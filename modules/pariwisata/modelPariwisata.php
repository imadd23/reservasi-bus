<?php
Class modelPariwisata extends model{

	public $wheres;

	public function __construct(){
		parent::__construct();
		$this->wheres = '';
	}

	public function where($whereData = array()){
		$where = '';
		if (!empty($whereData) && is_array($whereData)){
			$where .= ' WHERE ';
			foreach ($whereData as $key => $value) {
				$where .= $key." = '".$value."' AND ";
			}
			$where .= '1 = 1';
		}else
		if (!empty($whereData) && is_string($whereData)){
			$where .= ' WHERE '.$whereData;
		}
		$this->wheres = $where;
	}

	protected function setData($newData){
		$_newData = '';
		if (!empty($newData)){
			$maxKey = max(array_keys(array_values($newData)));
			$i = 0;
			foreach ($newData as $key => $value) {
				if ($i === $maxKey){
					$_newData .= $key." = '".$value."'";
				}else{
					$_newData .= $key." = '".$value."', ";
				}
				$i++;				
			}
		}
		return $_newData;
	}

	public function getArmada($tanggal_pemakaian = '', $tanggal_kembali = ''){
		$query = mysql_query("SELECT sewa_bus.*, armada.id_armada, armada.`muatan_penumpang`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada
			FROM sewa_bus
			LEFT JOIN armada ON sewa_bus.`id_armada` = armada.`id_armada`
			LEFT JOIN `penyewaan` ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			WHERE armada.`id_armada` NOT IN (
				SELECT armada.`id_armada`
				FROM sewa_bus
				LEFT JOIN armada ON sewa_bus.`id_armada` = armada.`id_armada`
				LEFT JOIN `penyewaan` ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
				WHERE
				'".$tanggal_pemakaian."' BETWEEN penyewaan.`tanggal_pemakaian` AND penyewaan.`tanggal_kembali` OR 
				'".$tanggal_kembali."' BETWEEN penyewaan.`tanggal_pemakaian` AND penyewaan.`tanggal_kembali`
			)
			GROUP BY id_sewa_bus
			ORDER BY nama_armada ASC");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getHarga($id_sewa_bus = 0){
		$query = mysql_query("SELECT * FROM sewa_bus WHERE id_sewa_bus = ".$id_sewa_bus);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function add($newData){
		$_newData = $this->setData($newData);
		$_query = "INSERT INTO penyewaan SET ".$_newData;
		$query = mysql_query($_query);
		return $query;
	}

	public function update($data = array()){
		$newData = $this->setData($data);
		$query = mysql_query("UPDATE penyewaan SET ".$newData." ".$this->wheres);
		return $query;
	}

	public function delete(){
		$query = mysql_query("DELETE FROM penyewaan ".$this->wheres);
		return $query;
	}

	public function getDataPariwisata(){
		$query = mysql_query("SELECT * FROM penyewaan ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPariwisata(){
		$query = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada, armada.`muatan_penumpang` 
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada` ".
			$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	function getDetailArmada($id_armada){
		$query = mysql_query("SELECT * FROM armada WHERE id_armada = '$id_armada'");
		return $this->result($query);
	}

	function getSewaBusById($id_sewa_bus){
		$query = mysql_query("SELECT sewa_bus.*, armada.id_armada, armada.`muatan_penumpang`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada
			FROM sewa_bus
			LEFT JOIN armada ON sewa_bus.`id_armada` = armada.`id_armada`
			LEFT JOIN `penyewaan` ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			WHERE sewa_bus.id_sewa_bus = '$id_sewa_bus'
			GROUP BY sewa_bus.id_sewa_bus
			ORDER BY nama_armada ASC");
		return $this->result($query);
	}

}
?>