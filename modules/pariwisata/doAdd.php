<?php
require_once 'modelPariwisata.php';
$model = new modelPariwisata();
$p = array(
	'user_id' => $_SESSION['user_id'],
	'id_sewa_bus' =>  $_POST['id_sewa_bus'],
	'tanggal_pemakaian' => date('Y-m-d', strtotime($_POST['tanggal_pemakaian'])),
	'tanggal_kembali' => date('Y-m-d', strtotime($_POST['tanggal_kembali'])),
	'jumlah_hari' => $_POST['jumlah_hari'],
	'harga_perhari' => $_POST['harga_perhari'],
	'total_harga' => $_POST['total_harga']
   );

if (isset($_POST['save'])){
    if (empty($p['id_sewa_bus']) || empty($p['tanggal_pemakaian']) || empty($p['tanggal_kembali']) || empty($p['jumlah_hari']) || empty($p['harga_perhari']) || empty($p['total_harga'])){
    	header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&statusAdd=0&'.http_build_query($_POST));
    }else
    if (empty($_POST['id_penyewaan'])){   	
    	$save = $model->add($p);
	    if ($save){
	    	header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&statusAdd=1');
	    }else{
	    	header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&statusAdd=0&'.http_build_query($_POST));
	    }
	}else
	if (!empty($_POST['id_penyewaan'])){
		$model->where(array('id_penyewaan' => $_POST['id_penyewaan']));
		$save = $model->update($p);
	    if ($save){
	    	header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&statusUpdate=1');
	    }else{
	    	header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&statusUpdate=0&'.http_build_query($_POST));
	    }
	}		
}
?>