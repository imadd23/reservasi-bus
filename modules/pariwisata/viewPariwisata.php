<?php
require_once 'modelPariwisata.php';
$model = new modelPariwisata();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$message = '<div class="alert alert-success">Data penyewaan bus berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data penyewaan bus gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data penyewaan bus berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data penyewaan bus gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
} 
$id_penyewaan = isset($_GET['id_penyewaan']) ? $_GET['id_penyewaan'] : '';
$id_sewa_bus = isset($_GET['id_sewa_bus']) ? $_GET['id_sewa_bus'] : ''; 
$tanggal_pemakaian = isset($_GET['tanggal_pemakaian']) ? date('d-m-Y', strtotime($_GET['tanggal_pemakaian'])) : '';
$tanggal_kembali = isset($_GET['tanggal_kembali']) ? date('d-m-Y', strtotime($_GET['tanggal_kembali'])) : '';  
$jumlah_hari = isset($_GET['jumlah_hari']) ? $_GET['jumlah_hari'] : '0';  
$harga_perhari = isset($_GET['harga_perhari']) ? $_GET['harga_perhari'] : '0';  
$total_harga = isset($_GET['total_harga']) ? $_GET['total_harga'] : '0';
$muatan_armada = isset($_GET['muatan_armada']) ? $_GET['muatan_armada'] : 'Pilih Armada';
$armada = $model->getSewaBusById($id_sewa_bus);
if (!empty($armada)){
	$armada = $armada[0];
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-8">
		<h3 class="text-center">Pesan Bus untuk Pariwisata</h3>
		<?php echo $message;?>
		<form method="post" action="<?php echo BASE_URL.'?m=pariwisata&c=doAdd&a=do';?>" class="form-horizontal">
			<input type="hidden" name="id_penyewaan" value="<?php echo $id_penyewaan;?>">
			<div class="form-group">
				<label class="control-label col-sm-4">Tanggal Pemakaian *</label>
				<div class="col-sm-8">
					<input type="text" name="tanggal_pemakaian" class="form-control input-sm datepicker" value="<?php echo $tanggal_pemakaian;?>" data-url="<?php echo BASE_URL.'?m=pariwisata&c=getArmadaJson&a=do';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Tanggal Kembali *</label>
				<div class="col-sm-8">
					<input type="text" name="tanggal_kembali" class="form-control input-sm datepicker" value="<?php echo $tanggal_kembali;?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Pilih Armada *</label>
				<div class="col-sm-6 radio-container-armada" data-url="<?php echo BASE_URL.'?m=pariwisata&c=getHargaJson&a=do';?>">
					<?php
					if (!empty($armada)){
						?>
						<label class="radio-inline">
					      <input type="radio" value="<?php echo $armada['id_sewa_bus'];?>" name="id_sewa_bus"><?php echo $armada['nama_armada'];?>&nbsp;<a class="btn-action btn-detail-armada" href="?m=pemesanan_tiket&c=getDetailArmada&a=do&ref_id=<?php echo $armada['id_armada'];?>">&nbsp;<img src="../icon/detail.png" title="detail"></a>
					    </label>
						<?php
					}
					?>
				</div>
				<div class="col-sm-2">
					<img class="imgLoaderArmada" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Jumlah Hari </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-jumlah-hari"><?php echo $jumlah_hari;?></label>
					<input type="hidden" name="jumlah_hari" value="<?php echo $jumlah_hari;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Harga per Hari </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-harga-perhari"><?php echo $harga_perhari;?></label>
					<input type="hidden" name="harga_perhari" value="<?php echo $harga_perhari;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Total Biaya </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-total-biaya"><?php echo $total_harga;?></label>
					<input type="hidden" name="total_harga" value="<?php echo $total_harga;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4"></label>
				<div class="col-sm-8">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal armada -->
<div id="modalDetailArmada" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Armada</h4>
      </div>
      <div class="modal-body" id="container-modal-detail-armada"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
	var _MS_PER_DAY = 1000 * 60 * 60 * 24;
	var href = '';

    // a and b are javascript Date objects
    function dateDiffInDays(a, b) {
      // Discard the time and time-zone information.
      var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
      var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

      return Math.floor((utc2 - utc1) / _MS_PER_DAY);
    }

    $(document).on('click', 'a.btn-detail-armada', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		href = $(this).attr('href');
		$('#modalDetailArmada').modal('show');
	});

  	$('#modalDetailArmada').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		$.ajax({
		  url : href,
		  type : "post",
		  dataType : 'html',
		  data : {},
		  success : function(response){
		      $('#container-modal-detail-armada').html(response);
		  },
		  error : function(){
		      $.notify('Terjadi kesalahan, coba ulangi kembali', 'warn');
		      $('#modalDetailArmada').modal('hide');
		  }
		});
  	});

	$(document).ready(function(){
		$('.datepicker').datepicker({
			format : 'dd-mm-yyyy',
			autoClose : true
		}).on('changeDate', function(ev){
			$(this).datepicker('hide');

			var selector = $(this);
	        var date1 = new Date($(this).datepicker('getDate'));
	        var date2 = new Date();
	        var diffDays = dateDiffInDays(date2, date1);
	        if (diffDays < 0){
	            selector.val('');
	            $.notify("Tanggal sudah kadaluarsa", "warn");
	        }
		});
	});

	$(document).on('change', 'input[name=id_sewa_bus]', function(){
        getHarga();
    });

    $(document).on('change', 'input[name=tanggal_kembali], input[name=tanggal_pemakaian]', function(){
        getArmada();
    });

    function getArmada(){
    	var _tanggal_pemakaian = $('input[name=tanggal_pemakaian]').val();
    	var _tanggal_kembali = $('input[name=tanggal_kembali]').val();
    	setDefaultLabel();
    	if (_tanggal_pemakaian != '' && _tanggal_kembali != ''){
    		$('.imgLoaderArmada').show();
    		$.ajax({
    			url : $('input[name=tanggal_pemakaian]').attr('data-url'),
    			type : 'post', 
    			data : {
    				tanggal_pemakaian : _tanggal_pemakaian,
    				tanggal_kembali : _tanggal_kembali
    			},
    			dataType : 'jSON',
    			success : function(response){
    				$('.imgLoaderArmada').hide();
    				var htmlOption = [];
    				if (response.success){
    					$.each(response.data, function(key, value){
    						htmlOption[key] = '<label class="radio-inline">'
											      +'<input type="radio" value="'+value.id_sewa_bus+'" name="id_sewa_bus">'+value.nama_armada+'<a class="btn-action btn-detail-armada" href="?m=pariwisata&c=getDetailArmada&a=do&ref_id='+value.id_armada+'">&nbsp;<img src="../icon/detail.png" title="detail"></a>'
											    +'</label>';
    					});
    					$('.radio-container-armada').html(htmlOption.join('<br>'));
    				}else{
    					$('.radio-container-armada').html('');
    				}
    			},
    			error : function(){
    				console.log('ajax call failed');
    				$('.imgLoaderArmada').hide();
    			}
    		});
    	} 
    }

	function getHarga(){
		var _id_sewa_bus = $('input[name=id_sewa_bus]:checked').val();
		var _tanggal_pemakaian = $('input[name=tanggal_pemakaian]').val();
		var _tanggal_kembali = $('input[name=tanggal_kembali]').val();

        var imgLoader = $('.imgLoader');

        if (_id_sewa_bus != '' && _tanggal_kembali != '' && _tanggal_pemakaian != ''){
        	imgLoader.show();
	        $.ajax({
	            url : $('.radio-container-armada').data('url'),
	            type : 'post', 
	            dataType : 'jSON',
	            data : {
	                id_sewa_bus : _id_sewa_bus,
	                tanggal_pemakaian : _tanggal_pemakaian,
	                tanggal_kembali : _tanggal_kembali
	            },
	            success : function(response){
	                imgLoader.hide();
	                if (response.success){
	                	$('label#label-jumlah-hari').html(response.jumlah_hari);
	                	$('input[name=jumlah_hari]').val(response.jumlah_hari);
	                	$('label#label-harga-perhari').html(response.harga_perhari);
	                	$('input[name=harga_perhari]').val(response.harga_perhari);
	                    $('label#label-total-biaya').html(response.total_harga);
	                    $('input[name=total_harga]').val(response.total_harga);
	                }
	            },
	            error : function(){
	                imgLoader.hide();
	                console.log('Gagal Mengambil data');
	            }
	        });
        }else{
        	setDefaultLabel();
        }
        return;        
	}

	function setDefaultLabel(){
		$('label#label-jumlah-hari').html('0');
    	$('input[name=jumlah_hari]').val('0');
    	$('label#label-harga-perhari').html('0');
    	$('input[name=harga_perhari]').val('0');
        $('label#label-total-biaya').html('0');
        $('input[name=total_harga]').val('0');
	}
</script>