<div class="">
	<?php
	$message = '';
	if (isset($_GET['loginFail']) && $_GET['loginFail'] == 1){
		$message = '<div class="alert alert-danger">Maaf, Username atau password salah</div>';
	}
	?>
    <div class="login-box">
      <div class="login-logo">
        <a href="<?php echo BASE_URL.'?m=login&c=viewLogin&a=do';?>">Login</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
      	<?php echo $message;?>
        <form action="<?php echo BASE_URL.'?m=login&c=doLogin&a=do';?>" method="post">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" name="email" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" name="password" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12">
            	<div class="pull-right">
	            	<a class="btn btn-primary btn-flat" href="<?php echo BASE_URL.'?m=register&c=viewRegister&a=view';?>">Daftar</a>
	              	<button type="submit" class="btn btn-primary btn-flat">Login</button>
	            </div>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
	</div>