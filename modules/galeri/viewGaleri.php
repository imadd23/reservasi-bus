<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'js/slick/slick.css';?>"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'js/slick/slick-theme.css';?>"/>
<style type="text/css">
	.slider {
        width: 80%;
        margin: 25px auto;
    }

    .slick-slide {
      margin: 0px 20px;
    }

    .slick-slide img {
      width: 100%;
    }

    .slick-prev:before,
    .slick-next:before {
        color: black;
    }

    .img-container{
    	height: 450px;
    	width: 700px;
    }

    .img-thumbnail-container{
    	height: 100px;
    	width: 150px;
    }

    .img-container img, .img-thumbnail-container img{
    	width: 100%;
    	height: 100%;
    }
</style>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/slick/slick.min.js';?>"></script>
<section class="slider-nav slider">
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/1.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/2.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/3.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/4.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/5.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/6.jpg';?>" />
    </div>
    <div class="img-thumbnail-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/7.jpg';?>" />
    </div>
</section>
<section class="slider-for slider">
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/1.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/2.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/3.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/4.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/5.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/6.jpg';?>" />
    </div>
    <div class="img-container">
    	<img src="<?php echo BASE_ADDRESS.'images/galery/7.jpg';?>" />
    </div>
</section>
<script type="text/javascript">
	$(document).ready(function(){
		$('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav',
  			cssEase: 'linear'
		});
		$('.slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: true,
			focusOnSelect: true,
			centerMode: true,
			arrows: true,
			autoplay: true,
  			autoplaySpeed: 2000
		});
	});
</script>