<?php
require_once 'modelProfil.php';
$model = new modelProfil();

$p = array(
	'nama' => $_POST['nama'],
	'no_rekening' => $_POST['no_rekening'],
	'id_bank' => $_POST['id_bank'],
	'no_hp' => $_POST['no_hp'],
	'email' => $_POST['email'],
	'username' => $_POST['username'],
	'pertanyaan' => $_POST['pertanyaan']
   );

if (isset($_POST['save'])){
    if (empty($p['nama']) || empty($p['no_rekening']) || empty($p['id_bank']) || empty($p['no_hp']) || empty($p['email']) || empty($p['username']) || empty($p['pertanyaan'])){
    	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&statusUpdate=0&tab=ubah_profil&'.http_build_query($p));
    }else
    if (!empty($_POST['password']) && $_POST['ulangi_password'] != $_POST['password']){
    	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&statusUpdate=0&tab=ubah_profil&'.http_build_query($p));
    }else{
		$update = $model->updateProfil($_SESSION['user_id'], $p['nama'], $p['no_rekening'], $p['id_bank'], $p['no_hp'], $p['email'], $p['username'], $_POST['password'], $p['pertanyaan'], $_POST['jawaban']);
	    if ($update){
	    	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&statusUpdate=1');
	    }else{
	    	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&statusUpdate=0&tab=ubah_profil&'.http_build_query($p));
	    }
	}
		
}
?>