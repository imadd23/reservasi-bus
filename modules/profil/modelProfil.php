<?php
Class modelProfil{

	public function __construct(){

	}

	public function getProfil($user_id = 0){
		$qProfil = mysql_query("SELECT login.*, role_name FROM login
			LEFT JOIN role ON login.`id_role` = role.`id_role` 
			WHERE login.user_id = '".$user_id."'");

		$arrayProfil = array();
		while ($dataProfil = mysql_fetch_array($qProfil)) {
		    $arrayProfil[] = $dataProfil;
		}

		return $arrayProfil;
	}

	public function getDataPembelianTiket($user_id = 0){
        $queryPembelian = mysql_query("SELECT 
            (
                SELECT GROUP_CONCAT(`id_pembelian_tiket` SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS id_pembelian_tiket,
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(tanggal_beli) SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_beli, 
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(`tanggal_pemberangkatan`) SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_pemberangkatan,
            (
                SELECT GROUP_CONCAT(DISTINCT atas_nama SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS atas_nama,
            (
                SELECT SUM(`detail_pembelian`.sub_total)
                FROM detail_pembelian
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS total_harga,
			IF( DP.status_batal = '0' AND TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), 
				'1', 
				'0') AS can_cancelled,
			PT.status_bayar, PT.tanggal_bayar, DP.status_batal
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            WHERE 
                PT.id_pelanggan = '$user_id'
                AND DP.status_batal = '0'
                AND PT.id_tiket_berangkat IS NULL
            GROUP BY PT.id_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");

        $arrayPembelian = array();
        while ($dataPembelian = mysql_fetch_array($queryPembelian)) {
            $arrayPembelian[] = $dataPembelian;
        }

        return $arrayPembelian;
    }

    public function getDetailDataPembelianTiket($refId){
        $qPembelian = mysql_query("SELECT 
            `PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
            PT.tanggal_bayar,
            CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
            CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
            nama_armada,
            IF( DP.status_batal = '0' AND TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), 
                '1', '0')
            AS can_cancelled, NK.no_kursi, P.hari, P.jam,
            (
                SELECT SUM(sub_total)
                FROM detail_pembelian
                WHERE
                `id_pembelian_tiket` IN (PT.id_pembelian_tiket)
            ) AS total_harga, DP.sub_total
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
            LEFT JOIN kota AS A
                ON A.`id_kota` = P.`id_kota_asal`
            LEFT JOIN provinsi AS C
                ON C.`id_provinsi` = A.`id_provinsi`
            LEFT JOIN kota AS B
                ON B.`id_kota` = P.`id_kota_tujuan`
            LEFT JOIN provinsi AS D
                ON D.`id_provinsi` = B.`id_provinsi`
            LEFT JOIN armada E
                ON P.`id_armada` = E.`id_armada`
            LEFT JOIN no_kursi AS NK
                ON NK.id_no_kursi = DP.id_no_kursi
            LEFT JOIN kota AS F
                ON F.`id_kota` = DP.`id_kota_tujuan`
            LEFT JOIN provinsi AS G
                ON G.`id_provinsi` = F.`id_provinsi`
            WHERE
            PT.id_pembelian_tiket IN ('".$refId."')
            GROUP BY DP.id_detail_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
        $arrayPembelian = array();
        while ($dataPembelian = mysql_fetch_array($qPembelian)){
            $arrayPembelian[] = $dataPembelian;
        }

        return $arrayPembelian;
    }

    public function getDataPembatalanTiket($user_id = 0){
        $queryPembelian = mysql_query("SELECT 
            (
                SELECT GROUP_CONCAT(`id_pembelian_tiket` SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS id_pembelian_tiket,
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(tanggal_beli) SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_beli, 
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(`tanggal_pemberangkatan`) SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_pemberangkatan,
            (
                SELECT GROUP_CONCAT(DISTINCT atas_nama SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS atas_nama,
            (
                SELECT SUM(`detail_pembelian`.sub_total)
                FROM detail_pembelian
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS total_harga
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            WHERE DP.status_batal = '1'
                AND PT.id_pelanggan = '$user_id'
                AND PT.id_tiket_berangkat IS NULL
            GROUP BY PT.id_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");

        $arrayPembelian = array();
        while ($dataPembelian = mysql_fetch_array($queryPembelian)) {
            $arrayPembelian[] = $dataPembelian;
        }

        return $arrayPembelian;
    }

	// public function getDataPembatalanTiket($user_id = 0){
	// 	$qPembelian = mysql_query("SELECT 
	// 		`PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
	// 		CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
	// 		CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
	// 		nama_armada
	// 		FROM `pembelian_tiket` AS PT
	// 		INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
	// 		LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
	// 		LEFT JOIN kota AS A
	// 		    ON A.`id_kota` = P.`id_kota_asal`
	// 		LEFT JOIN provinsi AS C
	// 		    ON C.`id_provinsi` = A.`id_provinsi`
	// 		LEFT JOIN kota AS B
	// 		    ON B.`id_kota` = P.`id_kota_tujuan`
	// 		LEFT JOIN provinsi AS D
	// 		    ON D.`id_provinsi` = B.`id_provinsi`
	// 		LEFT JOIN armada E
	// 		    ON P.`id_armada` = E.`id_armada`
	// 		LEFT JOIN kota AS F
	// 		    ON F.`id_kota` = DP.`id_kota_tujuan`
	// 		LEFT JOIN provinsi AS G
	// 		    ON G.`id_provinsi` = F.`id_provinsi`
	// 		WHERE DP.status_batal = '1' AND PT.id_pelanggan = '".$user_id."'
	// 		ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC
	// 		");
	// 	$arrayPembelian = array();
	// 	while ($dataPembelian = mysql_fetch_array($qPembelian)){
	// 		$arrayPembelian[] = $dataPembelian;
	// 	}

	// 	return $arrayPembelian;
	// }

	public function getDataPengiriman($user_id = 0){
		$qPengiriman = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			IFNULL(CONCAT(E.nama_kota, ' (', F.nama_provinsi, ')'), 'Tidak Diketahui') AS posisi_sekarang
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN kota AS E
				ON E.id_kota = pengiriman.posisi_terakhir
			LEFT JOIN provinsi AS F
				ON F.id_provinsi = E.id_provinsi
			WHERE pengiriman.`user_id` = '".$user_id."'
			ORDER BY pengiriman.`modified_on` DESC
			");
		$arrayPengiriman = array();
		while ($dataPengiriman = mysql_fetch_array($qPengiriman)){
			$arrayPengiriman[] = $dataPengiriman;
		}

		return $arrayPengiriman;
	}

	public function getDetailPengirimanForPrint($refId, $user_id){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			harga_pengiriman.id_paket_pengiriman, harga_pengiriman.id_kota_asal,  harga_pengiriman.id_kota_tujuan
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` 
				ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` 
				ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi` 
			WHERE 
			pengiriman.id_pengiriman = '".$refId."' AND pengiriman.user_id = '".$user_id."'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDataPariwisata($user_id = 0){
		$qPariwisata = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, armada.`muatan_penumpang`, CONCAT(supir.nama, ', hp: ', supir.no_hp) AS nama_supir,
			IFNULL(DATEDIFF(NOW(), penyewaan.`tanggal_status_penyewaan`), 0) AS waktu_persetujuan,
			IF(TIMESTAMPDIFF(HOUR, penyewaan.`tanggal_status_penyewaan`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), '1', '0') AS can_cancelled
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada`
			LEFT JOIN supir ON supir.id_supir = penyewaan.id_supir
			WHERE penyewaan.`user_id` = '".$user_id."'
			ORDER BY penyewaan.`tanggal_pemakaian` DESC
			");
		$arrayPariwisata = array();
		while ($dataPariwisata = mysql_fetch_array($qPariwisata)){
			$arrayPariwisata[] = $dataPariwisata;
		}

		return $arrayPariwisata;
	}

	public function getDataPariwisataForPrint($ref_id = 0, $user_id = 0){
		$qPariwisata = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, armada.`muatan_penumpang`, CONCAT(supir.nama, ', hp: ', supir.no_hp) AS nama_supir 
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada`
			LEFT JOIN supir ON supir.id_supir = penyewaan.id_supir
			WHERE penyewaan.`id_penyewaan` = '".$ref_id."' AND user_id='".$user_id."'
			ORDER BY penyewaan.`tanggal_pemakaian` DESC
			");
		$arrayPariwisata = array();
		while ($dataPariwisata = mysql_fetch_array($qPariwisata)){
			$arrayPariwisata[] = $dataPariwisata;
		}

		return $arrayPariwisata;
	}

	public function getBank(){
		$qBank = mysql_query("SELECT * FROM bank ORDER BY nama_bank ASC");
		$arrayBank = array();
		while ($data = mysql_fetch_array($qBank)) {
			$arrayBank[] = $data;
		}

		return $arrayBank;
	}

	public function getSettingPembelianTiket(){
		$query = mysql_query("SELECT * FROM setting WHERE id IN ('BT')");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getSettingPenyewaan(){
		$query = mysql_query("SELECT * FROM setting WHERE id IN ('BP')");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function updateProfil($user_id, $nama, $no_rekening, $id_bank, $no_hp, $email, $username, $password, $pertanyaan, $jawaban){
		$update_password = empty($password) ? '' : "password = MD5('".$password."'),";
		$update_jawaban = empty($jawaban) ? '' : "jawaban = MD5('".$jawaban."')";
		$qUpdate = "UPDATE login SET 
			nama = '".$nama."',
			no_rekening = '".$no_rekening."',
			id_bank = '".$id_bank."',
			no_hp = '".$no_hp."',
			email = '".$email."',
			username = '".$username."',
			".$update_password.
			"pertanyaan = '".$pertanyaan."',
			".$update_jawaban."
			WHERE user_id = '".$user_id."'
			";
		$update = mysql_query($qUpdate);
		return $update;
	}

	public function getdetailTiket($refId){
		$qPembelian = mysql_query("SELECT 
			`PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE DP.id_detail_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
		$arrayPembelian = array();
		while ($dataPembelian = mysql_fetch_array($qPembelian)){
			$arrayPembelian[] = $dataPembelian;
		}

		return $arrayPembelian;
	}

	public function getdetailTiketForPrint($refId){
		$qPembelian = mysql_query("SELECT 
			`PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE PT.id_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
		$arrayPembelian = array();
		while ($dataPembelian = mysql_fetch_array($qPembelian)){
			$arrayPembelian[] = $dataPembelian;
		}

		return $arrayPembelian;
	}

	public function doDeletePariwisata($id = 0, $user_id = 0){
		$delete = mysql_query("DELETE FROM penyewaan WHERE id_penyewaan = '".$id."' AND user_id = '".$user_id."'");
		return $delete;
	}
}
?>