<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();

$p = array(
	'tanggal_pemberangkatan' => date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan'])),
    'id_pemberangkatan' => $_POST['id_pemberangkatan'],
    'id_kota_tujuan' => $_POST['id_kota_tujuan'],
    'id_no_kursi' => $_POST['id_no_kursi'],
    'atas_nama' => $_POST['atas_nama'],
    'id_penumpang' => $_POST['id_penumpang'],
    'sub_total' => $_POST['sub_total']
   );

$id_pembelian_tiket = $model->getIdPembelianTiket();
$tanggal_beli = date('Y-m-d');
$id_pelanggan = $_SESSION['user_id'];
$nama_pelanggan = $_SESSION['username'];
$nomer_rekening = $_SESSION['no_rekening'];
$id_bank = $_SESSION['id_bank'];
$no_hp = $_SESSION['no_hp'];
$total_harga = $_POST['sub_total'];
$status_bayar = 'belum lunas';

if (isset($_POST['save'])){
    $cek = $model->cekPembelian($id_pelanggan, $tanggal_beli);
    if (empty($p['tanggal_pemberangkatan']) || empty($p['id_pemberangkatan']) || empty($p['id_kota_tujuan']) || empty($p['id_no_kursi']) || empty($p['atas_nama']) || empty($p['sub_total'])){
    	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view&statusAdd=0&'.http_build_query($p));
    }else
    if (empty($cek)){   	
    	$save = $model->addPembelian($id_pembelian_tiket, $id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $total_harga, $status_bayar);
	    if ($save){
	    	$model->addDetailPembelian($id_pembelian_tiket, $p['id_pemberangkatan'], $p['id_kota_tujuan'], $p['id_no_kursi'], $p['tanggal_pemberangkatan'], $p['atas_nama'], $p['id_penumpang'], $p['sub_total']);
	    	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view&statusAdd=1&ref_id='.$id_pembelian_tiket);
	    }else{
	    	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view&statusAdd=0&'.http_build_query($p));
	    }
	}else
	if (!empty($cek)){
		$update = $model->updatePembelian($cek->id_pembelian_tiket, $id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $total_harga, $status_bayar);
    	$p['ref_id'] = $cek->id_pembelian_tiket;
	    if ($update){
	    	$model->addDetailPembelian($p['ref_id'], $p['id_pemberangkatan'], $p['id_kota_tujuan'], $p['id_no_kursi'], $p['tanggal_pemberangkatan'], $p['atas_nama'], $p['id_penumpang'], $p['sub_total']);
	    	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view&statusUpdate=1&ref_id='.$p['ref_id']);
	    }else{
	    	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewPemesananTiket&a=view&statusUpdate=0&'.http_build_query($p));
	    }
	}
		
}
?>