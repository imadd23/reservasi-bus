<?php
require_once MODULE_PATH.'pariwisata/modelPariwisata.php';
$model = new modelPariwisata();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : 0;
$params = array(
	'id_penyewaan' => $refId
   );
$model->where($params);
$pariwisata = $model->getDetailPariwisata();
$pariwisata = !empty($pariwisata) ? http_build_query($pariwisata[0]) : '';
header('location: '.BASE_URL.'?m=pariwisata&c=viewPariwisata&a=view&'.$pariwisata);
?>