<?php
require_once 'modelProfil.php';
Library::load('html2pdf.php', 'html2pdf');
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
$model = new modelProfil();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$detail = $model->getDataPariwisataForPrint($refId, $_SESSION['user_id']);
if (empty($detail)){
	die('Data tidak ditemukan');
}
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding">NOTA PENYEWAAN BUS</h3>
<hr>
<table>
	<tr>
		<th>Order ID</th><td>: <?php echo $detail[0]['id_penyewaan'];?></td>
	</tr>
	<tr>
		<th>Tanggal Order</th><td>: <?php echo date('d M Y', strtotime($detail[0]['tanggal_order']));?></td>
	</tr>
</table>
<hr class="dashed">
<table class="table-control table-bordered">
	<col style="width: 50%;">
    <col style="width: 50%;">
	<?php
	foreach ($detail as $key => $value) {
		echo
		'<tr>'.
			'<th>Tanggal Pemakaian</th><td>'.date('d M Y', strtotime($value['tanggal_pemakaian'])).'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Tanggal Kembali</th><td>'.date('d M Y', strtotime($value['tanggal_kembali'])).'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Armada</th><td>'.$value['nama_armada'].'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Muatan Penumpang</th><td>'.$value['muatan_penumpang'].'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Jumlah Hari</th><td>'.$value['jumlah_hari'].'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Harga Perhari</th><td>Rp. '.number_format($value['harga_perhari']).'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Total Harga</th><td>Rp. '.number_format($value['total_harga']).'</td>'.
		'</tr>'.
		'<tr>'.
			'<th>Supir</th><td>'.$value['nama_supir'].'</td>'.
		'</tr>';
	}
	?>
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>Terima kasih atas kepercayaan Anda kepada PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	// $html2pdf = new html2pdf('P', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf = new html2pdf('L', array(250, 120), 'en', true, 'UTF-8', array(2, 2, 2, 2));
	$html2pdf->writeHTML($content);
	$html2pdf->Output('cetak.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>