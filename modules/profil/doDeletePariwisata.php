<?php
require_once 'modelProfil.php';
$model = new modelProfil();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$delete = $model->doDeletePariwisata($refId, $_SESSION['user_id']);
if ($delete){
	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&tab=pariwisata&statusCancelPariwisata=1');
}else{
	header('location: '.BASE_URL.'?m=profil&c=viewProfil&a=view&tab=pariwisata&statusCancelPariwisata=0');
}
?>