<?php
require_once MODULE_PATH.'pengiriman_barang/modelPengirimanBarang.php';
$model = new modelPengirimanBarang();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : 0;
$params = array(
	'id_pengiriman' => $refId
   );
$model->where($params);
$pengiriman = $model->getDetailPengiriman();
$pengiriman = !empty($pengiriman) ? http_build_query($pengiriman[0]) : '';
header('location: '.BASE_URL.'?m=pengiriman_barang&c=viewPengirimanBarang&a=view&'.$pengiriman);
?>