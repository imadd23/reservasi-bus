<?php
require_once 'modelProfil.php';
$model = new modelProfil();
$dataProfil = $model->getProfil($_SESSION['user_id']);
$dataPembelianTiket = $model->getDataPembelianTiket($_SESSION['user_id']);
$dataPembatalanTiket = $model->getDataPembatalanTiket($_SESSION['user_id']);
?>
<ul class="nav nav-tabs">
  <li <?php echo (!isset($_GET['tab']) || $_GET['tab'] == 'profil') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#profil"><i class="fa fa-user"></i> Data Profil</a></li>
  <li <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pemesanan_tiket') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#pemesanan_tiket"><i class="fa fa-database"></i> Riwayat Pemesanan Tiket</a></li>
  <li <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pembatalan_tiket') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#pembatalan_tiket"><i class="fa fa-database"></i> Riwayat Pembatalan Tiket</a></li>
  <!-- <li <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pengiriman_barang') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#pengiriman_barang"><i class="fa fa-car"></i> Riwayat Pengiriman Barang</a></li> -->
  <li <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pariwisata') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#pariwisata"><i class="fa fa-car"></i> Riwayat Reservasi Pariwisata</a></li>
  <li <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'ubah_profil') ? 'class="active"' : '' ;?>><a data-toggle="tab" href="#ubah_profil"><i class="fa fa-pencil"></i> Ubah Profil</a></li>
</ul>

<div class="tab-content">
  <div id="profil" class="tab-pane fade <?php echo (!isset($_GET['tab']) || $_GET['tab'] == 'profil') ? 'in active' : '' ;?>">
    <!-- <h3>Data Profil</h3> -->
    <p>
      <?php
      if (!empty($dataProfil)){
        echo 
        '<div class="table-responsive">'.
          '<table class="table table-striped">'.
            '<col style="width:35%;">'.
            '<col style="width:65%;">'.

            '<tr>'.
              '<th>Nama</th><td>'.$dataProfil[0]['nama'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>Email</th><td>'.$dataProfil[0]['email'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>No. HP</th><td>'.$dataProfil[0]['no_hp'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>Username Login</th><td>'.$dataProfil[0]['username'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>Hak Akses</th><td>'.$dataProfil[0]['role_name'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>Pertanyaan Lupa Password</th><td>'.$dataProfil[0]['pertanyaan'].'</td>'.
            '</tr>'.
            '<tr>'.
              '<th>Jawaban</th><td>'.$dataProfil[0]['jawaban'].'</td>'.
            '</tr>'.
          '</table>'.
        '</div>';
      }   
      ?>
    </p>
  </div>
  <div id="pemesanan_tiket" class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pemesanan_tiket') ? 'in active' : '' ;?>">
    <!-- <h3>Data Pemesanan Tiket</h3> -->
    <p>
      <div class="table-responsive">
          <?php 
          $waktu_pembatalan = $model->getSettingPembelianTiket();
          $jamCancelBeforeCancel = 0;
          $jamCancelAfterConfirm = 0;

          if (!empty($waktu_pembatalan)){
              foreach ($waktu_pembatalan as $key => $value) {
                  if ($value['id'] == 'BT'){
                      $jamCancelBeforeCancel = $value['set'];
                  }else
                  if ($value['id'] == 'PTK'){
                      $jamCancelAfterConfirm = $value['set'];
                  }
              }
          }
          $info = '<div class="alert alert-info"><ul><li>Tiket bisa diubah jika tidak dibatalkan, belum dikonfirmasi petugas dan masih dalam hari pemesanan tiket</li>';
          $info .= '<li>Tiket tidak bisa dibatalkan setelah dikonfirmasi petugas</li>';
          if (!empty($jamCancelBeforeCancel)){
              $info .= '<li>Pembatalan tiket sebelum dikonfirmasi petugas maksimal '.$jamCancelBeforeCancel.' jam dari waktu pembelian</li>';
          }
          if (!empty($jamCancelAfterConfirm)){
              $info .= '<li>Pembatalan tiket setelah dikonfirmasi petugas maksimal '.$jamCancelAfterConfirm.' jam</li>';
          }
          $info .= '</ul></div>';
          echo $info;
          ?>
        <table id="table-pemesanan-tiket" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=profil&c=getDataPemesanan&a=do';?>">
          <thead>
            <tr>
              <th class="text-center" nowrap>No</th>
              <th nowrap>ID Pembelian</th>
              <th nowrap>Tanggal Beli</th>
              <th nowrap>Tanggal Pemberangkatan</th>
              <th nowrap>Atas Nama</th>
              <th nowrap>Total</th>
              <th class="text-center" nowrap>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($dataPembelianTiket)){
              foreach ($dataPembelianTiket as $key => $value) {
                echo 
                '<tr>'.
                  '<td class="text-center">'.($key + 1).'</td>'.
                  '<td>'.$value['id_pembelian_tiket'].'</td>'.
                  '<td>'.$value['tanggal_beli'].'</td>'.
                  '<td>'.$value['tanggal_pemberangkatan'].'</td>'.
                  '<td>'.$value['atas_nama'].'</td>'.
                  '<td>'.$value['total_harga'].'</td>'.
                  '<td class="text-center" style="display: inline-flex;">'.
                    '<a class="btn-action btn-detail-tiket" href="?m=profil&c=getDetailTiket&a=do&ref_id='.$value['id_pembelian_tiket'].'"><img src="'.BASE_ADDRESS.'icon/detail.png" title="detail"></a>&nbsp';
                    if ($value['status_batal'] != '1' && empty($value['tanggal_bayar']) && date('Y-m-d') == date('Y-m-d', strtotime($value['tanggal_beli']))){
                      echo 
                        '<a class="btn-action btn-edit" data-nama="'.$value['id_pembelian_tiket'].')" href="'.BASE_URL.'?m=profil&amp;c=doEditTiket&amp;a=do&amp;ref_id='.$value['id_pembelian_tiket'].'"><img src="'.BASE_ADDRESS.'icon/edit.png" title="ubah"></a>&nbsp;';
                    }

                    if ($value['status_bayar'] == 'lunas' && !empty($value['tanggal_bayar']) && $value['status_batal'] != '1'){
                      echo 
                        '<a class="btn-action btn-print" target="_blank" data-nama="'.$value['id_pembelian_tiket'].')" href="'.BASE_URL.'?m=profil&amp;c=doPrintNota&amp;a=do&amp;ref_id='.$value['id_pembelian_tiket'].'"><img src="'.BASE_ADDRESS.'icon/nota.png" title="cetak nota pembelian"></a>';
                    }
                  
                    echo
                  '</td>'.
                '</tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </p>
  </div>

  <div id="pembatalan_tiket" class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pembatalan_tiket') ? 'in active' : '' ;?>">
    <!-- <h3>Data Pembatalan Tiket</h3> -->
    <p>
      <?php echo $info;?>
      <div class="table-responsive">
        <table id="table-pembatalan-tiket" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=profil&c=getDataPemesanan&a=do';?>">
          <thead>
            <tr>
              <th class="text-center" nowrap>No</th>
              <th nowrap>ID Pembelian</th>
              <th nowrap>Tanggal Beli</th>
              <th nowrap>Tanggal Pemberangkatan</th>
              <th nowrap>Atas Nama</th>
              <th nowrap>Total</th>
              <th class="text-center">Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if (!empty($dataPembatalanTiket)){
              foreach ($dataPembatalanTiket as $key => $value) {
                echo 
                '<tr>'.
                  '<td class="text-center">'.($key + 1).'</td>'.
                  '<td>'.$value['id_pembelian_tiket'].'</td>'.
                  '<td>'.$value['tanggal_beli'].'</td>'.
                  '<td>'.$value['tanggal_pemberangkatan'].'</td>'.
                  '<td>'.$value['atas_nama'].'</td>'.
                  '<td>'.$value['total_harga'].'</td>'.
                  '<td class="text-center">'.
                    '<a class="btn-action btn-detail-tiket" href="?m=profil&c=getDetailTiket&a=do&ref_id='.$value['id_pembelian_tiket'].'"><img src="'.BASE_ADDRESS.'icon/detail.png" title="detail"></a>'.
                  '</td>'.
                '</tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </p>
  </div>
  <!--<div id="pengiriman_barang" class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pengiriman_barang') ? 'in active' : '' ;?>">-->
    <!-- <h3>Data Pengiriman Barang</h3> -->
    <!--<p>
      <div class="table-responsive">
        <table id="table-pengiriman-barang" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=profil&c=getDataPengiriman&a=do';?>">
          <thead>
            <tr> 
              <th class="text-center" nowrap>No</th>
              <th nowrap>ID Pengiriman</th>
              <th nowrap>Paket</th>
              <th nowrap>Tanggal Kirim</th>
              <th nowrap>Tanggal Sampai</th>
              <th nowrap>Kota Asal</th>
              <th nowrap>Kota Tujuan</th>
              <th nowrap>Posisi Terakhir</th>
              <th nowrap>Berat</th>
              <th nowrap>Harga (Kg)</th>
              <th nowrap>Total Biaya</th>-->
              <!-- <th class="text-center" nowrap>Aksi</th> -->
            <!--</tr>
          </thead>
          <tbody>
            <?php
            $dataPengiriman = $model->getDataPengiriman($_SESSION['user_id']);
            if (!empty($dataPengiriman)){
              foreach ($dataPengiriman as $key => $value) {
                echo 
                '<tr>'.
                  '<td class="text-center">'.($key + 1).'</td>'.
                  '<td>'.$value['id_pengiriman'].'</td>'.
                  '<td>'.$value['nama_paket'].'</td>'.
                  '<td>'.$value['tanggal_kirim'].'</td>'.
                  '<td>'.$value['tanggal_sampai'].'</td>'.
                  '<td>'.$value['kota_asal'].'</td>'.
                  '<td>'.$value['kota_tujuan'].'</td>'.
                  '<td>'.$value['posisi_sekarang'].'</td>'.
                  '<td>'.$value['berat'].'</td>'.
                  '<td>'.$value['harga_per_kilo'].'</td>'.
                  '<td>'.$value['total_harga'].'</td>';
                  
                  // '<td class="text-center">';
                  // if ($value['status_pengiriman'] == '0'){
                  //   echo 
                  //     '<a class="btn-action btn-edit" data-nama="'.$value['kota_asal'].' - '.$value['kota_tujuan'].'('.$value['nama_paket'].')" href="'.BASE_URL.'?m=profil&c=doEditPengiriman&a=do&ref_id='.$value['id_pengiriman'].'"><img src="'.BASE_ADDRESS.'icon/edit.png" title="ubah"></a>&nbsp';
                  // }else
                  // if ($value['status_pengiriman'] == '1'){
                  //   echo 
                  //     '<a class="btn-action btn-print" target="_blank" data-nama="'.$value['kota_asal'].' - '.$value['kota_tujuan'].'('.$value['nama_paket'].')" href="'.BASE_URL.'?m=profil&c=doPrintResi&a=do&ref_id='.$value['id_pengiriman'].'"><img src="'.BASE_ADDRESS.'icon/print.png" title="cetak nota"></a>';
                  // }
                  // echo 
                  // '</td>'.
                '</tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </p>
  </div>-->
  <div id="pariwisata" class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'pariwisata') ? 'in active' : '' ;?>">
    <!-- <h3>Data Reservasi Pariwisata</h3> -->
    <p>
      <?php 
      $waktu_pembatalan = $model->getSettingPenyewaan();
      $jamCancelBeforeCancel = 0;
      $jamCancelAfterConfirm = 0;
      if (!empty($waktu_pembatalan)){
          foreach ($waktu_pembatalan as $key => $value) {
              if ($value['id'] == 'BP'){
                  $jamCancelAfterConfirm = $value['set'];
              }
          }
      }
      $info = '<div class="alert alert-info"><ul><li>Penyewaan bus bisa dibatalkan jika belum dikonfirmasi petugas</li>';
      if (!empty($jamCancelAfterConfirm)){
          $info .= '<li>Pembatalan Penyewaan bus setelah dikonfirmasi petugas maksimal '.$jamCancelAfterConfirm.' jam</li>';
      }
      $info .= '</ul></div>';
      echo $info;
      ?>
      <?php
      $messagePariwisata = '';
      if (isset($_GET['statusCancelPariwisata']) && $_GET['statusCancelPariwisata'] === '1'){
        $messagePariwisata = '<div class="alert alert-success">Data Penyewaan Bus berhasil dihapus</div>';
      }
      if (isset($_GET['statusCancelPariwisata']) && $_GET['statusCancelPariwisata'] === '0'){
        $messagePariwisata = '<div class="alert alert-success">Data Penyewaan Bus gagal dihapus, coba ulangi kembali</div>';
      }
      echo $messagePariwisata;
      ?>
      <div class="table-responsive">
        <table id="table-pariwisata" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=profil&c=getDataPariwisata&a=do';?>">
          <thead>
            <tr>
              <th class="text-center" nowrap>No</th>
              <th nowrap class="text-center">ID Penyewaan</th>
              <th nowrap class="text-center">Status</th>
              <th nowrap>Tanggal Konfirmasi</th>
              <th nowrap>Supir</th>
              <th nowrap>Tanggal Pemakaian</th>
              <th nowrap>Tanggal Kembali</th>
              <th nowrap>Jumlah Hari</th>
              <th nowrap>Harga Per Hari</th>
              <th nowrap>Total Harga</th>
              <th nowrap>Armada</th>
              <th nowrap>Muatan Penumpang</th>
              <th class="text-center" nowrap>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $dataPariwisata = $model->getDataPariwisata($_SESSION['user_id']);
            if (!empty($dataPariwisata)){
              foreach ($dataPariwisata as $key => $value) {
                if ($value['status_penyewaan'] == '1'){
                  $label = '<label class="label label-success">dikonfirmasi</label>';
                }else{
                  $label = '<label class="label label-warning">menunggu konfirmasi</label>';
                }
                echo 
                '<tr>'.
                  '<td class="text-center">'.($key + 1).'</td>'.
                  '<td class="text-center">'.$value['id_penyewaan'].'</td>'.
                  '<td class="text-center">'.$label.'</td>'.
                  '<td>'.$value['tanggal_status_penyewaan'].'</td>'.
                  '<td>'.$value['nama_supir'].'</td>'.
                  '<td>'.$value['tanggal_pemakaian'].'</td>'.
                  '<td>'.$value['tanggal_kembali'].'</td>'.
                  '<td>'.$value['jumlah_hari'].'</td>'.
                  '<td>'.$value['harga_perhari'].'</td>'.
                  '<td>'.$value['total_harga'].'</td>'.
                  '<td>'.$value['nama_armada'].'</td>'.
                  '<td>'.$value['muatan_penumpang'].'</td>'.
                  '<td class="text-center">';

                  /* jika belum distujui, maka data bisa diubah */
                  if ($value['status_penyewaan'] == '0'){
                    echo 
                      '<a class="btn-action btn-edit" data-nama="'.$value['tanggal_pemakaian'].' - '.$value['tanggal_kembali'].'('.$value['harga_perhari'].')" href="'.BASE_URL.'?m=profil&amp;c=doEditPariwisata&amp;a=do&amp;ref_id='.$value['id_penyewaan'].'"><img src="'.BASE_ADDRESS.'icon/edit.png" title="ubah"></a>&nbsp';
                  }else
                  if ($value['status_penyewaan'] == '1'){
                    echo 
                      '<a class="btn-action btn-print" target="_blank" data-nama="'.$value['tanggal_pemakaian'].' - '.$value['tanggal_kembali'].'('.$value['harga_perhari'].')" href="'.BASE_URL.'?m=profil&amp;c=doPrintPariwisata&amp;a=do&amp;ref_id='.$value['id_penyewaan'].'"><img src="'.BASE_ADDRESS.'icon/print.png" title="cetak Nota"></a>&nbsp;';
                  }

                  if ($value['can_cancelled'] == '1' || $value['status_penyewaan'] == '0'){
                    echo 
                      '<a class="btn-action btn-delete-sewa-bus" data-nama="'.$value['tanggal_pemakaian'].' s.d '.$value['tanggal_kembali'].' (Rp. '.number_format($value['harga_perhari']).')" href="'.BASE_URL.'?m=profil&amp;c=doDeletePariwisata&amp;a=do&amp;ref_id='.$value['id_penyewaan'].'"><img src="'.BASE_ADDRESS.'icon/trash.png" title="hapus"></a>&nbsp';  
                  }
                  echo 
                  '</td>'.
                '</tr>';
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </p>
  </div>
  <div id="ubah_profil" class="tab-pane fade <?php echo (isset($_GET['tab']) && $_GET['tab'] == 'ubah_profil') ? 'in active' : '' ;?>">
    <p>
      <?php
      if (!empty($dataProfil)){
        // $arrayBank = $model->getBank();
        // $optionBank =  '<option value="">Pilih Bank</option>';
        // if (!empty($arrayBank)){
        //   foreach ($arrayBank as $key => $value) {
        //     if ($value['id_bank'] == $dataProfil[0]['id_bank']){
        //       $optionBank .= '<option value="'.$value['id_bank'].'" selected>'.$value['nama_bank'].'</option>';
        //     }else{
        //       $optionBank .= '<option value="'.$value['id_bank'].'">'.$value['nama_bank'].'</option>';
        //     }
        //   }
        // }
        echo 
        '<div class="table-responsive">'.
          '<form action="'.BASE_URL.'?m=profil&c=doUbahProfil&a=do" method="post">'.
            '<table class="table table-striped">'.
              '<col style="width:30%;">'.
              '<col style="width:70%;">'.

              '<tr>'.
                '<th>Nama</th><td><input type="text" class="input-sm form-control" name="nama" value="'.$dataProfil[0]['nama'].'" required></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Email</th><td><input type="text" class="input-sm form-control" name="email" value="'.$dataProfil[0]['email'].'" required></td>'.
              '</tr>'.
              '<tr>'.
                '<th>No. HP</th><td><input type="text" class="input-sm form-control" name="no_hp" value="'.$dataProfil[0]['no_hp'].'" required></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Username Login</th><td><input type="text" class="input-sm form-control" name="username" value="'.$dataProfil[0]['username'].'" required></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Password</th><td><input type="password" class="input-sm form-control" name="password" value=""></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Ulangi Password</th><td><input type="password" class="input-sm form-control" name="ulangi_password" value=""></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Hak Akses</th><td>'.$dataProfil[0]['role_name'].'</td>'.
              '</tr>'.
              // '<tr>'.
              //   '<th>No. Rekening</th><td><input type="text" class="input-sm form-control" name="no_rekening" value="'.$dataProfil[0]['no_rekening'].'" required></td>'.
              // '</tr>'.
              // '<tr>'.
              //   '<th>Bank</th><td><select class="input-sm form-control" name="id_bank" required>'.$optionBank.'</select></td>'.
              // '</tr>'.
              '<tr>'.
                '<th>Pertanyaan Lupa Password</th><td><input type="text" class="input-sm form-control" name="pertanyaan" value="'.$dataProfil[0]['pertanyaan'].'" required></td>'.
              '</tr>'.
              '<tr>'.
                '<th>Jawaban</th><td><input type="password" class="input-sm form-control" name="jawaban" value="'.$dataProfil[0]['jawaban'].'"></td>'.
              '</tr>'.
              '<tr>'.
                '<th></th><td class="text-right"><button name="save" type="submit" class="btn btn-sm btn-primary"><img src="'.BASE_ADDRESS.'icon/save.png"> Simpan</button></td>'.
              '</tr>'.
            '</table>'.
          '</form>'.
        '</div>';
      }   
      ?>
    </p>
  </div>
</div>

<!-- Modal detail tiket-->
<div id="modalDetailTiket" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pembelian</h4>
      </div>
      <div class="modal-body" id="container-detail-tiket"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>

  </div>
</div>
<!-- end Modal detail tiket -->

<script type="text/javascript">
  $(document).ready(function(){
    $('#table-pemesanan-tiket').dataTable();
    $('#table-pembatalan-tiket').dataTable();
    $('#table-pengiriman-barang').dataTable();
    $('#table-pariwisata').dataTable();
  });

  $(document).on('click', 'a.btn-detail-tiket', function(e){
      e.preventDefault();
      e.stopImmediatePropagation();
      href = $(this).attr('href');
      $('#modalDetailTiket').modal('show');
  });

  $('#modalDetailTiket').on('shown.bs.modal', function(e) {
      invoker = $(e.relatedTarget);
      $.ajax({
          url : href,
          type : "post",
          dataType : 'html',
          data : {},
          success : function(response){
              $('#container-detail-tiket').html(response);
          },
          error : function(){
              $.notify('Terjadi kesalahan, coba ulangi kembali', 'warn');
              $('#modalDetailTiket').modal('hide');
          }
      });
  });

  $('#modalDetailTiket').on('hidden.bs.modal', function(e) {
      $('#container-detail-tiket').html('');
  });

  $('.btn-delete-sewa-bus').click(function(e){
    var title = $(this).attr('data-nama');
    var konfirmasi = confirm('Apakah Anda yakin akan menghapus data penyewaan tanggal '+title+' ?');
    if (konfirmasi){
      return true;
    }
    return false;
  });
</script>