<?php
require_once 'modelProfil.php';
Library::load('html2pdf.php', 'html2pdf');
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
$model = new modelProfil();
// $refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$arrId = explode(',', str_replace(' ', '', $_GET['ref_id']));
if (empty($arrId)){
	die('Maaf, referensi ID tidak ditemukan');
}
$dataTiket = $model->getDetailDataPembelianTiket($arrId[0]);
$totalHarga = $dataTiket[0]['total_harga'];

if (isset($arrId[1])){
	$dataTiketPulang = $model->getDetailDataPembelianTiket($arrId[1]);
	$totalHarga += $dataTiketPulang[0]['total_harga'];
}
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding">INVOICE PEMBELIAN TIKET</h3>
<hr>
<?php
if (!empty($dataTiket)){
	?>
	<table class="table-control table-bordered">
		<col style="width:35%"></col>
		<col style="width:65%"></col>
		<tr>
			<td>ID</td>
			<td>: <?php echo $dataTiket[0]['id_pembelian_tiket'];?></td>
		</tr>
		<tr>
			<td>Tanggal Pemberangkatan</td>
			<td>: <?php echo date('d M Y', strtotime($dataTiket[0]['tanggal_pemberangkatan']));?></td>
		</tr>
		<tr>
			<td>Pemberangkatan</td>
			<td>: <?php echo $dataTiket[0]['nama_armada'].'|'.$dataTiket[0]['kota_asal'].' - '.$dataTiket[0]['kota_tujuan'];?></td>
		</tr>
		<tr>
			<td>Kota Tujuan</td>
			<td>: <?php echo $dataTiket[0]['lokasi_tujuan'];?></td>
		</tr>
		<tr>
			<td>Harga Tiket</td>
			<td>: <?php echo 'Rp.'.number_format($dataTiket[0]['sub_total'], 0, 0, '.').',-';?></td>
		</tr>
		<tr>
			<td>Penumpang</td>
			<td>
				<table class="table-control table-bordered">
					<tr>
						<th align="center">No</th>
						<th>No. Kursi</th>
						<th>Atas Nama</th>
					</tr>
					<?php
					foreach ($dataTiket as $key => $value) {
						?>
						<tr>
							<td align="center"><?php echo ($key + 1);?></td>
							<td><?php echo $value['no_kursi'];?></td>
							<td><?php echo $value['atas_nama'];?></td>
						</tr>
						<?php
					}
					?>
				</table>
			</td>
		</tr>
	</table>
	<?php
}
if (!empty($dataTiketPulang)){
	?>
	<br>
	<table class="table-control table-bordered">
		<col style="width:35%"></col>
		<col style="width:65%"></col>
		<tr>
			<td>ID</td>
			<td>: <?php echo $dataTiketPulang[0]['id_pembelian_tiket'];?></td>
		</tr>
		<tr>
			<td>Tanggal Kembali</td>
			<td>: <?php echo date('d M Y', strtotime($dataTiketPulang[0]['tanggal_pemberangkatan']));?></td>
		</tr>
		<tr>
			<td>Pemberangkatan</td>
			<td>: <?php echo $dataTiketPulang[0]['nama_armada'].'|'.$dataTiketPulang[0]['kota_asal'].' - '.$dataTiketPulang[0]['kota_tujuan'];?></td>
		</tr>
		<tr>
			<td>Kota Tujuan</td>
			<td>: <?php echo $dataTiketPulang[0]['lokasi_tujuan'];?></td>
		</tr>
		<tr>
			<td>Harga Tiket</td>
			<td>: <?php echo 'Rp.'.number_format($dataTiketPulang[0]['sub_total'], 0, 0, '.').',-';?></td>
		</tr>
		<tr>
			<td>Penumpang</td>
			<td>
				<table class="table-control table-bordered">
					<tr>
						<th align="center">No</th>
						<th>No. Kursi</th>
						<th>Atas Nama</th>
					</tr>
					<?php
					foreach ($dataTiketPulang as $key => $value) {
						?>
						<tr>
							<td align="center"><?php echo ($key + 1);?></td>
							<td><?php echo $value['no_kursi'];?></td>
							<td><?php echo $value['atas_nama'];?></td>
						</tr>
						<?php
					}
					?>
				</table>
			</td>
		</tr>
	</table>
	<?php
}
?>
<br>
<table class="table-control table-bordered">
	<col style="width:35%"></col>
	<col style="width:65%"></col>
	<tr>
		<td>Total:</td>
		<td>Rp.<?php echo number_format($totalHarga, 0, 0, '.');?>,-</td>
	</tr>
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>Terima kasih atas kepercayaan Anda kepada PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	// $html2pdf = new html2pdf('P', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf = new html2pdf('P', 'folio', 'en', true, 'UTF-8', array(2, 2, 2, 2));
	$html2pdf->writeHTML($content);
	$html2pdf->Output('cetak.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>