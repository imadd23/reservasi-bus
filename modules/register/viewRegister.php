<?php
require_once 'modelRegister.php';
Library::load('autoload.php', 'Gregwar/Captcha');
use Gregwar\Captcha\CaptchaBuilder;

$model = new modelRegister();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$message = '<div class="alert alert-success">Proses pendaftaran berhasil, silahkan login dengan akun yang telah Anda buat.</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '2'){
	$message = '<div class="alert alert-danger">Password tidak konsisten</div>';
}
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '3'){
	$message = '<div class="alert alert-danger">Captcha yang Anda masukkan salah. Coba ulangi kembali</div>';
}

if (isset($_GET['statusDuplicated']) && $_GET['statusDuplicated'] == '1'){
	$message = '<div class="alert alert-danger">Email Anda sudah terdaftar</div>';
}

$nama = isset($_GET['nama']) ? $_GET['nama'] : '';
$no_rekening = isset($_GET['no_rekening']) ? $_GET['no_rekening'] : '';
$id_bank = isset($_GET['id_bank']) ? $_GET['id_bank'] : '';
$no_hp = isset($_GET['no_hp']) ? $_GET['no_hp'] : '';
$email = isset($_GET['email']) ? $_GET['email'] : '';
$username = isset($_GET['username']) ? $_GET['username'] : '';
$pertanyaan = isset($_GET['pertanyaan']) ? $_GET['pertanyaan'] : '';
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-8">
		<h3 class="text-center">Daftar Akun PO Family Ceria</h3>
		<?php echo $message;?>
		<form method="post" action="<?php echo BASE_URL.'?m=register&c=doAdd&a=do';?>" class="form-horizontal" id="form-register">
			<div class="control-group">
				<label class="control-label col-sm-6">Nama *</label>
				<div class="col-sm-6">
					<input type="text" name="nama" class="form-control input-sm" value="<?php echo $nama;?>" 
					data-validation="required"
					data-validation-error-msg-required="Nama Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Email *</label>
				<div class="col-sm-6">
					<input type="text" name="email" class="form-control input-sm" value="<?php echo $email;?>" 
					data-validation="email" 
					data-validation-error-msg-email="Format email salah">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Nomer Handphone *</label>
				<div class="col-sm-6">
					<input type="number" name="no_hp" class="form-control input-sm" value="<?php echo $no_hp;?>"
					data-validation="number"
					data-validation-error-msg-number="No. Handphone harus angka">
					<p class="help-block"></p>
				</div>
			</div>
			<!-- <div class="control-group">
				<label class="control-label col-sm-6">Nomer Rekening *</label>
				<div class="col-sm-6">
					<input type="text" name="no_rekening" class="form-control input-sm" value="<?php echo $no_rekening;?>" required>
				</div>
			</div> -->
			<!-- <div class="control-group">
				<label class="control-label col-sm-6">Bank *</label>
				<div class="col-sm-6">
					<select name="id_bank" class="form-control input-sm" required>
						<option value="">Pilih Bank</option>
						<?php
						if (!empty($model->getBank())){
							foreach ($model->getBank() as $key => $value) {
								if ($value['id_bank'] == $id_bank){
									echo '<option value="'.$value['id_bank'].'" selected>'.$value['nama_bank'].'</option>';
								}else{
									echo '<option value="'.$value['id_bank'].'">'.$value['nama_bank'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div> -->
			<div class="control-group">
				<label class="control-label col-sm-6">Username login *</label>
				<div class="col-sm-6">
					<input type="text" name="username" class="form-control input-sm" value="<?php echo $username;?>" 
					data-validation="required"
					data-validation-error-msg-required="Username Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Password *</label>
				<div class="col-sm-6">
					<input type="password" name="password" class="form-control input-sm" value="" 
					data-validation="required"
					data-validation-error-msg-required="Password Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Ulangi Password *</label>
				<div class="col-sm-6">
					<input type="password" name="ulangi_password" class="form-control input-sm" value="" 
					data-validation="required"
					data-validation-error-msg-required="Repeat Password Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Pertanyaan lupa password *</label>
				<div class="col-sm-6">
					<input type="text" name="pertanyaan" class="form-control input-sm" value="<?php echo $pertanyaan;?>" 
					data-validation="required"
					data-validation-error-msg-required="Pertanyaan lupa password tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Jawaban *</label>
				<div class="col-sm-6">
					<input type="password" name="jawaban" class="form-control input-sm" value="" 
					data-validation="required"
					data-validation-error-msg-required="Jawaban Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6"></label>
				<div class="col-sm-6">
					<?php
					$builder = new CaptchaBuilder;
					$builder->setDistortion(false);
					$builder->setIgnoreAllEffects(true);
					$builder->setBackgroundColor(251, 251, 251);
					$builder->build();
					$_SESSION['captcha'] = $builder->getPhrase();
					?>
					<img src="<?php echo $builder->inline(); ?>" />
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label col-sm-6">Masukkan Kode captcha di atas*</label>
				<div class="col-sm-6">
					<input type="text" name="captcha" class="form-control input-sm" value="" 
					data-validation="required"
					data-validation-error-msg-required="Captcha Tidak boleh kosong">
					<p class="help-block"></p>
				</div>
			</div>
			<div class="control-group">
				<label class="col-sm-6"></label>
				<div class="col-sm-6">
					<button type="reset" name="save" class="btn btn-sm btn-danger">Reset</button>
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
					<p class="help-block"></p>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/jquery.form-validator.min.js';?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
			format : 'dd-mm-yyyy'
		});
		// $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
	});

	$.validate({
	  	form : '#form-register'
	});
</script>