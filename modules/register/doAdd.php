<?php
require_once 'modelRegister.php';
$model = new modelRegister();

$p = array(
	'nama' => $_POST['nama'],
   	'email' => $_POST['email'],
    'no_hp' => $_POST['no_hp'],
    'username' => $_POST['username'],
    'password' => $_POST['password'],
    'ulangi_password' => $_POST['ulangi_password'],
    'pertanyaan' => $_POST['pertanyaan'],
    'jawaban' => $_POST['jawaban']
   );

if($_POST['captcha'] !== $_SESSION['captcha']){
    unset($p['password'], $p['ulangi_password'], $p['jawaban'], $_SESSION['captcha']);
    header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusAdd=3&'.http_build_query($p));
    die();
}

if (isset($_POST['save'])){
    $cek = $model->cek($p['email']);
    if (!empty($p['password']) && $p['password'] != $p['ulangi_password']){
        header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusAdd=2&'.http_build_query($p));
    }else
    if (in_array('', array_values($p))){
    	unset($p['password'], $p['ulangi_password'], $p['jawaban']);
    	header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusAdd=0&'.http_build_query($p));
    }else
    if (empty($cek)){ 	
    	$save = $model->add($p['nama'], $p['email'], $p['no_hp'], $p['username'], $p['password'], $p['pertanyaan'], $p['jawaban']);
	    if ($save){
	    	//include MODULE_PATH.'login/doLogin'.EXT;
            header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusAdd=1');
	    }else{
	    	unset($p['password'], $p['ulangi_password'], $p['jawaban']);
	    	header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusAdd=0&'.http_build_query($p));
	    }
	}else
	if (!empty($cek)){
		unset($p['password'], $p['ulangi_password'], $p['jawaban']);
		header('location: '.BASE_URL.'?m=register&c=viewRegister&a=view&statusDuplicated=1&'.http_build_query($p));
	}
		
}
?>