<?php
require_once 'modelPembatalan.php';
$model = new modelPembatalan();
$explRefId = explode(",", str_replace(' ', '', $_GET['ref_id']));
$detailTiket = array();
$totalBayar = 0;
if (!empty($explRefId)){
	foreach ($explRefId as $key => $value) {
		$detailTiket[] = $model->getDetailDataPembelianTiket($value);
	}

	if (!empty($detailTiket)){
		foreach ($detailTiket as $key => $value) {
			$totalBayar += $value[0]['total_harga'];
		}
	}
}
?>
<table class="table table-bordered">
	<col class="col-sm-3"></col>
	<col class="col-sm-9"></col>
	<tr>
		<th>Tanggal Berangkat</th><td><?php echo ucwords($detailTiket[0][0]['hari']).', '.date('d-m-Y', strtotime($detailTiket[0][0]['tanggal_pemberangkatan']));?></td>
	</tr>
	<tr>
		<th>Jam</th><td><?php echo date('H:i', strtotime($detailTiket[0][0]['jam']));?></td>
	</tr>
	<tr>
		<th>Kota Asal</th><td><?php echo $detailTiket[0][0]['kota_asal'];?></td>
	</tr>
	<tr>
		<th>Kota Tujuan</th><td><?php echo $detailTiket[0][0]['kota_tujuan'];?></td>
	</tr>
	<tr>
		<th>Nama Armada</th><td><?php echo $detailTiket[0][0]['nama_armada'];?></td>
	</tr>
	<tr>
		<th>Penumpang</th>
		<td>
			<table class="table table-bordered">
				<tr>
					<th class="text-center">No</th>
					<th>Nama Penumpang</th>
					<th>No Kursi</th>
				</tr>
				<?php
				foreach ($detailTiket[0] as $k => $v) {
					?>
					<tr>
						<td class="text-center"><?php echo ($k+1);?></td>
						<td><?php echo $v['atas_nama'];?></td>
						<td><?php echo $v['no_kursi'];?></td>
					</tr>
					<?php
				}
				?>
			</table>
		</td>
	</tr>
</table>


<?php
if (count($explRefId) > 1){
	?>
	<table class="table table-bordered">
		<col class="col-sm-3"></col>
		<col class="col-sm-9"></col>
		<tr>
			<th>Tanggal Pulang</th><td><?php echo ucwords($detailTiket[1][0]['hari']).', '.date('d-m-Y', strtotime($detailTiket[1][0]['tanggal_pemberangkatan']));?></td>
		</tr>
		<tr>
			<th>Jam</th><td><?php echo date('H:i', strtotime($detailTiket[1][0]['jam']));?></td>
		</tr>
		<tr>
			<th>Kota Asal</th><td><?php echo $detailTiket[1][0]['kota_asal'];?></td>
		</tr>
		<tr>
			<th>Kota Tujuan</th><td><?php echo $detailTiket[1][0]['kota_tujuan'];?></td>
		</tr>
		<tr>
			<th>Nama Armada</th><td><?php echo $detailTiket[1][0]['nama_armada'];?></td>
		</tr>
		<tr>
			<th>Penumpang</th>
			<td>
				<table class="table table-bordered">
					<tr>
						<th class="text-center">No</th>
						<th>Nama Penumpang</th>
						<th>No Kursi</th>
					</tr>
					<?php
					foreach ($detailTiket[1] as $k => $v) {
						?>
						<tr>
							<td class="text-center"><?php echo ($k+1);?></td>
							<td><?php echo $v['atas_nama'];?></td>
							<td><?php echo $v['no_kursi'];?></td>
						</tr>
						<?php
					}
					?>
				</table>
			</td>
		</tr>
	</table>
	<?php
}
?>
<table class="table table-bordered">
	<col class="col-sm-3"></col>
	<col class="col-sm-9"></col>
	<tr>
		<th>Total</th><td>Rp. <?php echo number_format($totalBayar, 0, 0, '.');?>,-</td>
	</tr>
</table>