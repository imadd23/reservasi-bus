<?php
require_once 'modelPembatalan.php';
$model = new modelPembatalan();
$arrayPembelian = $model->getDataPembatalan();

if (!empty($arrayPembelian)){
	foreach ($arrayPembelian as $key => $value) {
		$arrayPembelian[$key]['no'] = $key + 1;
		$arrayPembelian[$key]['aksi'] = "<a class='btn-action btn-detail' href='".BASE_URL."?m=pembatalan&c=getDetailTiket&a=do&ref_id=".$value['id_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/detail.png' title='Detail Pembelian'></a>\n&nbsp;&nbsp;";
		$arrayPembelian[$key]['aksi'] .= "<a class='btn-action btn-cancel' data-id='".$value['id_pembelian_tiket']."' href='".BASE_URL."?m=pembatalan&c=doCancel&a=do&ref_id=".$value['id_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/trash.png' title='Batalkan'></a>";

	}
}

echo json_encode(array('data' => $arrayPembelian));
?>