<?php
require_once 'modelPembatalan.php';
$model = new modelPembatalan();
$setting = $model->getSettingPembatalan();

$jamCancelBeforeCancel = 0;
$jamCancelAfterConfirm = 0;

if (!empty($setting)){
    foreach ($setting as $key => $value) {
        if ($value['id'] == 'BT'){
            $jamCancelBeforeCancel = $value['set'];
        }else
        if ($value['id'] == 'PTK'){
            $jamCancelAfterConfirm = $value['set'];
        }
    }
}
$info = '<div class="alert alert-info">Tiket tidak bisa dibatalkan setelah dikonfirmasi petugas';
if (!empty($jamCancelBeforeCancel)){
    $info .= '<br>';
    $info .= 'Pembatalan tiket sebelum dikonfirmasi petugas maksimal '.$jamCancelBeforeCancel.' jam dari waktu pembelian';
}
if (!empty($jamCancelAfterConfirm)){
    $info .= '<br>';
    $info .= 'Pembatalan tiket setelah dikonfirmasi petugas maksimal '.$jamCancelAfterConfirm.' jam';
}
$info .= '</div>';

$message = '';
if (isset($_GET['statusCancel']) && $_GET['statusCancel'] ==  '1'){
    $message = '<div class="alert alert-success">Pembelian tiket berhasil dibatalkan</div>';
}else
if (isset($_GET['statusCancel']) && $_GET['statusCancel'] == '0'){
    $message = '<div class="alert alert-danger">Pembelian tiket gagal dibatalkan, coba ulangi kembali</div>';
}
?>
<h3>Data Pembelian Tiket yang bisa dibatalkan</h3>
<?php echo $message;?>
<?php echo $info; ?>
<div class="col-sm-14">
    <div class="table-responsive">
        <table id="table-penumpang" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=pembatalan&c=getData&a=do';?>">
            <thead>
                <th class="text-center">No</th>
                <th>ID Pembelian</th>
                <th>Tanggal Beli</th>
                <th>Tanggal Pemberangkatan</th>
                <th>Atas Nama</th>
                <th>Biaya</th>
                <th class="text-center">Aksi</th>
            </thead>
        </table>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pembelian</h4>
      </div>
      <div class="modal-body" id="container-detail-tiket"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript">
    var href = '';
    $(document).ready(function(){
        $('#table-penumpang').dataTable({
            ajax : {
                url : $('#table-penumpang').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'text-center'},
                {data : 'id_pembelian_tiket'},
                {data : 'tanggal_beli'},
                {data : 'tanggal_pemberangkatan'},
                {data : 'atas_nama'},
                {data : 'total_harga', class : 'text-center'},
                {data : 'aksi', class : 'text-center'}
            ]
        });
    });

    $(document).on('click', 'a.btn-cancel', function(){
        if (confirm('Anda yakin akan membatalkan pebelian tiket '+$(this).attr('data-id')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });

    $(document).on('click', 'a.btn-detail', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        href = $(this).attr('href');
        $('#myModal').modal('show');
    });

    $('#myModal').on('shown.bs.modal', function(e) {
        invoker = $(e.relatedTarget);
        $.ajax({
            url : href,
            type : "post",
            dataType : 'html',
            data : {},
            success : function(response){
                $('#container-detail-tiket').html(response);
            },
            error : function(){
                $.notify('Terjadi kesalahan, coba ulangi kembali', 'warn');
                $('#myModal').modal('hide');
            }
        });
    });
    $('#myModal').on('hidden.bs.modal', function(e) {
        $('#container-detail-tiket').html('');
    });
</script>