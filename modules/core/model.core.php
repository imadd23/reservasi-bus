<?php
class model{

	public $wheres;

	public function __construct(){
		$this->wheres = '';
	}

	public function where($whereData = array()){
		$where = '';
		if (!empty($whereData) && is_array($whereData)){
			$where .= ' WHERE ';
			foreach ($whereData as $key => $value) {
				$where .= $key." = '".$value."' AND ";
			}
			$where .= '1 = 1';
		}else
		if (!empty($whereData) && is_string($whereData)){
			$where .= ' WHERE '.$whereData;
		}
		$this->wheres = $where;
	}

	protected function setData($newData){
		$_newData = '';
		if (!empty($newData)){
			$maxKey = max(array_keys(array_values($newData)));
			$i = 0;
			foreach ($newData as $key => $value) {
				if ($i === $maxKey){
					$_newData .= $key." = '".$value."'";
				}else{
					$_newData .= $key." = '".$value."', ";
				}
				$i++;				
			}
		}
		return $_newData;
	}

	public function clearWhere(){
		$this->wheres = '';
	}

	protected function result($rQuery){
		$result = array();
		while ($data = mysql_fetch_array($rQuery)){
			$result[] = $data;
		}
		return $result;
	}
	
	protected function startTransaction(){
		mysql_query("SET autocommit=0");
		mysql_query("START TRANSACTION");
	}

	protected function endTransaction($result){
		if ($result){
			mysql_query("COMMIT");
		}else{
			mysql_query("ROLLBACK");
		}
	}

	public function pemberangkatanQueryAB(){
		$query = "SELECT PBD.id_detail_pemberangkatan, PB.`id_pemberangkatan`, PB.`nama_pemberangkatan`, PBD.harga, PB.`jam_berangkat`, PB.`hari`, AR.`nama_armada`, 
			KA.`nama_kota` AS nama_kota_pemberangkatan, KT.`nama_kota` AS kota_tujuan, KA.`order_kota`, KT.`order_kota` AS order_kota_tujuan,
			KA.`id_kota` AS id_kota_asal, KT.id_kota AS id_kota_tujuan
			FROM 
			`pemberangkatan` AS PB
			LEFT JOIN `armada` AS AR ON AR.`id_armada` = PB.`id_armada`
			LEFT JOIN kota AS KA ON KA.`id_kota` = PB.`id_kota_asal`
			LEFT JOIN (
				SELECT `_PBD`.`id_pemberangkatan`, _PBD.`id_detail_pemberangkatan`, _PBD.`id_kota_tujuan`, _harga AS harga
				FROM `pemberangkatan_detail` AS _PBD
				LEFT JOIN (
					SELECT SUM(`harga`) AS _harga, `id_pemberangkatan`
					FROM `pemberangkatan_detail`
					GROUP BY `id_pemberangkatan`
				)AS TABLE_HARGA ON TABlE_HARGA.id_pemberangkatan = _PBD.`id_pemberangkatan`
				WHERE _PBD.`id_detail_pemberangkatan` IN (
					SELECT MAX(`id_detail_pemberangkatan`) AS max_idpd
					FROM `pemberangkatan_detail`
					GROUP BY `id_pemberangkatan`
				)
				GROUP BY _PBD.id_pemberangkatan
			)AS PBD ON PBD.`id_pemberangkatan` = PB.`id_pemberangkatan`
			LEFT JOIN kota AS KT ON KT.`id_kota` = PBD.id_kota_tujuan
			WHERE 
			PB.`tipe` = 'AB'
			AND KA.`id_kota` <> KT.`id_kota`
			HAVING 
			id_detail_pemberangkatan IS NOT NULL AND kota_tujuan IS NOT NULL

			UNION
			SELECT PBD.id_detail_pemberangkatan, PB.`id_pemberangkatan`, PB.`nama_pemberangkatan`, 
			(
				SELECT SUM(harga)
				FROM `pemberangkatan_detail` AS _PBD
				LEFT JOIN kota AS _KT ON _KT.`id_kota` = _PBD.`id_kota_tujuan`
				WHERE 
				_PBD.`id_pemberangkatan` = PBD.id_pemberangkatan
				AND (_KT.`order_kota` BETWEEN KA.`order_kota` AND KT.`order_kota`)
			) AS harga, 
			PBD.`jam_berangkat`, PBD.`hari`, AR.`nama_armada`, 
			KA.`nama_kota` AS nama_kota_pemberangkatan, KT.`nama_kota` AS kota_tujuan, KA.`order_kota`, KT.`order_kota` AS order_kota_tujuan,
			KA.`id_kota` AS id_kota_asal, KT.id_kota AS id_kota_tujuan
			FROM 
			`pemberangkatan_detail` AS PBD
			INNER JOIN `pemberangkatan` AS PB ON PBD.id_pemberangkatan = PB.`id_pemberangkatan`
			LEFT JOIN `armada` AS AR ON AR.`id_armada` = PB.`id_armada`
			LEFT JOIN `pemberangkatan_detail_relasi` PBDR ON PBDR.id_detail_pemberangkatan_asal = PBD.id_detail_pemberangkatan
			LEFT JOIN kota AS KA ON KA.`id_kota` = PBD.`id_kota_tujuan`
			LEFT JOIN pemberangkatan_detail AS PBD2 ON PBDR.id_detail_pemberangkatan_tujuan = PBD2.id_detail_pemberangkatan
			LEFT JOIN kota AS KT ON KT.`id_kota` = PBD2.id_kota_tujuan
			WHERE
			PB.`tipe` = 'AB'
			AND KA.`id_kota` <> KT.`id_kota`
			HAVING 
			id_detail_pemberangkatan IS NOT NULL AND kota_tujuan IS NOT NULL

			ORDER BY `id_pemberangkatan`, `order_kota`, `order_kota_tujuan` ASC";
		$qQuery = mysql_query($query);
		return $this->result($qQuery);
	}

	public function pemberangkatanQueryBA(){
		$query = "SELECT PBD.id_detail_pemberangkatan, PB.`id_pemberangkatan`, PB.`nama_pemberangkatan`, PBD.harga, PB.`jam_berangkat`, PB.`hari`, AR.`nama_armada`, 
			KA.`nama_kota` AS nama_kota_pemberangkatan, KT.`nama_kota` AS kota_tujuan, KA.`order_kota`, KT.`order_kota` AS order_kota_tujuan,
			KA.`id_kota` AS id_kota_asal, KT.id_kota AS id_kota_tujuan
			FROM 
			`pemberangkatan` AS PB
			LEFT JOIN `armada` AS AR ON AR.`id_armada` = PB.`id_armada`
			LEFT JOIN kota AS KA ON KA.`id_kota` = PB.`id_kota_asal`
			LEFT JOIN (
				SELECT `_PBD`.`id_pemberangkatan`, _PBD.`id_detail_pemberangkatan`, _PBD.`id_kota_tujuan`, _harga AS harga
				FROM `pemberangkatan_detail` AS _PBD
				LEFT JOIN (
					SELECT SUM(`harga`) AS _harga, `id_pemberangkatan`
					FROM `pemberangkatan_detail`
					GROUP BY `id_pemberangkatan`
				)AS TABLE_HARGA ON TABlE_HARGA.id_pemberangkatan = _PBD.`id_pemberangkatan`
				WHERE _PBD.`id_detail_pemberangkatan` IN (
					SELECT MAX(`id_detail_pemberangkatan`) AS max_idpd
					FROM `pemberangkatan_detail`
					GROUP BY `id_pemberangkatan`
				)
				GROUP BY _PBD.id_pemberangkatan
			)AS PBD ON PBD.`id_pemberangkatan` = PB.`id_pemberangkatan`
			LEFT JOIN kota AS KT ON KT.`id_kota` = PBD.id_kota_tujuan
			WHERE 
			PB.`tipe` = 'BA'
			AND KA.`id_kota` <> KT.`id_kota`
			HAVING 
			id_detail_pemberangkatan IS NOT NULL AND kota_tujuan IS NOT NULL

			UNION
			SELECT PBD.id_detail_pemberangkatan, PB.`id_pemberangkatan`, PB.`nama_pemberangkatan`, 
			(
				SELECT SUM(harga)
				FROM `pemberangkatan_detail` AS _PBD
				LEFT JOIN kota AS _KT ON _KT.`id_kota` = _PBD.`id_kota_tujuan`
				WHERE 
				_PBD.`id_pemberangkatan` = PBD.id_pemberangkatan
				AND (_KT.`order_kota` BETWEEN KA.`order_kota` AND KT.`order_kota`)
			) AS harga, 
			PBD.`jam_berangkat`, PBD.`hari`, AR.`nama_armada`, 
			KA.`nama_kota` AS nama_kota_pemberangkatan, KT.`nama_kota` AS kota_tujuan, KA.`order_kota`, KT.`order_kota` AS order_kota_tujuan,
			KA.`id_kota` AS id_kota_asal, KT.id_kota AS id_kota_tujuan
			FROM 
			`pemberangkatan_detail` AS PBD
			INNER JOIN `pemberangkatan` AS PB ON PBD.id_pemberangkatan = PB.`id_pemberangkatan`
			LEFT JOIN `armada` AS AR ON AR.`id_armada` = PB.`id_armada`
			LEFT JOIN `pemberangkatan_detail_relasi` PBDR ON PBDR.id_detail_pemberangkatan_asal = PBD.id_detail_pemberangkatan
			LEFT JOIN kota AS KA ON KA.`id_kota` = PBD.`id_kota_tujuan`
			LEFT JOIN pemberangkatan_detail AS PBD2 ON PBDR.id_detail_pemberangkatan_tujuan = PBD2.id_detail_pemberangkatan
			LEFT JOIN kota AS KT ON KT.`id_kota` = PBD2.id_kota_tujuan
			WHERE
			PB.`tipe` = 'BA'
			AND KA.`id_kota` <> KT.`id_kota`
			HAVING 
			id_detail_pemberangkatan IS NOT NULL AND kota_tujuan IS NOT NULL

			ORDER BY `id_pemberangkatan`, `order_kota`, `order_kota_tujuan` DESC";
		$qQuery = mysql_query($query);
		return $this->result($qQuery);
	}

	public function pemberangkatanQueryTipe($id_kota_asal, $id_kota_tujuan){
		$query = "SELECT IF(order_kota_a < order_kota_b, 'AB', 'BA') AS tipe
			FROM
			(
				SELECT order_kota AS order_kota_a
				FROM
				kota
				WHERE `id_kota` = $id_kota_asal
				LIMIT 1
			) AS TABLE_A LEFT JOIN
			(
				SELECT order_kota AS order_kota_b
				FROM
				kota
				WHERE `id_kota` = $id_kota_tujuan
				LIMIT 1
			)AS TABLE_B ON TRUE
			LIMIT 1";
		$qQuery = mysql_query($query);
		$result = $this->result($qQuery);
		$result = isset($result[0]['tipe']) ? $result[0]['tipe'] : 'AB';
		return $result;
	}
}