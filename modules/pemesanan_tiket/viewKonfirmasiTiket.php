<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$message = '';
$userId = $_SESSION['user_id'];
$dataPembelianTiket = $model->getPemesananTiketUnconfirm($userId);
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$message = '<div class="alert alert-success">Konfirmasi pembayaran berhasil dilakukan</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Konfirmasi pembayaran gagal dilakukan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-12">
		<h3 class="text-center">Konfirmasi Pembayaran Tiket</h3>
		<?php echo $message;?>
		<hr>
		<table class="table table-hover table-stripped table-bordered">
			<thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Tanggal Beli</th>
					<th class="text-center">Tanggal Pemberangkatan</th>
					<th class="text-center">Pemberangkatan</th>
					<th class="text-center">Total Harga</th>
					<th class="text-center">Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if (!empty($dataPembelianTiket)){
					foreach ($dataPembelianTiket as $key => $value) {
						$tanggal_pemberangkatan = explode(',', $value['tanggal_pemberangkatan']);
						$tglPemberangkatan = array();
						foreach ($tanggal_pemberangkatan as $k => $v) {
							$tglPemberangkatan[] = date('d-m-Y', strtotime($v));
						}
						$tglPemberangkatan = implode(',', $tglPemberangkatan);
						?>
						<tr>
							<td class="text-center"><?php echo ($key + 1);?></td>
							<td class="text-center"><?php echo date('d-m-Y', strtotime($value['tanggal_beli']));?></td>
							<td class="text-center"><?php echo $tglPemberangkatan;?></td>
							<td><?php echo $value['kota_pemberangkatan'];?></td>
							<td class='text-center'><?php echo 'Rp. '.number_format($value['total_harga'], 0, 0, '.').'-';?></td>
							<td class="text-center">
								<?php
								if ($value['status_bayar'] == 'belum lunas' && empty($value['bukti_transfer'])){
									?>
									<a class="btn-action btn-confirm" href="#" 
										data-id="<?php echo $value['id_pembelian_tiket'];?>" 
										data-toggle="modal" 
										data-target="#modal-upload"
										data-harga = "<?php echo 'Rp. '.number_format($value['total_harga'], 0, 0, '.').'-';?>"
										data-pemberangkatan="<?php echo $value['kota_pemberangkatan'];?>"
										data-tanggal="<?php echo date('d-m-Y', strtotime($value['tanggal_beli']));?>">
										<img src="<?php echo BASE_ADDRESS.'icon/upload.png';?>" title="upload">
									</a>
									<?php
								}else
								if (!empty($value['bukti_transfer'])){
									?>
									<a class="btn-action btn-confirm" href="<?php echo BASE_ADDRESS.$value['bukti_transfer'];?>" target="_blank">
										<img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>" title="detail bukti pembayaran">
									</a>
									<?php
								}
								?>
							</td>
						</tr>
						<?php
					}
				}else{
					?>
					<tr>
						<td colspan="6" class="text-center"><i>Maaf, data pembelian tidak ditemukan</i></td>
					</tr>
					<?php
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<!-- Modal upload confirm-->
<div id="modal-upload" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <form class="form-horizontal" method="post" action="?m=pemesanan_tiket&c=doUploadBuktiPembayaran&a=do" enctype="multipart/form-data">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Upload Bukti Pembayaran</h4>
	      </div>
	      <div class="modal-body">
	  		<div class="form-group">
	  			<label class="col-sm-3 control-label">Tanggal Beli</label>
	  			<div class="col-sm-4 tanggal-container"></div>
	  		</div>
	  		<div class="form-group">
	  			<label class="col-sm-3 control-label">Pemberangkatan</label>
	  			<div class="col-sm-9 pemberangkatan-container"></div>
	  		</div>
	  		<div class="form-group">
	  			<label class="col-sm-3 control-label">Total Harga</label>
	  			<div class="col-sm-9 total-container"></div>
	  		</div>
	  		<div class="form-group">
	  			<label class="col-sm-3 control-label">File Bukti Transfer <br><i>(image/pdf)</i></label>
	  			<div class="col-sm-4">
	  				<input type="file" name="bukti" required>
	  			</div>
	  		</div>
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="id_pembelian_tiket" value="0">
	        <button type="submit" class="btn btn-default">Upload</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
	      </div>
	    </div>
    </form>
  </div>
	
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-confirm').click(function(e){
			var $this = $(this);
			$('input[name=id_pembelian_tiket]').val($this.attr('data-id'));
			$('.tanggal-container').html($this.attr('data-tanggal'));
			$('.pemberangkatan-container').html($this.attr('data-pemberangkatan'));
			$('.total-container').html($this.attr('data-harga'));
		});
	});
</script>
<!-- end Modal upload confirm -->