<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$id_pemberangkatan = $_POST['ref_id'];
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$arrayNoKursi = $model->getNoKursi($id_pemberangkatan, $tanggal_pemberangkatan);
$pb = $model->getHarga($id_pemberangkatan);
$harga = $pb[0]['harga'];
$jam = $pb[0]['jam'];
$jam_sampai = $pb[0]['jam_sampai'];
$nama_armada = $pb[0]['nama_armada'].'&nbsp;';
$nama_armada .= '<a class="btn-action btn-detail-armada" href="?m=pemesanan_tiket&c=getDetailArmada&a=do&ref_id='.$pb[0]['id_armada'].'"><img src="'.BASE_ADDRESS.'icon/detail.png" title="detail"></a>';
echo json_encode(array('success' => true, 'data' => $arrayNoKursi, 'harga' => $harga, 'jam' => $jam, 'jam_sampai' => $jam_sampai, 'nama_armada' => $nama_armada));
?>