<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$waktu_pembatalan = $model->getSettingPembelianTiket();
	$message = '<div class="alert alert-success">Data pembelian berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data pembelian berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}

$tanggal_pemberangkatan = isset($_GET['tanggal_pemberangkatan']) ? date('d-m-Y', strtotime($_GET['tanggal_pemberangkatan'])) : '';
$id_pemberangkatan = isset($_GET['id_pemberangkatan']) ? $_GET['id_pemberangkatan'] : '';
$id_no_kursi = isset($_GET['id_no_kursi']) ? $_GET['id_no_kursi'] : '';
$atas_nama = isset($_GET['atas_nama']) ? $_GET['atas_nama'] : '';
$id_penumpang = isset($_GET['id_penumpang']) ? $_GET['id_penumpang'] : '';
$sub_total = isset($_GET['sub_total']) ? $_GET['sub_total'] : '';
$no_kursi = isset($_GET['no_kursi']) ? $_GET['no_kursi'] : 'Pilih Nomer Kursi';
$id_detail_pembelian_tiket = isset($_GET['id_detail_pembelian_tiket']) ? $_GET['id_detail_pembelian_tiket'] : '';
$nama_armada = isset($_GET['nama_armada']) ? $_GET['nama_armada'] : '';
$kota_asal = isset($_GET['kota_asal']) ? $_GET['kota_asal'] : '';
$id_kota_asal = isset($_GET['kota_asal']) ? $_GET['kota_asal'] : '';
$kota_tujuan = isset($_GET['kota_tujuan']) ? $_GET['kota_tujuan'] : '';
$id_kota_tujuan = isset($_GET['id_kota_tujuan']) ? $_GET['id_kota_tujuan'] : '';
$id_armada = isset($_GET['id_armada']) ? $_GET['id_armada'] : '';
$_pemberangkatan = !empty($id_pemberangkatan) ? '<option value="'.$id_pemberangkatan.'" data-id-armada="'.$id_armada.'" selected>'.$nama_armada.' | '.$kota_asal.' - '.$kota_tujuan.'</option>' : '<option>Pilih Pemberangkatan</option>';
$id_pembelian_tiket = isset($_SESSION['id_pembelian_tiket']) ? $_SESSION['id_pembelian_tiket'] : 0;
$detail_pemesanan = $model->getdetailTiket($id_pembelian_tiket);
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-12">
		<h3 class="text-center">Pemesanan Tiket Pulang</h3>
		<?php echo $message;?>
		<hr>
		<div class="panel panel-default" id="confirm-tiket-kembali">
			<div class="panel-heading text-center">Konfirmasi</div>
		  	<div class="panel-body text-center">Apakah Anda ingin memesan tiket pulang sekaligus?</div>
		  	<div class="panel-footer">
		  		<div class="text-center">
		  			<a class="btn btn-default btn-sm" href="<?php echo BASE_ADDRESS.'index.php/?m=pemesanan_tiket&c=doAddTiketKembali&a=do';?>" id="btn-no-tiket-kembali"><img src="<?php echo BASE_ADDRESS.'icon/delete.png';?>"> Tidak</a>
		  			<button class="btn btn-primary btn-sm" id="btn-tiket-kembali"><img src="<?php echo BASE_ADDRESS.'icon/answer.png';?>"> ya</button>
		  		</div>
		  	</div>
		</div>
		<form method="post" action="<?php echo BASE_URL.'?m=pemesanan_tiket&c=doAddTiketKembali&a=do';?>" class="form-horizontal" style="display:none;" id="form-pemesanan-tiket">
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Pulang *</label>
				<div class="col-sm-3">
					<input type="hidden" name="id_detail_pembelian_tiket[]" value="<?php echo $id_detail_pembelian_tiket;?>">
					<input type="hidden" name="id_tiket_berangkat" value="<?php echo $_SESSION['id_pembelian_tiket'];?>">
					<input type="hidden" name="id_penumpang[]" class="form-control input-sm" value="0" required>
					<input type="text" name="tanggal_pemberangkatan" class="form-control input-sm datepicker tanggal_pemberangkatan" value="<?php echo $tanggal_pemberangkatan;?>" required>
				</div>
				<div class="col-sm-1">
					<img id="imgLoaderTglPemberangkatan" class="imgLoaderTglPemberangkatan pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Asal *</label>
				<div class="col-sm-4">
					<select name="id_kota_asal" class="form-control input-sm id_kota_asal" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaAsal())){
							foreach ($model->getKotaAsal() as $key => $value) {
								if ($value['id_kota'] == $id_kota_asal){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_asal'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_asal'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Tujuan *</label>
				<div class="col-sm-4">
					<select name="id_kota_tujuan" class="form-control input-sm id_kota_tujuan" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaTujuan())){
							foreach ($model->getKotaTujuan() as $key => $value) {
								if ($value['id_kota'] == $id_kota_tujuan){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Pemberangkatan *</label>
				<div class="col-sm-9">
					<select name="id_pemberangkatan" class="form-control input-sm id_pemberangkatan" required>
						<option value="">Pilih Pemberangkatan</option>
						<?php
						if (!empty($model->getPemberangkatan($tanggal_pemberangkatan))){
							foreach ($model->getPemberangkatan($tanggal_pemberangkatan) as $key => $value) {
								if ($value['id_pemberangkatan'] == $id_pemberangkatan){
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'" selected>'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'">'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Harga Tiket *</label>
				<div class="col-sm-4">
					<label class="control-label label_sub_total" id="sub_total"><?php echo $sub_total;?></label>
				</div>
			</div>
			<hr>
			<div id="form-item-pemesanan">
				<?php
				if (!empty($detail_pemesanan)){
					foreach ($detail_pemesanan as $key => $value) {
						?>
						<table class="table table-bordered table-striped form-sub-item-pemesanan">
							<?php
							if ($key > 0){
								$show = '';
							}else{
								$show = 'hide';
							}
							?>
							<tr class="container-btn-remove <?php echo $show;?>">
								<td colspan="4" class="text-right">
									<a href="#" class="btn btn-sm btn-default btn-remove-form" title="hapus item pemesanan"><i class="fa fa-trash-o"></i></a>
								</td>
							</tr>
							<tr>
								<td>
									<label class="text-green">Nomer Kursi *</label>
								</td>
								<td>
									<div class="col-sm-11">
										<div class="input-group">
											<input type="hidden" name="id_no_kursi[]" class="id_no_kursi">
									      	<input type="text" name="no_kursi[]" class="form-control no_kursi" placeholder="Pilih No Kursi" readonly>
									      	<span class="input-group-btn">
									        	<button class="btn btn-default btn-cek-no-kursi" type="button" data-toggle="modal" data-target="#myModal">Cek</button>
									      	</span>
									    </div><!-- /input-group -->
									</div>
								</td>
								<td>
									<label class="text-green">Atas Nama *</label>
								</td>
								<td>
									<div class="col-sm-11">
										<input type="text" name="atas_nama[]" class="form-control input-sm atas_nama" value="<?php echo $value['atas_nama'];?>" required>
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="text-right">
									<input type="hidden" name="sub_total[]" class="sub_total" value="">
									<a href="#" class="btn btn-sm btn-default btn-add-form" title="tambah item pemesanan"><i class="fa fa-plus"></i></a>
								</td>
							</tr>
						</table>
						<?php
					}
				}else{
					?>
					<table class="table table-bordered table-striped form-sub-item-pemesanan">
						<tr class="container-btn-remove hide">
							<td colspan="4" class="text-right">
								<a href="#" class="btn btn-sm btn-default btn-remove-form" title="hapus item pemesanan"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								<label class="text-green">Nomer Kursi *</label>
							</td>
							<td>
								<div class="col-sm-11">
									<div class="input-group">
										<input type="hidden" name="id_no_kursi[]" class="id_no_kursi">
								      	<input type="text" name="no_kursi[]" class="form-control no_kursi" placeholder="Pilih No Kursi" readonly>
								      	<span class="input-group-btn">
								        	<button class="btn btn-default btn-cek-no-kursi" type="button" data-toggle="modal" data-target="#myModal">Cek</button>
								      	</span>
								    </div><!-- /input-group -->
								</div>
							</td>
							<td>
								<label class="text-green">Atas Nama *</label>
							</td>
							<td>
								<div class="col-sm-11">
									<input type="text" name="atas_nama[]" class="form-control input-sm atas_nama" value="<?php echo $atas_nama;?>" required>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-right">
								<input type="hidden" name="sub_total[]" class="sub_total" value="<?php echo $sub_total;?>">
								<a href="#" class="btn btn-sm btn-default btn-add-form" title="tambah item pemesanan"><i class="fa fa-plus"></i></a>
							</td>
						</tr>
					</table>
					<?php
				}
				?>
			</div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-5">
					<input type="hidden" name="total" class="total" value="0">
					<label class="text-success label-total">Total Bayar: Rp.0,-</label>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="text-center">
					<button type="button" name="save" id="btn-cancel" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/delete.png';?>"> Batal</button>
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih No. Kursi</h4>
      </div>
      <div class="modal-body" id="container-no-kursi"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
	var formItemPemesanan = '<table class="table table-bordered table-striped form-sub-item-pemesanan">'+
		$('table.form-sub-item-pemesanan:first').html()+
		'</table>';
	var idNoKursiUsed = [];
	var invoker;
	var currentHarga = 0;

	$(document).on('click', '.btn-add-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$('#form-item-pemesanan').append(formItemPemesanan);
		$('table.form-sub-item-pemesanan:last .container-btn-remove').removeClass('hide');
		$('table.form-sub-item-pemesanan:last .atas_nama').val('');
		init();
	});

	$(document).on('click', '.btn-remove-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).closest('table.form-sub-item-pemesanan').remove();
		init();
	});

	function NoKursiUsed(){
		$('select.id_no_kursi').each(function(index, value){
			var val = $(this).val();
			if (val != ''){
				idNoKursiUsed.push(val);
			}
		});
		return idNoKursiUsed;
	}

	function init(){
		var countItem = $('table.form-sub-item-pemesanan').length;
		var _total = currentHarga * countItem;
		$('input.total').val(_total);
        $('.label-total').html('Total Bayar Rp. '+_total+',-');
		$('.label_sub_total').html(currentHarga);
        $('input.sub_total').val(currentHarga);
	}

	function cekTanggal(callback){
		var imgLoader = $('.imgLoaderTglPemberangkatan');
		$.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getTanggalPembelianJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val()
            },
            success : function(response){
            	imgLoader.hide();
                if (response.success){
               		if (typeof callback == 'function'){
               			callback;
               		}
                }else{
                	$.notify(response.message, 'warn');
                	$('select.id_kota_asal, select.id_kota_tujuan').val('');
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan', 'error');
            }
        });
	}

	// @return void
	function _getPemberangkatan(selector){
		var opt = '<option value="">Pilih Pemberangkatan</option>';
		var imgLoader = $('.imgLoaderTglPemberangkatan');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatanJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val(),
                id_kota_asal: $('.id_kota_asal').val(),
                id_kota_tujuan: $('.id_kota_tujuan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        opt += '<option value="'+v['id_pemberangkatan']+'" data-id-armada="'+v['id_armada']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                    });
                    $('select.id_pemberangkatan').html(opt);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan', 'error');
            }
        });
		return;
	}

	$(document).ready(function(){
		$('#btn-tiket-kembali').click(function(){
			$('form#form-pemesanan-tiket').fadeIn();
			$('#confirm-tiket-kembali').fadeOut();
		});
		$('#btn-no-tiket-kembali, #btn-cancel').click(function(){
			$('form#form-pemesanan-tiket').fadeOut();
			$('#confirm-tiket-kembali').fadeIn();
		});
		$('input.tanggal_pemberangkatan').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			onSelect: function(dateText, inst) {
				alert();
			}
		}).on('changeDate', function(ev){
			var selector = $(this);
			var date1 = new Date($(this).datepicker('getDate'));
			var date2 = new Date();
			var diffDays = dateDiffInDays(date2, date1);
			if (diffDays < 0){
				selector.val('');
				$.notify("Tanggal sudah kadaluarsa", "warn");
				$('select.id_pemberangkatan').html('');
			}else
			if ($('.id_kota_asal').val() == '' || $('.id_kota_tujuan').val() == ''){
				cekTanggal();
			}else
			if ($('.id_kota_asal').val() != '' && $('.id_kota_tujuan').val() != ''){
				cekTanggal(_getPemberangkatan(selector));
			}
		});

		$('select.id_kota_asal, select.id_kota_tujuan').change(function(){
			if ($(this).val() != ''){
				var selector = $(this);
				cekTanggal(_getPemberangkatan(selector));
			}
		});
	});

	var _MS_PER_DAY = 1000 * 60 * 60 * 24;

	// a and b are javascript Date objects
	function dateDiffInDays(a, b) {
	  // Discard the time and time-zone information.
	  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}

	$(document).on('change', 'select.id_pemberangkatan', function(e){
		e.stopImmediatePropagation();
		var selector = $(this);
        var id_pemberangkatan = $(this).val();
        var imgLoader = $('.imgLoaderIdPemberangkatan');
        var optionNoKursi = '<option value="">Pilih Nomer Kursi</option>';
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursiJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $('.label_sub_total').html(response.harga);
                    $('input.sub_total').val(response.harga);
                    currentHarga = response.harga;
                    var _total = 0;
                    $('input.sub_total').each(function(i, v){
                    	_total = _total + new Number($(this).val());
                    });
                    $('input.total').val(_total);
                    $('.label-total').html('Total Bayar Rp. '+_total+',-');
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data nomer kursi kosong', 'error');
            }
        });
    });

	$('.btn-cek-no-kursi').click(function(){
		if ($('input.tanggal_pemberangkatan').val() == ''){
			$.notify('Pilih Tanggal Pemberangkatan terlebih dulu', 'error');
			return false;
		}else
		if ($('select.id_pemberangkatan').val() == ''){
			$.notify('Pilih Pemberangkatan terlebih dulu', 'error');
			return false;
		}else{
			return true;
		}
		return false;
	});

	$('#myModal').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		var _no_kursi_used = [];
		var _id_kota_asal_all = [];
		var _id_kota_tujuan_all = [];
		var _id_pemberangkatan_all = [];
		var _id_armada_all = [];

		$.each($('input.id_no_kursi'), function(i, v){
			if ($(this).val() != ''){
				_no_kursi_used.push($(this).val());
			}			
		});

		$.each($('.id_kota_asal'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_asal_all.push($(this).val());
			}
		});

		$.each($('.id_kota_tujuan'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_tujuan_all.push($(this).val());
			}
		});

		$.each($('.id_pemberangkatan'), function(i, v){
			if ($(this).val() != ''){
				_id_pemberangkatan_all.push($(this).val());
				_id_armada_all.push($(this).find('option:selected').attr('data-id-armada'));
			}
		});

	    $.ajax({
	    	url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursi&a=do';?>",
	    	type : "post",
	    	dataType : 'html',
	    	data : {
	    		id_pemberangkatan : $('select.id_pemberangkatan').val(),
	    		tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val(),
	    		no_kursi_terpakai : _no_kursi_used,
	    		id_kota_asal: $('select.id_kota_asal').val(),
	    		id_kota_tujuan: $('select.id_kota_tujuan').val(),
	    		id_pemberangkatan_all : _id_pemberangkatan_all,
	    		id_kota_asal_all : _id_kota_asal_all,
	    		id_kota_tujuan_all : _id_kota_tujuan_all,
	    		id_armada_all : _id_armada_all
	    	},
	    	success : function(response){
	    		$('#container-no-kursi').html(response);
	    	},
	    	error : function(){
	    		$.notify('Terjadi kesalahan, coba ulangi kembali');
	    		$('#myModal').modal('hide');
	    	}
	    });
	});
</script>