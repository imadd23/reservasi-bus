<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$id_kota_asal = $_POST['id_kota_asal']; 
$id_kota_tujuan = $_POST['id_kota_tujuan'];
$pemberangkatan = $model->getPemberangkatan($tanggal_pemberangkatan, $id_kota_asal, $id_kota_tujuan);
?>
<div class="row">
	<div class="col-sm-12">
	    <div class="table-responsive">
	    	<table class="table table-stripped table-bordered">
	    		<thead>
	    			<tr>
	    				<th></th>
	    				<th class="text-left">Nama Pemberangkatan</th>
	    				<th class="text-left">Kota Asal</th>
	    				<th class="text-left">Kota Tujuan</th>
	    				<th class="text-left">Hari</th>
	    				<th class="text-left">Jam Berangkat</th>
	    				<th class="text-left">Armada</th>
	    				<th class="text-right">Harga</th>
	    			</tr>
	    		</thead>
	    		<tbody>
	    			<?php
	    			if (!empty($pemberangkatan)){
	    				foreach ($pemberangkatan as $key => $value) {
	    					?>
	    					<tr>
	    						<td class="text-center">
	    							<button class="btn btn-sm btn-flat btn-default" type="button" data-id-pemberangkatan="<?php echo $value['id_detail_pemberangkatan'];?>" data-label="">
	    								<i class="fa fa-check"></i>
	    							</button>
	    						</td>
	    						<td><?php echo $value['nama_pemberangkatan'];?></td>
	    						<td><?php echo $value['kota_asal'];?></td>
	    						<td><?php echo $value['kota_tujuan'];?></td>
	    						<td><?php echo $value['hari'];?></td>
	    						<td><?php echo $value['jam_berangkat'];?></td>
	    						<td><?php echo $value['nama_armada'];?></td>
	    						<td class="text-right"><?php echo 'Rp. '.number_format($value['harga'], 0, 0, '.').',-';?></td>
	    					</tr>
	    					<?php
	    				}
	    			}else{
	    				?>
	    				<tr>
	    					<td class="text-center" colspan="8"><i>Pemberangkatan tidak ditemukan</i></td>
	    				</tr>
	    				<?php
	    			}
	    			?>
	    		</tbody>
	    	</table>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-no-kursi').click(function(){
			var no_kursi = $(this).attr('data-no-kursi');
			var id_no_kursi = $(this).attr('data-id-no-kursi');
			$('#myModal').modal('hide');
			invoker.closest('table').find('input.no_kursi').val(no_kursi);
			invoker.closest('table').find('input.id_no_kursi').val(id_no_kursi);
		});

		$('.btn-no-kursi-used').click(function(){
			$.notify('Maaf, kursi ini sudah terisi penumpang', 'error');
		});
	});
</script>