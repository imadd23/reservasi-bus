<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$id_pemberangkatan = $_POST['id_pemberangkatan'];
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$id_kota_asal = $_POST['id_kota_asal']; 
$id_kota_tujuan = $_POST['id_kota_tujuan'];
$arrayNoKursiUsed = $model->getNoKursiUsed($id_pemberangkatan, $tanggal_pemberangkatan);
$harga = $model->getHarga($id_pemberangkatan);
$harga = (empty($harga)) ? 0 : $harga[0]['harga'];
$query = mysql_query(" SELECT * FROM armada WHERE id_armada = (SELECT id_armada FROM pemberangkatan WHERE id_pemberangkatan = '$id_pemberangkatan' LIMIT 1)");
$a = mysql_fetch_object($query);
if ($a){
    $id_armada = $a->id_armada;
    $nama_armada = $a->nama_armada;
}else{
    $id_armada = '';
    $nama_armada = '';
}
$dataNoKursi = $model->getSusunanNoKursi($id_armada);
$idPemberangkatanAll = $_POST['id_pemberangkatan_all'];
$idArmadaAll = $_POST['id_armada_all'];
$noKursiUsed = !empty($_POST['no_kursi_terpakai']) ? $_POST['no_kursi_terpakai'] : array();

if (!empty($arrayNoKursiUsed)){
	foreach ($arrayNoKursiUsed as $key => $value) {
		$noKursiUsed[] = $value['id_no_kursi'];
	}
}
?>
<div class="row">
	<div class="col-sm-12">
	    <div class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Nama armada </label>
	            <div class="col-sm-9">
	                <input type="text" value="<?php echo $nama_armada;?>" disabled>
	                <input type="hidden" name="id_armada" value="<?php echo $id_armada;?>">
	            </div>            
	        </div>
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Nomer kursi *</label>
	            <div class="col-sm-6">
	                <table class="table table-bordered">
	                    <tr>
	                        <td colspan="5" class="bg-warning">Pintu<span class="pull-right">Supir</span></td>
	                    </tr>
	                    <?php
	                        foreach ($dataNoKursi as $key => $value) {
	                            if ($key == 10){
	                                ?>
	                                <tr>
	                                    <td colspan="3" class="bg-warning">Pintu</td>
	                                    <?php
	                                    foreach ($value as $k => $v) {
	                                        if (!empty($v) && !in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi btn-sm btn-info" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else
	                                        if (!empty($v) && in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi-used btn-sm btn-danger" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else{
	                                            ?>
	                                            <td></td>
	                                            <?php
	                                        }                                        
	                                    }
	                                    ?>
	                                </tr>
	                                <?php
	                            }else
	                            if ($key == 11){
	                                ?>
	                                <tr>
	                                    <td colspan="2" class="text-center">TOILET</td>
	                                    <?php
	                                    foreach ($value as $k => $v) {
	                                        if (!empty($v) && !in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi btn-sm btn-info" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else
	                                        if (!empty($v) && in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi-used btn-sm btn-danger" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else{
	                                            ?>
	                                            <td></td>
	                                            <?php
	                                        }                                        
	                                    }
	                                    ?>
	                                </tr>
	                                <?php
	                            }else{
	                                ?>
	                                <tr>
	                                    <?php
	                                    foreach ($value as $k => $v) {
	                                        if ($k == 2){
	                                            ?>
	                                            <td width="20%" class="bg-warning"></td>
	                                            <?php
	                                        }
	                                        if (!empty($v) && !in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi btn-sm btn-info" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else
	                                        if (!empty($v) && in_array($v['id_no_kursi'], $noKursiUsed)){
	                                            ?>
	                                            <td>
	                                                <button class="btn btn-no-kursi-used btn-sm btn-danger" name="no_kursi[]" data-id-no-kursi="<?php echo $v['id_no_kursi'];?>" data-no-kursi="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;"><?php echo $v['no_kursi'];?></button>
	                                            </td>
	                                            <?php
	                                        }else{
	                                            ?>
	                                            <td></td>
	                                            <?php
	                                        }                                        
	                                    }
	                                    ?>
	                                </tr>
	                                <?php
	                            }                            
	                        }
	                    ?>
	                </table>
	            </div>
	        </div>
	    </div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.btn-no-kursi').click(function(){
			var no_kursi = $(this).attr('data-no-kursi');
			var id_no_kursi = $(this).attr('data-id-no-kursi');
			$('#myModal').modal('hide');
			invoker.closest('table').find('input.no_kursi').val(no_kursi);
			invoker.closest('table').find('input.id_no_kursi').val(id_no_kursi);
		});

		$('.btn-no-kursi-used').click(function(){
			$.notify('Maaf, kursi ini sudah terisi penumpang', 'error');
		});
	});
</script>