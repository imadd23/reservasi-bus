<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$id_kota_asal = $_POST['id_kota_asal'];
$id_kota_tujuan = $_POST['id_kota_tujuan'];
$data = $model->getPemberangkatan($tanggal_pemberangkatan, $id_kota_asal, $id_kota_tujuan);
echo json_encode(array('success' => true, 'data' => $data));
?>