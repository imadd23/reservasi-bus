<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$waktu_pembatalan = $model->getSettingPembelianTiket();
	$message = '<div class="alert alert-success">Data pembelian berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data pembelian berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}

$tanggal_pemberangkatan = isset($_GET['tanggal_pemberangkatan']) ? date('d-m-Y', strtotime($_GET['tanggal_pemberangkatan'])) : '';
$id_pemberangkatan = isset($_GET['id_pemberangkatan']) ? $_GET['id_pemberangkatan'] : '';
$id_no_kursi = isset($_GET['id_no_kursi']) ? $_GET['id_no_kursi'] : '';
$atas_nama = isset($_GET['atas_nama']) ? $_GET['atas_nama'] : '';
$id_penumpang = isset($_GET['id_penumpang']) ? $_GET['id_penumpang'] : '';
$sub_total = isset($_GET['sub_total']) ? $_GET['sub_total'] : '';
$no_kursi = isset($_GET['no_kursi']) ? $_GET['no_kursi'] : 'Pilih Nomer Kursi';
$id_detail_pembelian_tiket = isset($_GET['id_detail_pembelian_tiket']) ? $_GET['id_detail_pembelian_tiket'] : '';
$nama_armada = isset($_GET['nama_armada']) ? $_GET['nama_armada'] : '';
$kota_asal = isset($_GET['kota_asal']) ? $_GET['kota_asal'] : '';
$id_kota_asal = isset($_GET['kota_asal']) ? $_GET['kota_asal'] : '';
$kota_tujuan = isset($_GET['kota_tujuan']) ? $_GET['kota_tujuan'] : '';
$id_kota_tujuan = isset($_GET['id_kota_tujuan']) ? $_GET['id_kota_tujuan'] : '';
$id_armada = isset($_GET['id_armada']) ? $_GET['id_armada'] : '';
$_pemberangkatan = !empty($id_pemberangkatan) ? '<option value="'.$id_pemberangkatan.'" data-id-armada="'.$id_armada.'" selected>'.$nama_armada.' | '.$kota_asal.' - '.$kota_tujuan.'</option>' : '<option>Pilih Pemberangkatan</option>';
?>
<input type="hidden" name="id_pembelian_tiket[]" value="<?php echo $id_pembelian_tiket;?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-12">
		<h3 class="text-center">Pemesanan Tiket</h3>
		<?php echo $message;?>
		<hr>
		<form method="post" action="<?php echo BASE_URL.'?m=pemesanan_tiket&c=doAdd&a=do';?>" class="form-horizontal" id="form-pemesanan-tiket">
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Pemberangkatan *</label>
				<div class="col-sm-3">
					<input type="hidden" name="id_detail_pembelian_tiket[]" value="<?php echo $id_detail_pembelian_tiket;?>">
					<input type="hidden" name="id_penumpang[]" class="form-control input-sm" value="0" required>
					<input type="text" name="tanggal_pemberangkatan" class="form-control input-sm datepicker tanggal_pemberangkatan" value="<?php echo $tanggal_pemberangkatan;?>" required>
				</div>
				<div class="col-sm-1">
					<img id="imgLoaderTglPemberangkatan" class="imgLoaderTglPemberangkatan pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Asal *</label>
				<div class="col-sm-4">
					<select name="id_kota_asal" class="form-control input-sm id_kota_asal" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaAsal())){
							foreach ($model->getKotaAsal() as $key => $value) {
								if ($value['id_kota'] == $id_kota_asal){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_asal'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_asal'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Tujuan *</label>
				<div class="col-sm-4">
					<select name="id_kota_tujuan" class="form-control input-sm id_kota_tujuan" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaTujuan())){
							foreach ($model->getKotaTujuan() as $key => $value) {
								if ($value['id_kota'] == $id_kota_tujuan){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Pemberangkatan *</label>
				<div class="col-sm-5">
					<!-- <div class="input-group">
				      <span class="input-group-btn">
				        <button class="btn btn-secondary" type="button" data-toggle="modal" data-target="#modalPemberangkatan"><i class="fa fa-search"></i></button>
				      </span>
				      <input type="text" class="form-control" placeholder="" readonly>
				    </div> -->
					<select name="id_pemberangkatan" class="form-control input-sm id_pemberangkatan" required>
						<option value="">Pilih Pemberangkatan</option>
						<?php
						if (!empty($model->getPemberangkatan($tanggal_pemberangkatan))){
							foreach ($model->getPemberangkatan($tanggal_pemberangkatan) as $key => $value) {
								if ($value['id_pemberangkatan'] == $id_pemberangkatan){
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'" selected>'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'">'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Harga Tiket</label>
				<div class="col-sm-4">
					<label class="control-label label_sub_total" id="sub_total"><?php echo $sub_total;?></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Armada</label>
				<div class="col-sm-4">
					<label class="control-label label_armada_berangkat"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jam Berangkat</label>
				<div class="col-sm-4">
					<label class="control-label label_jam_berangkat"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Jam Sampai</label>
				<div class="col-sm-4">
					<label class="control-label label_jam_sampai"></label>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Pulang Pergi</label>
				<div class="col-sm-4">
					<label class="radio-inline">
				      <input type="radio" value="1" name="pp">Ya
				    </label>
				    <label class="radio-inline">
				      <input type="radio" value="0" name="pp">Tidak
				    </label>
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Tanggal Pulang *</label>
				<div class="col-sm-3">
					<input type="text" name="tanggal_pulang" class="form-control input-sm datepicker tanggal_pulang" value="">
				</div>
				<div class="col-sm-1">
					<img id="imgLoaderTglPemberangkatanPulang" class="imgLoaderTglPemberangkatanPulang pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Pemberangkatan Pulang*</label>
				<div class="col-sm-5">
					<select name="id_pemberangkatan_pulang" class="form-control input-sm id_pemberangkatan_pulang">
						<option value="">Pilih Pemberangkatan</option>
					</select>
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Harga Tiket Pulang</label>
				<div class="col-sm-4">
					<label class="control-label label_sub_total_pulang" id="sub_total_pulang"><?php echo $sub_total;?></label>
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Armada</label>
				<div class="col-sm-4">
					<label class="control-label label_armada_pulang"></label>
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Jam Berangkat Pulang</label>
				<div class="col-sm-4">
					<label class="control-label label_jam_berangkat_pulang"></label>
				</div>
			</div>
			<div class="form-group container-pulang" style="display:none;">
				<label class="col-sm-3 control-label">Jam Sampai Pulang</label>
				<div class="col-sm-4">
					<label class="control-label label_jam_sampai_pulang"></label>
				</div>
			</div>
			<hr>
			<div id="form-item-pemesanan">
				<table class="table table-bordered table-stiped form-sub-item-pemesanan">
					<tr class="container-btn-remove hide">
						<td colspan="4" class="text-right">
							<a href="#" class="btn btn-sm btn-default btn-remove-form" title="hapus item pemesanan"><i class="fa fa-trash-o"></i></a>
						</td>
					</tr>
					<tr>
						<td>
							<label class="text-green">Nomer Kursi *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<div class="input-group">
									<input type="hidden" name="id_no_kursi[]" class="id_no_kursi">
							      	<input type="text" name="no_kursi[]" class="form-control no_kursi" placeholder="Pilih No Kursi" readonly>
							      	<span class="input-group-btn">
							        	<button class="btn btn-default btn-cek-no-kursi" type="button" data-toggle="modal" data-target="#myModal">Cek</button>
							      	</span>
							    </div><!-- /input-group -->
							</div>
						</td>
						<td>
							<label class="text-green">Atas Nama *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<input type="text" name="atas_nama[]" class="form-control input-sm atas_nama" value="<?php echo $atas_nama;?>" required>
							</div>
						</td>
					</tr>
					<tr>
						<td class="hide no-kursi-pulang-container">
							<label class="text-green">Nomer Kursi Pulang*</label>
						</td>
						<td class="hide no-kursi-pulang-container">
							<div class="col-sm-11">
								<div class="input-group">
									<input type="hidden" name="id_no_kursi_pulang[]" class="id_no_kursi_pulang">
							      	<input type="text" name="no_kursi_pulang[]" class="form-control no_kursi_pulang" placeholder="Pilih No Kursi" readonly>
							      	<span class="input-group-btn">
							        	<button class="btn btn-default btn-cek-no-kursi-pulang" type="button" data-toggle="modal" data-target="#myModalPulang">Cek</button>
							      	</span>
							    </div><!-- /input-group -->
							</div>
						</td>
						<td colspan="4" class="text-right no-kursi-pulang-colspan">
							<input type="hidden" name="sub_total[]" class="sub_total" value="<?php echo $sub_total;?>">
							<input type="hidden" name="sub_total_pulang[]" class="sub_total_pulang" value="">
							<a href="#" class="btn btn-sm btn-default btn-add-form" title="tambah item pemesanan"><i class="fa fa-plus"></i></a>
						</td>
					</tr>
				</table>
			</div>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-5">
					<input type="hidden" name="total" class="total" value="0">
					<input type="hidden" name="total_pulang" class="total_pulang" value="0">
					<label class="text-success label-total">Total Bayar: Rp.0,-</label>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-5">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih No. Kursi</h4>
      </div>
      <div class="modal-body" id="container-no-kursi"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<div id="myModalPulang" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih No. Kursi Pulang</h4>
      </div>
      <div class="modal-body" id="container-no-kursi-pulang"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal pemberangkatan -->
<div id="modalPemberangkatan" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih Pemberangkatan</h4>
      </div>
      <div class="modal-body" id="container-modal-pemberangkatan"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<!-- Modal armada -->
<div id="modalDetailArmada" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Armada</h4>
      </div>
      <div class="modal-body" id="container-modal-detail-armada"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
	var formItemPemesanan = $('table.form-sub-item-pemesanan:first').closest('#form-item-pemesanan').html();
	var idNoKursiUsed = [];
	var invoker;
	var currentHarga = 0;
	var currentHargaPulang = 0;
	var href;

	$(document).on('click', '.btn-add-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$('#form-item-pemesanan').append(formItemPemesanan);
		$('table.form-sub-item-pemesanan:last .container-btn-remove').removeClass('hide');
		init();
	});


	$(document).on('click', '.btn-remove-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).closest('table.form-sub-item-pemesanan').remove();
		init();
	});

	$(document).on('click', 'a.btn-detail-armada', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		href = $(this).attr('href');
		$('#modalDetailArmada').modal('show');
	});

  	$('#modalDetailArmada').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		$.ajax({
		  url : href,
		  type : "post",
		  dataType : 'html',
		  data : {},
		  success : function(response){
		      $('#container-modal-detail-armada').html(response);
		  },
		  error : function(){
		      $.notify('Terjadi kesalahan, coba ulangi kembali', 'warn');
		      $('#modalDetailArmada').modal('hide');
		  }
		});
  	});
  	
	function NoKursiUsed(){
		$('select.id_no_kursi').each(function(index, value){
			var val = $(this).val();
			if (val != ''){
				idNoKursiUsed.push(val);
			}
		});
		return idNoKursiUsed;
	}

	function init(){
		var countItem = $('table.form-sub-item-pemesanan').length;
		var _total = currentHarga * countItem;
		var _totalPulang = currentHargaPulang * countItem;
		var total = (_total + _totalPulang);

		if ($('input[name=pp]:checked').val() == '0'){
			total = _total;
		}
		$('input.total').val(total);
        $('.label-total').html('Total Bayar Rp. '+total+',-');
		$('.label_sub_total').html(currentHarga);
        $('input.sub_total').val(currentHarga);
        $('.label_sub_total_pulang').html(currentHargaPulang);
        $('input.sub_total_pulang').val(currentHargaPulang);
	}

	// @return void
	function _getPemberangkatan(selector){
		var opt = '<option value="">Pilih Pemberangkatan</option>';
        var imgLoader = $('.imgLoaderTglPemberangkatan');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatanJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val(),
                id_kota_asal: $('.id_kota_asal').val(),
                id_kota_tujuan: $('.id_kota_tujuan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        opt += '<option value="'+v['id_pemberangkatan']+'" data-id-armada="'+v['id_armada']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                    });
                    $('select.id_pemberangkatan').html(opt);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan', 'error');
            }
        });
		return;
	}

	// @return void
	function _getPemberangkatanPulang(selector){
		var opt = '<option value="">Pilih Pemberangkatan</option>';
        var imgLoader = $('.imgLoaderTglPemberangkatanPulang');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatanJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : $('.tanggal_pulang').val(),
                id_kota_asal: $('.id_kota_tujuan').val(),
                id_kota_tujuan: $('.id_kota_asal').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        opt += '<option value="'+v['id_pemberangkatan']+'" data-id-armada="'+v['id_armada']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                    });
                    $('select.id_pemberangkatan_pulang').html(opt);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan pulang', 'error');
            }
        });
		return;
	}

	$(document).ready(function(){
		// init();
		$('input.tanggal_pemberangkatan').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			onSelect: function(dateText, inst) {
				alert();
			}
		}).on('changeDate', function(ev){
			var selector = $(this);
			var date1 = new Date($(this).datepicker('getDate'));
			var date2 = new Date();
			var diffDays = dateDiffInDays(date2, date1);
			if (diffDays < 0){
				selector.val('');
				$.notify("Tanggal sudah kadaluarsa", "warn");
				$('select.id_pemberangkatan').html('');
			}else
			if ($('.id_kota_asal').val() != '' && $('.id_kota_tujuan').val() != ''){
				_getPemberangkatan(selector);
			}
		});

		$('input.tanggal_pulang').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			onSelect: function(dateText, inst) {
				alert();
			}
		}).on('changeDate', function(ev){
			var selector = $(this);
			var date1 = new Date($(this).datepicker('getDate'));
			if (date1 != '' && $('.id_kota_asal').val() != '' && $('.id_kota_tujuan').val() != ''){
				_getPemberangkatanPulang(selector);
			}
		});

		$('select.id_kota_asal, select.id_kota_tujuan').change(function(){
			if ($(this).val() != ''){
				var selector = $(this);
				_getPemberangkatan(selector);
			}
		});

		$('input[name=pp]').change(function(){
			if ($(this).val() == '1'){
				$('.container-pulang').show();
				$('.no-kursi-pulang-container').removeClass('hide');
				$('.no-kursi-pulang-colspan').attr('colspan', '2');
				formItemPemesanan = $('table.form-sub-item-pemesanan:first').closest('#form-item-pemesanan').html();
			}else{
				$('.container-pulang').hide();
				$('.no-kursi-pulang-container').addClass('hide');
				$('.no-kursi-pulang-colspan').attr('colspan', '4');
				formItemPemesanan = $('table.form-sub-item-pemesanan:first').closest('#form-item-pemesanan').html();
			}
			init();
		});
	});

	var _MS_PER_DAY = 1000 * 60 * 60 * 24;

	// a and b are javascript Date objects
	function dateDiffInDays(a, b) {
	  // Discard the time and time-zone information.
	  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}

	$(document).on('change', 'select.id_pemberangkatan', function(e){
		e.stopImmediatePropagation();
		var selector = $(this);
        var id_pemberangkatan = $(this).val();
        var imgLoader = $('.imgLoaderIdPemberangkatan');
        var optionNoKursi = '<option value="">Pilih Nomer Kursi</option>';
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursiJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $('.label_sub_total').html(response.harga);
                    $('input.sub_total').val(response.harga);
                    currentHarga = Number(response.harga);
                    var _total = 0;
                    $('input.sub_total').each(function(i, v){
                    	_total = _total + new Number($(this).val());
                    });
                    $('input.total').val(_total);
                    $('.label-total').html('Total Bayar Rp. '+_total+',-');
                    $('.label_jam_berangkat').html(response.jam);
                    $('.label_jam_sampai').html(response.jam_sampai);
                    $('.label_armada_berangkat').html(response.nama_armada);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data nomer kursi kosong', 'error');
            }
        });
    });

	$(document).on('change', 'select.id_pemberangkatan_pulang', function(e){
		e.stopImmediatePropagation();
		var selector = $(this);
        var id_pemberangkatan = $(this).val();
        var imgLoader = $('.imgLoaderIdPemberangkatan');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursiJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : $('input.tanggal_pulang').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $('.label_sub_total_pulang').html(response.harga);
                    $('input.sub_total_pulang').val(response.harga);
                    currentHargaPulang = Number(response.harga);
                    var _total = 0;
                    $('input.sub_total_pulang').each(function(i, v){
                    	_total = _total + new Number($(this).val());
                    });
                    $('input.total_pulang').val(_total);
                    $('.label-total').html('Total Bayar Rp. '+(currentHarga + currentHargaPulang)+',-');
                    $('.label_jam_berangkat_pulang').html(response.jam);
                    $('.label_jam_sampai_pulang').html(response.jam_sampai);
                    $('.label_armada_pulang').html(response.nama_armada);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data nomer kursi kosong', 'error');
            }
        });
    });

	$('.btn-cek-no-kursi').click(function(){
		if ($('input.tanggal_pemberangkatan').val() == ''){
			$.notify('Pilih Tanggal Pemberangkatan terlebih dulu', 'error');
			return false;
		}else
		if ($('select.id_pemberangkatan').val() == ''){
			$.notify('Pilih Pemberangkatan terlebih dulu', 'error');
			return false;
		}else{
			return true;
		}
		return false;
	});

	$('#myModal').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		var _no_kursi_used = [];
		var _id_kota_asal_all = [];
		var _id_kota_tujuan_all = [];
		var _id_pemberangkatan_all = [];
		var _id_armada_all = [];

		$.each($('input.id_no_kursi'), function(i, v){
			if ($(this).val() != ''){
				_no_kursi_used.push($(this).val());
			}			
		});

		$.each($('.id_kota_asal'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_asal_all.push($(this).val());
			}
		});

		$.each($('.id_kota_tujuan'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_tujuan_all.push($(this).val());
			}
		});

		$.each($('.id_pemberangkatan'), function(i, v){
			if ($(this).val() != ''){
				_id_pemberangkatan_all.push($(this).val());
				_id_armada_all.push($(this).find('option:selected').attr('data-id-armada'));
			}
		});

	    $.ajax({
	    	url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursi&a=do';?>",
	    	type : "post",
	    	dataType : 'html',
	    	data : {
	    		id_pemberangkatan : $('select.id_pemberangkatan').val(),
	    		tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val(),
	    		no_kursi_terpakai : _no_kursi_used,
	    		id_kota_asal: $('select.id_kota_asal').val(),
	    		id_kota_tujuan: $('select.id_kota_tujuan').val(),
	    		id_pemberangkatan_all : _id_pemberangkatan_all,
	    		id_kota_asal_all : _id_kota_asal_all,
	    		id_kota_tujuan_all : _id_kota_tujuan_all,
	    		id_armada_all : _id_armada_all
	    	},
	    	success : function(response){
	    		$('#container-no-kursi').html(response);
	    	},
	    	error : function(){
	    		$.notify('Terjadi kesalahan, coba ulangi kembali');
	    		$('#myModal').modal('hide');
	    	}
	    });
	});

	$('#modalPemberangkatan').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		$.post(
			"<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatan&a=do';?>",
			{
				tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val(),
	            id_kota_asal: $('.id_kota_asal').val(),
	            id_kota_tujuan: $('.id_kota_tujuan').val()
			}, function(response){
				$('#container-modal-pemberangkatan').html(response);
			}
		);
	});

	$('#myModalPulang').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		var _no_kursi_used = [];
		var _id_kota_asal_all = [];
		var _id_kota_tujuan_all = [];
		var _id_pemberangkatan_all = [];
		var _id_armada_all = [];

		$.each($('input.id_no_kursi_pulang'), function(i, v){
			if ($(this).val() != ''){
				_no_kursi_used.push($(this).val());
			}			
		});

		$.each($('.id_kota_tujuan'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_asal_all.push($(this).val());
			}
		});

		$.each($('.id_kota_asal'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_tujuan_all.push($(this).val());
			}
		});

		$.each($('.id_pemberangkatan_pulang'), function(i, v){
			if ($(this).val() != ''){
				_id_pemberangkatan_all.push($(this).val());
				_id_armada_all.push($(this).find('option:selected').attr('data-id-armada'));
			}
		});

	    $.ajax({
	    	url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursiPulang&a=do';?>",
	    	type : "post",
	    	dataType : 'html',
	    	data : {
	    		id_pemberangkatan : $('select.id_pemberangkatan_pulang').val(),
	    		tanggal_pemberangkatan : $('input.tanggal_pulang').val(),
	    		no_kursi_terpakai : _no_kursi_used,
	    		id_kota_asal: $('select.id_kota_tujuan').val(),
	    		id_kota_tujuan: $('select.id_kota_asal').val(),
	    		id_pemberangkatan_all : _id_pemberangkatan_all,
	    		id_kota_asal_all : _id_kota_asal_all,
	    		id_kota_tujuan_all : _id_kota_tujuan_all,
	    		id_armada_all : _id_armada_all
	    	},
	    	success : function(response){
	    		$('#container-no-kursi-pulang').html(response);
	    	},
	    	error : function(){
	    		$.notify('Terjadi kesalahan, coba ulangi kembali');
	    		$('#myModalPulang').modal('hide');
	    	}
	    });
	});

	$('#modalPemberangkatan').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		$.post(
			"<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatan&a=do';?>",
			{
				tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val(),
	            id_kota_asal: $('.id_kota_asal').val(),
	            id_kota_tujuan: $('.id_kota_tujuan').val()
			}, function(response){
				$('#container-modal-pemberangkatan').html(response);
			}
		);
	});
</script>