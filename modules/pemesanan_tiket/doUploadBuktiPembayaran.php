<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$id = explode(',', $_POST['id_pembelian_tiket']);
$fileName = $_SESSION['user_id'].'-'.date('YmdHis');
$aTipe = explode('.', $_FILES['bukti']['name']);
$maxKey = max(array_keys($aTipe));
$tipe = $aTipe[$maxKey];
$fileName .= '.'.$tipe;
$dest = ROOTPATH.'/files/bukti_transfer/';
$upload = move_uploaded_file($_FILES['bukti']['tmp_name'], $dest.$fileName);
if ($upload){
	foreach ($id as $key => $value) {
		$upload = $model->doUpdateBuktiPembayaran($value, 'files/bukti_transfer/'.$fileName);
	}
}
if ($upload){
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewKonfirmasiTiket&a=view&statusAdd=1');
}else{
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewKonfirmasiTiket&a=view&statusAdd=0');
}
?>