<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$id_armada = $_GET['ref_id'];
$armada = $model->getDetailArmada($id_armada);
$armada = $armada[0];
$gambar = !empty($armada['gambar']) ? BASE_ADDRESS.'files/armada/'.$armada['gambar'] : BASE_ADDRESS.'images/armada.jpg';
?>
<div class="row">
	<div class="col-sm-12">
	    <div class="form-horizontal">
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Nama Armada </label>
	            <label class="col-sm-9 control-label" style="text-align: left;"><?php echo $armada['nama_armada'];?></label>            
	        </div>
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Nomer Plat</label>
	            <label class="col-sm-9 control-label" style="text-align: left;"><?php echo $armada['no_plat'];?></label>
	        </div>
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Muatan Penumpang</label>
	            <label class="col-sm-9 control-label" style="text-align: left;"><?php echo $armada['muatan_penumpang'];?></label>
	        </div>
	        <div class="form-group">
	            <label class="col-sm-3 control-label">Gambar</label>
	            <div class="col-sm-9">
	            	<div style="width: 450px; height: 300px;">
	            		<img class="img-responsive" src="<?php echo $gambar;?>">
	            	</div>
	            </div>
	        </div>
	    </div>
	</div>
</div>