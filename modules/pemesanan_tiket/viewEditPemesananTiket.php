<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$waktu_pembatalan = $model->getSettingPembelianTiket();
	$message = '<div class="alert alert-success">Data pembelian berhasil diubah, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal diubah, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data pembelian berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}
$arrId = explode(',', str_replace(' ', '', $_GET['ref_id']));
$detailPembelian = array();
if (!empty($arrId)){
	foreach ($arrId as $key => $value) {
		$detailPembelian[] = $model->getdetailTiket($value);
	}
}

$tiketBerangkat = isset($detailPembelian[0]) ? $detailPembelian[0] : array();
$tiketPulang = isset($detailPembelian[1]) ? $detailPembelian[1] : array();

if (!empty($tiketBerangkat)){
	$tanggal_pemberangkatan = date('d-m-Y', strtotime($tiketBerangkat[0]['tanggal_pemberangkatan']));
	$id_pemberangkatan = $tiketBerangkat[0]['id_pemberangkatan'];
	$nama_armada = $tiketBerangkat[0]['nama_armada'];
	$id_kota_asal = $tiketBerangkat[0]['id_kota_asal'];
	$id_kota_tujuan = $tiketBerangkat[0]['id_kota_tujuan'];
	$id_armada = $tiketBerangkat[0]['id_armada'];
	$total_harga = $tiketBerangkat[0]['total_harga'];
	$sub_total = $tiketBerangkat[0]['sub_total'];
	$id_pembelian_tiket = $tiketBerangkat[0]['id_pembelian_tiket'];
}
$id_tiket_pulang = 0;
if (!empty($tiketPulang)){
	$id_tiket_pulang = $tiketPulang[0]['id_pembelian_tiket'];
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-12">
		<h3 class="text-center">Ubah Pemesanan Tiket</h3>
		<?php echo $message;?>
		<hr>
		<form method="post" action="<?php echo BASE_URL.'?m=pemesanan_tiket&c=doEdit&a=do';?>" class="form-horizontal" id="form-pemesanan-tiket">
			<div class="form-group">
				<label class="col-sm-3 control-label">Tanggal Pemberangkatan *</label>
				<div class="col-sm-3">
					<input type="hidden" name="id_pembelian_tiket" value="<?php echo $id_pembelian_tiket;?>">
					<input type="hidden" name="id_tiket_pulang" value="<?php echo $id_tiket_pulang;?>">
					<input type="text" name="tanggal_pemberangkatan" class="form-control input-sm datepicker tanggal_pemberangkatan" value="<?php echo $tanggal_pemberangkatan;?>" required>
				</div>
				<div class="col-sm-1">
					<img id="imgLoaderTglPemberangkatan" class="imgLoaderTglPemberangkatan pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Asal *</label>
				<div class="col-sm-4">
					<select name="id_kota_asal" class="form-control input-sm id_kota_asal" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaAsal())){
							foreach ($model->getKotaAsal() as $key => $value) {
								if ($value['id_kota'] == $id_kota_asal){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_asal'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_asal'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Kota Tujuan *</label>
				<div class="col-sm-4">
					<select name="id_kota_tujuan" class="form-control input-sm id_kota_tujuan" required>
						<option value="">Pilih Kota Tujuan</option>
						<?php
						if (!empty($model->getKotaTujuan())){
							foreach ($model->getKotaTujuan() as $key => $value) {
								if ($value['id_kota'] == $id_kota_tujuan){
									echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Pemberangkatan *</label>
				<div class="col-sm-4">
					<select name="id_pemberangkatan" class="form-control input-sm id_pemberangkatan" required>
						<option value="">Pilih Pemberangkatan</option>
						<?php
						$pemberangkatan = $model->getPemberangkatan($tanggal_pemberangkatan, $id_kota_asal, $id_kota_tujuan);
						if (!empty($pemberangkatan)){
							foreach ($pemberangkatan as $key => $value) {
								if ($value['id_pemberangkatan'] == $id_pemberangkatan){
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'" selected>'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}else{
									echo '<option value="'.$value['id_pemberangkatan'].'" data-id-armada="'.$value['id_armada'].'">'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
								}
							}
						}
						?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label">Harga Tiket *</label>
				<div class="col-sm-4">
					<label class="control-label label_sub_total" id="sub_total"><?php echo $sub_total;?></label>
				</div>
			</div>
			<hr>
			<div id="form-item-pemesanan">
				<?php
				foreach ($tiketBerangkat as $key => $value) {
					if ($key > 0){
						$hide = '';
					}else{
						$hide = 'hide';
					}
					?>
					<table class="table table-bordered table-stiped form-sub-item-pemesanan">
						<tr class="container-btn-remove <?php echo $hide;?>">
							<td colspan="4" class="text-right">
								<a href="#" class="btn btn-sm btn-default btn-remove-form" title="hapus item pemesanan"><i class="fa fa-trash-o"></i></a>
							</td>
						</tr>
						<tr>
							<td>
								<label class="text-green">Nomer Kursi *</label>
							</td>
							<td>
								<div class="col-sm-11">
									<div class="input-group">
										<input type="hidden" name="id_no_kursi[]" value="<?php echo $value['id_no_kursi'];?>" class="id_no_kursi">
								      	<input type="text" name="no_kursi[]" class="form-control no_kursi" value="<?php echo $value['no_kursi'];?>" placeholder="Pilih No Kursi" readonly>
								      	<span class="input-group-btn">
								        	<button class="btn btn-default btn-cek-no-kursi" type="button" data-toggle="modal" data-target="#myModal">Cek</button>
								      	</span>
								    </div><!-- /input-group -->
								</div>
							</td>
							<td>
								<label class="text-green">Atas Nama *</label>
							</td>
							<td>
								<div class="col-sm-11">
									<input type="text" name="atas_nama[]" class="form-control input-sm atas_nama" value="<?php echo $value['atas_nama'];?>" required>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4" class="text-right">
								<input type="hidden" name="sub_total[]" class="sub_total" value="<?php echo $value['sub_total'];?>">
								<a href="#" class="btn btn-sm btn-default btn-add-form" title="tambah item pemesanan"><i class="fa fa-plus"></i></a>
							</td>
						</tr>
					</table>
					<?php
				}
				?>
			</div>
			<div class="form-group">
				<div class="col-sm-12 text-center">
					<input type="hidden" name="total" class="total" value="<?php echo $total_harga;?>">
					<label class="text-success label-total">Total Bayar: Rp.<?php echo number_format($total_harga, 0, 0, '.');?>,-</label>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<div class="col-sm-12 text-center">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih No. Kursi</h4>
      </div>
      <div class="modal-body" id="container-no-kursi"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
	var formItemPemesanan = $('table.form-sub-item-pemesanan:first').closest('#form-item-pemesanan').html();
	var idNoKursiUsed = [];
	var invoker;
	var currentHarga = '<?php echo $sub_total;?>';

	$(document).on('click', '.btn-add-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$('#form-item-pemesanan').append(formItemPemesanan);
		$('table.form-sub-item-pemesanan:last .container-btn-remove').removeClass('hide');
		$('table.form-sub-item-pemesanan:last input.atas_nama').val('');
		$('table.form-sub-item-pemesanan:last input.no_kursi').val('');
		$('table.form-sub-item-pemesanan:last input.id_no_kursi').val('');
		init();
	});

	$(document).on('click', '.btn-remove-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).closest('table.form-sub-item-pemesanan').remove();
		init();
	});

	function NoKursiUsed(){
		$('select.id_no_kursi').each(function(index, value){
			var val = $(this).val();
			if (val != ''){
				idNoKursiUsed.push(val);
			}
		});
		return idNoKursiUsed;
	}

	function init(){
		var countItem = $('table.form-sub-item-pemesanan').length;
		var _total = currentHarga * countItem;
		$('input.total').val(_total);
        $('.label-total').html('Total Bayar Rp. '+_total+',-');
		$('.label_sub_total').html(currentHarga);
        $('input.sub_total').val(currentHarga);
	}

	// @return void
	function _getPemberangkatan(selector){
		var opt = '<option value="">Pilih Pemberangkatan</option>';
        var imgLoader = $('.imgLoaderTglPemberangkatan');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getPemberangkatanJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : $('.tanggal_pemberangkatan').val(),
                id_kota_asal: $('.id_kota_asal').val(),
                id_kota_tujuan: $('.id_kota_tujuan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        opt += '<option value="'+v['id_pemberangkatan']+'" data-id-armada="'+v['id_armada']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                    });
                    $('select.id_pemberangkatan').html(opt);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan', 'error');
            }
        });
		return;
	}

	$(document).ready(function(){
		// init();
		$('input.tanggal_pemberangkatan').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			onSelect: function(dateText, inst) {
				alert();
			}
		}).on('changeDate', function(ev){
			var selector = $(this);
			var date1 = new Date($(this).datepicker('getDate'));
			var date2 = new Date();
			var diffDays = dateDiffInDays(date2, date1);
			if (diffDays < 0){
				selector.val('');
				$.notify("Tanggal sudah kadaluarsa", "warn");
				$('select.id_pemberangkatan').html('');
			}else
			if ($('.id_kota_asal').val() != '' && $('.id_kota_tujuan').val() != ''){
				_getPemberangkatan(selector);
			}
		});

		$('select.id_kota_asal, select.id_kota_tujuan').change(function(){
			if ($(this).val() != ''){
				var selector = $(this);
				_getPemberangkatan(selector);
			}
		});
	});

	var _MS_PER_DAY = 1000 * 60 * 60 * 24;

	// a and b are javascript Date objects
	function dateDiffInDays(a, b) {
	  // Discard the time and time-zone information.
	  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}

	$(document).on('change', 'select.id_pemberangkatan', function(e){
		e.stopImmediatePropagation();
		var selector = $(this);
        var id_pemberangkatan = $(this).val();
        var imgLoader = $('.imgLoaderIdPemberangkatan');
        var optionNoKursi = '<option value="">Pilih Nomer Kursi</option>';
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursiJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                	// var _noKursiUsed = NoKursiUsed();
                 //    $.each(response.data, function(i, v){
                 //    	if ($.inArray(v['id_no_kursi'], _noKursiUsed) < 0){
                 //    		optionNoKursi += '<option value="'+v['id_no_kursi']+'">'+v['no_kursi']+'</option>';
                 //    	}                        
                 //    });
                 //    selector.closest('table').find('select.id_no_kursi').html(optionNoKursi);
                    $('.label_sub_total').html(response.harga);
                    $('input.sub_total').val(response.harga);
                    currentHarga = response.harga;
                    var _total = 0;
                    $('input.sub_total').each(function(i, v){
                    	_total = _total + new Number($(this).val());
                    });
                    $('input.total').val(_total);
                    $('.label-total').html('Total Bayar Rp. '+_total+',-');
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data nomer kursi kosong', 'error');
            }
        });
    });

	$('.btn-cek-no-kursi').click(function(){
		if ($('input.tanggal_pemberangkatan').val() == ''){
			$.notify('Pilih Tanggal Pemberangkatan terlebih dulu', 'error');
			return false;
		}else
		if ($('select.id_pemberangkatan').val() == ''){
			$.notify('Pilih Pemberangkatan terlebih dulu', 'error');
			return false;
		}else{
			return true;
		}
		return false;
	});

	$('#myModal').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		var _no_kursi_used = [];
		var _id_kota_asal_all = [];
		var _id_kota_tujuan_all = [];
		var _id_pemberangkatan_all = [];
		var _id_armada_all = [];

		$.each($('input.id_no_kursi'), function(i, v){
			if ($(this).val() != ''){
				_no_kursi_used.push($(this).val());
			}			
		});

		$.each($('.id_kota_asal'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_asal_all.push($(this).val());
			}
		});

		$.each($('.id_kota_tujuan'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_tujuan_all.push($(this).val());
			}
		});

		$.each($('.id_pemberangkatan'), function(i, v){
			if ($(this).val() != ''){
				_id_pemberangkatan_all.push($(this).val());
				_id_armada_all.push($(this).find('option:selected').attr('data-id-armada'));
			}
		});

	    $.ajax({
	    	url : "<?php echo BASE_URL.'?m=pemesanan_tiket&c=getNoKursi&a=do';?>",
	    	type : "post",
	    	dataType : 'html',
	    	data : {
	    		id_pemberangkatan : $('select.id_pemberangkatan').val(),
	    		tanggal_pemberangkatan : $('input.tanggal_pemberangkatan').val(),
	    		no_kursi_terpakai : _no_kursi_used,
	    		id_kota_asal: $('select.id_kota_asal').val(),
	    		id_kota_tujuan: $('select.id_kota_tujuan').val(),
	    		id_pemberangkatan_all : _id_pemberangkatan_all,
	    		id_kota_asal_all : _id_kota_asal_all,
	    		id_kota_tujuan_all : _id_kota_tujuan_all,
	    		id_armada_all : _id_armada_all
	    	},
	    	success : function(response){
	    		$('#container-no-kursi').html(response);
	    	},
	    	error : function(){
	    		$.notify('Terjadi kesalahan, coba ulangi kembali');
	    		$('#myModal').modal('hide');
	    	}
	    });
	});
</script>