<?php
ob_start();
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();
$old_id_pembelian_tiket = isset($_SESSION['id_pembelian_tiket']) ? $_SESSION['id_pembelian_tiket'] : 0;
$tglTiketLama = $model->getTanggalPemberangkatan($old_id_pembelian_tiket);
$tanggal_pemberangkatan = $_POST['tanggal_pemberangkatan'];

if ($tglTiketLama && strtotime($tglTiketLama->tanggal_pemberangkatan) >= strtotime($tanggal_pemberangkatan)){
	$result = false;
	echo json_encode(array('success' => false, 'message' => 'Tanggal Pemberangkatan pulang tidak boleh kurang dari '.date('d-m-Y', strtotime($tglTiketLama->tanggal_pemberangkatan))));
}else
if ($tglTiketLama && strtotime($tglTiketLama->tanggal_pemberangkatan) < strtotime($tanggal_pemberangkatan)){
	echo json_encode(array('success' => true, 'message' => ''));
}else{
	echo json_encode(array('success' => false, 'message' => 'Terjadi kesalahan, tanggal pemberangkatan tidak bisa diinisiasi'));
}
?>