<?php
Class modelPemesananTiket extends model{

	public function __construct(){
		parent::__construct();
	}

	public function getPemberangkatan($tanggal_pemberangkatan = 0, $id_kota_asal = 0, $id_kota_tujuan = 0){
		if (!empty($tanggal_pemberangkatan)){
			$tanggal_pemberangkatan = date('Y-m-d', strtotime($tanggal_pemberangkatan));
		}
		$tipe = $this->getOrderKota($id_kota_asal, $id_kota_tujuan);
		$day = $this->dateToDayIndonesia($tanggal_pemberangkatan);
		$qPemberangkatan = "SELECT * FROM (
			SELECT P.*, DAYNAME('$tanggal_pemberangkatan') AS hari_english,
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			nama_armada,
			(
				SELECT COUNT(NK1.`id_no_kursi`) 
				FROM no_kursi AS NK1 
				WHERE NK1.id_armada = E.`id_armada` 
				LIMIT 1
			) AS jumlah_kursi,
			(
				SELECT COUNT(DP2.id_no_kursi) 
				FROM detail_pembelian AS DP2 
				WHERE 
					DP2.id_pemberangkatan = P.`id_pemberangkatan` 
					AND DATE(tanggal_pemberangkatan) = '$tanggal_pemberangkatan'
					AND DP2.status_batal = '0'
			) AS jumlah_kursi_terpesan
			FROM pemberangkatan AS P
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			GROUP BY P.`id_pemberangkatan`
			ORDER BY CONCAT(`id_kota_asal`, `id_kota_tujuan`, `jumlah_kursi_terpesan`) DESC
			) AS temp_table 
			WHERE LOWER(hari) = LOWER('$day')
			AND id_kota_asal = '$id_kota_asal'
			AND id_kota_tujuan = '$id_kota_tujuan'
			GROUP BY `id_pemberangkatan`";
		$qPemberangkatan = mysql_query($qPemberangkatan);

		$arrayPemberangkatan = array();
		// ambil bus yang belum full terisi penumpang, dan tunggu sampai bus dengan jurusan yang sama full;
		while ($dataPemberangkatan = mysql_fetch_array($qPemberangkatan)) {
			$key = $dataPemberangkatan['id_kota_asal'].$dataPemberangkatan['id_kota_tujuan'];
			$a = (int) $dataPemberangkatan['jumlah_kursi_terpesan'];
			$b = (int) $dataPemberangkatan['jumlah_kursi'];
			if (!array_key_exists($key, $arrayPemberangkatan) && $a < $b){
				$arrayPemberangkatan[$key] = $dataPemberangkatan;
			}
		}
		return $arrayPemberangkatan;
	}

	public function getOrderKota($id_kota_asal, $id_kota_tujuan){
		$query = "SELECT order_kota_asal, order_kota_tujuan, IF(order_kota_asal < order_kota_tujuan, 'AB', 'BA') AS tipe
			FROM
			(
				SELECT order_kota AS `order_kota_asal`
				FROM kota
				WHERE
				id_kota = '$id_kota_asal'
				LIMIT 1
			)AS table_kota_asal
			LEFT JOIN (
				SELECT order_kota AS `order_kota_tujuan`
				FROM kota
				WHERE
				id_kota = '$id_kota_tujuan'
				LIMIT 1
			)AS table_kota_tujuan ON TRUE";
		$result = $this->result(mysql_query($query));
		$result = isset($result[0]['tipe']) ? $result[0]['tipe'] : 'AB';
		return $result;
	}

	public function getKotaAsal(){
		$qKotaAsal = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_asal
			FROM pemberangkatan
			LEFT JOIN kota ON pemberangkatan.`id_kota_asal` = kota.`id_kota`
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
			GROUP BY id_kota
			ORDER BY `order_kota` ASC");
		return $this->result($qKotaAsal);
	}

	public function getKotaTujuan(){
		$qKotaTujuan = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_tujuan
			FROM pemberangkatan
			LEFT JOIN kota ON pemberangkatan.`id_kota_tujuan` = kota.`id_kota`
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
			GROUP BY id_kota
			ORDER BY `order_kota` ASC");
		return $this->result($qKotaTujuan);
	}

	public function getNoKursi($id_pemberangkatan, $tanggal_pemberangkatan){
		$qNokursi = mysql_query("SELECT * FROM no_kursi 
			WHERE id_armada = (SELECT id_armada FROM pemberangkatan WHERE id_pemberangkatan = '".$id_pemberangkatan."' LIMIT 1)
			AND id_no_kursi NOT IN (SELECT id_no_kursi FROM detail_pembelian WHERE id_pemberangkatan = '".$id_pemberangkatan."' AND DATE(tanggal_pemberangkatan) = '".$tanggal_pemberangkatan."' AND status_batal = '0')
			ORDER BY id_no_kursi ASC");
		

		return $this->result($qNokursi);
	}

	public function getNoKursiUsed($id_pemberangkatan, $tanggal_pemberangkatan){
		$qNokursi = mysql_query("SELECT * FROM no_kursi 
			WHERE id_armada = (
				SELECT id_armada 
				FROM pemberangkatan 
				WHERE id_pemberangkatan = '$id_pemberangkatan' 
				LIMIT 1
				)
			AND id_no_kursi IN (
				SELECT id_no_kursi 
				FROM detail_pembelian
				LEFT JOIN `kota` 
					ON kota.`id_kota` = detail_pembelian.`id_kota_tujuan`
				WHERE id_pemberangkatan = '$id_pemberangkatan' 
				AND DATE(tanggal_pemberangkatan) = '$tanggal_pemberangkatan' 
				AND status_batal = '0'
				)
			ORDER BY id_no_kursi ASC");

		return $this->result($qNokursi);
	}

	public function getHarga($id_pemberangkatan){
		$qHarga = mysql_query("SELECT 
			harga, jam, jam_sampai, `nama_armada`, armada.`id_armada`
			FROM pemberangkatan
			LEFT JOIN `armada` ON armada.`id_armada` = `pemberangkatan`.`id_armada`
			WHERE id_pemberangkatan = '".$id_pemberangkatan."'");
		return $this->result($qHarga);
	}

	public function getIdPembelianTiket(){
		$qIdPembelianTiket = mysql_query("SELECT id FROM
						(
						SELECT @id := REPLACE(id_pembelian_tiket, 'O-', '') + 1, CONCAT('O-', @id) AS id 
						FROM pembelian_tiket 
						UNION
						SELECT @id := '', 'O-1' AS id
						)
						AS temp_table
						ORDER BY REPLACE(id, 'O-', '') * 1 DESC 
						LIMIT 1");
		$dataIdPembelianTiket = mysql_fetch_object($qIdPembelianTiket);
		return $dataIdPembelianTiket->id;
	}

	public function cekPembelian($id_pelanggan, $tanggal_beli){
		$qcek = mysql_query("SELECT * FROM pembelian_tiket WHERE id_pelanggan = '".$id_pelanggan."' AND DATE(tanggal_beli) = '".$tanggal_beli."' AND status_bayar = 'belum lunas'");
		return mysql_fetch_object($qcek);
	}

	public function getdetailTiket($refId){
		$qPembelian = mysql_query("SELECT 
			`PT`.`id_pembelian_tiket`, PT.id_tiket_berangkat, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, PT.total_harga, P.id_kota_asal, P.id_kota_tujuan AS id_tujuan, P.id_armada
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE PT.id_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");

		return $this->result($qPembelian);
	}

	public function getdetailTiketForEdit($refId){
		$qPembelian = mysql_query("SELECT 
			`PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, PT.total_harga, P.id_kota_asal, P.id_armada, PT.id_tiket_berangkat
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE DP.id_detail_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");

		return $this->result($qPembelian);
	}

	public function addPembelian($id_pembelian_tiket, $id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $total_harga, $status_bayar){
		$query = " INSERT INTO pembelian_tiket SET 	
		    		id_pembelian_tiket = '".$id_pembelian_tiket."',
		    		tanggal_beli = CURRENT_TIMESTAMP(),
		    		id_pelanggan = '".$id_pelanggan."',
		    		nama_pelanggan = '".$nama_pelanggan."',
		    		no_hp = '".$no_hp."',
		    		total_harga = '".$total_harga."',
		    		status_bayar = '".$status_bayar."'
		    	";
		$save = mysql_query($query);
		return $save;
	}

	public function addPembelianPulang($id_pembelian_tiket, $old_id_pembelian_tiket, $id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $totalHarga, $status_bayar){
		$query = " INSERT INTO pembelian_tiket SET 	
		    		id_pembelian_tiket = '".$id_pembelian_tiket."',
		    		id_tiket_berangkat = '".$old_id_pembelian_tiket."',
		    		tanggal_beli = CURRENT_TIMESTAMP(),
		    		id_pelanggan = '".$id_pelanggan."',
		    		nama_pelanggan = '".$nama_pelanggan."',
		    		no_hp = '".$no_hp."',
		    		total_harga = '".$totalHarga."',
		    		status_bayar = '".$status_bayar."'
		    	";
		$save = mysql_query($query);
		return $save;
	}

	public function updatePembelian($id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $totalHarga, $status_bayar, $id_pembelian_tiket){
		$update = mysql_query(" UPDATE pembelian_tiket SET
		    		id_pelanggan = '".$id_pelanggan."',
		    		nama_pelanggan = '".$nama_pelanggan."',
		    		tanggal_beli = CURRENT_TIMESTAMP(),
		    		no_hp = '".$no_hp."',
		    		total_harga = '".$totalHarga."',
		    		status_bayar = '".$status_bayar."'
		    		WHERE
		    		id_pembelian_tiket = '".$id_pembelian_tiket."'
		    	");
		return $update;
	}

	public function deleteDetail($id){
		$query = "DELETE FROM detail_pembelian WHERE id_pembelian_tiket = '".$id."'";
		$result = mysql_query($query);
		return $result;
	}

	public function addDetailPembelian($id_pembelian_tiket, $id_pemberangkatan, $id_kota_tujuan, $id_no_kursi, $tanggal_pemberangkatan, $atas_nama, $id_penumpang, $sub_total){
		$query = "INSERT INTO 
			detail_pembelian
				(id_pembelian_tiket,  id_pemberangkatan,  id_kota_tujuan,  id_no_kursi,  tanggal_pemberangkatan,  atas_nama,  id_penumpang,  sub_total) 
			VALUES 
				(
					'".$id_pembelian_tiket."', 
					'".$id_pemberangkatan."', 
					'".$id_kota_tujuan."', 
					'".$id_no_kursi."', 
					'".$tanggal_pemberangkatan."', 
					'".$atas_nama."', 
					'".$id_penumpang."', 
					'".$sub_total."'
				)";
		// return $query;
		$insert = mysql_query($query);
		return $insert;
	}

	public function EditDetailPembelian($id_pembelian_tiket, $id_pemberangkatan, $id_kota_tujuan, $id_no_kursi, $tanggal_pemberangkatan, $atas_nama, $id_penumpang, $sub_total, $id_detail_pembelian_tiket){
		$query = "UPDATE
			detail_pembelian
			SET
			id_pembelian_tiket = '".$id_pembelian_tiket."', 
			id_pemberangkatan = '".$id_pemberangkatan."', 
			id_kota_tujuan = '".$id_kota_tujuan."', 
			id_no_kursi = '".$id_no_kursi."', 
			tanggal_pemberangkatan = '".$tanggal_pemberangkatan."', 
			atas_nama = '".$atas_nama."', 
			id_penumpang = '".$id_penumpang."', 
			sub_total = '".$sub_total."'
			WHERE id_detail_pembelian_tiket = '".$id_detail_pembelian_tiket."'";
		$update = mysql_query($query);
		return $update;
	}

	public function getSettingPembelianTiket(){
		$query = mysql_query("SELECT * FROM setting WHERE id = 'PT'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		if (!empty($arrayData)){
			$result = $arrayData[0]['set'];
		}else{
			$result = 0;
		}

		return $result;
	}

	public function dayToEnglish($hari){
		switch (strtolower($hari)) {
			case 'minggu':
				$result = 'sunday';
				break;
			case 'senin':
				$result = 'monday';
				break;
			case 'selasa':
				$result = 'tuesday';
				break;
			case 'rabu':
				$result = 'wednesday';
				break;
			case 'kamis':
				$result = 'thursday';
				break;
			case 'jumat':
				$result = 'friday';
				break;
			case 'sabtu':
				$result = 'saturday';
				break;
			default:
				$result = 'sunday';
				break;
		}
		return $result;
	}

	public function dayToIndonesia($hari){
		$day = substr($hari, 0, 3);
		switch (strtolower($day)) {
			case 'sun':
				$result = 'minggu';
				break;
			case 'mon':
				$result = 'senin';
				break;
			case 'tue':
				$result = 'selasa';
				break;
			case 'wed':
				$result = 'rabu';
				break;
			case 'thu':
				$result = 'kamis';
				break;
			case 'fri':
				$result = 'jumat';
				break;
			case 'sat':
				$result = 'sabtu';
				break;
			default:
				$result = 'minggu';
				break;
		}
		return $result;
	}

	public function dateToDayIndonesia($date){
		if (!empty($date)){
			$day = date('D', strtotime($date));
			return $this->dayToIndonesia($day);
		}else{
			return false;
		}		
	}

	public function getDataNoKursi($id_armada){
		$queryNoKursi = "SELECT no_kursi.*, nama_armada FROM no_kursi 
		LEFT JOIN armada ON armada.`id_armada` = no_kursi.`id_armada`
		WHERE armada.id_armada = '$id_armada'
		GROUP BY armada.id_armada, no_kursi.no_kursi
		ORDER BY id_no_kursi ASC";
		$qNoKursi = mysql_query($queryNoKursi);
		$arrayNokursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNoKursi)) {
		    $arrayNokursi[] = $dataNoKursi;
		}
		return $arrayNokursi;
	}

	public function coreNoKursi(){
		$lineKursi = range(0, 11);
		$array = array();
		foreach ($lineKursi as $key => $value) {
            if ($key == 10){
            	$array[$key][0] = '';
            	$array[$key][1] = '';
            }else
            if ($key == 11){
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            }else{
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            	$array[$key][3] = '';
            }                            
        }
        return $array;
	}

	public function getSusunanNoKursi($id_armada){
		$arraySusunan = $this->coreNoKursi();
		$dataNoKursi = $this->getDataNoKursi($id_armada);
		$index = 0;
		foreach ($arraySusunan as $key => $value) {
			foreach ($value as $k => $v) {
				if (isset($dataNoKursi[$index])){
					$arraySusunan[$key][$k] = $dataNoKursi[$index];
					$index++;
				}
			}
		}
		return $arraySusunan;
	}

	public function getTanggalPemberangkatan($id_pembelian_tiket){
		$query = "SELECT DATE(`tanggal_pemberangkatan`) AS `tanggal_pemberangkatan`
		FROM `detail_pembelian`
		LEFT JOIN `pembelian_tiket` ON `pembelian_tiket`.`id_pembelian_tiket` = `detail_pembelian`.`id_pembelian_tiket`
		WHERE
		`detail_pembelian`.`id_pembelian_tiket` = '$id_pembelian_tiket'
		LIMIT 1";
		return mysql_fetch_object(mysql_query($query));
	}

	public function getPemesananTiketUnconfirm($userId){
		$queryPembelian = mysql_query("SELECT 
            (
                SELECT GROUP_CONCAT(`id_pembelian_tiket` SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS id_pembelian_tiket,
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(tanggal_beli) SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_beli, 
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(`tanggal_pemberangkatan`) SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_pemberangkatan,
            (
                SELECT GROUP_CONCAT(DISTINCT atas_nama SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS atas_nama,
            (
                SELECT SUM(`detail_pembelian`.sub_total)
                FROM detail_pembelian
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS total_harga,
			(
                SELECT GROUP_CONCAT( DISTINCT CONCAT(kota_a.nama_kota, '(', prov_a.nama_provinsi,')', ' - ', kota_b.nama_kota, '(',prov_b.nama_provinsi,')') SEPARATOR '<br>') AS kota_pemberangkatan
                FROM detail_pembelian
                LEFT JOIN pemberangkatan ON pemberangkatan.id_pemberangkatan = detail_pembelian.id_pemberangkatan
                LEFT JOIN kota AS kota_a ON kota_a.id_kota = pemberangkatan.id_kota_asal
                LEFT JOIN provinsi AS prov_a ON prov_a.id_provinsi = kota_a.id_provinsi
                LEFT JOIN kota AS kota_b ON kota_b.id_kota = pemberangkatan.`id_kota_tujuan`
                LEFT JOIN provinsi AS prov_b ON prov_b.id_provinsi = kota_b.id_provinsi
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS kota_pemberangkatan,
			PT.`status_bayar`,
			PT.bukti_transfer
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            WHERE DP.status_batal = '0'
                AND PT.id_pelanggan = '$userId'
                AND PT.id_tiket_berangkat IS NULL
                AND CURRENT_DATE() <= DP.tanggal_pemberangkatan
            GROUP BY PT.id_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
		$result = $this->result($queryPembelian);

        return $result;
	}

	function doUpdateBuktiPembayaran($id, $fileUrl){
		$update = mysql_query("UPDATE pembelian_tiket 
			SET bukti_transfer = '$fileUrl'
			WHERE id_pembelian_tiket = '$id'");
		return $update;
	}

	function getDetailArmada($id_armada){
		$query = mysql_query("SELECT * FROM armada WHERE id_armada = '$id_armada'");
		return $this->result($query);
	}

}
?>