<?php
require_once 'modelPemesananTiket.php';
$model = new modelPemesananTiket();

$tanggal_pemberangkatan = $_POST['tanggal_pemberangkatan'];
$arrId = array();
$arrId[] = $_POST['id_pembelian_tiket'];
if (!empty($_POST['id_tiket_pulang'])){
	$arrId[] = $_POST['id_tiket_pulang'];
}

if (empty($tanggal_pemberangkatan)){
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewEditPemesananTiket&a=view&statusUpdate=0&ref_id='.implode(',', $arrId));
}
$id_pembelian_tiket = $_POST['id_pembelian_tiket'];
$add = true;

$totalHarga = 0;
foreach ($_POST['no_kursi'] as $key => $value) {
	$totalHarga = $totalHarga + $_POST['sub_total'][$key];
}

$delete = $model->deleteDetail($id_pembelian_tiket);

if ($delete){
	foreach ($_POST['no_kursi'] as $key => $value) {
		$p = array(
			'tanggal_pemberangkatan' => date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan'])),
		    'id_pemberangkatan' => $_POST['id_pemberangkatan'],
		    'id_kota_tujuan' => $_POST['id_kota_tujuan'],
		    'id_no_kursi' => $_POST['id_no_kursi'][$key],
		    'atas_nama' => $_POST['atas_nama'][$key],
		    'id_penumpang' => '',
		    'sub_total' => $_POST['sub_total'][$key]
		   );

		$tanggal_beli = date('Y-m-d');
		$id_pelanggan = $_SESSION['user_id'];
		$nama_pelanggan = $_SESSION['username'];
		$nomer_rekening = '';
		$id_bank = '';
		$no_hp = $_SESSION['no_hp'];
		$total_harga = $_POST['sub_total'][$key];
		$status_bayar = 'belum lunas';

		if (isset($_POST['save'])){

		    if (empty($p['tanggal_pemberangkatan']) || empty($p['id_pemberangkatan']) || empty($p['id_kota_tujuan']) || empty($p['id_no_kursi']) || empty($p['atas_nama']) || empty($p['sub_total'])){
		    	$add = $add && false;
		    }else{
		    	$cek = 0;
		    	if ($key == 0){
		    		$save = $model->updatePembelian($id_pelanggan, $nama_pelanggan, $no_hp, $nomer_rekening, $id_bank, $totalHarga, $status_bayar, $id_pembelian_tiket);
		    	}else{
		    		$save = true;
		    	}  	
		    	
			    if ($save){
			    	$a = $model->addDetailPembelian($id_pembelian_tiket, $p['id_pemberangkatan'], $p['id_kota_tujuan'], $p['id_no_kursi'], $p['tanggal_pemberangkatan'], $p['atas_nama'], $p['id_penumpang'], $p['sub_total']);

			    	$add = $add && true;	
			    }else{
			    	$add = $add && false;
			    }
			}
				
		}	# code...
	}
}

if ($add && !isset($arrId[1])){
	sendEmail($id_pembelian_tiket);
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewEditPemesananTiket&a=view&statusUpdate=1&ref_id='.$id_pembelian_tiket);
}else
if ($add && isset($arrId[1])){
	$_SESSION['id_pembelian_tiket'] = $arrId[1];
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewEditPemesananTiketPulang&a=view&ref_id='.$arrId[1].'&statusUpdate=1');
}else{
	header('location: '.BASE_URL.'?m=pemesanan_tiket&c=viewEditPemesananTiket&a=view&statusUpdate=0');
}

function sendEmail($id_pembelian_tiket){
	Library::load('PHPMailerAutoload.php', 'phpmailer');
	$model = new modelPemesananTiket();
	$dataTiket = $model->getdetailTiket($id_pembelian_tiket);
	$totalHarga = $dataTiket[0]['total_harga'];

	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	// $mail->isSMTP();
	$mail->SMTPSecure = 'tls';
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	//$mail->SMTPDebug = 2;
	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	//$mail->Host = "mailtrap.io";
	$mail->Host = $config['email_host'];
	//Set the SMTP port number - likely to be 25, 465 or 587
	// $mail->Port = 2525;
	$mail->Port = $config['email_port'];
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication
	// $mail->Username = "ee74382b6cc229";
	$mail->Username = $config['email_username'];
	//Password to use for SMTP authentication
	$mail->Password = $config['email_password'];
	$mail->setFrom('noreplay@familyceria.com', 'Admin PO Family Ceria');
	//Set an alternative reply-to address
	// $mail->addReplyTo('user_ceria@localhost', 'User Ceria');
	//Set who the message is to be sent to
	$mail->addAddress($_SESSION['email'], $_SESSION['username']);
	//Set the subject line
	$mail->Subject = 'Perubahan Data Pemesanan Tiket PO Family Ceria';
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	ob_start();
	?>
	<table width="100%">
		<tr>
			<th style="text-align:center; background-color: #EAD748; padding: 5px"><h2>PO Family Ceria</h3></th>
		</tr>
		<tr>
			<td>
				Dear <?php echo $_SESSION['username'];?>,<br>
				Anda telah melakukan perubahan pemesanan tiket di PO Family Ceria. Berikut adalah detail pemesanan tiket anda:<br><br><br>
				<?php
				if (!empty($dataTiket)){
					?>
					<table border="1" width="70%">
						<col width="35%"></col>
						<col width="65%"></col>
						<tr>
							<td>ID</td>
							<td>: <?php echo $dataTiket[0]['id_pembelian_tiket'];?></td>
						</tr>
						<tr>
							<td>Tanggal Pemberangkatan</td>
							<td>: <?php echo date('d M Y', strtotime($dataTiket[0]['tanggal_pemberangkatan']));?></td>
						</tr>
						<tr>
							<td>Pemberangkatan</td>
							<td>: <?php echo $dataTiket[0]['nama_armada'].'|'.$dataTiket[0]['kota_asal'].' - '.$dataTiket[0]['kota_tujuan'];?></td>
						</tr>
						<tr>
							<td>Kota Tujuan</td>
							<td>: <?php echo $dataTiket[0]['lokasi_tujuan'];?></td>
						</tr>
						<tr>
							<td>Harga Tiket</td>
							<td>: <?php echo 'Rp.'.number_format($dataTiket[0]['sub_total'], 0, 0, '.').',-';?></td>
						</tr>
						<tr>
							<td>Penumpang</td>
							<td>
								<table border="1">
									<tr>
										<th align="center">No</th>
										<th>No. Kursi</th>
										<th>Atas Nama</th>
									</tr>
									<?php
									foreach ($dataTiket as $key => $value) {
										?>
										<tr>
											<td align="center"><?php echo ($key + 1);?></td>
											<td><?php echo $value['no_kursi'];?></td>
											<td><?php echo $value['atas_nama'];?></td>
										</tr>
										<?php
									}
									?>
								</table>
							</td>
						</tr>
					</table>
					<?php
				}
				?>
				<br>
				<table border="1" width="70%">
					<col width="35%"></col>
					<col width="65%"></col>
					<tr>
						<td>Total:</td>
						<td>Rp.<?php echo number_format($totalHarga, 0, 0, '.');?>,-</td>
					</tr>
				</table>
				<br>
				Silahkan melakukan pembayaran ke outlet kami. terimakasih.
			</td>
		</tr>
		<tr>
			<td style="text-align:center">
				<i>Terima Kasih atas Kepercayaan Anda kepada Kami</i>
			</td>
		</tr>
	</table>
	<?php
	$html = ob_get_clean();
	$mail->msgHTML($html, dirname(__FILE__));
	//Replace the plain text body with one created manually
	// $mail->AltBody = 'test email from family ceria';
	//Attach an image file
	//$mail->addAttachment('images/phpmailer_mini.png');

	//send the message, check for errors
	if (!$mail->send()) {
	    return false;
	} else{
		return true;
	}
}
?>