/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 5.6.24 : Database - reservasi_bus_erick
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `armada` */

DROP TABLE IF EXISTS `armada`;

CREATE TABLE `armada` (
  `id_armada` int(11) NOT NULL AUTO_INCREMENT,
  `nama_armada` varchar(150) NOT NULL,
  `no_plat` varchar(10) NOT NULL,
  `muatan_penumpang` int(11) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_armada`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `armada` */

insert  into `armada`(`id_armada`,`nama_armada`,`no_plat`,`muatan_penumpang`,`modified_on`) values (2,'Armada 4','A-4',60,'2016-06-11 22:01:47'),(4,'Armada 3','A-3',60,'2016-06-11 22:06:46'),(5,'Armada 2','A-2',60,'2016-06-14 21:39:31'),(6,'Armada 1','BH1344KL',60,'2016-12-17 00:00:51'),(7,'Armada-5','A-0001-NS',60,'2017-03-17 21:22:08');

/*Table structure for table `detail_pembelian` */

DROP TABLE IF EXISTS `detail_pembelian`;

CREATE TABLE `detail_pembelian` (
  `id_detail_pembelian_tiket` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pembelian_tiket` varchar(20) DEFAULT NULL,
  `id_pemberangkatan` varchar(50) DEFAULT NULL,
  `id_kota_tujuan` bigint(20) DEFAULT NULL,
  `id_no_kursi` bigint(20) DEFAULT NULL,
  `tanggal_pemberangkatan` datetime DEFAULT NULL,
  `atas_nama` varchar(150) DEFAULT NULL,
  `id_penumpang` varchar(50) DEFAULT NULL,
  `sub_total` float DEFAULT NULL,
  `status_batal` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id_detail_pembelian_tiket`),
  KEY `FK_detail_pembelian` (`id_pembelian_tiket`),
  KEY `id_no_kursi` (`id_no_kursi`),
  KEY `id_pemberangkatan` (`id_pemberangkatan`),
  KEY `id_kota_tujuan` (`id_kota_tujuan`),
  CONSTRAINT `FK_detail_pembelian` FOREIGN KEY (`id_pembelian_tiket`) REFERENCES `pembelian_tiket` (`id_pembelian_tiket`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_pembelian_ibfk_1` FOREIGN KEY (`id_no_kursi`) REFERENCES `no_kursi` (`id_no_kursi`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_pembelian_ibfk_2` FOREIGN KEY (`id_pemberangkatan`) REFERENCES `pemberangkatan` (`id_pemberangkatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_pembelian_ibfk_3` FOREIGN KEY (`id_kota_tujuan`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

/*Data for the table `detail_pembelian` */

insert  into `detail_pembelian`(`id_detail_pembelian_tiket`,`id_pembelian_tiket`,`id_pemberangkatan`,`id_kota_tujuan`,`id_no_kursi`,`tanggal_pemberangkatan`,`atas_nama`,`id_penumpang`,`sub_total`,`status_batal`) values (1,'O-21','P-23',19,13,'2017-04-18 00:00:00','ucok','0',160000,'0'),(2,'O-22','P-6',23,7,'2017-04-17 00:00:00','reno','0',210000,'0'),(3,'O-23','P-4',24,7,'2017-04-17 00:00:00','erik','0',240000,'0'),(4,'O-24','P-7',22,7,'2017-04-24 00:00:00','nhkihloih','0',200000,'0'),(5,'O-25','P-10',19,7,'2017-06-05 00:00:00','sia','0',170000,'0'),(6,'O-26','P-10',19,66,'2017-04-17 00:00:00','noni','0',170000,'0'),(7,'O-27','P-8',21,7,'2017-04-24 00:00:00','daa','0',190000,'0'),(8,'O-28','P-10',19,8,'2017-04-24 00:00:00','dfa','0',170000,'0'),(9,'O-29','P-8',21,66,'2017-04-17 00:00:00','reree','0',190000,'0'),(10,'O-30','P-24',18,13,'2017-04-11 00:00:00','agus','0',160000,'0'),(14,'O-34','P-57',19,223,'2017-04-21 00:00:00','asas','0',180000,'0'),(15,'O-35','P-58',18,223,'2017-04-21 00:00:00','sa','0',170000,'0'),(16,'O-36','P-58',18,224,'2017-04-21 00:00:00','sas','0',170000,'0'),(17,'O-38','P-58',18,224,'2017-04-21 00:00:00','sas','0',170000,'0'),(18,'O-39','P-58',18,224,'2017-04-21 00:00:00','sas','0',170000,'0'),(19,'O-40','P-58',18,224,'2017-04-21 00:00:00','sas','0',170000,'0'),(20,'O-41','P-58',18,224,'2017-04-21 00:00:00','sas','0',170000,'0'),(21,'O-42','P-58',18,225,'2017-04-21 00:00:00','dsa','0',170000,'0'),(22,'O-42','P-58',18,226,'2017-04-21 00:00:00','dsq','0',170000,'0'),(23,'O-43','P-58',18,225,'2017-04-21 00:00:00','dsa','0',170000,'0'),(24,'O-43','P-58',18,226,'2017-04-21 00:00:00','dsq','0',170000,'0'),(25,'O-44','P-58',18,225,'2017-04-21 00:00:00','dsa','0',170000,'0'),(26,'O-44','P-58',18,226,'2017-04-21 00:00:00','dsq','0',170000,'0'),(27,'O-45','P-46',18,1,'2017-07-27 00:00:00','imad','0',170000,'0'),(28,'O-46','P-12',17,7,'2017-07-31 00:00:00','ahmad','0',150000,'0'),(29,'O-46','P-12',17,8,'2017-07-31 00:00:00','asep','',150000,'0'),(32,'O-47','P-11',18,7,'2017-07-31 00:00:00','agung','0',160000,'0'),(33,'O-47','P-11',18,8,'2017-07-31 00:00:00','rahmat','',160000,'0'),(34,'O-1','P-11',18,7,'2017-08-14 00:00:00','agung','0',160000,'0'),(35,'O-1','P-11',18,8,'2017-08-14 00:00:00','ahmad','',160000,'0'),(36,'O-2','P-25',17,14,'2017-08-22 00:00:00','agung','0',150000,'0'),(37,'O-2','P-25',17,15,'2017-08-22 00:00:00','ahmad','',150000,'0');

/*Table structure for table `harga_pengiriman` */

DROP TABLE IF EXISTS `harga_pengiriman`;

CREATE TABLE `harga_pengiriman` (
  `id_harga_pengiriman` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_paket_pengiriman` bigint(20) DEFAULT NULL,
  `id_kota_asal` bigint(20) DEFAULT NULL,
  `id_kota_tujuan` bigint(20) DEFAULT NULL,
  `estimasi_waktu` int(11) NOT NULL DEFAULT '1',
  `harga` float NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_harga_pengiriman`),
  KEY `id_paket_pengiriman` (`id_paket_pengiriman`),
  KEY `id_kota_asal` (`id_kota_asal`),
  KEY `id_kota_tujuan` (`id_kota_tujuan`),
  CONSTRAINT `harga_pengiriman_ibfk_1` FOREIGN KEY (`id_paket_pengiriman`) REFERENCES `paket_pengiriman` (`id_paket_pengiriman`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `harga_pengiriman_ibfk_2` FOREIGN KEY (`id_kota_asal`) REFERENCES `kota` (`id_kota`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `harga_pengiriman_ibfk_3` FOREIGN KEY (`id_kota_tujuan`) REFERENCES `kota` (`id_kota`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `harga_pengiriman` */

insert  into `harga_pengiriman`(`id_harga_pengiriman`,`id_paket_pengiriman`,`id_kota_asal`,`id_kota_tujuan`,`estimasi_waktu`,`harga`,`modified_on`) values (1,1,NULL,NULL,1,10000,'2016-06-26 22:23:50'),(2,2,NULL,NULL,1,15000,'2016-06-26 22:24:08'),(3,1,NULL,NULL,2,30000,'2016-06-26 22:24:29'),(4,2,NULL,NULL,2,40000,'2016-06-26 22:24:43'),(5,2,NULL,NULL,1,10000,'2016-12-03 12:28:22'),(6,2,NULL,NULL,3,5000,'2017-03-17 20:35:02');

/*Table structure for table `jurusan` */

DROP TABLE IF EXISTS `jurusan`;

CREATE TABLE `jurusan` (
  `id_jurusan` varchar(20) NOT NULL,
  `kota_tujuan` varchar(20) DEFAULT NULL,
  `jenis_kelas` varchar(20) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `kota_asal` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_jurusan`),
  KEY `FK_jurusan` (`jenis_kelas`),
  CONSTRAINT `FK_jurusan` FOREIGN KEY (`jenis_kelas`) REFERENCES `kelas` (`jenis_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jurusan` */

insert  into `jurusan`(`id_jurusan`,`kota_tujuan`,`jenis_kelas`,`harga`,`kota_asal`) values ('1','lampung','ac',360000,'yogyakarta'),('2','yogyakarta','non-ac',380000,'lampung');

/*Table structure for table `kelas` */

DROP TABLE IF EXISTS `kelas`;

CREATE TABLE `kelas` (
  `jenis_kelas` varchar(20) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`jenis_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kelas` */

insert  into `kelas`(`jenis_kelas`,`modified_on`) values ('ac','2016-06-12 00:58:23'),('non-ac','2016-06-12 00:58:23');

/*Table structure for table `kota` */

DROP TABLE IF EXISTS `kota`;

CREATE TABLE `kota` (
  `id_kota` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_provinsi` int(11) DEFAULT NULL,
  `nama_kota` varchar(100) DEFAULT NULL,
  `order_kota` int(11) DEFAULT '1' COMMENT 'urutan titik dari timur ke barat',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kota`),
  KEY `id_provinsi` (`id_provinsi`),
  CONSTRAINT `kota_ibfk_1` FOREIGN KEY (`id_provinsi`) REFERENCES `provinsi` (`id_provinsi`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `kota` */

insert  into `kota`(`id_kota`,`id_provinsi`,`nama_kota`,`order_kota`,`modified_on`) values (16,4,'Yogyakarta',1,'2017-04-07 21:04:35'),(17,6,'B.Lampung',2,'2017-04-09 12:12:28'),(18,5,'M. Enim',3,'2017-04-09 12:14:07'),(19,5,'Lahat',4,'2017-04-09 12:15:51'),(20,5,'L. Linggau',5,'2017-04-09 12:16:48'),(21,5,'Bangko',6,'2017-04-09 12:17:09'),(22,5,'M. BUNGO',7,'2017-04-09 12:17:31'),(23,5,'S. Rumbai',8,'2017-04-09 12:18:05'),(24,3,'Kiliranjao',9,'2017-04-09 12:18:31'),(25,3,'TL. Kuantan',10,'2017-04-09 12:18:51'),(26,3,'P.Baru',11,'2017-04-09 12:19:05'),(27,3,'Ujung batu',12,'2017-04-09 12:19:46');

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_role` int(11) DEFAULT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(500) NOT NULL DEFAULT '-',
  `jenis_kelamin` enum('L','P') NOT NULL DEFAULT 'L',
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `pertanyaan` varchar(500) DEFAULT NULL,
  `jawaban` varchar(500) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `login_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `login` */

insert  into `login`(`user_id`,`id_role`,`nama`,`alamat`,`jenis_kelamin`,`no_hp`,`email`,`username`,`password`,`pertanyaan`,`jawaban`,`status`) values (1,1,'admin','-','L','098987876765','admin@localhost','admin@localhost','21232f297a57a5a743894a0e4a801fc3','','','1'),(7,2,'User Family Ceria','-','L','098987876765','user_ceria@localhost','user_ceria@localhost','ee11cbb19052e40b07aac0ca060c23ee','apa email saya?','978d80fa9b1e93b4537fb951fb5a8d67','1'),(8,3,'petugas1','-','L','213','petugas1@localhost','petugas1@localhost','afb91ef692fd08c445e8cb1bab2ccf9c','1+1','2','1'),(10,2,'tes','-','L','0814567','agus@mail.com','tes','28b662d883b6d76fd96e4ddc5e9ba780','tes','7694f4a66316e53c8cdd9d9954bd611d','1'),(12,2,'User Ceria 2','-','L','085729822872','user_ceria2@localhost.com','user ceria 2','ee11cbb19052e40b07aac0ca060c23ee','a','92eb5ffee6ae2fec3ad71c777531578f','1'),(13,2,'tes_lagi','-','L','09898787886','tes_lagi@localhost','tes_lagi@localhost','5f4dcc3b5aa765d61d8327deb882cf99','-','-','1'),(14,2,'tes_lagi2','-','L','098987876767','tes_lagi2@localhost','tes_lagi2@localhost','5f4dcc3b5aa765d61d8327deb882cf99','-','-','1');

/*Table structure for table `no_kursi` */

DROP TABLE IF EXISTS `no_kursi`;

CREATE TABLE `no_kursi` (
  `id_no_kursi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` int(11) NOT NULL,
  `no_kursi` varchar(50) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_no_kursi`),
  KEY `id_armada` (`id_armada`),
  CONSTRAINT `no_kursi_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id_armada`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=268 DEFAULT CHARSET=utf8;

/*Data for the table `no_kursi` */

insert  into `no_kursi`(`id_no_kursi`,`id_armada`,`no_kursi`,`modified_on`) values (1,2,'1A','2016-06-12 22:49:26'),(2,2,'1B','2016-06-12 22:49:44'),(3,2,'1C','2016-06-12 22:50:03'),(7,6,'1A','2016-12-17 00:05:50'),(8,6,'1B','2016-12-17 00:05:57'),(9,6,'1C','2016-12-17 00:06:05'),(10,6,'1D','2016-12-17 00:06:12'),(11,6,'2A','2016-12-17 00:06:19'),(12,5,'1A','2016-12-17 00:06:35'),(13,5,'1B','2016-12-17 00:06:43'),(14,5,'1C','2016-12-17 00:06:50'),(15,5,'1D','2016-12-17 00:07:02'),(16,5,'2A','2016-12-17 00:07:14'),(17,4,'1A','2016-12-17 00:07:30'),(18,4,'1B','2016-12-17 00:07:38'),(19,4,'1C','2016-12-17 00:07:50'),(20,4,'1D','2016-12-17 00:07:57'),(21,4,'2A','2016-12-17 00:08:08'),(22,2,'1D','2016-12-17 00:08:53'),(23,2,'2A','2016-12-17 00:09:02'),(24,6,'2B','2017-02-26 16:27:24'),(64,6,'2C','2017-02-26 16:31:05'),(65,6,'2D','2017-02-26 16:31:05'),(66,6,'3A','2017-02-26 16:32:53'),(67,6,'3B','2017-02-26 16:32:53'),(68,6,'3C','2017-02-26 16:32:53'),(69,6,'3D','2017-02-26 16:32:53'),(70,6,'4A','2017-02-26 16:32:53'),(71,6,'4B','2017-02-26 16:32:53'),(72,6,'4C','2017-02-26 16:32:53'),(73,6,'4D','2017-02-26 16:32:53'),(74,6,'5A','2017-02-26 16:32:53'),(75,6,'5B','2017-02-26 16:32:53'),(76,6,'5C','2017-02-26 16:32:53'),(77,6,'5D','2017-02-26 16:32:54'),(78,6,'6A','2017-02-26 16:32:54'),(79,6,'6B','2017-02-26 16:32:54'),(80,6,'6C','2017-02-26 16:32:54'),(81,6,'6D','2017-02-26 16:32:54'),(82,6,'7A','2017-02-26 16:32:54'),(83,6,'7B','2017-02-26 16:32:54'),(84,6,'7C','2017-02-26 16:32:54'),(85,6,'7D','2017-02-26 16:32:54'),(86,6,'8A','2017-02-26 16:32:54'),(87,6,'8B','2017-02-26 16:32:54'),(88,6,'8C','2017-02-26 16:32:54'),(89,6,'8D','2017-02-26 16:32:54'),(90,6,'9A','2017-02-26 16:32:54'),(91,6,'9B','2017-02-26 16:32:54'),(92,6,'9C','2017-02-26 16:32:54'),(93,6,'9D','2017-02-26 16:32:54'),(94,6,'10A','2017-02-26 16:32:54'),(95,6,'10B','2017-02-26 16:32:54'),(96,6,'10C','2017-02-26 16:32:54'),(97,6,'10D','2017-02-26 16:32:54'),(98,6,'11C','2017-02-26 16:32:54'),(99,6,'11D','2017-02-26 16:32:54'),(100,6,'12A','2017-02-26 16:32:54'),(101,6,'12C','2017-02-26 16:32:54'),(102,6,'12D','2017-02-26 16:32:54'),(103,5,'2B','2017-02-26 16:38:06'),(104,5,'2C','2017-02-26 16:38:06'),(105,5,'2D','2017-02-26 16:38:06'),(106,5,'3A','2017-02-26 16:38:06'),(107,5,'3B','2017-02-26 16:38:06'),(108,5,'3C','2017-02-26 16:38:07'),(109,5,'3D','2017-02-26 16:38:07'),(110,5,'4A','2017-02-26 16:38:07'),(111,5,'4B','2017-02-26 16:38:07'),(112,5,'4C','2017-02-26 16:38:07'),(113,5,'4D','2017-02-26 16:38:07'),(114,5,'5A','2017-02-26 16:38:07'),(115,5,'5B','2017-02-26 16:38:07'),(116,5,'5C','2017-02-26 16:38:07'),(117,5,'5D','2017-02-26 16:38:07'),(118,5,'6A','2017-02-26 16:38:07'),(119,5,'6B','2017-02-26 16:38:07'),(120,5,'6C','2017-02-26 16:38:07'),(121,5,'6D','2017-02-26 16:38:07'),(122,5,'7A','2017-02-26 16:38:07'),(123,5,'7B','2017-02-26 16:38:07'),(124,5,'7C','2017-02-26 16:38:07'),(125,5,'7D','2017-02-26 16:38:07'),(126,5,'8A','2017-02-26 16:38:08'),(127,5,'8B','2017-02-26 16:38:08'),(128,5,'8C','2017-02-26 16:38:08'),(129,5,'8D','2017-02-26 16:38:08'),(130,5,'9A','2017-02-26 16:38:08'),(131,5,'9B','2017-02-26 16:38:08'),(132,5,'9C','2017-02-26 16:38:08'),(133,5,'9D','2017-02-26 16:38:08'),(134,5,'10A','2017-02-26 16:38:08'),(135,5,'10B','2017-02-26 16:38:08'),(136,5,'10C','2017-02-26 16:38:08'),(137,5,'10D','2017-02-26 16:38:08'),(138,5,'11C','2017-02-26 16:38:08'),(139,5,'11D','2017-02-26 16:38:08'),(140,5,'12A','2017-02-26 16:38:08'),(141,5,'12C','2017-02-26 16:38:08'),(142,5,'12D','2017-02-26 16:38:08'),(143,4,'2B','2017-02-26 16:40:26'),(144,4,'2C','2017-02-26 16:40:26'),(145,4,'2D','2017-02-26 16:40:26'),(146,4,'3A','2017-02-26 16:40:27'),(147,4,'3B','2017-02-26 16:40:27'),(148,4,'3C','2017-02-26 16:40:27'),(149,4,'3D','2017-02-26 16:40:27'),(150,4,'4A','2017-02-26 16:40:27'),(151,4,'4B','2017-02-26 16:40:27'),(152,4,'4C','2017-02-26 16:40:27'),(153,4,'4D','2017-02-26 16:40:27'),(154,4,'5A','2017-02-26 16:40:27'),(155,4,'5B','2017-02-26 16:40:27'),(156,4,'5C','2017-02-26 16:40:27'),(157,4,'5D','2017-02-26 16:40:27'),(158,4,'6A','2017-02-26 16:40:27'),(159,4,'6B','2017-02-26 16:40:27'),(160,4,'6C','2017-02-26 16:40:27'),(161,4,'6D','2017-02-26 16:40:27'),(162,4,'7A','2017-02-26 16:40:27'),(163,4,'7B','2017-02-26 16:40:27'),(164,4,'7C','2017-02-26 16:40:27'),(165,4,'7D','2017-02-26 16:40:27'),(166,4,'8A','2017-02-26 16:40:27'),(167,4,'8B','2017-02-26 16:40:27'),(168,4,'8C','2017-02-26 16:40:27'),(169,4,'8D','2017-02-26 16:40:28'),(170,4,'9A','2017-02-26 16:40:28'),(171,4,'9B','2017-02-26 16:40:28'),(172,4,'9C','2017-02-26 16:40:28'),(173,4,'9D','2017-02-26 16:40:28'),(174,4,'10A','2017-02-26 16:40:28'),(175,4,'10B','2017-02-26 16:40:28'),(176,4,'10C','2017-02-26 16:40:28'),(177,4,'10D','2017-02-26 16:40:28'),(178,4,'11C','2017-02-26 16:40:28'),(179,4,'11D','2017-02-26 16:40:28'),(180,4,'12A','2017-02-26 16:40:28'),(181,4,'12C','2017-02-26 16:40:28'),(182,4,'12D','2017-02-26 16:40:28'),(183,2,'2B','2017-02-26 16:42:21'),(184,2,'2C','2017-02-26 16:42:21'),(185,2,'2D','2017-02-26 16:42:21'),(186,2,'3A','2017-02-26 16:42:21'),(187,2,'3B','2017-02-26 16:42:21'),(188,2,'3C','2017-02-26 16:42:21'),(189,2,'3D','2017-02-26 16:42:21'),(190,2,'4A','2017-02-26 16:42:21'),(191,2,'4B','2017-02-26 16:42:21'),(192,2,'4C','2017-02-26 16:42:21'),(193,2,'4D','2017-02-26 16:42:21'),(194,2,'5A','2017-02-26 16:42:21'),(195,2,'5B','2017-02-26 16:42:21'),(196,2,'5C','2017-02-26 16:42:21'),(197,2,'5D','2017-02-26 16:42:21'),(198,2,'6A','2017-02-26 16:42:21'),(199,2,'6B','2017-02-26 16:42:21'),(200,2,'6C','2017-02-26 16:42:21'),(201,2,'6D','2017-02-26 16:42:21'),(202,2,'7A','2017-02-26 16:42:21'),(203,2,'7B','2017-02-26 16:42:21'),(204,2,'7C','2017-02-26 16:42:21'),(205,2,'7D','2017-02-26 16:42:21'),(206,2,'8A','2017-02-26 16:42:22'),(207,2,'8B','2017-02-26 16:42:22'),(208,2,'8C','2017-02-26 16:42:22'),(209,2,'8D','2017-02-26 16:42:22'),(210,2,'9A','2017-02-26 16:42:22'),(211,2,'9B','2017-02-26 16:42:22'),(212,2,'9C','2017-02-26 16:42:22'),(213,2,'9D','2017-02-26 16:42:22'),(214,2,'10A','2017-02-26 16:42:22'),(215,2,'10B','2017-02-26 16:42:22'),(216,2,'10C','2017-02-26 16:42:22'),(217,2,'10D','2017-02-26 16:42:22'),(218,2,'11C','2017-02-26 16:42:22'),(219,2,'11D','2017-02-26 16:42:22'),(220,2,'12A','2017-02-26 16:42:22'),(221,2,'12C','2017-02-26 16:42:22'),(222,2,'12D','2017-02-26 16:42:22'),(223,7,'E1','2017-03-17 21:25:06'),(224,7,'E2','2017-03-17 21:25:06'),(225,7,'E3','2017-03-17 21:25:16'),(226,7,'E4','2017-03-17 21:25:16'),(227,7,'E5','2017-04-04 23:43:13'),(228,7,'E6','2017-04-04 23:43:13'),(229,7,'E7','2017-04-04 23:43:13'),(230,7,'E8','2017-04-04 23:43:13'),(231,7,'E9','2017-04-04 23:43:13'),(232,7,'E10','2017-04-04 23:43:13'),(233,7,'E11','2017-04-04 23:43:13'),(234,7,'E12','2017-04-04 23:43:13'),(235,7,'E13','2017-04-04 23:43:13'),(236,7,'E14','2017-04-04 23:43:13'),(237,7,'E15','2017-04-04 23:43:13'),(238,7,'E16','2017-04-04 23:43:13'),(239,7,'E17','2017-04-04 23:43:13'),(240,7,'E18','2017-04-04 23:43:13'),(241,7,'E19','2017-04-04 23:43:13'),(242,7,'E20','2017-04-04 23:43:13'),(243,7,'E21','2017-04-04 23:43:13'),(244,7,'E22','2017-04-04 23:43:13'),(245,7,'E23','2017-04-04 23:43:13'),(246,7,'E24','2017-04-04 23:43:13'),(247,7,'E25','2017-04-04 23:43:13'),(248,7,'E26','2017-04-04 23:43:13'),(249,7,'E27','2017-04-04 23:43:13'),(250,7,'E28','2017-04-04 23:43:13'),(251,7,'E29','2017-04-04 23:43:14'),(252,7,'E30','2017-04-04 23:43:14'),(253,7,'E31','2017-04-04 23:43:14'),(254,7,'E32','2017-04-04 23:43:14'),(255,7,'E33','2017-04-04 23:43:14'),(256,7,'E34','2017-04-04 23:43:14'),(257,7,'E35','2017-04-04 23:43:14'),(258,7,'E36','2017-04-04 23:43:14'),(259,7,'E37','2017-04-04 23:43:14'),(260,7,'E38','2017-04-04 23:43:14'),(261,7,'E39','2017-04-04 23:43:14'),(262,7,'E40','2017-04-04 23:43:14'),(263,7,'E41','2017-04-04 23:43:14'),(264,7,'E42','2017-04-04 23:43:14'),(265,7,'E43','2017-04-04 23:43:14'),(266,7,'E44','2017-04-04 23:43:14'),(267,7,'E45','2017-04-04 23:43:14');

/*Table structure for table `page` */

DROP TABLE IF EXISTS `page`;

CREATE TABLE `page` (
  `page` varchar(150) NOT NULL,
  `label` varchar(500) NOT NULL,
  `destination_page` enum('FO','BO') NOT NULL DEFAULT 'BO',
  `need_to_login` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `page` */

insert  into `page`(`page`,`label`,`destination_page`,`need_to_login`) values ('armada','armada','BO',1),('bank','referensi bank','BO',1),('dashboard','beranda','BO',1),('harga_pengiriman','harga paket kurir','BO',1),('kelas','kelas bus','BO',1),('kota','kota','BO',1),('kurir','pengiriman barang','BO',1),('laporan','laporan','BO',1),('nomerKursi','nomer kursi','BO',1),('paket_pengiriman','referensi nama paket pengiriman','BO',1),('pembayaran','konfirmasi pembayaran tiket','BO',1),('pemberangkatan','pemberangkatan','BO',1),('penumpang','penumpang bus','BO',1),('provinsi','provinsi','BO',1),('role','hak akses','BO',1),('setting','pengaturan','BO',1),('sewaBus','penyewaan bus','BO',1),('supir','supir','BO',1),('user','user','BO',1);

/*Table structure for table `paket_pengiriman` */

DROP TABLE IF EXISTS `paket_pengiriman`;

CREATE TABLE `paket_pengiriman` (
  `id_paket_pengiriman` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_paket` varchar(250) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` enum('1','0') DEFAULT '1',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_paket_pengiriman`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `paket_pengiriman` */

insert  into `paket_pengiriman`(`id_paket_pengiriman`,`nama_paket`,`deskripsi`,`status`,`modified_on`) values (1,'reguler','paket pengiriman reguler melalui bus penumpang','1','2016-06-26 22:20:48'),(2,'expresss','paket pengiriman melalui kurir kusus po family ceria. waktu 2 kali lebih cepat dari paket reguler','1','2016-06-26 22:20:57');

/*Table structure for table `pembelian_tiket` */

DROP TABLE IF EXISTS `pembelian_tiket`;

CREATE TABLE `pembelian_tiket` (
  `id_pembelian_tiket` varchar(20) NOT NULL,
  `id_tiket_berangkat` varchar(20) DEFAULT NULL,
  `id_detail_pemberangkatan` bigint(20) DEFAULT NULL,
  `tanggal_beli` datetime NOT NULL,
  `id_pelanggan` int(20) DEFAULT NULL,
  `nama_pelanggan` varchar(20) DEFAULT NULL,
  `no_hp` varchar(15) NOT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `metode_pembayaran` enum('transfer','cash') DEFAULT 'cash',
  `total_bayar` float DEFAULT NULL,
  `bukti_transfer` text,
  `status_bayar` enum('lunas','belum lunas') NOT NULL DEFAULT 'lunas',
  `tanggal_bayar` datetime DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pembelian_tiket`),
  KEY `id_detail_pemberangkatan` (`id_detail_pemberangkatan`),
  KEY `pembelian_tiket_ibfk_2` (`id_pelanggan`),
  CONSTRAINT `pembelian_tiket_ibfk_2` FOREIGN KEY (`id_pelanggan`) REFERENCES `login` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pembelian_tiket_ibfk_3` FOREIGN KEY (`id_detail_pemberangkatan`) REFERENCES `pemberangkatan_detail` (`id_detail_pemberangkatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pembelian_tiket` */

insert  into `pembelian_tiket`(`id_pembelian_tiket`,`id_tiket_berangkat`,`id_detail_pemberangkatan`,`tanggal_beli`,`id_pelanggan`,`nama_pelanggan`,`no_hp`,`total_harga`,`metode_pembayaran`,`total_bayar`,`bukti_transfer`,`status_bayar`,`tanggal_bayar`,`modified_on`) values ('O-1',NULL,NULL,'2017-08-13 07:40:41',7,'user_ceria@localhost','098987876765',320000,'cash',NULL,NULL,'belum lunas',NULL,'2017-08-13 07:40:41'),('O-2','O-1',NULL,'2017-08-13 07:41:36',7,'user_ceria@localhost','098987876765',300000,'cash',NULL,NULL,'belum lunas',NULL,'2017-08-13 07:41:36');

/*Table structure for table `pemberangkatan` */

DROP TABLE IF EXISTS `pemberangkatan`;

CREATE TABLE `pemberangkatan` (
  `id_pemberangkatan` varchar(20) NOT NULL,
  `id_kota_asal` bigint(20) DEFAULT NULL,
  `id_kota_tujuan` bigint(20) DEFAULT NULL,
  `id_armada` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `hari` enum('senin','selasa','rabu','kamis','jumat','sabtu','minggu') NOT NULL,
  `jam` time DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pemberangkatan`),
  KEY `id_kota_asal` (`id_kota_asal`),
  KEY `id_kota_tujuan` (`id_kota_tujuan`),
  KEY `id_armada` (`id_armada`),
  CONSTRAINT `pemberangkatan_ibfk_2` FOREIGN KEY (`id_kota_asal`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pemberangkatan_ibfk_3` FOREIGN KEY (`id_kota_tujuan`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pemberangkatan_ibfk_4` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id_armada`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pemberangkatan` */

insert  into `pemberangkatan`(`id_pemberangkatan`,`id_kota_asal`,`id_kota_tujuan`,`id_armada`,`harga`,`hari`,`jam`,`modified_on`) values ('P-1',16,27,6,300000,'senin','16:00:00','2017-04-09 12:21:09'),('P-10',16,19,6,170000,'senin','16:00:00','2017-04-09 12:29:30'),('P-11',16,18,6,160000,'senin','16:00:00','2017-04-09 12:30:00'),('P-12',16,17,6,150000,'senin','16:00:00','2017-04-09 12:30:22'),('P-13',16,27,5,300000,'selasa','16:00:00','2017-04-09 12:33:22'),('P-14',16,26,5,280000,'selasa','16:00:00','2017-04-09 12:34:03'),('P-15',16,25,5,260000,'selasa','16:00:00','2017-04-09 12:34:25'),('P-16',16,24,5,240000,'selasa','16:00:00','2017-04-09 12:34:50'),('P-17',16,23,5,220000,'selasa','16:00:00','2017-04-09 12:36:32'),('P-18',16,22,5,210000,'selasa','16:00:00','2017-04-09 12:36:58'),('P-19',16,22,5,200000,'selasa','16:00:00','2017-04-09 12:37:49'),('P-2',16,26,6,280000,'senin','16:00:00','2017-04-09 12:21:41'),('P-20',16,21,5,190000,'selasa','16:00:00','2017-04-09 12:38:14'),('P-21',16,20,5,180000,'selasa','16:00:00','2017-04-09 12:39:38'),('P-23',16,19,5,160000,'selasa','16:00:00','2017-04-09 12:40:34'),('P-24',16,18,5,160000,'selasa','16:00:00','2017-04-09 12:41:27'),('P-25',16,17,5,150000,'selasa','16:00:00','2017-04-09 12:41:55'),('P-26',16,27,4,300000,'rabu','16:00:00','2017-04-09 12:44:13'),('P-27',16,26,4,280000,'rabu','16:00:00','2017-04-09 12:44:43'),('P-28',16,25,4,260000,'rabu','16:00:00','2017-04-09 12:45:08'),('P-29',16,24,4,240000,'rabu','16:00:00','2017-04-09 12:45:46'),('P-3',16,25,6,260000,'senin','16:00:00','2017-04-09 12:23:06'),('P-30',16,23,4,220000,'rabu','16:00:00','2017-04-09 12:46:07'),('P-31',16,22,4,210000,'rabu','16:00:00','2017-04-09 12:46:36'),('P-32',16,21,4,200000,'rabu','16:00:00','2017-04-09 12:47:49'),('P-33',16,20,4,190000,'rabu','16:00:00','2017-04-09 12:48:24'),('P-34',16,19,4,180000,'rabu','16:00:00','2017-04-09 12:50:09'),('P-35',16,18,4,160000,'rabu','16:00:00','2017-04-09 12:50:48'),('P-36',16,17,4,150000,'rabu','16:00:00','2017-04-09 12:51:15'),('P-37',16,27,2,300000,'kamis','16:00:00','2017-04-09 12:52:15'),('P-38',16,26,2,280000,'kamis','16:00:00','2017-04-09 12:52:40'),('P-39',16,25,2,260000,'kamis','16:00:00','2017-04-09 12:53:36'),('P-4',16,24,6,240000,'senin','16:00:00','2017-04-09 12:23:49'),('P-40',16,24,2,240000,'kamis','16:00:00','2017-04-09 12:54:00'),('P-41',16,23,2,220000,'kamis','16:00:00','2017-04-09 12:54:28'),('P-42',16,22,2,210000,'kamis','16:00:00','2017-04-09 12:55:32'),('P-43',16,21,2,200000,'kamis','16:00:00','2017-04-09 12:56:12'),('P-44',16,20,2,190000,'kamis','16:00:00','2017-04-09 12:56:35'),('P-45',16,19,2,170000,'kamis','16:00:00','2017-04-09 13:01:22'),('P-46',16,18,2,170000,'kamis','16:00:00','2017-04-09 13:01:51'),('P-47',16,17,2,150000,'kamis','16:00:00','2017-04-09 13:02:14'),('P-48',16,27,7,300000,'jumat','16:00:00','2017-04-09 13:03:51'),('P-49',16,26,7,280000,'jumat','16:00:00','2017-04-09 13:04:46'),('P-5',16,24,6,220000,'senin','16:00:00','2017-04-09 12:24:37'),('P-50',16,25,7,260000,'jumat','16:00:00','2017-04-09 13:05:11'),('P-51',16,24,7,240000,'jumat','16:00:00','2017-04-09 13:05:31'),('P-52',16,23,7,220000,'jumat','16:00:00','2017-04-09 13:06:06'),('P-53',16,22,7,210000,'jumat','08:00:00','2017-04-09 13:06:25'),('P-54',16,21,7,200000,'jumat','16:00:00','2017-04-09 13:07:48'),('P-55',16,20,7,190000,'jumat','16:00:00','2017-04-09 13:08:09'),('P-56',16,20,7,180000,'jumat','16:00:00','2017-04-09 13:08:32'),('P-57',16,19,7,180000,'jumat','16:00:00','2017-04-09 13:08:52'),('P-58',16,18,7,170000,'jumat','16:00:00','2017-04-09 13:09:11'),('P-59',16,17,7,150000,'jumat','16:00:00','2017-04-09 13:09:56'),('P-6',16,23,6,210000,'senin','16:00:00','2017-04-09 12:26:10'),('P-60',17,18,6,50000,'selasa','09:00:00','2017-04-09 13:21:42'),('P-61',17,19,6,60000,'selasa','13:00:00','2017-04-09 13:54:56'),('P-7',16,22,6,200000,'senin','16:00:00','2017-04-09 12:26:40'),('P-8',16,21,6,190000,'senin','16:00:00','2017-04-09 12:28:03'),('P-9',16,20,6,180000,'senin','16:00:00','2017-04-09 12:28:36');

/*Table structure for table `pemberangkatan_rute` */

DROP TABLE IF EXISTS `pemberangkatan_rute`;

CREATE TABLE `pemberangkatan_rute` (
  `id_pemberangkatan` varchar(20) CHARACTER SET latin1 NOT NULL,
  `id_kota` bigint(20) NOT NULL,
  KEY `id_pemberangkatan` (`id_pemberangkatan`),
  KEY `id_kota` (`id_kota`),
  CONSTRAINT `pemberangkatan_rute_ibfk_1` FOREIGN KEY (`id_pemberangkatan`) REFERENCES `pemberangkatan` (`id_pemberangkatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pemberangkatan_rute_ibfk_2` FOREIGN KEY (`id_kota`) REFERENCES `kota` (`id_kota`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `pemberangkatan_rute` */

/*Table structure for table `pengiriman` */

DROP TABLE IF EXISTS `pengiriman`;

CREATE TABLE `pengiriman` (
  `id_pengiriman` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `tanggal_order` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `no_telp_pengirim` varchar(25) DEFAULT NULL,
  `no_telp_tujuan` varchar(25) DEFAULT NULL,
  `alamat_tujuan` varchar(1000) DEFAULT NULL,
  `id_harga_pengiriman` bigint(20) DEFAULT NULL,
  `berat` float DEFAULT NULL,
  `harga_per_kilo` float DEFAULT NULL,
  `total_harga` float DEFAULT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `estimasi_tanggal_sampai` date DEFAULT NULL,
  `tanggal_sampai` date DEFAULT NULL,
  `posisi_terakhir` bigint(20) DEFAULT NULL,
  `status_pengiriman` enum('1','0') DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pengiriman`),
  KEY `id_harga_pengiriman` (`id_harga_pengiriman`),
  KEY `user_id` (`user_id`),
  KEY `posisi_terakhir` (`posisi_terakhir`),
  CONSTRAINT `pengiriman_ibfk_1` FOREIGN KEY (`id_harga_pengiriman`) REFERENCES `harga_pengiriman` (`id_harga_pengiriman`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pengiriman_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `login` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pengiriman_ibfk_3` FOREIGN KEY (`posisi_terakhir`) REFERENCES `kota` (`id_kota`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `pengiriman` */

insert  into `pengiriman`(`id_pengiriman`,`user_id`,`tanggal_order`,`no_telp_pengirim`,`no_telp_tujuan`,`alamat_tujuan`,`id_harga_pengiriman`,`berat`,`harga_per_kilo`,`total_harga`,`tanggal_kirim`,`estimasi_tanggal_sampai`,`tanggal_sampai`,`posisi_terakhir`,`status_pengiriman`,`modified_on`) values (2,7,'2017-03-17 21:36:46','09898876765','098987876','asdasd',4,10,40000,400000,'2017-03-17',NULL,NULL,NULL,'1','2017-03-17 21:36:46');

/*Table structure for table `penyewaan` */

DROP TABLE IF EXISTS `penyewaan`;

CREATE TABLE `penyewaan` (
  `id_penyewaan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_sewa_bus` bigint(20) DEFAULT NULL,
  `id_supir` bigint(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `tanggal_order` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tanggal_pemakaian` date DEFAULT NULL,
  `tanggal_kembali` date DEFAULT NULL,
  `jumlah_hari` int(11) DEFAULT NULL,
  `harga_perhari` float DEFAULT NULL,
  `total_harga` float DEFAULT NULL,
  `total_bayar` float DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `tanggal_status_penyewaan` datetime DEFAULT NULL,
  `status_penyewaan` enum('1','0') DEFAULT '0',
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_penyewaan`),
  KEY `id_sewa_bus` (`id_sewa_bus`),
  KEY `user_id` (`user_id`),
  KEY `id_supir` (`id_supir`),
  CONSTRAINT `penyewaan_ibfk_1` FOREIGN KEY (`id_sewa_bus`) REFERENCES `sewa_bus` (`id_sewa_bus`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `penyewaan_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `login` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `penyewaan_ibfk_3` FOREIGN KEY (`id_supir`) REFERENCES `supir` (`id_supir`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `penyewaan` */

insert  into `penyewaan`(`id_penyewaan`,`id_sewa_bus`,`id_supir`,`user_id`,`tanggal_order`,`tanggal_pemakaian`,`tanggal_kembali`,`jumlah_hari`,`harga_perhari`,`total_harga`,`total_bayar`,`tanggal_bayar`,`tanggal_status_penyewaan`,`status_penyewaan`,`modified_on`) values (3,5,1,7,'2017-03-17 21:18:56','2017-03-27','2017-03-28',1,2000000,2000000,NULL,NULL,'2017-03-17 21:34:46','1','2017-03-17 21:18:56');

/*Table structure for table `provinsi` */

DROP TABLE IF EXISTS `provinsi`;

CREATE TABLE `provinsi` (
  `id_provinsi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_provinsi` varchar(100) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `provinsi` */

insert  into `provinsi`(`id_provinsi`,`nama_provinsi`,`modified_on`) values (3,'Jambi','2016-06-11 22:28:20'),(4,'DI Yogyakarta','2016-06-11 22:28:29'),(5,'Sumatera Selatan','2017-04-07 20:38:19'),(6,'Lampung','2017-04-07 20:56:01');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(150) NOT NULL,
  `login_destination` varchar(500) NOT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id_role`,`role_name`,`login_destination`,`modified_on`) values (1,'admin','admin','2016-06-18 20:55:39'),(2,'user','index.php/?m=profil&c=viewProfil&a=view','2016-06-18 20:55:48'),(3,'petugas','admin','2016-08-03 21:43:02');

/*Table structure for table `role_page` */

DROP TABLE IF EXISTS `role_page`;

CREATE TABLE `role_page` (
  `id_role` int(11) NOT NULL,
  `page` varchar(150) NOT NULL,
  PRIMARY KEY (`id_role`,`page`),
  KEY `page` (`page`),
  CONSTRAINT `role_page_ibfk_1` FOREIGN KEY (`id_role`) REFERENCES `role` (`id_role`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_page_ibfk_2` FOREIGN KEY (`page`) REFERENCES `page` (`page`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `role_page` */

insert  into `role_page`(`id_role`,`page`) values (1,'armada'),(1,'dashboard'),(3,'dashboard'),(1,'harga_pengiriman'),(3,'harga_pengiriman'),(1,'kelas'),(1,'kota'),(1,'kurir'),(1,'laporan'),(1,'nomerKursi'),(1,'paket_pengiriman'),(1,'pembayaran'),(3,'pembayaran'),(1,'pemberangkatan'),(3,'pemberangkatan'),(1,'penumpang'),(3,'penumpang'),(1,'provinsi'),(1,'role'),(1,'setting'),(1,'sewaBus'),(3,'sewaBus'),(1,'supir'),(3,'supir'),(1,'user');

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `id` varchar(15) NOT NULL,
  `label` varchar(250) DEFAULT NULL,
  `set` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `setting` */

insert  into `setting`(`id`,`label`,`set`) values ('BP','batas akhir pembatalan sewa bus','3'),('BT','batas akhir pembatalan tiket','1'),('PT','batas akhir pembayaran tiket','3'),('PTK','batas akhir pembatalan tiket setelah dikonfirmasi','3');

/*Table structure for table `sewa_bus` */

DROP TABLE IF EXISTS `sewa_bus`;

CREATE TABLE `sewa_bus` (
  `id_sewa_bus` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_armada` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sewa_bus`),
  KEY `FK_sewa_bus` (`id_armada`),
  CONSTRAINT `sewa_bus_ibfk_1` FOREIGN KEY (`id_armada`) REFERENCES `armada` (`id_armada`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `sewa_bus` */

insert  into `sewa_bus`(`id_sewa_bus`,`id_armada`,`harga`,`modified_on`) values (4,2,1500000,'2016-06-18 15:10:55'),(5,5,2000000,'2016-06-28 22:36:51');

/*Table structure for table `supir` */

DROP TABLE IF EXISTS `supir`;

CREATE TABLE `supir` (
  `id_supir` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `alamat` varchar(500) NOT NULL DEFAULT '-',
  `jenis_kelamin` enum('L','P') NOT NULL DEFAULT 'L',
  `no_hp` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `aktif` enum('1','0') DEFAULT NULL,
  PRIMARY KEY (`id_supir`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `supir` */

insert  into `supir`(`id_supir`,`nama`,`alamat`,`jenis_kelamin`,`no_hp`,`email`,`aktif`) values (1,'Anton','Cilacap','L','087987876765','anton@familyceria.com','1'),(2,'Agus','Yogyakarta','L','098987876','as@mail.com','1');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
