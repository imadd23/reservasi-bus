-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2016 at 06:58 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bus`
--

-- --------------------------------------------------------

--
-- Table structure for table `armada`
--

CREATE TABLE IF NOT EXISTS `armada` (
  `Nama_armada` varchar(20) NOT NULL,
  PRIMARY KEY (`Nama_armada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembelian`
--

CREATE TABLE IF NOT EXISTS `detail_pembelian` (
  `id_pembeliantiket` varchar(20) DEFAULT NULL,
  `id_armada` varchar(20) DEFAULT NULL,
  KEY `FK_detail_pembelian` (`id_pembeliantiket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE IF NOT EXISTS `jurusan` (
  `id_jurusan` varchar(20) NOT NULL,
  `kota_tujuan` varchar(20) DEFAULT NULL,
  `id_kelas` varchar(20) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `kota_asal` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_jurusan`),
  KEY `FK_jurusan` (`id_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `kota_tujuan`, `id_kelas`, `harga`, `kota_asal`) VALUES
('1', 'lampung', NULL, 360000, 'yogyakarta'),
('2', 'lahat', NULL, 380000, 'lampung');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `jenis_kelas` varchar(20) NOT NULL,
  PRIMARY KEY (`jenis_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`user_id`, `username`, `password`) VALUES
(1, 'Petugas', '123'),
(0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_tiket`
--

CREATE TABLE IF NOT EXISTS `pembelian_tiket` (
  `Id_pembeliantiket` varchar(20) NOT NULL,
  `tanggal_beli` varchar(20) DEFAULT NULL,
  `nama_pelanggan` varchar(20) DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `kota_tujuan` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Id_pembeliantiket`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pemberangkatan`
--

CREATE TABLE IF NOT EXISTS `pemberangkatan` (
  `jadwal` varchar(20) NOT NULL,
  `asal` varchar(20) DEFAULT NULL,
  `tujuan` varchar(20) DEFAULT NULL,
  `bus` varchar(20) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `jam` time DEFAULT NULL,
  PRIMARY KEY (`jadwal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE IF NOT EXISTS `petugas` (
  `Nama_petugas` varchar(20) NOT NULL,
  `alamat` varchar(40) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `jenis_kelamin` varchar(11) DEFAULT NULL,
  `Email` varchar(40) DEFAULT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Nama_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sewa_bus`
--

CREATE TABLE IF NOT EXISTS `sewa_bus` (
  `id_sewabus` varchar(20) NOT NULL,
  `id_armada` varchar(20) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_sewabus`),
  KEY `FK_sewa_bus` (`id_armada`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_pembelian`
--
ALTER TABLE `detail_pembelian`
  ADD CONSTRAINT `FK_detail_pembelian` FOREIGN KEY (`id_pembeliantiket`) REFERENCES `pembelian_tiket` (`Id_pembeliantiket`);

--
-- Constraints for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `FK_jurusan` FOREIGN KEY (`id_kelas`) REFERENCES `kelas` (`id_kelas`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
