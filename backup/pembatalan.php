<div class="list-group" >
        <div class="list-group-item active" >
          <h4 class="list-group-item-heading" style="color:white;font-family:verdana;font-size:15px;"><b>Pembatalan Tiket</b><p style="font-size:12px;"><i>Canceling Ticket</i></p></h4>
        </div>
        <div class="list-group-item" style="font-family:verdana;font-size:13px;">
		<style type="text/css">

	      .form-signin {
	        max-width: 300px;
	        padding: 19px 29px 29px;
	        margin: 0 auto 20px;
	        background-color: #fff;
	        border: 1px solid #e5e5e5;
	        -webkit-border-radius: 5px;
	           -moz-border-radius: 5px;
	                border-radius: 5px;
	        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
	                box-shadow: 0 1px 2px rgba(0,0,0,.05);
	      }
	      .form-signin .form-signin-heading,
	      .form-signin .checkbox {
	        margin-bottom: 10px;
	      }
	      .form-signin input[type="text"],
	      .form-signin input[type="password"] {
	        font-size: 16px;
	        height: auto;
	        margin-bottom: 15px;
	        padding: 7px 9px;
	      }

	    </style>
			<?php
					// check for a successful form post
					if (isset($_GET['e'])) echo "<div class=\"alert alert-error\">".$_GET['e']."</div>";
			?> 

      <form class="form-signin" action="media.php?p=cancel" method="POST">
	  	<center>
        <h2 class="form-signin-heading" style="font-family:verdana; font-size:14px;"><b>Silahkan login</b></h2>
        <input type="text" class="input-block-level" name="userid" pattern="[A-z ]{3,}" title="Please enter a valid username." placeholder="User ID" required>
        <input type="password" class="input-block-level" name="password" placeholder="Password" required>
		<input type="hidden" name="save" value="signin">
		<button type="submit" class="btn btn-info">
			<i class="icon-ok icon-white"></i> Login
		</button>
		<button type="reset" class="btn">
			<i class="icon-refresh icon-black"></i> Clear
		</button>
		</center>
      </form>

    </div>
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>window.jQuery || document.write('<script src="_assets/js/jquery-latest.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="_assets/js/bootstrap.js"></script>
</div>