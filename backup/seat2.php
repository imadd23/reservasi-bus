<!--
<?php error_reporting(0) ?>
<Ticket-Booking>
Copyright (C) <2013>  
<Abhijeet Ashok Muneshwar>
<openingknots@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
 any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->

<div class="list-group">
        <div class="list-group-item active">
			<h4 class="list-group-item-heading" style="color:white;font-family:verdana;font-size:15px;"><b>Lengkapi Form Booking Anda</b><p style="font-size:12px;"><i>Finished Your Booking Form</i></p></h4>
        </div>
        <div class="list-group-item" style="font-family:verdana;">
		 <table class="table">
				<div class="span5" style="margin-left:0px;">
					<form action="media.php?p=book" method="POST" onsubmit="return validateCheckBox();">
						
						<ul class="thumbnails">
						<?php
							$date = strip_tags( utf8_decode( $_POST['wkt_plan'] ) );
							$query = "select * from seat where date = '" . $date . "'";
							$result = mysql_query($query);
							if ( mysql_num_rows($result) == 0 )
							{
								for($i=1; $i<31; $i++)
								{
									echo "<div class='col-md-3'>";
										echo "<a href='#' class='thumbnail' title='Available' style='margin-bottom:0px;'>";
											echo "<img src='_assets/img/Available.png' alt='Available Seat'/>";
											echo "<label class='checkbox'>";
												echo "<input type='checkbox' name='ch".$i."'/>No".$i;
											echo "</label>";
										echo "</a>";
									echo "</div>";								
								}
							}
							else
							{
								$seats = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
								while($row = mysql_fetch_array($result))
								{
									$pnr = explode("-", $row['PNR']);
									$pnr[3] = intval($pnr[3]) - 1;
									$seats[$pnr[3]] = 1;
								}
								for($i=1; $i<31; $i++)
								{
									$j = $i - 1;
									if($seats[$j] == 1)
									{
										echo "<li class='span1'>";
											echo "<a href='#' class='thumbnail' title='Booked' style='margin-bottom:0px;'>";
												echo "<img src='_assets/img/occupied.png' alt='Booked Seat'/>";
												echo "<label class='checkbox'>";
													echo "<input type='checkbox' name='ch".$i."' disabled/>No".$i;
												echo "</label>";
											echo "</a>";
										echo "</li>";
									}
									else
									{
										echo "<li class='span1'>";
											echo "<a href='#' class='thumbnail' title='Available' style='margin-bottom:0px;'>";
												echo "<img src='_assets/img/Available.png' alt='Available Seat'/>";
												echo "<label class='checkbox'>";
													echo "<input type='checkbox' name='ch".$i."'/>No".$i;
												echo "</label>";
											echo "</a>";
										echo "</li>";
									}
								}									
								
							}
						?>
						</ul>
						
						Keterangan :
						<br>
								<img src="_assets/img/Available.png" width=50 alt="Available Seat" value="tersedia" />= Tersedia
								<img src="_assets/img/occupied.png" width=50 alt="occupied Seat" value="Booked" />= Terisi							
						<left>
						<div class="form-group">
							<label for="date" class="col-sm-2 control-label">Tanggal</label>
								<div class="col-sm-10">
								<label>
								<?php
									echo "<input type='text' class='form-control' name='wkt_plan' value='". $date ."' readonly/>";
								?>
								</label>
							</div>
						</div>
							<br><br>
							<button type="submit" class="btn btn-info">
								<i class="icon-ok icon-white"></i> Kirim
							</button>
							<button type="reset" class="btn btn-default">
								<i class="icon-refresh icon-black"></i> Bersih
							</button>
							<a href="media.php?p=seat" class="btn btn-danger"><i class="icon-arrow-left icon-white"></i> Back </a>
						</left>
					</form>
				</div>
				</table>
				</div>
				</div>
				
		<script src="http://code.jquery.com/jquery-latest.min.js"></script>
		<script>window.jQuery || document.write('<script src="_assets/js/jquery-latest.min.js">\x3C/script>')</script>
		<script type="text/javascript" src="_assets/js/bootstrap.js"></script>
		<script type="text/javascript">
			function validateCheckBox()
			{
				var c = document.getElementsByTagName('input');
				for (var i = 0; i < c.length; i++)
				{
					if (c[i].type == 'checkbox')
					{
						if (c[i].checked) 
						{
							return true;
						}
					}
				}
				alert('Silahkan Pilih Dulu.');
				return false;
			}
		</script>
	</BODY>
</HTML>