<?php
/* initialize ROOTPATH */
if (!defined('ROOTPATH'))
	define('ROOTPATH', dirname(__FILE__));
/* end initialize ROOTPATH */

/* require config */
require_once 'config/config.php';
require_once 'config/session.php';
require_once 'libraries/loader/Library.class.php';
/* end require config */

/* login handling */
if(((!isset($_GET['m']) || !isset($_GET['c']) || !isset($_GET['a'])) && !isset($_SESSION['isLoggedIn'])) || 
	(isset($_SESSION['isLoggedIn']) && $_SESSION['isLoggedIn'] == FALSE)){
	$module = DEFAULT_MODULE;
	$controller = DEFAULT_CONTROLLER;
	$action = 'VIEW';
}else{
	$module = isset($_GET['m']) ? $_GET['m'] : 'dashboard';
	$controller = isset($_GET['c']) ? $_GET['c'] : 'viewDashboard';
	$action = isset($_GET['a']) ? $_GET['a'] : 'view';
}
/* end login handling */

/* handling module*/
$realModulePath = MODULE_PATH.$module.'/'.$controller.EXT;
if (!file_exists($realModulePath)){
	echo '<center><bold>404</bold> Module '.$module.', controller '.$controller.EXT.' tidak ditemukan di folder '.MODULE_PATH.'</center>';
	exit();
}
/* end hendling module */

/* security handling */
if ($module != 'login' && isset($_SESSION['id_role'])){
	Library::load('security.class.php', 'security');
	$security = new security($_SESSION['id_role']);
	$security->restrict($module);
}else
if ($module != 'login' && !isset($_SESSION['id_role'])){
	$front_page = str_replace('/admin', '', BASE_URL);
	header('location:'.$front_page);
	die();
}	
/* end security handling */

/* content begin */
if (strtoupper($module) == 'LOGIN' || strtoupper($action) == 'DO'){
	include $realModulePath;
}else
if (strtoupper($action) == 'VIEW'){
	include 'header.php';
	include $realModulePath;
	include 'footer.php';
}else{
	echo '<center><bold>404</bold> Aksi (view/do) tidak ditemukan</center>';
	exit();
}
/* end of content */
?>
				
	