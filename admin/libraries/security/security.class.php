<?php
/* library for Security Access */
Class Security{

	var $rolePage;

	function __construct($roleId){
		$this->rolePage = $this->getPage($roleId);
	}

	public function getPage($roleId){
		$query = mysql_query("SELECT r.*, GROUP_CONCAT(p.page) AS module
			FROM role AS r
			LEFT JOIN role_page AS rp ON rp.id_role = r.`id_role`
			LEFT JOIN page AS p ON rp.page = p.page
			WHERE r.id_role = '".$roleId."'
			GROUP BY `role_name`");

		$result = mysql_fetch_object($query);
		$pages = empty($result) ? array() : explode(',', $result->module); 
		return $pages;
	}

	public function restrict($moduleName = ''){
		if ($moduleName != 'login' && $moduleName != 'worker' && !in_array($moduleName, $this->rolePage)){
			die('Anda tidak memiliki hak akses untuk halaman ini');
		}
	}

	public function showMenu($moduleName, $link){
		if (in_array($moduleName, $this->rolePage)){
			echo $link;
		}
		return;
	}
}
?>