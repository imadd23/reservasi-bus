<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<title>FAMILY RAYA CERIA</title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/style.css';?>">
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/resp.css';?>"> -->
		<!-- font-awesome -->
		<link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/font-awesome.min.css';?>">
		<!-- bootstrap css -->
		<link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/bootstrap.min.css';?>">
		<link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/AdminLTE.css';?>">
		<link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/ionicons/css/ionicons.min.css';?>">
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/custom.css';?>">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datatables/jquery.dataTables.css';?>">-->
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datatables/jquery.dataTables.min.css';?>">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datatables/dataTables.bootstrap.css';?>">-->
		<!-- jQuery framework js -->
		<script src="<?php echo BASE_ADDRESS.'js/jquery-1.11.1.min.js';?>"></script>
		<script src="<?php echo BASE_ADDRESS.'js/bootstrap.min.js';?>"></script>
		<script src="<?php echo BASE_ADDRESS.'js/datatables/jquery.dataTables.min.js';?>"></script>
	</head>
	<body>
	<div id="wrp">
		<div id="header">
			<div id="logo"><img src="<?php echo BASE_ADDRESS.'images/1.jpg';?>" width="100%" height="100%"></div>
		</div>
		<div style="clear:both"></div>
									
		<div class="td_konten">
			<div id="content-center">
				<!-- <nav style="margin-bottom:0px;" class="navbar navbar-inverse"> -->
				<nav style="margin-bottom:0px;" class="navbar navbar-default">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <a class="navbar-brand" href="<?php echo BASE_URL.'?m=dashboard&c=viewDashboard&a=view';?>">Administrator</a>
				    </div>
				    <ul class="nav navbar-nav">
				    	<?php ob_start();?>
				    	<li <?php if (!isset($_GET['m']) || ((isset($_GET['m']) && $_GET['m'] == 'dashboard'))) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=dashboard&c=viewDashboard&a=view';?>">Beranda</a></li>
				    	<?php $linkDashboard = ob_get_clean();?>
				    	<?php $security->showMenu('dashboard', $linkDashboard);?>

				      	<li <?php if ((isset($_GET['m']) && in_array($_GET['m'], array('user', 'armada', 'nomerKursi', 'provinsi', 'kota', 'pemberangkatan', 'kelas', 'bank', 'role')))) echo 'class="dropdown active"'; else echo 'class="dropdown"';?>>
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Data Master
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					        	<?php ob_start(); ?>
					        	<li><a href="<?php echo BASE_URL.'?m=user&c=viewUser&a=view';?>">User</a></li>
					        	<?php $linkUser = ob_get_clean();?>
					        	<?php $security->showMenu('user', $linkUser);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=role&c=viewRole&a=view';?>">Hak Akses</a></li>
					        	<?php $linkrole = ob_get_clean();?>
					        	<?php $security->showMenu('role', $linkrole);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=armada&c=viewArmada&a=view';?>">Armada</a></li>
					        	<?php $linkArmada = ob_get_clean();?>
					        	<?php $security->showMenu('armada', $linkArmada);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=supir&c=viewSupir&a=view';?>">Supir</a></li>
					        	<?php $linkSupir = ob_get_clean();?>
					        	<?php $security->showMenu('supir', $linkSupir);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=nomerKursi&c=viewNomerKursi&a=view';?>">Nomer Kursi</a></li>
					        	<?php $linkNomerKursi = ob_get_clean();?>
					        	<?php $security->showMenu('nomerKursi', $linkNomerKursi);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=provinsi&c=viewProvinsi&a=view';?>">Provinsi</a></li>
					        	<?php $linkProvinsi = ob_get_clean();?>
					        	<?php $security->showMenu('provinsi', $linkProvinsi);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=kota&c=viewKota&a=view';?>">Kota</a></li>
					        	<?php $linkKota = ob_get_clean();?>
					        	<?php $security->showMenu('kota', $linkKota);?>

					        	<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=pemberangkatan&c=viewPemberangkatan&a=view';?>">Pemberangkatan</a></li>
								<?php $linkPemberangkatan = ob_get_clean();?>
								<?php $security->showMenu('pemberangkatan', $linkPemberangkatan);?>

								<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=paket_pengiriman&c=viewPaket&a=view';?>">Referensi Nama Paket</a></li>
								<?php $linkPaket = ob_get_clean();?>
								<?php $security->showMenu('paket_pengiriman', $linkPaket);?>

								<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=harga_pengiriman&c=viewHargaPengiriman&a=view';?>">Harga Pengiriman</a></li>
								<?php $linkHarga = ob_get_clean();?>
								<?php $security->showMenu('harga_pengiriman', $linkHarga);?>

								<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=kelas&c=viewKelas&a=view';?>">Kelas</a></li>
								<?php $linkKelas = ob_get_clean();?>
								<?php $security->showMenu('kelas', $linkKelas);?>

								<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=bank&c=viewBank&a=view';?>">Bank</a></li>
								<?php $linkBank = ob_get_clean();?>
								<?php $security->showMenu('bank', $linkBank);?>

								<?php ob_start();?>
								<li><a href="<?php echo BASE_URL.'?m=setting&c=viewSetting&a=view';?>">Pengaturan</a></li>
								<?php $linkPengaturan = ob_get_clean();?>
								<?php $security->showMenu('setting', $linkPengaturan);?>
					        </ul>
				      	</li>

				      	<?php ob_start();?>
				      	<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'penumpang')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=penumpang&c=viewPenumpang&a=view';?>">Penumpang</a></li>
				      	<?php $linkPenumpang = ob_get_clean();?>
				      	<?php $security->showMenu('penumpang', $linkPenumpang);?>

				      	<?php ob_start();?>
				   		<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'pembayaran')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view';?>">Konfirmasi Pembayaran Tiket</a></li>
				   		<?php $linkPembayaran = ob_get_clean();?>
				   		<?php $security->showMenu('pembayaran', $linkPembayaran);?>

				   		<?php ob_start();?>
				   		<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'sewaBus')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=sewaBus&c=viewSewaBus&a=view';?>">Penyewaan Bus</a></li>
				   		<?php $linkSewaBus = ob_get_clean();?>
				   		<?php $security->showMenu('sewaBus', $linkSewaBus);?>

				   		<?php ob_start();?>
				   		<li <?php if ((isset($_GET['m']) && $_GET['m'] == 'kurir')) echo 'class="active"';?>><a href="<?php echo BASE_URL.'?m=kurir&c=viewKurir&a=view';?>">Kurir</a></li>
				   		<?php $linkKurir = ob_get_clean();?>
				   		<?php $security->showMenu('kurir', $linkKurir);?>

				   		<li <?php if ((isset($_GET['m']) && in_array($_GET['m'], array('laporan', 'laporan_penumpang', 'laporan_pembayaran_tiket', 'laporan_sewa_bus', 'laporan_kurir')))) echo 'class="dropdown active"'; else echo 'class="dropdown"';?>>
					        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Laporan
					        <span class="caret"></span></a>
					        <ul class="dropdown-menu">
					        	<?php ob_start(); ?>
					        	<li><a href="<?php echo BASE_URL.'?m=laporan&c=viewLaporanPenumpang&a=view';?>">Laporan Penumpang</a></li>
					        	<?php $links = ob_get_clean();?>
					        	<?php $security->showMenu('laporan', $links);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=laporan&c=viewLaporanSewaBus&a=view';?>">Laporan Sewa Bus</a></li>
					        	<?php $links = ob_get_clean();?>
					        	<?php $security->showMenu('armada', $links);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=laporan&c=viewLaporanKurir&a=view';?>">Laporan Kurir</a></li>
					        	<?php $links = ob_get_clean();?>
					        	<?php $security->showMenu('kurir', $links);?>

					        	<?php ob_start();?>
					        	<li><a href="<?php echo BASE_URL.'?m=laporan&c=viewLaporanNoKursi&a=view';?>">Laporan No. Kursi</a></li>
					        	<?php $links = ob_get_clean();?>
					        	<?php $security->showMenu('laporan', $links);?>
					        </ul>
				      	</li>
				    </ul>
				    <ul class="nav navbar-nav navbar-right">
				      <li><a href="<?php echo BASE_URL.'?m=login&c=doLogout&a=do';?>">Logout</a></li>
				    </ul>
				  </div>
				</nav>
			</div>
			<div id="content-center">
				<div class="content-isi">
				    <?php echo 'Selamat Datang '.$_SESSION['username'];?>
				</div>
			</div>
			<div id="content-center">
				<div class="content-isi">