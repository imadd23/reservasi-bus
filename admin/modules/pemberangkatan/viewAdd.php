<?php
$qKota = mysql_query(" SELECT id_kota, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS nama_kota 
FROM kota 
LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
ORDER BY nama_kota ASC");
$dataKota = array();
while ($arrayKota = mysql_fetch_array($qKota)){
    $dataKota[] = $arrayKota;
}

$qArmada = mysql_query("SELECT * FROM armada ORDER BY nama_armada ASC");
$dataArmada = array();
while ($arrayArmada = mysql_fetch_array($qArmada)) {
    $dataArmada[] = $arrayArmada;
}

$dataHari = array('senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu');

if (isset($_GET['harga'])){
    $id_kota_asal = $_GET['id_kota_asal'];
    $id_kota_tujuan = $_GET['id_kota_tujuan'];
    $id_armada = $_GET['id_armada'];
    $harga = $_GET['harga'];
    $hari = $_GET['hari'];
    $jam = $_GET['jam'];
    $jam_sampai = $_GET['jam_sampai'];
    $ref_id = (isset($_GET['ref_id'])) ? '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">' : '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM pemberangkatan WHERE id_pemberangkatan = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $id_kota_asal = $a->id_kota_asal;
        $id_kota_tujuan = $a->id_kota_tujuan;
        $id_armada = $a->id_armada;
        $harga = $a->harga;
        $hari = $a->hari;
        $jam = $a->jam;
        $jam_sampai = $a->jam_sampai;
    }else{
        $id_kota_asal = '';
        $id_kota_tujuan = '';
        $id_armada = '';
        $harga = '';
        $hari = '';
        $jam = '';
        $jam_sampai = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $id_kota_asal = '';
    $id_kota_tujuan = '';
    $id_armada = '';
    $harga = '';
    $hari = '';
    $jam = '';
    $jam_sampai = '';
    $ref_id = '';
}

$message = '';
if ((isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0') || (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0')){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/jquery.timepicker.css';?>">
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/jquery.timepicker.js';?>"></script>
<div class="col-sm-7">
    <h3>Tambah data pemberangkatan</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=pemberangkatan&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Kota Asal *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_kota_asal" id="id_kota_asal">
                    <option value="">Pilih Kota</option>
                    <?php
                    if (!empty($dataKota)){
                        foreach ($dataKota as $key => $value) {
                            if ($value['id_kota'] == $id_kota_asal){
                                echo '<option value="'.$value['id_kota'].'" selected>'.$value['nama_kota'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_kota'].'">'.$value['nama_kota'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Kota Tujuan *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_kota_tujuan" id="id_kota_tujuan">
                    <option value="">Pilih Kota</option>
                    <?php
                    if (!empty($dataKota)){
                        foreach ($dataKota as $key => $value) {
                            if ($value['id_kota'] == $id_kota_tujuan){
                                echo '<option value="'.$value['id_kota'].'" selected>'.$value['nama_kota'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_kota'].'">'.$value['nama_kota'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Armada *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_armada" id="id_armada">
                    <option value="">Pilih Armada</option>
                    <?php
                    if (!empty($dataArmada)){
                        foreach ($dataArmada as $key => $value) {
                            if ($value['id_armada'] == $id_armada){
                                echo '<option value="'.$value['id_armada'].'" selected>'.$value['nama_armada'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_armada'].'">'.$value['nama_armada'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Harga *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="harga" id="harga" value="<?php echo $harga;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Hari *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="hari" id="hari">
                    <option value="">Pilih Hari</option>
                    <?php
                    if (!empty($dataHari)){
                        foreach ($dataHari as $key => $value) {
                            if ($value == $hari){
                                echo '<option value="'.$value.'" selected>'.ucwords($value).'</option>';
                            }else{
                                echo '<option value="'.$value.'">'.ucwords($value).'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Jam Berangkat *</label>
            <div class="col-sm-9">
                <input type="date" class="input-sm form-control" name="jam" id="jam" value="<?php echo $jam;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Jam Sampai *</label>
            <div class="col-sm-9">
                <input type="date" class="input-sm form-control" name="jam_sampai" id="jam_sampai" value="<?php echo $jam_sampai;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=pemberangkatan&c=viewPemberangkatan&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#jam, #jam_sampai').timepicker({
            'timeFormat' : 'HH:mm',
            'startTime' : '06:00:00',
            'maxHour' : 23,
            'dropdown' : true,
            'scrollbar' : true,
            'dynamic' : false
        });
    });
</script>