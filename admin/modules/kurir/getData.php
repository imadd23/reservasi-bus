<?php
ob_start();
require_once 'modelKurir.class.php';
$model = new modelKurir();
$arrayKurir = $model->getListKurir();

if (!empty($arrayKurir)){
    foreach ($arrayKurir as $key => $value) {
        $arrayKurir[$key]['no'] = ($key + 1);
        $button = '';
        if ($value['status_pengiriman'] == '0'){
        	$button .= "<a class='btn-action btn-kirim' href='#' data-id='".$value['id_pengiriman']."' data-tgl-kirim='".$value['tanggal_kirim']."' data-tgl-sampai='".$value['tanggal_sampai']."'><img src='".BASE_ADDRESS."icon/checkbox.png' title='kirim'>&nbsp\n
        		<a class='btn-action' href='".BASE_URL."?m=kurir&c=viewAdd&a=view&ref_id=".$value['id_pengiriman']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'>&nbsp\n
        		</a><a class='btn-action btn-hapus' data-nama='".$value['id_pengiriman']."' href='".BASE_URL."?m=kurir&c=doDelete&a=do&ref_id=".$value['id_pengiriman']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>";
        }else
        if ($value['status_pengiriman'] == '1'){
        	$button .= "<a class='btn-action btn-kirim' href='#' data-id='".$value['id_pengiriman']."' data-tgl-kirim='".$value['tanggal_kirim']."' data-tgl-sampai='".$value['tanggal_sampai']."'><img src='".BASE_ADDRESS."icon/checkbox.png' title='kirim'>&nbsp\n
        		<a class='btn-action' target='_blank' href='".BASE_URL."?m=kurir&c=doPrintResi&a=do&ref_id=".$value['id_pengiriman']."'><img src='".BASE_ADDRESS."icon/print.png' title='cetak resi'>&nbsp\n";
        }
        $arrayKurir[$key]['aksi'] = $button;
    }
}
echo json_encode(array('data' => $arrayKurir));
exit();
?>