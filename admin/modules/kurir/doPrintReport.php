<?php
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
require_once 'modelKurir.class.php';
Library::load('html2pdf.php', 'html2pdf');
$model = new modelKurir();
$p = array(
	'from' => date('Y-m-d', strtotime($_REQUEST['from'])),
	'to' => date('Y-m-d', strtotime($_REQUEST['to'])),	
   );
$ekspedisi = $model->getListKurirForReport($p['from'], $p['to']);

ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.right{
		text-align: right;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding">REPORT PENGIRIMAN BARANG DARI TANGGAL <?php echo $_REQUEST['from'];?> S.D <?php echo $_REQUEST['to'];?></h3>
<hr class="dashed">
<table class="table-control table-bordered">
	<col style="width: 3%;">
    <col style="width: 10%;">
    <col style="width: 8%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
	<tr>
		<th class="center">No</th>
        <th>ID Pengiriman</th>
        <th>Paket</th>
        <th>Tanggal Kirim</th>
        <th>Nama Pengirim</th>
        <th>Nama Penerima</th>
        <th>Kota Asal</th>
        <th>Kota Tujuan</th>
        <th>Berat</th>
        <th class="center">Harga (Kg)</th>
        <th class="center">Total Biaya</th>
	</tr>
	<?php
	if (!empty($ekspedisi)){
		$total = 0;
		foreach ($ekspedisi as $key => $value) {
			$total += $value['total_harga'];
			echo 
			'<tr>'.
				'<td class="center">'.($key + 1).'</td>'.
				'<td>'.$value['id_pengiriman'].'</td>'.
				'<td>'.$value['nama_paket'].'</td>'.
				'<td>'.date('d M Y', strtotime($value['tanggal_kirim'])).'</td>'.
				'<td>'.$value['nama_pengirim'].'</td>'.
				'<td>'.$value['nama_penerima'].'</td>'.
				'<td>'.$value['kota_asal'].'</td>'.
				'<td>'.$value['kota_tujuan'].'</td>'.
				'<td>'.$value['berat'].'</td>'.
				'<td class="center">'.$value['harga_per_kilo'].'</td>'.
				'<td class="center">'.$value['total_harga'].'</td>'.
			'</tr>';
		}
		echo 
		'<tr>'.
			'<td class="right" colspan="10"><b>Total</b></td>'.
			'<td class="center"><b>'.$total.'</b></td>'.
		'</tr>';
	}else{
		echo 
		'<tr>'.
			'<td class="center" colspan="11">Data Kosong</td>'.
		'</tr>';
	}
	?>
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	$html2pdf = new html2pdf('L', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf->writeHTML($content);
	$html2pdf->Output('report-pengiriman-barang-'.date('dmy').'.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>