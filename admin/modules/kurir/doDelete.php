<?php
require_once 'modelKurir.class.php';
$model = new modelKurir();

$id_pengiriman = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$model->where(array('id_pengiriman' => $id_pengiriman));
$delete = $model->delete($id_pengiriman);
if ($delete){
	header('location: '.BASE_URL.'?m=kurir&c=viewKurir&a=view&statusDelete=true');
}else{
	header('location: '.BASE_URL.'?m=kurir&c=viewKurir&a=view&statusDelete=false');
}
?>