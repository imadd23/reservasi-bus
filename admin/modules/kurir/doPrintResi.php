<?php
require_once 'modelKurir.class.php';
Library::load('html2pdf.php', 'html2pdf');
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
$model = new modelKurir();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$detail = $model->getDetailPengirimanForPrint($refId);
if (empty($detail)){
	die('Data tidak ditemukan');
}
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding">RESI PENGIRIMAN BARANG</h3>
<hr>
<table>
	<tr>
		<th>No. Resi</th><td>: <?php echo $detail[0]['id_pengiriman'];?></td>
	</tr>
	<tr>
		<th>Tanggal Order</th><td>: <?php echo date('d M Y', strtotime($detail[0]['tanggal_order']));?></td>
	</tr>
	<tr>
		<th>Nama Pengirim</th><td>: <?php echo $detail[0]['nama_pengirim'].' ('.$detail[0]['no_telp_pengirim'].')';?></td>
	</tr>
	<tr>
		<th>Alamat Pengirim</th><td>: <?php echo $detail[0]['alamat_pengirim'];?></td>
	</tr>
	<tr>
		<th>Nama Penerima</th><td>: <?php echo $detail[0]['nama_penerima'].' ('.$detail[0]['no_telp_tujuan'].')';?></td>
	</tr>
	<tr>
		<th>Alamat Tujuan</th><td>: <?php echo $detail[0]['alamat_tujuan'];?></td>
	</tr>
</table>
<hr class="dashed">
<table class="table-control table-bordered">
	<col style="width: 5%;">
    <col style="width: 10%;">
    <col style="width: 15%;">
    <col style="width: 5%;">
    <col style="width: 10%;">
    <col style="width: 20%;">
    <col style="width: 15%;">
    <col style="width: 15%;">
    <col style="width: 5%;">
	<tr>
		<th>No</th>
		<th>Nama Paket</th>
		<th>Jenis Barang</th>
		<th>Berat</th>
		<th>Harga(Kg)</th>
		<th>Alamat Tujuan</th>
		<th>Origin</th>
		<th>Destination</th>
		<th>Total Harga</th>
	</tr>
	<?php
	foreach ($detail as $key => $value) {
		echo 
		'<tr>'.
			'<td>'.($key + 1).'</td>'.
			'<td>'.$value['nama_paket'].'</td>'.
			'<td>'.$value['jenis_barang'].'</td>'.
			'<td>'.$value['berat'].'</td>'.
			'<td>'.$value['harga'].'</td>'.
			'<td>'.$value['alamat_tujuan'].'</td>'.
			'<td>'.$value['kota_asal'].'</td>'.
			'<td>'.$value['kota_tujuan'].'</td>'.
			'<td>'.$value['total_harga'].'</td>'.
		'</tr>';
	}
	?>
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>Terima kasih atas kepercayaan Anda kepada PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	// $html2pdf = new html2pdf('P', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf = new html2pdf('L', array(250, 100), 'en', true, 'UTF-8', array(2, 2, 2, 2));
	$html2pdf->writeHTML($content);
	$html2pdf->Output('cetak.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>