<?php
require_once MODULE_PATH.'core/model.core.php';

class modelKurir extends model{

	public function __construct(){
		parent::__construct();
	}

	function getListKurir(){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			IFNULL(CONCAT(E.nama_kota, ' (', F.nama_provinsi, ')'), 'Tidak Diketahui') AS posisi_sekarang
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN kota AS E
				ON E.id_kota = pengiriman.posisi_terakhir
			LEFT JOIN provinsi AS F
				ON F.id_provinsi = E.id_provinsi
			ORDER BY pengiriman.`modified_on` DESC");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getListKurirForReport($from, $to){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			WHERE
				pengiriman.tanggal_kirim BETWEEN '".$from."' AND '".$to."'
			ORDER BY pengiriman.`modified_on` DESC");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	public function getUser(){
		$query = mysql_query("SELECT * FROM login WHERE id_role = '2'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getPaket(){
		$query = mysql_query("SELECT * FROM paket_pengiriman WHERE status = '1'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getHarga(){

		$query = mysql_query("SELECT * FROM harga_pengiriman ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getKota(){
		$query = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_tujuan
			FROM kota
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi".
			$this->wheres."
			ORDER BY CONCAT(nama_kota, ' (', nama_provinsi, ')') ASC ");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function add($newData = array()){
		$_newData = $this->setData($newData);
		$query = "INSERT INTO pengiriman SET ".$_newData;
		$query = mysql_query($query);
		return $query;
	}

	public function update($data = array()){
		$newData = $this->setData($data);
		$query = "UPDATE pengiriman SET ".$newData." ".$this->wheres;
		$update = mysql_query($query);
		return $update;
	}

	public function delete(){
		$query = mysql_query("DELETE FROM pengiriman ".$this->wheres);
		return $query;
	}

	public function getDataPengiriman(){
		$query = mysql_query("SELECT * FROM pengiriman ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPengiriman(){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			harga_pengiriman.id_paket_pengiriman, harga_pengiriman.id_kota_asal,  harga_pengiriman.id_kota_tujuan
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi` ".
			$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPengirimanForPrint($refId){
		$query = mysql_query("SELECT pengiriman.*, harga_pengiriman.`harga`, harga_pengiriman.`estimasi_waktu`,  
			paket_pengiriman.`nama_paket`, paket_pengiriman.`deskripsi`, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan,
			harga_pengiriman.id_paket_pengiriman, harga_pengiriman.id_kota_asal,  harga_pengiriman.id_kota_tujuan
			FROM `pengiriman`
			LEFT JOIN `harga_pengiriman` ON `harga_pengiriman`.`id_harga_pengiriman` = pengiriman.`id_harga_pengiriman`
			LEFT JOIN `paket_pengiriman` ON `paket_pengiriman`.`id_paket_pengiriman` = harga_pengiriman.`id_paket_pengiriman`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = harga_pengiriman.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = harga_pengiriman.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi` 
			WHERE 
			id_pengiriman = '".$refId."'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getLokasiPengiriman(){
		$data = $this->getKota();
		$status = array('0' => 'Pilih Kota');
		if (!empty($data)){
			foreach ($data as $key => $value) {
				$status[$value['id_kota']] = $value['lokasi_tujuan'];
			}
		}

		return $status;
	}
}
?>