<?php
ob_start();
require_once 'modelKurir.class.php';
$model = new modelKurir();

$wheres = array(
	'id_paket_pengiriman' => $_POST['id_paket_pengiriman'],
	'id_kota_asal' => $_POST['id_kota_asal'],
	'id_kota_tujuan' => $_POST['id_kota_tujuan']
	);
$model->where($wheres);
$data = $model->getHarga();
if (!empty($data)){
	$return = $data[0];
	$return['success'] = true;
	$return['estimasi_hari'] = $data[0]['estimasi_waktu'];
	$return['total_harga'] = $data[0]['harga'] * $_POST['berat'];
}else{
	$return['success'] = false;
	$return['estimasi_hari'] = '';
	$return['total_harga'] = '';
}
echo json_encode($return);
?>