<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<div class="col-sm-7">
    <h3>Daftar Pengaturan</h3>
    <?php echo $message;?>
    <br>
    <br>
    <table id="table-setting" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=setting&c=getData&a=do';?>">
        <thead>
            <th class="center" style="width:10%">No</th>
            <th style="width:70%">Nama Pengaturan</th>
            <th style="width:70%">Durasi Jam</th>
            <th class="center" style="width:20%">Aksi</th>
        </thead>
    </table>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#table-setting').dataTable({
                ajax : {
                    url : $('#table-setting').attr('data-url'),
                    type : 'post',
                    dataType : 'json'
                },
                columns : [
                    {data : 'no', class : 'center'},
                    {data : 'label'},
                    {data : 'set', class : 'center'},
                    {data : 'aksi', class : 'center'}
                ]
            });
        });

        $(document).on('click', 'a.btn-hapus', function(){
            if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
                return true;
            }else{
                return false;
            }
            return false;
        });
    </script>
</div>