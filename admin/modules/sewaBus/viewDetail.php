<?php
$id_armada = isset($_GET['id_armada']) ? $_GET['id_armada'] : 0;
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}

if (isset($_GET['statusKonfirmasi']) && $_GET['statusKonfirmasi'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dikonfirmasi</div>';
}

if (isset($_GET['statusKonfirmasi']) && $_GET['statusKonfirmasi'] == false){
    $message = '<div class="alert alert-danger">Data gagal dikonfirmasi</div>';
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">

<h3>Detail Penyewaan Bus</h3>
<?php echo $message;?>
<a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=sewaBus&c=viewAddDetail&a=view&id_armada=".$_GET['id_armada'];?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah sewaBus'> Tambah</a>
<br>
<br>
<div class="table-responsive">
    <table id="table-sewaBus" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=sewaBus&c=getDataDetail&a=do&id_armada='.$id_armada;?>">
        <thead>
            <tr>
                <th class="text-center" nowrap>No</th>
                <th nowrap>ID Penyewaan</th>
                <th nowrap>Status</th>
                <th>Supir</th>
                <th nowrap>Tanggal Pemakaian</th>
                <th nowrap>Tanggal Kembali</th>
                <th nowrap>Jumlah Hari</th>
                <th nowrap>Harga Per Hari</th>
                <th nowrap>Total Harga</th>
                <th nowrap>Armada</th>
                <th nowrap>Muatan Penumpang</th>
                <th class="text-center col-sm-2">Aksi</th>
            </tr>
        </thead>
    </table>
</div>
<form action="<?php echo BASE_URL."?m=sewaBus&c=doKonfirmasi&a=do";?>" method="POST" enctype="multipart/form-data" id="form-konfirmasi" class="form-horizontal">
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Konfirmasi data penyewaan bus</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label">ID Penyewaan Bus</label> 
                    <div class="col-sm-7">
                        <input type="text" name="id_penyewaan" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Armada</label> 
                    <div class="col-sm-7">
                        <input type="hidden" name="id_armada" value="".
                        <input type="text" name="nama_armada" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Pemakaian</label> 
                    <div class="col-sm-7">
                        <input type="text" name="tanggal_pemakaian" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Tanggal Kembali</label> 
                    <div class="col-sm-7">
                        <input type="text" name="tanggal_kembali" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Jumlah Hari</label> 
                    <div class="col-sm-7">
                        <input type="text" name="jumlah_hari" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Harga Per-Hari</label> 
                    <div class="col-sm-7">
                        <input type="text" name="harga_perhari" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Total Harga</label> 
                    <div class="col-sm-7">
                        <input type="text" name="total_harga" class="input-sm form-control" value="" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Supir</label> 
                    <div class="col-sm-7">
                        <select name="id_supir" class="form-control">
                            <option value="">Pilih Supir</option>
                        <?php
                        require_once 'modelSewaBus.php';
                        $model = new modelSewaBus();
                        $supir = $model->getDataSupir();
                        if (!empty($supir)){
                            foreach ($supir as $key => $value) {
                                echo '<option value="'.$value['id_supir'].'">'.$value['nama'].'</option>';
                            }
                        }
                        ?>
                        </select>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Konfirmasi</button>
          </div>
        </div>

      </div>
    </div>
</form>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#table-sewaBus').dataTable({
            ajax : {
                url : $('#table-sewaBus').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'id_penyewaan'},
                {data : 'status_penyewaan', class : 'text-center'},
                {data : 'nama_supir'},
                {data : 'tanggal_pemakaian'},
                {data : 'tanggal_kembali'},
                {data : 'jumlah_hari'},
                {data : 'harga_perhari'},
                {data : 'total_harga'},
                {data : 'nama_armada'},
                {data : 'muatan_penumpang'},
                {data : 'aksi', class : 'center col-sm-2'}
            ]
        });

        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });

    $(document).on('click', 'a.btn-konfirmasi', function(e){
        e.preventDefault();
        var selector = $(this);
        var data = {
            id_armada : selector.attr('data-id_armada'),
            tanggal_pemakaian : selector.attr('data-tanggal_pemakaian'),
            tanggal_kembali : selector.attr('data-tanggal_kembali'),
            jumlah_hari : selector.attr('data-jumlah_hari'),
            harga_perhari : selector.attr('data-harga_perhari'),
            total_harga : selector.attr('data-total_harga'),
            total_bayar : selector.attr('data-total_bayar'),
            nama_armada : selector.attr('data-nama_armada'),
            id_penyewaan : selector.attr('data-id_penyewaan')
        }

        $.each(data, function(index, value){
            $('#form-konfirmasi input[name='+index+']').val(value);
        });

        $('#myModal').modal('show');
    });
</script>