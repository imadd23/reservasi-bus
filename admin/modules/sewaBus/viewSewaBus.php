<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  '1'){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  '1'){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  '0'){
    $message = '<div class="alert alert-danger">Data gagal diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  '1'){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<h3>Penyewaan Bus</h3>
<?php echo $message;?>
<a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=sewaBus&c=viewAdd&a=view";?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah sewaBus'> Tambah</a>
<!-- <a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a>
 --><br>
<br>
<div class="table-responsive">
    <table id="table-sewaBus" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=sewaBus&c=getData&a=do';?>">
        <thead>
            <th class="center col-sm-1">No</th>
            <th class="col-sm-6">Nama Armada</th>
            <th class="col-sm-4">Harga</th>
            <th class="center col-sm-1">Aksi</th>
        </thead>
    </table>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">
<form action="<?php echo BASE_URL;?>" method="get" class="form-horizontal">
    <input type="hidden" name="m" value="sewaBus">
    <input type="hidden" name="c" value="doPrintReport">
    <input type="hidden" name="a" value="do">
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cetak Report Penyewaan Bus</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Dari Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="from" class="input-sm form-control datepicker" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Sampai Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="to" class="input-sm form-control datepicker" required>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Cetak</button>
          </div>
        </div>

      </div>
    </div>
</form>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('#table-sewaBus').dataTable({
            ajax : {
                url : $('#table-sewaBus').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'nama_armada'},
                {data : 'harga'},
                {data : 'aksi', class : 'center'}
            ]
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });
</script>