<?php
require_once MODULE_PATH.'core/model.core.php';

class modelSewaBus extends model{

	public function __construct(){
		parent::__construct();
	}

	public function getArmada($tanggal_pemakaian = '', $tanggal_kembali = ''){
		$query = mysql_query("SELECT sewa_bus.*, armada.`muatan_penumpang`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada
			FROM sewa_bus
			LEFT JOIN armada ON sewa_bus.`id_armada` = armada.`id_armada`
			LEFT JOIN `penyewaan` ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			WHERE armada.`id_armada` NOT IN (
				SELECT armada.`id_armada`
				FROM sewa_bus
				LEFT JOIN armada ON sewa_bus.`id_armada` = armada.`id_armada`
				LEFT JOIN `penyewaan` ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
				WHERE 
				'".$tanggal_pemakaian."' BETWEEN penyewaan.`tanggal_pemakaian` AND penyewaan.`tanggal_kembali` OR 
				'".$tanggal_kembali."' BETWEEN penyewaan.`tanggal_pemakaian` AND penyewaan.`tanggal_kembali`
			)
			GROUP BY sewa_bus.id_sewa_bus
			ORDER BY nama_armada ASC");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getHarga($id_sewa_bus = 0){
		$query = mysql_query("SELECT * FROM sewa_bus WHERE id_sewa_bus = ".$id_sewa_bus);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function add($newData){
		$_newData = $this->setData($newData);
		$_query = "INSERT INTO penyewaan SET ".$_newData;
		$query = mysql_query($_query);
		return $query;
	}

	public function update($data = array()){
		$newData = $this->setData($data);
		$query = mysql_query("UPDATE penyewaan SET ".$newData." ".$this->wheres);
		return $query;
	}

	public function delete(){
		$query = mysql_query("DELETE FROM penyewaan ".$this->wheres);
		return $query;
	}

	public function getDataPariwisata(){
		$query = mysql_query("SELECT * FROM penyewaan ".$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPariwisata(){
		$query = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada, armada.`muatan_penumpang` 
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada` ".
			$this->wheres);

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDetailPariwisataForReport($from, $to){
		$query = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, CONCAT(armada.nama_armada, ' - ', armada.muatan_penumpang, ' Kursi') AS muatan_armada, armada.`muatan_penumpang` 
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada` 
			WHERE
			tanggal_pemakaian BETWEEN '".$from."' AND '".$to."'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getDataPariwisataForPrint($ref_id = 0){
		$qPariwisata = mysql_query("SELECT penyewaan.*, sewa_bus.`harga`, armada.`nama_armada`, armada.`muatan_penumpang`, CONCAT(supir.nama, ', no. hp : ', supir.no_hp) AS nama_supir 
			FROM penyewaan
			LEFT JOIN sewa_bus ON sewa_bus.`id_sewa_bus` = penyewaan.`id_sewa_bus`
			LEFT JOIN armada ON armada.`id_armada` = sewa_bus.`id_armada`
			LEFT JOIN supir ON penyewaan.id_supir = supir.id_supir
			WHERE penyewaan.`id_penyewaan` = '".$ref_id."'
			ORDER BY penyewaan.`tanggal_pemakaian` DESC
			");
		$arrayPariwisata = array();
		while ($dataPariwisata = mysql_fetch_array($qPariwisata)){
			$arrayPariwisata[] = $dataPariwisata;
		}

		return $arrayPariwisata;
	}

	public function getUser(){
		$query = mysql_query("SELECT * FROM login WHERE id_role = '2'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function doKonfirmasi($id_sewabus, $id_supir){
		$query = mysql_query("UPDATE penyewaan SET
			tanggal_status_penyewaan = NOW(),
			status_penyewaan = '1',
			id_supir = '".$id_supir."'
			WHERE id_penyewaan = '".$id_sewabus."'");
		return $query;
	}

	public function getDataSupir(){
		$query = mysql_query("SELECT * FROM supir");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

}
?>