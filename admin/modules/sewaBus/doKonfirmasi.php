<?php
require_once 'modelSewaBus.php';
$model = new modelSewaBus();

$id_sewabus = (isset($_POST['id_penyewaan'])) ? $_POST['id_penyewaan'] : 0;
$id_supir = isset($_POST['id_supir']) ? $_POST['id_supir'] : 0;
$konfirmasi = $model->doKonfirmasi($id_sewabus, $id_supir);
if ($konfirmasi){
	header('location: '.BASE_URL.'?m=sewaBus&c=viewDetail&a=view&statusKonfirmasi=true&id_armada='.$_POST['id_armada']);
}else{
	header('location: '.BASE_URL.'?m=sewaBus&c=viewDetail&a=view&statusKonfirmasi=false&id_armada='.$_POST['id_armada']);
}
?>