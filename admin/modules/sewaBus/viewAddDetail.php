<?php
require_once 'modelSewaBus.php';
$model = new modelSewaBus();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$message = '<div class="alert alert-success">Data penyewaan bus berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data penyewaan bus gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data penyewaan bus berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data penyewaan bus gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
} 

$id_penyewaan = isset($_GET['id_penyewaan']) ? $_GET['id_penyewaan'] : '';
$id_sewa_bus = isset($_GET['id_sewa_bus']) ? $_GET['id_sewa_bus'] : ''; 
$tanggal_pemakaian = isset($_GET['tanggal_pemakaian']) ? date('d-m-Y', strtotime($_GET['tanggal_pemakaian'])) : '';
$tanggal_kembali = isset($_GET['tanggal_kembali']) ? date('d-m-Y', strtotime($_GET['tanggal_kembali'])) : '';  
$jumlah_hari = isset($_GET['jumlah_hari']) ? $_GET['jumlah_hari'] : '0';  
$harga_perhari = isset($_GET['harga_perhari']) ? $_GET['harga_perhari'] : '0';  
$total_harga = isset($_GET['total_harga']) ? $_GET['total_harga'] : '0';
$muatan_armada = isset($_GET['muatan_armada']) ? $_GET['muatan_armada'] : 'Pilih Armada';
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : '';
$status_penyewaan = isset($_GET['status_penyewaan']) ? $_GET['status_penyewaan'] : '0';

if (isset($_GET['ref_id'])){
	$model->where(array(
		'id_penyewaan' => $_GET['ref_id']
		));
	$detail = $model->getDetailPariwisata();
	if (!empty($detail)){
		$d = $detail[0];
		$id_penyewaan = $d['id_penyewaan'];
		$id_sewa_bus = $d['id_sewa_bus'];
		$tanggal_pemakaian = date('d-m-Y', strtotime($d['tanggal_pemakaian']));
		$tanggal_kembali = date('d-m-Y', strtotime($d['tanggal_kembali']));
		$jumlah_hari = $d['jumlah_hari'];
		$harga_perhari = $d['harga_perhari'];
		$total_harga = $d['total_harga'];
		$muatan_armada = $d['muatan_armada'];
		$user_id = $d['user_id'];
		$status_penyewaan = $d['status_penyewaan'];
	}
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-8">
		<h3 class="text-center">Pesan Bus untuk Pariwisata</h3>
		<?php echo $message;?>
		<form method="post" action="<?php echo BASE_URL.'?m=sewaBus&c=doAddDetail&a=do&id_armada='.$_GET['id_armada'];?>" class="form-horizontal">
			<input type="hidden" name="id_penyewaan" value="<?php echo $id_penyewaan;?>">
			<div class="form-group">
				<label class="control-label col-sm-4">Pilih User *</label>
				<div class="col-sm-6">
					<select name="user_id" class="form-control input-sm" required>
						<?php
						$user = $model->getUser();
						$index = count($user);
						$user[$index] = array(
							'user_id' => '',
							'username' => 'Pilih User'
							);
						if (!empty($user)){
							foreach ($user as $key => $value) {
								if ($value['user_id'] === $user_id){
									echo '<option value="'.$value['user_id'].'" selected>'.$value['username'].'</option>';
								}else{
									echo '<option value="'.$value['user_id'].'">'.$value['username'].'</option>';
								}
							}
						}
						?>						
					</select>
				</div>
				<div class="col-sm-2">
					<img class="imgLoaderArmada" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Tanggal Pemakaian *</label>
				<div class="col-sm-4">
					<input type="text" name="tanggal_pemakaian" class="form-control input-sm datepicker" value="<?php echo $tanggal_pemakaian;?>" data-url="<?php echo BASE_URL.'?m=sewaBus&c=getArmadaJson&a=do';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Tanggal Kembali *</label>
				<div class="col-sm-4">
					<input type="text" name="tanggal_kembali" class="form-control input-sm datepicker" value="<?php echo $tanggal_kembali;?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Pilih Armada *</label>
				<div class="col-sm-6">
					<select name="id_sewa_bus" class="form-control input-sm" required data-url="<?php echo BASE_URL.'?m=sewaBus&c=getHargaJson&a=do';?>">
						<option value="<?php echo $id_sewa_bus;?>"><?php echo $muatan_armada;?></option>
					</select>
				</div>
				<div class="col-sm-2">
					<img class="imgLoaderArmada" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Jumlah Hari </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-jumlah-hari"><?php echo $jumlah_hari;?></label>
					<input type="hidden" name="jumlah_hari" value="<?php echo $jumlah_hari;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Status Penyewaan *</label>
				<div class="col-sm-6">
					<select name="status_penyewaan" class="form-control input-sm" required>
						<?php
						$arrayStatus = array(
							'1' => 'disetujui',
							'0' => 'belum disetujui'							
							);
						foreach ($arrayStatus as $key => $value) {
							if ((string) $key === (string) $status_penyewaan){
								echo '<option value="'.$key.'" selected>'.$value.'</option>';
							}else{
								echo '<option value="'.$key.'">'.$value.'</option>';
							}
						}
						?>						
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Harga per Hari </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-harga-perhari"><?php echo $harga_perhari;?></label>
					<input type="hidden" name="harga_perhari" value="<?php echo $harga_perhari;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-4">Total Biaya </label>
				<div class="col-sm-6">
					<label class="control-label" id="label-total-biaya"><?php echo $total_harga;?></label>
					<input type="hidden" name="total_harga" value="<?php echo $total_harga;?>">
				</div>
				<div class="col-sm-2">
					<img class="imgLoader" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-4"></label>
				<div class="col-sm-8">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
					<a href="<?php echo BASE_URL.'?m=sewaBus&c=viewDetail&a=view&id_armada='.$_GET['id_armada'];?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
				</div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.datepicker').datepicker({
			format : 'dd-mm-yyyy',
			autoClose : true
		}).on('changeDate', function(ev){
			$(this).datepicker('hide');
		});
	});

	$(document).on('change', 'select[name=id_sewa_bus]', function(){
        getHarga();
    });

    $(document).on('change', 'input[name=tanggal_kembali], input[name=tanggal_pemakaian]', function(){
        getArmada();
    });

    function getArmada(){
    	var _tanggal_pemakaian = $('input[name=tanggal_pemakaian]').val();
    	var _tanggal_kembali = $('input[name=tanggal_kembali]').val();
    	setDefaultLabel();
    	if (_tanggal_pemakaian != '' && _tanggal_kembali != ''){
    		$('.imgLoaderArmada').show();
    		$.ajax({
    			url : $('input[name=tanggal_pemakaian]').attr('data-url'),
    			type : 'post', 
    			data : {
    				tanggal_pemakaian : _tanggal_pemakaian,
    				tanggal_kembali : _tanggal_kembali
    			},
    			dataType : 'jSON',
    			success : function(response){
    				$('.imgLoaderArmada').hide();
    				var htmlOption = '<option value="">Pilih Armada</option>';
    				if (response.success){
    					$.each(response.data, function(key, value){
    						htmlOption += '<option value='+value['id_sewa_bus']+'>'+value['muatan_armada']+'</option>';
    					});
    					$('select[name=id_sewa_bus]').html(htmlOption);
    				}else{
    					$('select[name=id_sewa_bus]').html(htmlOption);
    				}
    			},
    			error : function(){
    				console.log('ajax call failed');
    				$('.imgLoaderArmada').hide();
    			}
    		});
    	} 
    }

	function getHarga(){
		var _id_sewa_bus = $('select[name=id_sewa_bus]').val();
		var _tanggal_pemakaian = $('input[name=tanggal_pemakaian]').val();
		var _tanggal_kembali = $('input[name=tanggal_kembali]').val();

        var imgLoader = $('.imgLoader');

        if (_id_sewa_bus != '' && _tanggal_kembali != '' && _tanggal_pemakaian != ''){
        	imgLoader.show();
	        $.ajax({
	            url : $('select[name=id_sewa_bus]').attr('data-url'),
	            type : 'post', 
	            dataType : 'jSON',
	            data : {
	                id_sewa_bus : _id_sewa_bus,
	                tanggal_pemakaian : _tanggal_pemakaian,
	                tanggal_kembali : _tanggal_kembali
	            },
	            success : function(response){
	                imgLoader.hide();
	                if (response.success){
	                	$('label#label-jumlah-hari').html(response.jumlah_hari);
	                	$('input[name=jumlah_hari]').val(response.jumlah_hari);
	                	$('label#label-harga-perhari').html(response.harga_perhari);
	                	$('input[name=harga_perhari]').val(response.harga_perhari);
	                    $('label#label-total-biaya').html(response.total_harga);
	                    $('input[name=total_harga]').val(response.total_harga);
	                }
	            },
	            error : function(){
	                imgLoader.hide();
	                console.log('Gagal Mengambil data');
	            }
	        });
        }else{
        	setDefaultLabel();
        }
        return;        
	}

	function setDefaultLabel(){
		$('label#label-jumlah-hari').html('0');
    	$('input[name=jumlah_hari]').val('0');
    	$('label#label-harga-perhari').html('0');
    	$('input[name=harga_perhari]').val('0');
        $('label#label-total-biaya').html('0');
        $('input[name=total_harga]').val('0');
	}
</script>