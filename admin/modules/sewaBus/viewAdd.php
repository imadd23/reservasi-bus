<?php
$qArmada = mysql_query(" SELECT * FROM armada ORDER BY nama_armada ASC");
$dataArmada = array();
while ($arrayArmada = mysql_fetch_array($qArmada)){
    $dataArmada[] = $arrayArmada;
}

if (isset($_GET['id_armada'])){
    $harga = $_GET['harga'];
    $id_armada = $_GET['id_armada'];
    $ref_id = '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM sewa_bus WHERE id_sewa_bus = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $id_armada = $a->id_armada;
        $harga = $a->harga;
    }else{
        $id_armada = '';
        $harga = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $id_armada = '';
    $harga = '';
    $ref_id = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data penyewaan bus</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=sewaBus&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Armada *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_armada" id="id_armada">
                    <option value="">Pilih Armada</option>
                    <?php
                    if (!empty($dataArmada)){
                        foreach ($dataArmada as $key => $value) {
                            if ($value['id_armada'] == $id_armada){
                                echo '<option value="'.$value['id_armada'].'" selected>'.$value['nama_armada'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_armada'].'">'.$value['nama_armada'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Harga *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="harga" id="harga" value="<?php echo $harga;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=sewaBus&c=viewSewaBus&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>