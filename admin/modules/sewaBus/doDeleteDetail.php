<?php
require_once 'modelSewaBus.php';
$model = new modelSewaBus();

$id_sewabus = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$model->where(array('id_penyewaan' => $id_sewabus));
$delete = $model->delete();
if ($delete){
	header('location: '.BASE_URL.'?m=sewaBus&c=viewDetail&a=view&statusDelete=true&id_armada='.$_GET['id_armada']);
}else{
	header('location: '.BASE_URL.'?m=sewaBus&c=viewDetail&a=view&statusDelete=false&id_armada='.$_GET['id_armada']);
}
?>