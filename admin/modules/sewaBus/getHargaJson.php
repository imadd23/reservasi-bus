<?php
ob_start();
require_once 'modelSewaBus.php';
$model = new modelSewaBus();

$id_sewa_bus = $_POST['id_sewa_bus'];
$tanggal_pemakaian = date('Y-m-d', strtotime($_POST['tanggal_pemakaian']));
$tanggal_kembali = date('Y-m-d', strtotime($_POST['tanggal_kembali']));
$diff = date_diff(date_create($tanggal_kembali), date_create($tanggal_pemakaian));
$jumlah_hari = $diff->days;
$data = $model->getHarga($id_sewa_bus);
$return['success'] = true;
if (!empty($data)){
	$return['jumlah_hari'] = $jumlah_hari;
	$return['harga_perhari'] = $data[0]['harga'];
	$return['total_harga'] = $return['jumlah_hari'] * $return['harga_perhari'];
}else{
	$return['jumlah_hari'] = $jumlah_hari;
	$return['harga_perhari'] = 0;
	$return['total_harga'] = 0;
}
echo json_encode($return);
?>