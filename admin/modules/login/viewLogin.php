<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Login Administrator</title>
		<link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/bootstrap.min.css';?>">
	    <!-- Font Awesome -->
	    <link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/font-awesome.min.css';?>">
	    <!-- theme -->
	    <link rel="stylesheet" href="<?php echo BASE_ADDRESS.'style/AdminLTE.min.css';?>">
		<!-- jQuery 2.1.4 -->
	    <script src="<?php echo BASE_ADDRESS.'js/jQuery-2.1.4.min.js';?>"></script>
	    <!-- Bootstrap 3.3.5 -->
	    <script src="<?php echo BASE_ADDRESS.'style/bootstrap.min.js';?>"></script>
	</head>
	<body class="hold-transition login-page">
		<?php
		$message = '';
		if (isset($_GET['loginFail']) && $_GET['loginFail'] == 1){
			$message = '<div class="alert alert-danger">Maaf, Username atau password salah</div>';
		}
		?>
	    <div class="login-box">
	      <div class="login-logo">
	        <a href="<?php echo BASE_URL;?>">Login Administrator</a>
	      </div><!-- /.login-logo -->
	      <div class="login-box-body">
	      	<?php echo $message;?>
	        <form action="<?php echo BASE_URL.'?m=login&c=doLogin&a=do';?>" method="post">
	          <div class="form-group has-feedback">
	            <input type="text" class="form-control" name="email" placeholder="Email">
	            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	          </div>
	          <div class="form-group has-feedback">
	            <input type="password" class="form-control" name="password" placeholder="Password">
	            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	          </div>
	          <div class="row">
	            <div class="col-xs-8"></div><!-- /.col -->
	            <div class="col-xs-4">
	              <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
	            </div><!-- /.col -->
	          </div>
	        </form>

	      </div><!-- /.login-box-body -->
	    </div><!-- /.login-box -->
  	</body>
</html>