<?php
ob_start();
require_once 'modelSupir.class.php';
$model = new modelSupir();
$arrayuser = $model->getListUser();

if (!empty($arrayuser)){
    foreach ($arrayuser as $key => $value) {
        $arrayuser[$key]['no'] = ($key + 1);
        $arrayuser[$key]['aksi'] = "<a class='btn-action' href='".BASE_URL."?m=supir&c=viewAdd&a=view&ref_id=".$value['id_supir']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'></a><a class='btn-action btn-hapus' data-nama='".$value['nama']."' href='".BASE_URL."?m=supir&c=doDelete&a=do&ref_id=".$value['id_supir']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>";
    }
}
echo json_encode(array('data' => $arrayuser));
exit();
?>