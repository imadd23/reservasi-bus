<?php
require_once 'modelSupir.class.php';
$model = new modelSupir();

$userId = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$delete = $model->deleteUser($userId);
if ($delete){
	header('location: '.BASE_URL.'?m=supir&c=viewSupir&a=view&statusDelete=true');
}else{
	header('location: '.BASE_URL.'?m=supir&c=viewSupir&a=view&statusDelete=false');
}
?>