<?php
require_once MODULE_PATH.'core/model.core.php';

class modelSupir extends model{

	public function __construct(){
		parent::__construct();
	}

	function getListUser(){
		$query = mysql_query("SELECT supir.*, IF(jenis_kelamin = 'L', 'Laki-laki', 'Perempuan') AS jenis_kelamin, IF(aktif = '1', 'Aktif', 'Tidak Aktif') AS aktif
			FROM supir
			ORDER BY nama ASC");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getUser($userId){
		$query = mysql_query(" SELECT * FROM supir WHERE id_supir = '".$userId."'");
    	$p = mysql_fetch_object($query);
    	return $p;
	}

	function addUser($data){
		$newData = $this->setData($data);
		mysql_query(" INSERT INTO supir 
			SET ".$newData);
	    $affectedRows = mysql_affected_rows();
	    return $affectedRows;
	}

	function updateUser($data){
		$newData = $this->setData($data);
		$update = mysql_query(" UPDATE supir SET ".$newData." ".$this->wheres);
    	return $update;
	}

	function deleteUser($user_id){
		 $delete = mysql_query("DELETE FROM supir WHERE id_supir = '".$user_id."'");
		 return $delete;
	}
}
?>