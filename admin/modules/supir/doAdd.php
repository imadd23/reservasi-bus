<?php
require_once 'modelSupir.class.php';
$model = new modelSupir();

if (isset($_POST['save'])){
	$p = array(
	    'nama' => $_POST['nama'],
	    'alamat' => $_POST['alamat'],
	    'jenis_kelamin' => $_POST['jenis_kelamin'],
	    'no_hp' => $_POST['no_hp'],
	    'email' => $_POST['email'],
	    'aktif' => $_POST['aktif']
    );
    if (empty($p['nama']) || empty($p['alamat']) || empty($p['jenis_kelamin']) || empty($p['no_hp']) || empty($p['email'])){
    	header('location: '.BASE_URL.'?m=supir&c=viewAdd&a=view&statusAdd=0&'.http_build_query($p));
    }else
    if (!isset($_POST['ref_id'])){
	    $save = $model->addUser($p);
	    if ($save > 0){
	    	header('location: '.BASE_URL.'?m=supir&c=viewSupir&a=view&statusAdd=true');
	    }else{
	    	header('location: '.BASE_URL.'?m=supir&c=viewAdd&a=view&statusAdd=false&'.http_build_query($p));
	    }
	}else{
    	$model->where(array('id_supir' => $_POST['ref_id']));
    	$update = $model->updateUser($p);
	    if ($update){
	    	header('location: '.BASE_URL.'?m=supir&c=viewSupir&a=view&statusUpdate=true');
	    }else{
	    	$p['ref_id'] = $_POST['ref_id'];
	    	header('location: '.BASE_URL.'?m=supir&c=viewAdd&a=view&statusUpdate=false&'.http_build_query($p));
	    }
	}
		
}

?>