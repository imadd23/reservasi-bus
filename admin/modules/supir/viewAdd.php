<?php
require_once 'modelSupir.class.php';
$model = new modelSupir();

if (isset($_GET['nama'])){
    $nama = $_GET['nama'];
    $alamat = $_GET['alamat'];
    $jenis_kelamin = $_GET['jenis_kelamin'];
    $no_hp = $_GET['no_hp'];
    $email = $_GET['email'];
    $aktif = $_GET['aktif'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $p = $model->getUser($_GET['ref_id']);
    if ($p){
        $nama = $p->nama;
        $alamat = $p->alamat;
        $jenis_kelamin = $p->jenis_kelamin;
        $no_hp = $p->no_hp;
        $email = $p->email;
        $aktif = $p->aktif;
    }else{
        $nama = '';
        $alamat = '';
        $jenis_kelamin = '';
        $no_rekening = '';
        $id_bank = '';
        $no_hp = '';
        $email = '';
        $aktif = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $ref_id = '';
    $nama = '';
    $alamat = '';
    $jenis_kelamin = '';
    $no_rekening = '';
    $id_bank = '';
    $no_hp = '';
    $email = '';
    $aktif = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data user</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=supir&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="nama" id="nama" value="<?php echo $nama;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Alamat *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="alamat" value="<?php echo $alamat;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">jenis Kelamin *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="jenis_kelamin" id="jk">
                    <?php
                    $jk = array('L' => 'Laki-laki', 'P' => 'Perempuan');
                    foreach ($jk as $key => $value) {
                        if ($key == $jenis_kelamin){
                            echo '<option value="'.$key.'" selected>'.$value.'</option>';
                        }else{
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                    }
                    ?>
    			</select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">No. Telp *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="no_hp" value="<?php echo $no_hp;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Email *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="email" value="<?php echo $email;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Status *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="aktif" id="aktif">
                    <?php
                    $st = array('1' => 'Aktif', '0' => 'Tidak Aktif');
                    foreach ($st as $key => $value) {
                        if ($key === $aktif){
                            echo '<option value="'.$key.'" selected>'.$value.'</option>';
                        }else{
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=supir&c=viewSupir&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>