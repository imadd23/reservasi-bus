<?php
class model{

	public $wheres;

	public function __construct(){
		$this->wheres = '';
	}

	public function where($whereData = array()){
		$where = '';
		if (!empty($whereData) && is_array($whereData)){
			$where .= ' WHERE ';
			foreach ($whereData as $key => $value) {
				$where .= $key." = '".$value."' AND ";
			}
			$where .= '1 = 1';
		}else
		if (!empty($whereData) && is_string($whereData)){
			$where .= ' WHERE '.$whereData;
		}
		$this->wheres = $where;
	}

	protected function setData($newData){
		$_newData = '';
		if (!empty($newData)){
			$maxKey = max(array_keys(array_values($newData)));
			$i = 0;
			foreach ($newData as $key => $value) {
				if ($i === $maxKey){
					$_newData .= $key." = '".$value."'";
				}else{
					$_newData .= $key." = '".$value."', ";
				}
				$i++;				
			}
		}
		return $_newData;
	}

	public function clearWhere(){
		$this->wheres = '';
	}

	private function result($rQuery){
		$result = array();
		while ($data = mysql_fetch_array($rQuery)){
			$result[] = $data;
		}
		return $result;
	}
	
	public function startTransaction(){
		mysql_query("SET autocommit=0");
		mysql_query("START TRANSACTION");
	}

	public function endTransaction($result){
		if ($result){
			mysql_query("COMMIT");
		}else{
			mysql_query("ROLLBACK");
		}
	}
}