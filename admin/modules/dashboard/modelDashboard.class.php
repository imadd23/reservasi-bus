<?php
require_once MODULE_PATH.'core/model.core.php';

class modelDashboard extends model{

	public function __construct(){
		parent::__construct();
	}

	public function getPenumpang(){
		$query = mysql_query("SELECT COUNT(*) AS total
			FROM
				detail_pembelian");

		$arrayData = mysql_fetch_object($query);
		return $arrayData;
	}

	public function getKonfirmasiTiket(){
		$query = mysql_query("SELECT COUNT(*) AS total 
			FROM pembelian_tiket 
			WHERE status_bayar = 'belum lunas'");

		$arrayData = mysql_fetch_object($query);
		return $arrayData;
	}

	public function getSewaBus(){
		$query = mysql_query("SELECT COUNT(*) AS total
			FROM penyewaan");
		$arrayData = mysql_fetch_object($query);
		return $arrayData;
	}

	public function getKurir(){
		$query = mysql_query("SELECT COUNT(*) AS total
			FROM pengiriman");
		$arrayData = mysql_fetch_object($query);
		return $arrayData;
	}

	public function getPemberangkatan(){
		$query = mysql_query("SELECT P.*, 
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			nama_armada
			FROM pemberangkatan AS P
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			ORDER BY hari ASC, harga ASC");
		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
			$arrayData[] = $data;
		}
		return $arrayData;
	}
}
?>