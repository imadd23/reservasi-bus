<?php
require_once 'modelDashboard.class.php';
$model = new modelDashboard();
?>
<div class="cont-Beranda">
	<div class="cont-judul"><!-- PO. FAMILY RAYA CERIA --></div>
    <div class="cont-text" style="min-height:200px;">
    	<div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?php echo empty($model->getPenumpang()) ? 0 : $model->getPenumpang()->total;?></h3>
                  <p>Penumpang</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="<?php echo BASE_URL.'?m=penumpang&c=viewPenumpang&a=view';?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <!-- <h3>53<sup style="font-size: 20px">%</sup></h3>-->
                  <h3><?php echo empty($model->getKonfirmasiTiket()) ? 0 : $model->getKonfirmasiTiket()->total;?></h3>
                  <p>Konfirmasi Tiket</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="<?php echo BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view';?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo empty($model->getSewaBus()) ? 0 : $model->getSewaBus()->total;?></h3>
                  <p>Sewa Bus</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo BASE_URL.'?m=sewaBus&c=viewSewaBus&a=view';?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo empty($model->getKurir()) ? 0 : $model->getKurir()->total;?></h3>
                  <p>Kurir</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="<?php echo BASE_URL.'?m=kurir&c=viewKurir&a=view';?>" class="small-box-footer">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
        </div>

        <div class="row">
        	<div class="col-sm-12">
        		<div class="panel panel-default">
        			<div class="panel-heading">
        				<h3>Jadwal Pemberangkatan Bus</h3>
        			</div>
        			<div class="panel-body">
        				<div class="table-responsive">
        					<table id="table-pemberangkatan" class="table table-hover table-bordered table-striped" width="99%" cell-padding="0">
							    <thead>
							        <th class="center">No</th>
							        <th>ID</th>
							        <th>Hari</th>
							        <th>Jam</th>
							        <th>Harga</th>
							        <th>Nama Armada</th>
							        <th>Kota Asal</th>
							        <th>Kota Tujuan</th>
							    </thead>
							    <tbody>
							    	<?php
							    	if (!empty($model->getPemberangkatan())){
							    		foreach ($model->getPemberangkatan() as $key => $value) {
							    			echo 
							    			"<tr>
							    				<td class='text-center'>".($key+1)."</td>
							    				<td>".$value['id_pemberangkatan']."</td>
							    				<td>".$value['hari']."</td>
							    				<td>".date('H:i', strtotime($value['jam']))."</td>
							    				<td>Rp. ".number_format($value['harga'])."</td>
							    				<td>".$value['nama_armada']."</td>
							    				<td>".$value['kota_asal']."</td>
							    				<td>".$value['kota_tujuan']."</td>
							    			</tr>";
							    		}
							    	}
							    	?>
							    </tbody>
							</table>
        				</div>
        			</div>
        		</div>
        	</div>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
        $('#table-pemberangkatan').dataTable({
        	order : [[0, 'asc']]
        });
    });
</script>
		
