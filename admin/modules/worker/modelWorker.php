<?php
require_once MODULE_PATH.'core/model.core.php';

Class modelWorker extends model{

	public function __construct(){
		parent::__construct();
	}


	function resetPembelianTiket(){
		$query = "UPDATE `detail_pembelian`
			SET `status_batal` = '1'
			WHERE
			`id_pembelian_tiket` IN (
				SELECT `id_pembelian_tiket`
				FROM `pembelian_tiket`
				WHERE
				TIMESTAMPDIFF(HOUR, `tanggal_beli`, CURRENT_TIMESTAMP()) > (
					SELECT `set`
					FROM `setting`
					WHERE
					id = 'PT'
					LIMIT 1
				)
				AND (bukti_transfer IS NULL OR bukti_transfer = '')
				AND `status_bayar` = 'belum lunas'
			)";
		$result = mysql_query($query);
		return mysql_affected_rows();
	}
}
?>