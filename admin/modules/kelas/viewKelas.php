<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<div class="col-sm-7">
    <h3>Daftar Kelas</h3>
    <?php echo $message;?>
    <a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=kelas&c=viewAdd&a=view";?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah kelas'> Tambah</a>
    <br>
    <br>
    <table id="table-kelas" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=kelas&c=getData&a=do';?>">
        <thead>
            <th class="center">No</th>
            <th>Jenis Kelas</th>
            <th class="center">Aksi</th>
        </thead>
    </table>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#table-kelas').dataTable({
                ajax : {
                    url : $('#table-kelas').attr('data-url'),
                    type : 'post',
                    dataType : 'json'
                },
                columns : [
                    {data : 'no', class : 'center'},
                    {data : 'jenis_kelas'},
                    {data : 'aksi', class : 'center'}
                ]
            });
        });

        $(document).on('click', 'a.btn-hapus', function(){
            if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
                return true;
            }else{
                return false;
            }
            return false;
        });
    </script>
</div>