<?php
if (isset($_GET['nama_provinsi'])){
    $nama_provinsi = $_GET['nama_provinsi'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM provinsi WHERE id_provinsi = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $nama_provinsi = $a->nama_provinsi;
    }else{
        $nama_provinsi = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $nama_provinsi = '';
    $ref_id = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data provinsi</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=provinsi&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama Provinsi *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="nama_provinsi" id="Nama_petugas" value="<?php echo $nama_provinsi;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=provinsi&c=viewProvinsi&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>