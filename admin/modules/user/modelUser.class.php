<?php
require_once MODULE_PATH.'core/model.core.php';

class modelUser extends model{

	public function __construct(){
		parent::__construct();
	}

	function getListUser(){
		$query = mysql_query("SELECT login.*, role_name
			FROM login
			LEFT JOIN role ON login.id_role = role.id_role
			ORDER BY nama ASC");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getUser($userId){
		$query = mysql_query(" SELECT * FROM login WHERE user_id = '".$userId."'");
    	$p = mysql_fetch_object($query);
    	return $p;
	}

	function getRole(){
		$query = mysql_query("SELECT * FROM role");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getBank(){
		$query = mysql_query("SELECT * FROM bank");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function addUser($data){
		$newData = $this->setData($data);
		mysql_query(" INSERT INTO login 
			SET ".$newData);
	    $affectedRows = mysql_affected_rows();
	    return $affectedRows;
	}

	function updateUser($data){
		$newData = $this->setData($data);
		$update = mysql_query(" UPDATE login SET ".$newData." ".$this->wheres);
    	return $update;
	}

	function deleteUser($user_id){
		 $delete = mysql_query("DELETE FROM login WHERE user_id = '".$user_id."'");
		 return $delete;
	}
}
?>