<?php
require_once 'modelUser.class.php';
$model = new modelUser();

$userId = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$delete = $model->deleteUser($userId);
if ($delete){
	header('location: '.BASE_URL.'?m=user&c=viewUser&a=view&statusDelete=true');
}else{
	header('location: '.BASE_URL.'?m=user&c=viewUser&a=view&statusDelete=false');
}
?>