<?php
ob_start();
require_once 'modelUser.class.php';
$model = new modelUser();
$arrayuser = $model->getListUser();

if (!empty($arrayuser)){
    foreach ($arrayuser as $key => $value) {
        $arrayuser[$key]['no'] = ($key + 1);
        $arrayuser[$key]['aksi'] = "<a class='btn-action' href='".BASE_URL."?m=user&c=viewAdd&a=view&ref_id=".$value['user_id']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'></a><a class='btn-action btn-hapus' data-nama='".$value['nama']."' href='".BASE_URL."?m=user&c=doDelete&a=do&ref_id=".$value['user_id']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>";
    }
}
echo json_encode(array('data' => $arrayuser));
exit();
?>