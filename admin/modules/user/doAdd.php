<?php
require_once 'modelUser.class.php';
$model = new modelUser();

if (isset($_POST['save'])){
	$p = array(
	    'id_role' => $_POST['id_role'],
	    'nama' => $_POST['nama'],
	    'alamat' => $_POST['alamat'],
	    'jenis_kelamin' => $_POST['jenis_kelamin'],
	    // 'no_rekening' => $_POST['no_rekening'],
	    // 'id_bank' => $_POST['id_bank'],
	    'no_hp' => $_POST['no_hp'],
	    'email' => $_POST['email'],
	    'username' => $_POST['email'],
	    'pertanyaan' => $_POST['pertanyaan'],
	    'jawaban' => $_POST['jawaban'],
	    'status' => $_POST['status']
    );
    if (empty($p['nama']) || empty($p['alamat']) || empty($p['jenis_kelamin']) || empty($p['no_hp']) || empty($p['email'])){
    	header('location: '.BASE_URL.'?m=user&c=viewAdd&a=view&statusAdd=0&'.http_build_query($p));
    }else
    if (!isset($_POST['ref_id'])){
    	$p['password'] = MD5($_POST['password']);
	    $save = $model->addUser($p);
	    if ($save > 0){
	    	header('location: '.BASE_URL.'?m=user&c=viewuser&a=view&statusAdd=true');
	    }else{
	    	header('location: '.BASE_URL.'?m=user&c=viewAdd&a=view&statusAdd=false&'.http_build_query($p));
	    }
	}else{
		if (!empty($_POST['password'])){
			$p['password'] = MD5($_POST['password']);
		}
    	$model->where(array('user_id' => $_POST['ref_id']));
    	$update = $model->updateUser($p);
	    if ($update){
	    	header('location: '.BASE_URL.'?m=user&c=viewuser&a=view&statusUpdate=true');
	    }else{
	    	$p['ref_id'] = $_POST['ref_id'];
	    	header('location: '.BASE_URL.'?m=user&c=viewAdd&a=view&statusUpdate=false&'.http_build_query($p));
	    }
	}
		
}

?>