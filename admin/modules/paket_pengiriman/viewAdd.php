<?php
if (isset($_GET['nama_paket'])){
    $nama_paket = $_GET['nama_paket'];
    $deskripsi = $_GET['deskripsi'];
    $status = $_GET['status'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM paket_pengiriman WHERE id_paket_pengiriman = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $nama_paket = $a->nama_paket;
        $deskripsi = $a->deskripsi;
        $status = $a->status;
    }else{
        $nama_paket = '';
        $deskripsi = '';
        $status = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $nama_paket = '';
    $deskripsi = '';
    $status = '';
    $ref_id = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data referensi paket pengiriman</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=paket_pengiriman&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama Paket *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="nama_paket" id="nama_paket" value="<?php echo $nama_paket;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Deskripsi *</label>
            <div class="col-sm-9">
                <textarea name="deskripsi" class="form-control"><?php echo $deskripsi;?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Status *</label>
            <div class="col-sm-3">
                <?php
                $arrStatus = array(
                    array('id' => '1', 'name' => 'Aktif'), 
                    array('id' => '0', 'name' => 'Tidak Aktif')
                    );
                ?>
                <select name="status" value="" class="form-control">
                    <option value="">Pilih Status</option>
                    <?php
                    foreach ($arrStatus as $key => $value) {
                        if ($value['id'] == $status){
                            $selected = 'selected="selected"';
                        }else{
                            $selected = '';
                        }
                        echo '<option value="'.$value['id'].'" '.$selected.'>'.$value['name'].'</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=paket_pengiriman&c=viewPaket&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>