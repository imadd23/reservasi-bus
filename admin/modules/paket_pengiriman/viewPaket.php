<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<div class="col-sm-7">
    <h3>Daftar Referensi Nama Paket Pengiriman</h3>
    <?php echo $message;?>
    <a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=paket_pengiriman&c=viewAdd&a=view";?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah paket_pengiriman'> Tambah</a>
    <br>
    <br>
    <table id="table-paket" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=paket_pengiriman&c=getData&a=do';?>">
        <thead>
            <th class="center">No</th>
            <th>Nama Paket</th>
            <th>Deskripsi Paket</th>
            <th>Status</th>
            <th class="center">Aksi</th>
        </thead>
    </table>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#table-paket').dataTable({
                ajax : {
                    url : $('#table-paket').attr('data-url'),
                    type : 'post',
                    dataType : 'json'
                },
                columns : [
                    {data : 'no', class : 'text-center'},
                    {data : 'nama_paket'},
                    {data : 'deskripsi'},
                    {data : 'status_paket', class : 'text-center'},
                    {data : 'aksi', class : 'text-center'}
                ]
            });
        });

        $(document).on('click', 'a.btn-hapus', function(){
            if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
                return true;
            }else{
                return false;
            }
            return false;
        });
    </script>
</div>