<?php
require_once 'modelPembayaran.class.php';
Library::load('PHPMailerAutoload.php', 'phpmailer');
$model = new modelPembayaran();

$arrId = explode(',', str_replace(' ', '', $_POST['id_pembelian_tiket']));
// $total_bayar = $_POST['total_bayar'];
// $metode_pembayaran = $_POST['metode_pembayaran'];
$metode_pembayaran = 'cash';
$tanggal_bayar = date('Y-m-d H:i:s');

if (!empty($arrId)){
	foreach ($arrId as $key => $value) {
		$update = $model->konfirmasi($value);
	}
}
$dataTiket = $model->getDetailDataPembelianTiket($arrId[0]);
$totalHarga = $dataTiket[0]['total_harga'];
if (isset($arrId[1])){
	$dataTiketPulang = $model->getDetailDataPembelianTiket($arrId[0]);
	$totalHarga += $dataTiketPulang[0]['total_harga'];
}

if ($update){
	//Create a new PHPMailer instance
	$mail = new PHPMailer;
	//Tell PHPMailer to use SMTP
	//$mail->isSMTP();
	$mail->SMTPSecure = 'tls';
	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	//$mail->SMTPDebug = 2;
	$mail->SMTPDebug = 0;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';
	//Set the hostname of the mail server
	//$mail->Host = "mailtrap.io";
	$mail->Host = $config['email_host'];
	//Set the SMTP port number - likely to be 25, 465 or 587
	// $mail->Port = 2525;
	$mail->Port = $config['email_port'];
	//Whether to use SMTP authentication
	$mail->SMTPAuth = true;
	//Username to use for SMTP authentication
	// $mail->Username = "ee74382b6cc229";
	$mail->Username = $config['email_username'];
	//Password to use for SMTP authentication
	$mail->Password = $config['email_password'];
	//Set who the message is to be sent from
	$mail->setFrom('noreplay@familyceria.com', 'Admin PO Family Ceria');
	//Set an alternative reply-to address
	// $mail->addReplyTo('user_ceria@localhost', 'User Ceria');
	//Set who the message is to be sent to
	$mail->addAddress($update['email'], $update['username']);
	//Set the subject line
	$mail->Subject = 'Konfirmasi Pembelian Tiket';
	//Read an HTML message body from an external file, convert referenced images to embedded,
	//convert HTML into a basic plain-text alternative body
	ob_start();
	?>
	<table width="100%">
		<tr>
			<th style="text-align:center; background-color: #EAD748; padding: 5px"><h2>PO Family Ceria</h3></th>
		</tr>
		<tr>
			<td>
				<p>
					Dear <?php echo $update['username'];?>,<br>
					Kami telah mengonfirmasi pembelian tiket Anda.
					Harap melakukan pembayaran ke kasir PO. Family ceria sebelum tanggal pemberangkatan (maksimal H-1 pemberangkatan) dengan menyertakan nota yang telah Anda cetak
				</p>
				<p>
					Detail Pemesanan Anda:
					<?php
					if (!empty($dataTiket)){
						?>
						<table border="1" width="70%">
							<col width="35%"></col>
							<col width="65%"></col>
							<tr>
								<td>ID</td>
								<td>: <?php echo $dataTiket[0]['id_pembelian_tiket'];?></td>
							</tr>
							<tr>
								<td>Tanggal Pemberangkatan</td>
								<td>: <?php echo date('d M Y', strtotime($dataTiket[0]['tanggal_pemberangkatan']));?></td>
							</tr>
							<tr>
								<td>Pemberangkatan</td>
								<td>: <?php echo $dataTiket[0]['nama_armada'].'|'.$dataTiket[0]['kota_asal'].' - '.$dataTiket[0]['kota_tujuan'];?></td>
							</tr>
							<tr>
								<td>Kota Tujuan</td>
								<td>: <?php echo $dataTiket[0]['lokasi_tujuan'];?></td>
							</tr>
							<tr>
								<td>Harga Tiket</td>
								<td>: <?php echo 'Rp.'.number_format($dataTiket[0]['sub_total'], 0, 0, '.').',-';?></td>
							</tr>
							<tr>
								<td>Penumpang</td>
								<td>
									<table border="1">
										<tr>
											<th align="center">No</th>
											<th>No. Kursi</th>
											<th>Atas Nama</th>
										</tr>
										<?php
										foreach ($dataTiket as $key => $value) {
											?>
											<tr>
												<td align="center"><?php echo ($key + 1);?></td>
												<td><?php echo $value['no_kursi'];?></td>
												<td><?php echo $value['atas_nama'];?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</td>
							</tr>
						</table>
						<?php
					}
					if (!empty($dataTiketPulang)){
						?>
						<br>
						<table border="1" width="70%">
							<col width="35%"></col>
							<col width="65%"></col>
							<tr>
								<td>ID</td>
								<td>: <?php echo $dataTiketPulang[0]['id_pembelian_tiket'];?></td>
							</tr>
							<tr>
								<td>Tanggal Kembali</td>
								<td>: <?php echo date('d M Y', strtotime($dataTiketPulang[0]['tanggal_pemberangkatan']));?></td>
							</tr>
							<tr>
								<td>Pemberangkatan</td>
								<td>: <?php echo $dataTiketPulang[0]['nama_armada'].'|'.$dataTiketPulang[0]['kota_asal'].' - '.$dataTiketPulang[0]['kota_tujuan'];?></td>
							</tr>
							<tr>
								<td>Kota Tujuan</td>
								<td>: <?php echo $dataTiketPulang[0]['lokasi_tujuan'];?></td>
							</tr>
							<tr>
								<td>Harga Tiket</td>
								<td>: <?php echo 'Rp.'.number_format($dataTiketPulang[0]['sub_total'], 0, 0, '.').',-';?></td>
							</tr>
							<tr>
								<td>Penumpang</td>
								<td>
									<table border="1">
										<tr>
											<th align="center">No</th>
											<th>No. Kursi</th>
											<th>Atas Nama</th>
										</tr>
										<?php
										foreach ($dataTiketPulang as $key => $value) {
											?>
											<tr>
												<td align="center"><?php echo ($key + 1);?></td>
												<td><?php echo $value['no_kursi'];?></td>
												<td><?php echo $value['atas_nama'];?></td>
											</tr>
											<?php
										}
										?>
									</table>
								</td>
							</tr>
						</table>
						<?php
					}
					?>
					<br>
					<table border="1" width="70%">
						<col width="35%"></col>
						<col width="65%"></col>
						<tr>
							<td>Total:</td>
							<td>Rp.<?php echo number_format($totalHarga, 0, 0, '.');?>,-</td>
						</tr>
					</table>
					<br>
					<a target="_blank" href="<?php echo 'http://localhost/reservasibus/index.php/?m=profil&c=doPrintNota&a=do&ref_id='.implode(',', $arrId); ?>">Cetak Invoice</a>
				</p>
			</td>
		</tr>
		<tr>
			<td style="text-align:center">
				<i>Terima Kasih atas Kepercayaan Anda kepada Kami</i>
			</td>
		</tr>
	</table>
	<?php
	$html = ob_get_clean();
	$mail->msgHTML($html, dirname(__FILE__));
	if (!$mail->send()) {
	    echo "Mailer Error: " . $mail->ErrorInfo;
	}else{
		header('location: '.BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view&statusKonfirmasi=1');
	}
}else{
	header('location: '.BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view&statusKonfirmasi=0');
}
?>