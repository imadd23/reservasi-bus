<?php
ob_start();
require_once 'modelPembayaran.class.php';
$model = new modelPembayaran();
$arrayPenumpang = $model->getPembayaranAll();
if (!empty($arrayPenumpang)){
	foreach ($arrayPenumpang as $key => $value) {
		$arrayPenumpang[$key]['no'] = $key + 1;
		$img = '';
		if (!empty($value['bukti_transfer'])){
			$img = str_replace('admin/', '', BASE_ADDRESS).$value['bukti_transfer'];
		}
		$arrayPenumpang[$key]['aksi'] = "<a class='btn-action btn-detail' data-nama='".$value['id_pembelian_tiket']."' href='".BASE_URL."?m=pembayaran&c=getDetailTiket&a=do&ref_id=".$value['id_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/detail.png' title='detail'></a>\n";
		if ($value['status_batal'] == '0'){
			$arrayPenumpang[$key]['aksi'] .= "<a class='btn-action btn-konfirmasi' href='#' data-id='".$value['id_pembelian_tiket']."' data-tanggal-beli='".$value['tanggal_beli']."' data-nama-pelanggan='".$value['nama_pelanggan']."' data-total-harga='".$value['total_harga']."' data-bukti-transfer='".$img."'><img src='".BASE_ADDRESS."icon/checkbox.png' title='konfirmasi lunas'></a>\n";
		}else{
			$arrayPenumpang[$key]['aksi'] .= "<a class='btn-action btn-hapus' data-nama='".$value['id_pembelian_tiket']."' href='".BASE_URL."?m=pembayaran&c=doDelete&a=do&ref_id=".$value['id_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>\n";
		}
            
	}
}

echo json_encode(array('data' => $arrayPenumpang));
?>