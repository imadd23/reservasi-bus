<?php
$message = '';
if (isset($_GET['statusKonfirmasi']) && $_GET['statusKonfirmasi'] ==  '1'){
    $message = '<div class="alert alert-success">Data berhasil dikonfirmasi</div>';
}else
if (isset($_GET['statusKonfirmasi']) && $_GET['statusKonfirmasi'] ==  '0'){
    $message = '<div class="alert alert-danger">Data gagal dikonfirmasi, coba ulangi kembali</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<h3>Daftar Pembelian tiket yang perlu dikonfirmasi</h3>
<?php echo $message;?>
<table id="table-konfirmasi" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=pembayaran&c=getData&a=do';?>">
    <thead>
        <th class="center">No</th>
        <th>ID Pembelian</th>
        <th>Tanggal Beli</th>
        <th>Nama Pelanggan</th>
        <th>Atas Nama</th>
        <th>No. HP</th>
        <th class="center">Total Bayar</th>
        <th class="center">Aksi</th>
    </thead>
</table>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <form action="<?php echo BASE_URL."?m=pembayaran&c=doKonfirmasi&a=do";?>" method="POST" enctype="multipart/form-data" id="form-konfirmasi" class="form-horizontal">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Konfirmasi data pembayaran</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">ID Pembelian Tiket</label> 
                        <div class="col-sm-7">
                            <input type="text" name="id_pembelian_tiket" class="input-sm form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Tanggal Pembelian</label> 
                        <div class="col-sm-7">
                            <input type="text" name="tanggal_beli" class="input-sm form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nama Pembeli</label> 
                        <div class="col-sm-7">
                            <input type="text" name="nama_pelanggan" class="input-sm form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Total Harga</label> 
                        <div class="col-sm-7">
                            <input type="text" name="total_harga" class="input-sm form-control" value="" readonly>
                        </div>
                    </div>
                    <div class="form-group bukti-transfer-container" style="display:none;">
                        <label class="col-sm-3 control-label">Bukti Transfer</label> 
                        <div class="col-sm-7">
                            <div style="width:350px; height:350px;">
                                <img class="img-responsive" src="" id="bukti-transfer-img"></img>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Konfirmasi</button>
          </div>
        </div>
    </form>
  </div>
</div>

<!-- Modal -->
<div id="modalDetail" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pembelian</h4>
      </div>
      <div class="modal-body" id="container-detail-tiket"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
      </div>
    </div>

  </div>
</div>

<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('#table-konfirmasi').dataTable({
            ajax : {
                url : $('#table-konfirmasi').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'id_pembelian_tiket'},
                {data : 'tanggal_beli'},
                {data : 'nama_pelanggan'},
                {data : 'atas_nama'},
                {data : 'no_hp'},
                {data : 'total_harga', class : 'center'},
                {data : 'aksi', class : 'center'}
            ],
            order : [[2, 'desc']]
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });

    $(document).on('click', 'a.btn-konfirmasi', function(e){
        e.preventDefault();
        var selector = $(this);
        var data = {
            id : selector.attr('data-id'),
            tanggal : selector.attr('data-tanggal-beli'),
            nama_pelanggan : selector.attr('data-nama-pelanggan'),
            total_harga : selector.attr('data-total-harga')
        }

        $('#form-konfirmasi input[name="id_pembelian_tiket"]').val(data.id);
        $('#form-konfirmasi input[name="tanggal_beli"]').val(data.tanggal);
        $('#form-konfirmasi input[name="nama_pelanggan"]').val(data.nama_pelanggan);
        $('#form-konfirmasi input[name="total_harga"]').val(data.total_harga);
        if (selector.data('bukti-transfer') != ''){
            $('#form-konfirmasi .bukti-transfer-container').show();
            $('#form-konfirmasi #bukti-transfer-img').attr('src', selector.data('bukti-transfer'));
        }else{
            $('#form-konfirmasi .bukti-transfer-container').hide();
            $('#form-konfirmasi #bukti-transfer-img').attr('src', '');
        }        
        $('#myModal').modal('show');
    });

    $(document).on('click', 'a.btn-detail', function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        href = $(this).attr('href');
        $('#modalDetail').modal('show');
    });

    $('#modalDetail').on('shown.bs.modal', function(e) {
        invoker = $(e.relatedTarget);
        $.ajax({
            url : href,
            type : "post",
            dataType : 'html',
            data : {},
            success : function(response){
                $('#container-detail-tiket').html(response);
            },
            error : function(){
                $.notify('Terjadi kesalahan, coba ulangi kembali', 'warn');
                $('#modalDetail').modal('hide');
            }
        });
    });
    $('#modalDetail').on('hidden.bs.modal', function(e) {
        $('#container-detail-tiket').html('');
    });
</script>