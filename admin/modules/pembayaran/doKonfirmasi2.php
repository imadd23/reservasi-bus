<?php
require_once 'modelPembayaran.class.php';
$model = new modelPembayaran();
$id_pembelian_tiket = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$update = $model->konfirmasi($id_pembelian_tiket);
if ($update){
	ob_start();
	?>
	<table width="100%">
		<tr>
			<th style="text-align:center; background-color: #EAD748; padding: 5px"><h2>PO Family Ceria</h3></th>
		</tr>
		<tr>
			<td>
				Dear <?php echo $update['username'];?>,<br>
				Kami telah mengonfirmasi pembelian tiket Anda. Silahkan cetak tiket Anda dengan mengklik tombol di bawah ini. Harap melakukan pembayaran ke kasir PO. Family ceria sebelum tanggal pemberangkatan (maksimal H-1 pemberangkatan) dengan menyertakan nota yang telah Anda cetak<br>
				<a target="_blank" href="<?php echo 'http://localhost/reservasibus/index.php/?m=profil&c=doPrintTiket&a=do&ref_id='.$id_pembelian_tiket; ?>">Cetak Tiket</a>
			</td>
		</tr>
		<tr>
			<td style="text-align:center">
				<i>Terima Kasih atas Kepercayaan Anda kepada Kami</i>
			</td>
		</tr>
	</table>
	<?php
	$html = ob_get_clean();

	$from = "imadd@pepipost.com";

	$fromname = "Admin PO Family Ceria";

	$to = "aimzelofty@gmail.com";//Recipients list (semicolon separated)
	// $to = $update['username'];
	$api_key = " 10f8cacc0b9e28d37e023a49150845e6";

	$subject = "Konfirmasi Pembelian Tiket";

	$content = $html;

	$data=array();

	$data['subject']= rawurlencode($subject);

	$data['fromname']= rawurlencode($fromname);

	$data['api_key']= $api_key;

	$data['from']= $from;

	$data['content']= rawurlencode($content);

	$data['recipients']= $to;

	$apiresult = callApi(@$api_type,@$action,$data);

	echo trim($apiresult);
	// if ($res['error']){ // check if operation succeeds
 //        die('Error: ' . $res['text']);
 //    } else {
 //        header('location: '.BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view&statusKonfirmasi=true');
 //    }
}else{
	header('location: '.BASE_URL.'?m=pembayaran&c=viewPembayaran&a=view&statusKonfirmasi=false');
}


function callApi($api_type='', $api_activity='', $api_input=''){

    $data = array();

    $result = http_post_form("https://api.pepipost.com/api/web.send.rest", $api_input);

    return $result;

}

function http_post_form($url,$data,$timeout=20){

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL,$url);

    curl_setopt($ch, CURLOPT_FAILONERROR,1);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);

    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

    curl_setopt($ch, CURLOPT_FOLLOWLOCATION,0);

    curl_setopt($ch, CURLOPT_POST,1);

    curl_setopt($ch, CURLOPT_RANGE,"1-2000000");

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

    curl_setopt($ch,  CURLOPT_REFERER,@$_SERVER['REQUEST_URI']);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    $result = curl_exec($ch);

    $result = curl_error($ch)? curl_error($ch): $result;

    curl_close($ch);

    return $result;

}

?>

