<?php
require_once MODULE_PATH.'core/model.core.php';

class modelPembayaran extends model{

	public function __construct(){
		parent::__construct();
	}

	public function getPembayaranAll(){
		$queryPenumpang = "SELECT 
            (
                SELECT GROUP_CONCAT(`id_pembelian_tiket` SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS id_pembelian_tiket,
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(tanggal_beli) SEPARATOR ', ')
                FROM pembelian_tiket
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_beli, 
            (
                SELECT GROUP_CONCAT(DISTINCT DATE(`tanggal_pemberangkatan`) SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS tanggal_pemberangkatan,
            (
                SELECT GROUP_CONCAT(DISTINCT atas_nama SEPARATOR ', ')
                FROM detail_pembelian
                LEFT JOIN pembelian_tiket ON detail_pembelian.id_pembelian_tiket = pembelian_tiket.id_pembelian_tiket
                WHERE 
                detail_pembelian.id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS atas_nama,
            (
                SELECT SUM(`detail_pembelian`.sub_total)
                FROM detail_pembelian
                WHERE 
                id_pembelian_tiket IN (
                    SELECT `id_pembelian_tiket` 
                    FROM `pembelian_tiket`
                    WHERE
                    pembelian_tiket.id_pembelian_tiket = PT.id_pembelian_tiket
                    OR pembelian_tiket.id_tiket_berangkat = PT.id_pembelian_tiket
                )
            ) AS total_harga, DP.status_batal, PT.nama_pelanggan, PT.no_hp, PT.id_pelanggan, PT.bukti_transfer
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            LEFT JOIN login ON PT.id_pelanggan = login.user_id
            WHERE 
            DP.status_batal = '0'
            AND PT.status_bayar = 'belum lunas'
            AND PT.id_tiket_berangkat IS NULL
            GROUP BY PT.id_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC";

		$qPenumpang = mysql_query($queryPenumpang);
		$arrayPenumpang = array();
		while ($dataPenumpang = mysql_fetch_array($qPenumpang)) {
		    $arrayPenumpang[] = $dataPenumpang;
		}
		return $arrayPenumpang;
	}

	public function getDetailDataPembelianTiket($refId){
        $qPembelian = mysql_query("SELECT 
            `PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
            PT.tanggal_bayar,
            CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
            CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
            nama_armada,
            IF( DP.status_batal = '0' AND TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), 
                '1', '0')
            AS can_cancelled, NK.no_kursi, P.hari, P.jam,
            (
                SELECT SUM(sub_total)
                FROM detail_pembelian
                WHERE
                `id_pembelian_tiket` IN (PT.id_pembelian_tiket)
            ) AS total_harga
            FROM `pembelian_tiket` AS PT
            INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
            LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
            LEFT JOIN kota AS A
                ON A.`id_kota` = P.`id_kota_asal`
            LEFT JOIN provinsi AS C
                ON C.`id_provinsi` = A.`id_provinsi`
            LEFT JOIN kota AS B
                ON B.`id_kota` = P.`id_kota_tujuan`
            LEFT JOIN provinsi AS D
                ON D.`id_provinsi` = B.`id_provinsi`
            LEFT JOIN armada E
                ON P.`id_armada` = E.`id_armada`
            LEFT JOIN no_kursi AS NK
                ON NK.id_no_kursi = DP.id_no_kursi
            LEFT JOIN kota AS F
                ON F.`id_kota` = DP.`id_kota_tujuan`
            LEFT JOIN provinsi AS G
                ON G.`id_provinsi` = F.`id_provinsi`
            WHERE
            PT.id_pembelian_tiket IN ('".$refId."')
            GROUP BY DP.id_detail_pembelian_tiket
            ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
        $arrayPembelian = array();
        while ($dataPembelian = mysql_fetch_array($qPembelian)){
            $arrayPembelian[] = $dataPembelian;
        }

        return $arrayPembelian;
    }

	public function getPembeian($id_pembelian_tiket = 0){
		$query = mysql_query("SELECT pembelian_tiket.*, login.email, login.username
			FROM pembelian_tiket 
			LEFT JOIN login ON pembelian_tiket.id_pelanggan = login.user_id
			WHERE pembelian_tiket.id_pembelian_tiket = '".$id_pembelian_tiket."'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getUser(){
		$query = mysql_query("SELECT * FROM login WHERE id_role = '2'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getUserById($user_id = 0){
		$query = mysql_query("SELECT * FROM login WHERE user_id = '".$user_id."'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function konfirmasi($id_pembelian_tiket = 0){
		$update = mysql_query("UPDATE pembelian_tiket SET 
			status_bayar = 'lunas',
			tanggal_bayar = CURRENT_DATE()
			WHERE id_pembelian_tiket = '".$id_pembelian_tiket."'");
		if ($update){
			$user = $this->getPembeian($id_pembelian_tiket);
			return  (empty($user)) ? array() : $user[0];
		}else{
			return false;
		}
	}
}
?>