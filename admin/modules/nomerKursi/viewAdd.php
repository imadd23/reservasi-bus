<?php
require_once 'modelNomerKursi.class.php';
$model = new modelNomerKursi();
$arrayArmada = $model->getArmada();

if (isset($_GET['id_armada'])){
    $query = mysql_query(" SELECT * FROM armada WHERE id_armada = '".$_GET['id_armada']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $id_armada = $a->id_armada;
        $nama_armada = $a->nama_armada;
    }else{
        $id_armada = '';
        $nama_armada = '';
    }
}else{
    $id_armada = '';
    $nama_armada = '';
}
$dataNoKursi = $model->getSusunanNoKursi($_GET['id_armada']);

$message = '';
if ((isset($_GET['statusAdd']) && $_GET['statusAdd'] == 'false') && (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == 'false')){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == 'true'){
    $message = '<div class="alert alert-success">Data Berhasil disimpan</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data nomer kursi</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=nomerKursi&c=doAdd&a=do';?>" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama armada </label>
            <div class="col-sm-9">
                <input type="text" value="<?php echo $nama_armada;?>" disabled>
                <input type="hidden" name="id_armada" value="<?php echo $id_armada;?>">
            </div>            
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nomer kursi *</label>
            <div class="col-sm-6">
                <table class="table table-bordered">
                    <tr>
                        <td colspan="5" class="bg-warning">Pintu<span class="pull-right">Supir</span></td>
                    </tr>
                    <?php
                        foreach ($dataNoKursi as $key => $value) {
                            if ($key == 10){
                                ?>
                                <tr>
                                    <td colspan="3" class="bg-warning">Pintu</td>
                                    <?php
                                    foreach ($value as $k => $v) {
                                        if (!empty($v)){
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" value="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]" value="<?php echo $v['id_no_kursi'];?>">
                                            </td>
                                            <?php
                                        }else{
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]">
                                            </td>
                                            <?php
                                        }                                        
                                    }
                                    ?>
                                </tr>
                                <?php
                            }else
                            if ($key == 11){
                                ?>
                                <tr>
                                    <td colspan="2" class="text-center">TOILET</td>
                                    <?php
                                    foreach ($value as $k => $v) {
                                        if (!empty($v)){
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" value="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]" value="<?php echo $v['id_no_kursi'];?>">
                                            </td>
                                            <?php
                                        }else{
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]">
                                            </td>
                                            <?php
                                        }                                        
                                    }
                                    ?>
                                </tr>
                                <?php
                            }else{
                                ?>
                                <tr>
                                    <?php
                                    foreach ($value as $k => $v) {
                                        if ($k == 2){
                                            ?>
                                            <td width="20%" class="bg-warning"></td>
                                            <?php
                                        }
                                        if (!empty($v)){
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" value="<?php echo $v['no_kursi'];?>" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]" value="<?php echo $v['id_no_kursi'];?>">
                                            </td>
                                            <?php
                                        }else{
                                            ?>
                                            <td>
                                                <input type="text" name="no_kursi[]" class="text-center" style="width:50px;">
                                                <input type="hidden" name="id_no_kursi[]">
                                            </td>
                                            <?php
                                        }                                        
                                    }
                                    ?>
                                </tr>
                                <?php
                            }                            
                        }
                    ?>
                </table>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=nomerKursi&c=viewNomerKursi&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>