<?php
require_once MODULE_PATH.'core/model.core.php';

class modelNomerKursi extends model{

	public function __construct(){
		parent::__construct();
	}

	// public function getArmada(){
	// 	$queryNoKursi = "SELECT no_kursi.*, nama_armada FROM no_kursi 
	// 	RIGHT JOIN armada ON armada.`id_armada` = no_kursi.`id_armada`
	// 	GROUP BY armada.id_armada
	// 	ORDER BY nama_armada ASC";
	// 	$qNoKursi = mysql_query($queryNoKursi);
	// 	$arrayNokursi = array();
	// 	while ($dataNoKursi = mysql_fetch_array($qNoKursi)) {
	// 	    $arrayNokursi[] = $dataNoKursi;
	// 	}
	// 	return $arrayNokursi;
	// }

	public function getArmada(){
		$queryNoKursi = "SELECT *
		FROM armada
		ORDER BY nama_armada ASC";
		$qNoKursi = mysql_query($queryNoKursi);
		$arrayNokursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNoKursi)) {
		    $arrayNokursi[] = $dataNoKursi;
		}
		return $arrayNokursi;
	}

	public function getNoKursi($id_armada){
		$queryNoKursi = "SELECT no_kursi.*, nama_armada FROM no_kursi 
		LEFT JOIN armada ON armada.`id_armada` = no_kursi.`id_armada`
		WHERE armada.id_armada = '$id_armada'
		GROUP BY armada.id_armada, no_kursi.no_kursi
		ORDER BY id_no_kursi ASC";
		$qNoKursi = mysql_query($queryNoKursi);
		$arrayNokursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNoKursi)) {
		    $arrayNokursi[] = $dataNoKursi;
		}
		return $arrayNokursi;
	}

	public function coreNoKursi(){
		$lineKursi = range(0, 11);
		$array = array();
		foreach ($lineKursi as $key => $value) {
            if ($key == 10){
            	$array[$key][0] = '';
            	$array[$key][1] = '';
            }else
            if ($key == 11){
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            }else{
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            	$array[$key][3] = '';
            }                            
        }
        return $array;
	}

	public function getSusunanNoKursi($id_armada){
		$arraySusunan = $this->coreNoKursi();
		$dataNoKursi = $this->getNoKursi($id_armada);
		$index = 0;
		foreach ($arraySusunan as $key => $value) {
			foreach ($value as $k => $v) {
				if (isset($dataNoKursi[$index])){
					$arraySusunan[$key][$k] = $dataNoKursi[$index];
					$index++;
				}
			}
		}
		return $arraySusunan;
	}
}
?>