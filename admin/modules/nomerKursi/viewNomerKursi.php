<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<h3>Daftar Nomer Kursi</h3>
<legend>
    <h5>Pilih Armada</h5>
</legend>
<?php echo $message;?>
<table id="table-no-kursi" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=nomerKursi&c=getData&a=do';?>">
    <thead>
        <th class="center" width="5%">No</th>
        <th width="65%">Nama Armada</th>
        <th class="center" width="30%">Aksi</th>
    </thead>
</table>
<script type="text/javascript">
    $(document).ready(function(){
        $('#table-no-kursi').dataTable({
            ajax : {
                url : $('#table-no-kursi').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'nama_armada'},
                {data : 'aksi', class : 'center'}
            ]
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });
</script>