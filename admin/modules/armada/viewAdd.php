<?php
if (isset($_GET['nama_armada'])){
    $nama_armada = $_GET['nama_armada'];
    $no_plat = $_GET['no_plat'];
    $muatan_penumpang = $_GET['muatan_penumpang'];
    $fasilitas = $_GET['fasilitas'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM armada WHERE id_armada = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $nama_armada = $a->nama_armada;
        $no_plat = $a->no_plat;
        $muatan_penumpang = $a->muatan_penumpang;
        $fasilitas = $a->fasilitas;
    }else{
        $nama_armada = '';
        $no_plat = '';
        $muatan_penumpang = '';
        $fasilitas = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $nama_armada = '';
    $no_plat = '';
    $muatan_penumpang = '';
    $fasilitas = '';
    $ref_id = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data armada</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=armada&c=doAdd&a=do';?>" class="form-horizontal" enctype="multipart/form-data">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama Armada *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="nama_armada" id="Nama_petugas" value="<?php echo $nama_armada;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">No. Plat *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="no_plat" value="<?php echo $no_plat;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Muatan Penumpang *</label>
            <div class="col-sm-9">
                <input type="number" class="input-sm form-control" name="muatan_penumpang" value="<?php echo $muatan_penumpang;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Fasilitas *</label>
            <div class="col-sm-9">
                <textarea cols="70" name="fasilitas" required><?php echo $fasilitas;?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Gambar *</label>
            <div class="col-sm-9">
                <input type="file" name="gambar">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=armada&c=viewArmada&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>