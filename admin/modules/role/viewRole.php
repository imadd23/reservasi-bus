<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>
<h3>Daftar Hak Akses</h3>
<?php echo $message;?>
<a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=role&c=viewAdd&a=view";?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah hak akses'> Tambah</a>
<br>
<br>
<table id="table-role" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=role&c=getData&a=do';?>">
    <thead>
        <th class="center col-sm-1">No</th>
        <th class="col-sm-2">Nama Hak Akses</th>
        <th class="col-sm-2">Login Destination</th>
        <th class="col-sm-5">Halaman</th>
        <th class="center col-sm-2">Aksi</th>
    </thead>
</table>
<script type="text/javascript">
    $(document).ready(function(){
        $('#table-role').dataTable({
            ajax : {
                url : $('#table-role').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'text-center'},
                {data : 'role_name', class : 'text-center'},
                {data : 'login_destination'},
                {data : 'halaman'},
                {data : 'aksi', class : 'text-center'}
            ]
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });
</script>