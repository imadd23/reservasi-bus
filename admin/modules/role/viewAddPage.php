<?php
require_once 'modelRole.class.php';
$model = new modelRole();

$id_role = $_GET['ref_id'];
$role = $model->getListRolePage($id_role);
$roleDetal = array();
if (!empty($role)){
    foreach ($role as $key => $value) {
        $roleDetal[] = $value['page'];
    }
}

if (isset($_GET['nama'])){
    $role_name = $_GET['nama'];
    $login_destination = $_GET['alamat'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $p = $model->getRole($_GET['ref_id']);
    if ($p){
        $id_role = $p->id_role;
        $role_name = $p->role_name;
        $login_destination = $p->login_destination;
    }else{
        $id_role = '';
        $role_name = '';
        $login_destination = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $ref_id = '';
    $id_role = '';
    $role_name = '';
    $login_destination = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == 'true'){
    $message = '<div class="alert alert-success">Data sukses disimpan</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == 'false'){
    $message = '<div class="alert alert-success">Data gagal disimpan, coba ulangi kembali</div>';
}else
if (isset($_GET['empty']) && $_GET['empty'] == 'true'){
    $message = '<div class="alert alert-success">Semua hak akses telah dihapus</div>';
}
?>
<div class="col-sm-7">
    <h3>Hak Akses <?php echo $role_name;?></h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=role&c=doAddPage&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><input type="checkbox" name="checkall"></th>
                                <th>Page</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $arrayPage = $model->getPage();
                            foreach ($arrayPage as $key => $value) {
                                echo 
                                "<tr>";
                                    if (in_array($value['page'], $roleDetal)){
                                        echo "<td><input type='checkbox' class='checkbox' name='page[]' checked='checked' value='".$value['page']."'></td>";
                                    }else{
                                        echo "<td><input type='checkbox' class='checkbox' name='page[]' value='".$value['page']."'></td>";
                                    }
                                    echo 
                                    "<td>".$value['label']."</td>
                                </tr>";
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=role&c=viewRole&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>
<script>
    $('input[name=checkall]').change(function(){
        if ($(this).prop('checked')){
            $('.checkbox').attr('checked', 'checked');
            $('.checkbox').prop('checked', 'checked');
        }else{
            $('.checkbox').removeAttr('checked');
        }
    });
</script>