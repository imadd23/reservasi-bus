<?php
require_once 'modelRole.class.php';
$model = new modelRole();

$id_role = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$delete = $model->deleteRole($id_role);
if ($delete){
	header('location: '.BASE_URL.'?m=role&c=viewRole&a=view&statusDelete=true');
}else{
	header('location: '.BASE_URL.'?m=role&c=viewRole&a=view&statusDelete=false');
}
?>