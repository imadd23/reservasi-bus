<?php
require_once 'modelRole.class.php';
$model = new modelRole();

if (isset($_GET['nama'])){
    $role_name = $_GET['nama'];
    $login_destination = $_GET['alamat'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $p = $model->getRole($_GET['ref_id']);
    if ($p){
        $id_role = $p->id_role;
        $role_name = $p->role_name;
        $login_destination = $p->login_destination;
    }else{
        $id_role = '';
        $role_name = '';
        $login_destination = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $ref_id = '';
    $id_role = '';
    $role_name = '';
    $login_destination = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data hak akses</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=role&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="role_name" id="role_name" value="<?php echo $role_name;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Login Destination *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="login_destination" id="login_destination">
                    <?php
                    $arrayLD = array('' => 'Front Page', 'admin' => 'Administrator');
                    foreach ($arrayLD as $key => $value) {
                        if ($key === $login_destination){
                            echo '<option value="'.$key.'" selected>'.$value.'</option>';
                        }else{
                            echo '<option value="'.$key.'">'.$value.'</option>';
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=role&c=viewRole&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>