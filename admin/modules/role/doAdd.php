<?php
require_once 'modelRole.class.php';
$model = new modelRole();

if (isset($_POST['save'])){
	$p = array(
	    'role_name' => $_POST['role_name'],
	    'login_destination' => $_POST['login_destination']
    );
    if (empty($p['role_name'])){
    	header('location: '.BASE_URL.'?m=role&c=viewAdd&a=view&statusAdd=0&'.http_build_query($p));
    }else
    if (!isset($_POST['ref_id'])){
	    $save = $model->addRole($p);
	    if ($save > 0){
	    	header('location: '.BASE_URL.'?m=role&c=viewRole&a=view&statusAdd=true');
	    }else{
	    	header('location: '.BASE_URL.'?m=role&c=viewAdd&a=view&statusAdd=false&'.http_build_query($p));
	    }
	}else{
    	$model->where(array('id_role' => $_POST['ref_id']));
    	$update = $model->updateRole($p);
	    if ($update){
	    	header('location: '.BASE_URL.'?m=role&c=viewRole&a=view&statusUpdate=true');
	    }else{
	    	$p['ref_id'] = $_POST['ref_id'];
	    	header('location: '.BASE_URL.'?m=role&c=viewAdd&a=view&statusUpdate=false&'.http_build_query($p));
	    }
	}
		
}

?>