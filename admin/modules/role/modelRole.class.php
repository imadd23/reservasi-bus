<?php
require_once MODULE_PATH.'core/model.core.php';

class modelRole extends model{

	public function __construct(){
		parent::__construct();
	}

	function getListRole(){
		$query = mysql_query("SELECT r.*, IF(login_destination = '', 'Front Page', IF(login_destination = 'admin', 'Administrator', 'N/A')) AS login_destination, GROUP_CONCAT(label) AS halaman
			FROM role AS r
			LEFT JOIN role_page AS rp ON rp.id_role = r.`id_role`
			LEFT JOIN page AS p ON rp.page = p.page
			GROUP BY `role_name`");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getRole($id_role){
		$query = mysql_query("SELECT r.*, GROUP_CONCAT(label) AS halaman
			FROM role AS r
			LEFT JOIN role_page AS rp ON rp.id_role = r.`id_role`
			LEFT JOIN page AS p ON rp.page = p.page
			WHERE r.id_role = '".$id_role."'
			GROUP BY `role_name`");

		$result = mysql_fetch_object($query);
		return $result;
	}

	function getListRolePage($id_role){
		$query = mysql_query("SELECT r.*, p.page
			FROM role AS r
			LEFT JOIN role_page AS rp ON rp.id_role = r.`id_role`
			LEFT JOIN page AS p ON rp.page = p.page
			WHERE r.id_role = '".$id_role."'");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function getPage(){
		$query = mysql_query("SELECT * 
			FROM page");

		$result = array();
		while ($a = mysql_fetch_array($query)) {
		    $result[] = $a;
		}

		return $result;
	}

	function addRole($data){
		$newData = $this->setData($data);
		mysql_query(" INSERT INTO role 
			SET ".$newData);
	    $affectedRows = mysql_affected_rows();
	    return $affectedRows;
	}

	function updateRole($data){
		$newData = $this->setData($data);
		$update = mysql_query(" UPDATE role SET ".$newData." ".$this->wheres);
    	return $update;
	}

	function deleteRole($id_role){
		$delete = mysql_query("DELETE FROM role WHERE id_role = '".$id_role."'");
		return $delete;
	}

	function cekRolePage($data = array()){
		$this->where($data);
		$query = mysql_query("SELECT * FROM role_page ".$this->wheres);
		if (mysql_num_rows($query) > 0){
			return 1;
		}else{
			return 0;
		}
	}

	function addRolePage($data){
		$newData = $this->setData($data);
		$insert = mysql_query("INSERT INTO role_page SET ".$newData);
		return $insert;
	}

	function deleteRolePage($id_role, $data = 0){
		if (!empty($data)){
			$newData = array();
			foreach ($variable as $key => $value) {
				$newData[] = "'".$value."'";
			}
			$pages = implode(',', $newData);
			$delete = mysql_query("DELETE FROM role_page WHERE id_role = '".$id_role."' AND page NOT IN (".$pages.")");
		}else{
			$delete = mysql_query("DELETE FROM role_page WHERE id_role = '".$id_role."'");
		}		
		return $delete;
	}
}
?>