<?php
ob_start();
require_once 'modelRole.class.php';
$model = new modelRole();
$arrayRole = $model->getListRole();

if (!empty($arrayRole)){
    foreach ($arrayRole as $key => $value) {
        $arrayRole[$key]['no'] = ($key + 1);
        $arrayRole[$key]['aksi'] = "<a href='".BASE_URL."?m=role&c=viewAddPage&a=view&ref_id=".$value['id_role']."'><img src='".BASE_ADDRESS."icon/detail.png' title='detail'></a>&nbsp;\n
        	<a class='btn-action' href='".BASE_URL."?m=role&c=viewAdd&a=view&ref_id=".$value['id_role']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'></a>&nbsp;\n
        	<a class='btn-action btn-hapus' data-nama='".$value['role_name']."' href='".BASE_URL."?m=role&c=doDelete&a=do&ref_id=".$value['id_role']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>";
    }
}
echo json_encode(array('data' => $arrayRole));
exit();
?>