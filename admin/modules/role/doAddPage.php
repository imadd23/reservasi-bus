<?php
require_once 'modelRole.class.php';
$model = new modelRole();

if (isset($_POST['save'])){
	$p['id_role'] = $_POST['ref_id'];

    $queryUrl = array(
	    'ref_id' => $_POST['ref_id']
    );

    $save = array();
    if (!empty($_POST['page'])){
        $model->deleteRolePage($_POST['ref_id']);
    	foreach ($_POST['page'] as $key => $value) {
    		$p['page'] = $value;
    		$check = $model->cekRolePage($p);
    		$save[] = $model->addRolePage($p);
    	}   	

    	if (in_array(false, $save)){
    		header('location: '.BASE_URL.'?m=role&c=viewAddPage&a=view&statusAdd=false&'.http_build_query($queryUrl));
    	}else{
    		header('location: '.BASE_URL.'?m=role&c=viewAddPage&a=view&statusAdd=true&'.http_build_query($queryUrl));
    	}
    }else{
    	$model->deleteRolePage($_POST['ref_id']);
    	header('location: '.BASE_URL.'?m=role&c=viewAddPage&a=view&empty=true&'.http_build_query($queryUrl));
    }
		
}

?>