<?php
$qProvinsi = mysql_query(" SELECT * FROM provinsi ORDER BY nama_provinsi ASC");
$dataProvinsi = array();
while ($arrayProvinsi = mysql_fetch_array($qProvinsi)){
    $dataProvinsi[] = $arrayProvinsi;
}

if (isset($_GET['nama_kota'])){
    $nama_kota = $_GET['nama_kota'];
    $id_provinsi = $_GET['id_provinsi'];
    $order_kota = $_GET['order_kota'];
    $ref_id = '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM kota WHERE id_kota = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $nama_kota = $a->nama_kota;
        $id_provinsi = $a->id_provinsi;
        $order_kota = $a->order_kota;
    }else{
        $nama_kota = '';
        $id_provinsi = '';
        $order_kota = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $nama_kota = '';
    $id_provinsi = '';
    $order_kota = '';
    $ref_id = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<div class="col-sm-7">
    <h3>Tambah data kota</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=kota&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama Provinsi *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_provinsi" id="id_provinsi">
                    <option value="">Pilih Provinsi</option>
                    <?php
                    if (!empty($dataProvinsi)){
                        foreach ($dataProvinsi as $key => $value) {
                            if ($value['id_provinsi'] == $id_provinsi){
                                echo '<option value="'.$value['id_provinsi'].'" selected>'.$value['nama_provinsi'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_provinsi'].'">'.$value['nama_provinsi'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nama Kota *</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="nama_kota" id="nama_kota" value="<?php echo $nama_kota;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Order Kota *</label>
            <div class="col-sm-2">
                <input type="number" class="input-sm form-control" name="order_kota" id="order_kota" value="<?php echo $order_kota;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=kota&c=viewKota&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>