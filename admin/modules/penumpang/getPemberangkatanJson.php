<?php
ob_start();
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();
$tanggal_pemberangkatan = date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan']));
$id_kota_asal = isset($_POST['id_kota_asal']) ? $_POST['id_kota_asal'] : 0;
$id_kota_tujuan = isset($_POST['id_kota_tujuan']) ? $_POST['id_kota_tujuan'] : 0;
$data = $model->getPemberangkatan($tanggal_pemberangkatan, $id_kota_asal, $id_kota_tujuan);
echo json_encode(array('success' => true, 'data' => $data));
?>