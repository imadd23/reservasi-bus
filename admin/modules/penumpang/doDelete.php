<?php
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();

$id_detail_pembelian_tiket = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : 0;
$delete = $model->deleteDetailPembelian($id_detail_pembelian_tiket);
if ($delete){
	header('location: '.BASE_URL.'?m=penumpang&c=viewPenumpang&a=view&statusDelete=true');
}else{
	header('location: '.BASE_URL.'?m=penumpang&c=viewPenumpang&a=view&statusDelete=false');
}
?>