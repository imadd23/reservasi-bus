<?php
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
    $message = '<div class="alert alert-success">Data pembelian berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data pembelian gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
    $message = '<div class="alert alert-success">Data pembelian berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
    $message = '<div class="alert alert-danger">Data pembelian gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}

$tanggal_pemberangkatan = isset($_GET['tanggal_pemberangkatan']) ? date('d-m-Y', strtotime($_GET['tanggal_pemberangkatan'])) : '';
$id_pemberangkatan = isset($_GET['id_pemberangkatan']) ? $_GET['id_pemberangkatan'] : '';
$id_kota_tujuan = isset($_GET['id_kota_tujuan']) ? $_GET['id_kota_tujuan'] : '';
$id_no_kursi = isset($_GET['id_no_kursi']) ? $_GET['id_no_kursi'] : '';
$atas_nama = isset($_GET['atas_nama']) ? $_GET['atas_nama'] : '';
$nama_pelanggan = isset($_GET['nama_pelanggan']) ? $_GET['nama_pelanggan'] : '';
$id_penumpang = isset($_GET['id_penumpang']) ? $_GET['id_penumpang'] : '';
$sub_total = isset($_GET['sub_total']) ? $_GET['sub_total'] : '';
$no_hp = isset($_GET['no_hp']) ? $_GET['no_hp'] : '';
$arrayNoKursi = $model->getDetailNoKursi($id_no_kursi);
$no_kursi = !empty($arrayNoKursi) ? $arrayNoKursi[0]['no_kursi'] : 'Pilih Nomer Kursi';
$id_detail_pembelian_tiket = isset($_GET['id_detail_pembelian_tiket']) ? $_GET['id_detail_pembelian_tiket'] : '';
$id_pelanggan = isset($_GET['id_pelanggan']) ? $_GET['id_pelanggan'] : '';

$listNoKursi = '';
$readOnly = '';
if (isset($_GET['ref_id'])){
    $d = $model->getdetailTiket($_GET['ref_id']);
    if (!empty($d)){
        $detail = $d[0];
        $tanggal_pemberangkatan = date('d-m-Y', strtotime($detail['tanggal_pemberangkatan']));
        $id_pemberangkatan = $detail['id_pemberangkatan'];
        $id_kota_tujuan = $detail['id_kota_tujuan'];
        $id_no_kursi = $detail['id_no_kursi'];
        $atas_nama = $detail['atas_nama'];
        $nama_pelanggan = $detail['nama_pelanggan'];
        $id_penumpang = $detail['id_penumpang'];
        $sub_total = $detail['sub_total'];
        $no_hp = $detail['no_hp'];
        $no_kursi = $detail['no_kursi'];
        $id_detail_pembelian_tiket = $detail['id_detail_pembelian_tiket'] ;
        $id_pelanggan = empty($detail['id_pelanggan']) ? '' : $detail['id_pelanggan'];
        $listNoKursi = $model->getNoKursi($id_pemberangkatan, $tanggal_pemberangkatan);
        $readOnly = 'readonly';
    }
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">
<div class="row">
    <div class="col-lg-8">
        <h3 class="text-center"><?php echo (isset($_GET['ref_id'])) ? 'Ubah' : 'Tambah';?> Pemesanan Tiket</h3>
        <?php echo $message;?>
        <form method="post" action="<?php echo BASE_URL.'?m=penumpang&c=doAdd&a=do';?>" class="form-horizontal">
            <input type="hidden" name="id_detail_pembelian_tiket" value="<?php echo $id_detail_pembelian_tiket;?>">
            <div class="form-group">
                <label class="control-label col-sm-4">Pilih User</label>
                <div class="col-sm-8">
                    <select name="id_pelanggan" class="form-control input-sm">
                        <?php
                        if (!empty($model->getUser())){
                            $user = $model->getUser();
                            $index = sizeof($model->getUser());
                            $user[$index]['user_id'] = '';
                            $user[$index]['username'] = 'Pilih User';

                            foreach ($user as $key => $value) {
                                if ($value['user_id'] === $id_pelanggan){
                                    echo '<option value="'.$value['user_id'].'" selected>'.$value['username'].'</option>';
                                }else{
                                    echo '<option value="'.$value['user_id'].'">'.$value['username'].'</option>';
                                }                               
                            }
                        }
                        ?>                      
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Nama Pembeli *</label>
                <div class="col-sm-8">
                    <input type="text" name="nama_pelanggan" class="form-control input-sm" value="<?php echo $nama_pelanggan;?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Tanggal Pemberangkatan *</label>
                <div class="col-sm-8">
                    <input type="text" name="tanggal_pemberangkatan" class="form-control input-sm datepicker" value="<?php echo $tanggal_pemberangkatan;?>" required <?php echo $readOnly;?>>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Pemberangkatan *</label>
                <div class="col-sm-8">
                    <select name="id_pemberangkatan" class="form-control input-sm" data-url="<?php echo BASE_URL.'?m=penumpang&c=getNoKursiJson&a=do';?>" required <?php echo $readOnly;?>>
                        <option value="">Pilih Pemberangkatan</option>
                        <?php
                        if (!empty($model->getPemberangkatan())){
                            foreach ($model->getPemberangkatan() as $key => $value) {
                                if ($value['id_pemberangkatan'] == $id_pemberangkatan){
                                    echo '<option value="'.$value['id_pemberangkatan'].'" selected>'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
                                }else{
                                    echo '<option value="'.$value['id_pemberangkatan'].'">'.$value['nama_armada'].' | '.$value['kota_asal'].' - '.$value['kota_tujuan'].'</option>';
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="col-sm-1">
                    <img id="imgLoaderIdPemberangkatan" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Kota Tujuan *</label>
                <div class="col-sm-8">
                    <select name="id_kota_tujuan" class="form-control input-sm" required>
                        <option value="">Pilih Kota Tujuan</option>
                        <?php
                        if (!empty($model->getKotaTujuan())){
                            foreach ($model->getKotaTujuan() as $key => $value) {
                                if ($value['id_kota'] == $id_kota_tujuan){
                                    echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
                                }else{
                                    echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
                                }
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <?php
            if (isset($_GET['ref_id'])){
                ?>
                <div class="form-group">
                    <label class="control-label col-sm-4">No. Kursi Sebelumnya</label>
                    <label class="control-label col-sm-4 text-success text-left" style="text-align:left;"><b><?php echo $no_kursi;?></b></label>
                    <input type="hidden" name="id_no_kursi_before" value="<?php echo $id_no_kursi;?>">
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4">Ubah Nomer Kusi</label>
                    
                    <div class="col-sm-2 text-left">
                        <select name="id_no_kursi" class="form-control input-sm">
                            <option selected value="">Pilih</option>
                            <?php
                            if (!empty($listNoKursi)){
                                foreach ($listNoKursi as $key => $value) {
                                    if ($value['id_no_kursi'] == $id_no_kursi){
                                        continue;
                                    }else{
                                        echo '<option value="'.$value['id_no_kursi'].'">'.$value['no_kursi'].'</option>';
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
            }else{
                ?>
                <div class="form-group">
                    <label class="control-label col-sm-4">Nomer Kusi *</label>
                    
                    <div class="col-sm-2 text-left">
                        <select name="id_no_kursi" class="form-control input-sm" required>
                            <?php
                            if (!empty($listNoKursi)){
                                foreach ($listNoKursi as $key => $value) {
                                    if ($value['id_no_kursi'] == $id_no_kursi){
                                        continue;
                                    }else{
                                        echo '<option value="'.$value['id_no_kursi'].'">'.$value['no_kursi'].'</option>';
                                    }
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
            }
            ?>
            
            <div class="form-group">
                <label class="control-label col-sm-4">Atas Nama *</label>
                <div class="col-sm-8">
                    <input type="text" name="atas_nama" class="form-control input-sm" value="<?php echo $atas_nama;?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Nomer HP *</label>
                <div class="col-sm-8">
                    <input type="text" name="no_hp" class="form-control input-sm" value="<?php echo $no_hp;?>" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-4">Total Biaya *</label>
                <div class="col-sm-8">
                    <label class="control-label" id="sub_total"><?php echo $sub_total;?></label>
                    <input type="hidden" name="sub_total" class="form-control input-sm" value="<?php echo $sub_total;?>">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4"></label>
                <div class="col-sm-8">
                    <button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                    <a href="<?php echo BASE_URL.'?m=penumpang&c=viewPenumpang&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });
    });

    $(document).on('change', 'select[name=id_pemberangkatan]', function(){
        var id_pemberangkatan = $(this).val();
        var imgLoader = $('#imgLoaderIdPemberangkatan');
        var optionNoKursi = '<option value="">Pilih Nomer Kursi</option>';
        imgLoader.show();
        $.ajax({
            url : $(this).attr('data-url'),
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : $('input[name=tanggal_pemberangkatan]').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        optionNoKursi += '<option value="'+v['id_no_kursi']+'">'+v['no_kursi']+'</option>';
                    });
                    $('select[name=id_no_kursi]').html(optionNoKursi);
                    $('#sub_total').html(response.harga);
                    $('input[name=sub_total]').val(response.harga);
                }
            },
            error : function(){
                imgLoader.hide();
                alert('Gagal Mengambil data nomer kursi kosong');
            }
        });
    });
</script>