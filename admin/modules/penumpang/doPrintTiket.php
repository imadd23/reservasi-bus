<?php
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
require_once 'modelPenumpang.class.php';
Library::load('html2pdf.php', 'html2pdf');
$model = new modelPenumpang();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$detail = $model->getdetailTiket($refId);
if (empty($detail)){
	die('Tiket tidak ditemukan');
}
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	td.font-small{
		font-size: 11px;
	}
	table.table-control{
		padding: 5px;
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
		color: green;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid green;
		padding: 3px;
		color: green;
	}
</style>
<page>
	<div style="background-image: url(<?php echo BASE_ADDRESS.'images/bg-family-smaller.png'?>); background-repeat: repeat; margin-left;0px; margin-top:0px; height:99.7%;">
		<table class="table-control table-bordered">
			<col style="width:100%">
			<tr>
				<td><img src="<?php echo BASE_ADDRESS.'images/header-tiket.png'?>" style="width:100%;"></td>
			</tr>
		</table>
		<!-- <h2 class="center">PO FAMILIY RAYA CERIA</h2>
		<h3 class="center no-padding">TIKET BUS</h3> -->
		<hr>
		<!-- <table>
			<tr>
				<th>Order ID</th><td>: <?php echo $detail[0]['id_pembelian_tiket'];?></td>
			</tr>
			<tr>
				<th>Tanggal Order</th><td>: <?php echo date('d M Y', strtotime($detail[0]['tanggal_beli']));?></td>
			</tr>
		</table>
		<hr class="dashed"> -->
		<table class="table-control table-bordered">
			<col style="width: 35%;">
		    <col style="width: 20%;">
		    <col style="width: 15%;">
		    <col style="width: 15%;">
		    <col style="width: 15%;">
			<tr>
				<th>Nama Penumpang</th>
				<th>Tujuan</th>
				<th>Berangkat Tgl</th>
				<th>Berangkat Jam</th>
				<th>No. Tempat Duduk</th>
			</tr>
			<?php
			foreach ($detail as $key => $value) {
				echo 
				'<tr>'.
					'<td style="height:100px;">'.$value['atas_nama'].'</td>'.
					'<td style="height:100px;">'.$value['kota_tujuan'].'</td>'.
					'<td style="height:100px;">'.date('d M Y', strtotime($value['tanggal_pemberangkatan'])).'</td>'.
					'<td style="height:100px;">'.$value['jam'].'</td>'.
					'<td style="height:100px;">'.$value['no_kursi'].'</td>'.					
				'</tr>';
			}
			?>
		</table>
		<table class="table-control">
			<col style="width: 40%;">
		    <col style="width: 35%;">
		    <col style="width: 25%;">
			<tr>
				<td class="font-small">
					<table>
						<tr>
							<td colspan="2">PT. (Persero) "Jasa Raharja"</td>
						</tr>
						<tr>
							<td>Kantor Pusat</td><td>: Jl. Lintas Sumatra Km. 2 Bangko Telp(0746) 21642</td>
						</tr>
						<tr>
							<td>Yogyakarta</td><td>: Terminal Giwangan E-28 Telp(0224) 410034</td>
						</tr>
						<tr>
							<td></td><td>Jl. Gambitan No. 26 Telp. (0274) 378455</td>
						</tr>
					</table>
 				</td>
 				<td class="font-small" style="text-align:center;">
 					Pengurus Loket<br><br><br>(&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;)
 				</td>
 				<td class="font-small">
 					Jumlah Ongkos	:<br>
 					Uang Muka		:<br>
 					Ketinggalan		:<br>
 				</td>	
			</tr>
		</table>
	</div>
	<!-- <page_footer>
		<div class="col-sm-14 center" style="background-image: url(<?php echo BASE_ADDRESS.'images/bg-family-smaller.png'?>); background-repeat: repeat; margin-left;0px; margin-top:0px;">
			<hr class="dashed">
			<i>Terima kasih atas kepercayaan Anda kepada PO. Family Ceria</i>
		</div>
	</page_footer> -->
</page>
<?php $content = ob_get_clean();
try{
	// $html2pdf = new html2pdf('P', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf = new html2pdf('L', array(250, 100), 'en', true, 'UTF-8', array(0, 0, 0, 0));
	$html2pdf->pdf->SetTitle('Tiket');
	$html2pdf->writeHTML($content);
	$html2pdf->Output('cetak.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>