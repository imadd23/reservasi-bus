<?php
require_once MODULE_PATH.'core/model.core.php';

class modelPenumpang extends model{

	public function __construct(){
		parent::__construct();
	}

	public function addUser($data){
		$newData = $this->setData($data);
		mysql_query(" INSERT INTO login 
			SET ".$newData);
	    $affectedRows = mysql_affected_rows();
	    return $affectedRows;
	}

	public function getPemberangkatan($tanggal_pemberangkatan = 0, $id_kota_asal, $id_kota_tujuan){
		$tanggal_pemberangkatan = date('Y-m-d', strtotime($tanggal_pemberangkatan));
		$day = $this->dateToDayIndonesia($tanggal_pemberangkatan);
		$qPemberangkatan = "SELECT * FROM (
			SELECT P.*, DAYNAME('$tanggal_pemberangkatan') AS hari_english,
			CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			nama_armada,
			(
				SELECT COUNT(NK1.`id_no_kursi`) 
				FROM no_kursi AS NK1 
				WHERE NK1.id_armada = E.`id_armada` 
				LIMIT 1
			) AS jumlah_kursi,
			(
				SELECT COUNT(DP2.id_no_kursi) 
				FROM detail_pembelian AS DP2 
				WHERE 
					DP2.id_pemberangkatan = P.`id_pemberangkatan` 
					AND DATE(tanggal_pemberangkatan) = '$tanggal_pemberangkatan'
					AND DP2.status_batal = '0'
			) AS jumlah_kursi_terpesan
			FROM pemberangkatan AS P
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			GROUP BY P.id_pemberangkatan
			ORDER BY CONCAT(`id_kota_asal`, `id_kota_tujuan`, `jumlah_kursi_terpesan`) DESC
		) AS temp_table 
		WHERE LOWER(hari) = LOWER('$day')";
		if (!empty($id_kota_asal)){
			$qPemberangkatan .= "AND id_kota_asal = '$id_kota_asal'";
		}
		if (!empty($id_kota_tujuan)){
			$qPemberangkatan .= "AND id_kota_tujuan = '$id_kota_tujuan'";
		}
		$qPemberangkatan .= "GROUP BY `id_pemberangkatan`";
		
		// print_r('<pre>');
		// print_r($qPemberangkatan);
		// die();
		$qPemberangkatan = mysql_query($qPemberangkatan);

		$arrayPemberangkatan = array();
		// ambil bus yang belum full terisi penumpang, dan tunggu sampai bus dengan jurusan yang sama full;
		while ($dataPemberangkatan = mysql_fetch_array($qPemberangkatan)) {
			$key = $dataPemberangkatan['id_kota_asal'].$dataPemberangkatan['id_kota_tujuan'];
			$a = (int) $dataPemberangkatan['jumlah_kursi_terpesan'];
			$b = (int) $dataPemberangkatan['jumlah_kursi'];
			if (!array_key_exists($key, $arrayPemberangkatan) && $a < $b){
				$arrayPemberangkatan[$key] = $dataPemberangkatan;
			}
		}
		return $arrayPemberangkatan;
	}

	public function getSettingPembelianTiket(){
		$query = mysql_query("SELECT * FROM setting WHERE id IN ('BT')");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getUser(){
		$query = mysql_query("SELECT *, CONCAT(nama, '|', no_hp) AS nama FROM login WHERE id_role = '2'");

		$arrayData = array();
		while ($data = mysql_fetch_array($query)) {
		    $arrayData[] = $data;
		}

		return $arrayData;
	}

	public function getKotaAsal(){
		$qKotaAsal = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_asal
			FROM pemberangkatan
			LEFT JOIN kota ON pemberangkatan.`id_kota_asal` = kota.`id_kota`
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
			GROUP BY id_kota
			ORDER BY `order_kota` ASC");

		$arrayKotaAsal = array();
		while ($dataKotaAsal = mysql_fetch_array($qKotaAsal)) {
		    $arrayKotaAsal[] = $dataKotaAsal;
		}

		return $arrayKotaAsal;
	}

	public function getKotaTujuan(){
		$qKotaTujuan = mysql_query("SELECT kota.*, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS lokasi_tujuan
			FROM pemberangkatan
			LEFT JOIN kota ON pemberangkatan.`id_kota_tujuan` = kota.`id_kota`
			LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
			GROUP BY id_kota
			ORDER BY `order_kota` ASC");

		$arrayKotaTujuan = array();
		while ($dataKotaTujuan = mysql_fetch_array($qKotaTujuan)) {
		    $arrayKotaTujuan[] = $dataKotaTujuan;
		}

		return $arrayKotaTujuan;
	}

	public function getNoKursi($id_pemberangkatan, $tanggal_pemberangkatan){
		$qNokursi = mysql_query("SELECT * FROM no_kursi 
			WHERE id_armada = (SELECT id_armada FROM pemberangkatan WHERE id_pemberangkatan = '".$id_pemberangkatan."' LIMIT 1)
			AND id_no_kursi NOT IN (SELECT id_no_kursi FROM detail_pembelian WHERE id_pemberangkatan = '".$id_pemberangkatan."' AND DATE(tanggal_pemberangkatan) = '".$tanggal_pemberangkatan."' )
			ORDER BY no_kursi ASC");
		$arrayNoKursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNokursi)){
			$arrayNoKursi[] = $dataNoKursi;
		}

		return $arrayNoKursi;
	}

	public function getNoKursiUsed($id_pemberangkatan, $tanggal_pemberangkatan){
		$qNokursi = mysql_query("SELECT * FROM no_kursi 
			WHERE id_armada = (SELECT id_armada FROM pemberangkatan WHERE id_pemberangkatan = '".$id_pemberangkatan."' LIMIT 1)
			AND id_no_kursi IN (SELECT id_no_kursi FROM detail_pembelian WHERE id_pemberangkatan = '".$id_pemberangkatan."' AND DATE(tanggal_pemberangkatan) = '".$tanggal_pemberangkatan."' AND status_batal = '0')
			ORDER BY id_no_kursi ASC");
		$arrayNoKursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNokursi)){
			$arrayNoKursi[] = $dataNoKursi;
		}

		return $arrayNoKursi;
	}

	public function getHarga($id_pemberangkatan){
		$qHarga = mysql_query("SELECT harga FROM pemberangkatan WHERE id_pemberangkatan = '".$id_pemberangkatan."'");
		$arrayHarga = array();
		while ($dataHarga = mysql_fetch_array($qHarga)){
			$arrayHarga[] = $dataHarga;
		}

		return $arrayHarga;
	}

	public function getIdPembelianTiket(){
		$qIdPembelianTiket = mysql_query("SELECT id FROM
						(
						SELECT @id := REPLACE(id_pembelian_tiket, 'O-', '') + 1, CONCAT('O-', @id) AS id 
						FROM pembelian_tiket 
						UNION
						SELECT @id := '', 'O-1' AS id
						)
						AS temp_table
						ORDER BY REPLACE(id, 'O-', '') * 1 DESC 
						LIMIT 1");
		$dataIdPembelianTiket = mysql_fetch_object($qIdPembelianTiket);
		return $dataIdPembelianTiket->id;
	}

	public function getPenumpang(){
		$queryPenumpang = "SELECT 
			TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) AS jarak_waktu_beli,
			PT.id_pembelian_tiket, PT.tanggal_beli, PT.id_pelanggan,  PT.nama_pelanggan, PT.no_hp, PT.total_harga, 
			PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, CONCAT(nama_armada, ' | ', NK.no_kursi) AS armada_kursi,
			IF( DP.status_batal = '0' AND TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), 
				'1', '0')
			AS can_cancelled, PT.tanggal_bayar
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC";

		$qPenumpang = mysql_query($queryPenumpang);
		$arrayPenumpang = array();
		while ($dataPenumpang = mysql_fetch_array($qPenumpang)) {
		    $arrayPenumpang[] = $dataPenumpang;
		}
		return $arrayPenumpang;
	}

	public function getPenumpangPerPemberangkatan($tgl, $id_pemberangkatan){
		$queryPenumpang = "SELECT 
			TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) AS jarak_waktu_beli,
			PT.id_pembelian_tiket, PT.tanggal_beli, PT.id_pelanggan,  PT.nama_pelanggan, PT.no_hp, PT.total_harga, 
			PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, CONCAT(nama_armada, ' | ', NK.no_kursi) AS armada_kursi,
			IF( DP.status_batal = '0' AND TIMESTAMPDIFF(HOUR, PT.`tanggal_beli`, NOW()) <= (SELECT `set` FROM setting WHERE id = 'BT' LIMIT 1), 
				'1', '0')
			AS can_cancelled, PT.tanggal_bayar
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE
				DATE(DP.tanggal_pemberangkatan) = '".date('Y-m-d' ,strtotime($tgl))."' 
				AND DP.id_pemberangkatan = '$id_pemberangkatan' 
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC";

		$qPenumpang = mysql_query($queryPenumpang);
		$arrayPenumpang = array();
		while ($dataPenumpang = mysql_fetch_array($qPenumpang)) {
		    $arrayPenumpang[] = $dataPenumpang;
		}
		return $arrayPenumpang;
	}

	public function getPenumpangForReport($from, $to, $status_bayar){
		$queryPenumpang = "SELECT 
			PT.id_pembelian_tiket, PT.tanggal_beli, PT.id_pelanggan,  PT.nama_pelanggan, PT.no_hp, PT.total_harga, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, CONCAT(nama_armada, ' | ', NK.no_kursi) AS armada_kursi
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE 
				PT.tanggal_beli BETWEEN '".$from."' AND '".$to."'
				AND PT.status_bayar = '".$status_bayar."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC";

		$qPenumpang = mysql_query($queryPenumpang);
		$arrayPenumpang = array();
		while ($dataPenumpang = mysql_fetch_array($qPenumpang)) {
		    $arrayPenumpang[] = $dataPenumpang;
		}
		return $arrayPenumpang;
	}

	public function getdetailTiket($refId){
		$qPembelian = mysql_query("SELECT 
			PT.id_pembelian_tiket, PT.tanggal_beli, PT.id_pelanggan,  PT.nama_pelanggan, PT.no_hp, PT.total_harga, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, P.jam
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE DP.id_detail_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
		$arrayPembelian = array();
		while ($dataPembelian = mysql_fetch_array($qPembelian)){
			$arrayPembelian[] = $dataPembelian;
		}

		return $arrayPembelian;
	}

	public function getdetailTiketForEdit($refId){
		$qPembelian = mysql_query("SELECT 
			`PT`.`id_pembelian_tiket`, PT.`tanggal_beli`, PT.status_bayar, DP.*, CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
			CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
			CONCAT(F.nama_kota, ' (', G.nama_provinsi, ')') AS lokasi_tujuan,
			nama_armada, NK.no_kursi, PT.total_harga, P.id_kota_asal, P.id_armada
			FROM `pembelian_tiket` AS PT
			INNER JOIN detail_pembelian DP ON PT.id_pembelian_tiket = DP.id_pembelian_tiket
			LEFT JOIN pemberangkatan AS P ON DP.`id_pemberangkatan` = P.`id_pemberangkatan`
			LEFT JOIN kota AS A
			    ON A.`id_kota` = P.`id_kota_asal`
			LEFT JOIN provinsi AS C
			    ON C.`id_provinsi` = A.`id_provinsi`
			LEFT JOIN kota AS B
			    ON B.`id_kota` = P.`id_kota_tujuan`
			LEFT JOIN provinsi AS D
			    ON D.`id_provinsi` = B.`id_provinsi`
			LEFT JOIN armada E
			    ON P.`id_armada` = E.`id_armada`
			LEFT JOIN no_kursi AS NK
				ON DP.id_no_kursi = NK.id_no_kursi
			LEFT JOIN kota AS F
			    ON F.`id_kota` = DP.`id_kota_tujuan`
			LEFT JOIN provinsi AS G
			    ON G.`id_provinsi` = F.`id_provinsi`
			WHERE DP.id_detail_pembelian_tiket = '".$refId."'
			ORDER BY PT.`tanggal_beli`, PT.`modified_on` DESC");
		$arrayPembelian = array();
		while ($dataPembelian = mysql_fetch_array($qPembelian)){
			$arrayPembelian[] = $dataPembelian;
		}

		return $arrayPembelian;
	}

	public function getDetailNoKursi($id_no_kursi){
		$qNokursi = mysql_query("SELECT * FROM no_kursi 
			WHERE id_no_kursi = '".$id_no_kursi."'");
		$arrayNoKursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNokursi)){
			$arrayNoKursi[] = $dataNoKursi;
		}

		return $arrayNoKursi;
	}

	public function cekPembelian($id_pelanggan, $nama_pelanggan, $tanggal_beli){
		$qcek = mysql_query("SELECT * 
			FROM pembelian_tiket 
			WHERE 
			(
				id_pelanggan = '".$id_pelanggan."' AND 
				DATE(tanggal_beli) = '".$tanggal_beli."'
			) OR 
			(
				nama_pelanggan = '".$nama_pelanggan."' AND
				DATE(tanggal_beli) = '".$tanggal_beli."'
			)");
		return mysql_fetch_object($qcek);
	}

	public function addPembelian($id_pembelian_tiket, $id_pelanggan, $total_harga, $status_bayar){
		$query = "INSERT INTO pembelian_tiket SET 	
		    		id_pembelian_tiket = '".$id_pembelian_tiket."',
		    		tanggal_beli = '".date('Y-m-d')."',
		    		id_pelanggan = '".$id_pelanggan."',
		    		nama_pelanggan = (SELECT IFNULL(nama, '') AS _nama FROM login WHERE user_id = '".$id_pelanggan."' LIMIT 1),
		    		no_hp = (SELECT IFNULL(no_hp, '') AS _no_hp FROM login WHERE user_id = '".$id_pelanggan."' LIMIT 1),
		    		total_harga = '".$total_harga."',
		    		status_bayar = '".$status_bayar."'";
		$save = mysql_query($query);
		return $save;
	}

	public function updatePembelian($id_pembelian_tiket, $id_pelanggan, $nama_pelanggan, $no_hp, $total_harga){
		$query = "UPDATE pembelian_tiket SET
		    		nama_pelanggan = '".$nama_pelanggan."',
		    		no_hp = '".$no_hp."',
		    		total_harga = (
		    			SELECT IFNULL(SUM(sub_total), 0) 
		    			FROM 
		    				detail_pembelian 
		    			WHERE 
		    				id_pembelian_tiket = '".$id_pembelian_tiket."'
		    				AND status_batal = '0'
		    		)";
		// if (!empty($id_pelanggan)){
		// 	$query .= ", id_pelanggan = '".$id_pelanggan."'";
		// }else{
		// 	$query .= ", id_pelanggan = NULL";
		// }

		$query .= " WHERE
		    		id_pembelian_tiket = '".$id_pembelian_tiket."'";

		$update = mysql_query($query);
		return $update;
	}

	public function addDetailPembelian($id_pembelian_tiket, $id_pemberangkatan, $id_kota_tujuan, $id_no_kursi, $tanggal_pemberangkatan, $atas_nama, $id_penumpang, $sub_total){
		$query = "INSERT INTO 
			detail_pembelian
				(id_pembelian_tiket,  id_pemberangkatan,  id_kota_tujuan,  id_no_kursi,  tanggal_pemberangkatan,  atas_nama,  id_penumpang,  sub_total) 
			VALUES 
				(
					'".$id_pembelian_tiket."', 
					'".$id_pemberangkatan."', 
					'".$id_kota_tujuan."', 
					'".$id_no_kursi."', 
					'".$tanggal_pemberangkatan."', 
					'".$atas_nama."', 
					'".$id_penumpang."', 
					'".$sub_total."'
				)";
		$insert = mysql_query($query);
		return $insert;
	}

	public function EditDetailPembelian($id_pembelian_tiket, $id_pemberangkatan, $id_kota_tujuan, $id_no_kursi, $tanggal_pemberangkatan, $atas_nama, $id_penumpang, $sub_total, $id_detail_pembelian_tiket){
		$query = "UPDATE
			detail_pembelian
			SET
			id_pembelian_tiket = '".$id_pembelian_tiket."', 
			id_pemberangkatan = '".$id_pemberangkatan."', 
			id_kota_tujuan = '".$id_kota_tujuan."', 
			id_no_kursi = '".$id_no_kursi."', 
			tanggal_pemberangkatan = '".$tanggal_pemberangkatan."', 
			atas_nama = '".$atas_nama."', 
			id_penumpang = '".$id_penumpang."', 
			sub_total = '".$sub_total."'
			WHERE id_detail_pembelian_tiket = '".$id_detail_pembelian_tiket."'";
		// return $query;
		$update = mysql_query($query);
		return $update;
	}

	public function deleteDetailPembelian($ref_id){
		$query = mysql_query("SELECT id_pembelian_tiket 
			WHERE 
			id_detail_pembelian_tiket = '".$ref_id."'");
		$result = mysql_fetch_object($query);
		$delete = mysql_query("DELETE FROM detail_pembelian 
			WHERE 
			id_detail_pembelian_tiket = '".$ref_id."'");
		if ($delete && !empty($result)){
			mysql_query("DELETE 
				FROM pembelian_tiket 
				WHERE 
				id_pembelian_tiket = '".$result->id_pembelian_tiket."' AND 
				(
					SELECT id_pembelian_tiket 
					FROM detail_pembelian 
					WHERE id_pembelian_tiket 
					LIMIT 1
				) IS NOT NULL
			");
		}
		return $delete;
	}

	public function dayToEnglish($hari){
		switch (strtolower($hari)) {
			case 'minggu':
				$result = 'sunday';
				break;
			case 'senin':
				$result = 'monday';
				break;
			case 'selasa':
				$result = 'tuesday';
				break;
			case 'rabu':
				$result = 'wednesday';
				break;
			case 'kamis':
				$result = 'thursday';
				break;
			case 'jumat':
				$result = 'friday';
				break;
			case 'sabtu':
				$result = 'saturday';
				break;
			default:
				$result = 'sunday';
				break;
		}
		return $result;
	}

	public function dayToIndonesia($hari){
		$day = substr($hari, 0, 3);
		switch (strtolower($day)) {
			case 'sun':
				$result = 'minggu';
				break;
			case 'mon':
				$result = 'senin';
				break;
			case 'tue':
				$result = 'selasa';
				break;
			case 'wed':
				$result = 'rabu';
				break;
			case 'thu':
				$result = 'kamis';
				break;
			case 'fri':
				$result = 'jumat';
				break;
			case 'sat':
				$result = 'sabtu';
				break;
			default:
				$result = 'minggu';
				break;
		}
		return $result;
	}

	public function dateToDayIndonesia($date){
		$day = date('D', strtotime($date));
		return $this->dayToIndonesia($day);
	}

	public function getDataNoKursi($id_armada){
		$queryNoKursi = "SELECT no_kursi.*, nama_armada FROM no_kursi 
		LEFT JOIN armada ON armada.`id_armada` = no_kursi.`id_armada`
		WHERE armada.id_armada = '$id_armada'
		GROUP BY armada.id_armada, no_kursi.no_kursi
		ORDER BY id_no_kursi ASC";
		$qNoKursi = mysql_query($queryNoKursi);
		$arrayNokursi = array();
		while ($dataNoKursi = mysql_fetch_array($qNoKursi)) {
		    $arrayNokursi[] = $dataNoKursi;
		}
		return $arrayNokursi;
	}

	public function coreNoKursi(){
		$lineKursi = range(0, 11);
		$array = array();
		foreach ($lineKursi as $key => $value) {
            if ($key == 10){
            	$array[$key][0] = '';
            	$array[$key][1] = '';
            }else
            if ($key == 11){
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            }else{
                $array[$key][0] = '';
            	$array[$key][1] = '';
            	$array[$key][2] = '';
            	$array[$key][3] = '';
            }                            
        }
        return $array;
	}

	public function getSusunanNoKursi($id_armada){
		$arraySusunan = $this->coreNoKursi();
		$dataNoKursi = $this->getDataNoKursi($id_armada);
		$index = 0;
		foreach ($arraySusunan as $key => $value) {
			foreach ($value as $k => $v) {
				if (isset($dataNoKursi[$index])){
					$arraySusunan[$key][$k] = $dataNoKursi[$index];
					$index++;
				}
			}
		}
		return $arraySusunan;
	}
}
?>