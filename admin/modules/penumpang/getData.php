<?php
ob_start();
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();
if (isset($_POST['tgl'])){
	$tgl = $_POST['tgl'];
	$id_pemberangkatan = $_POST['id_pemberangkatan'];
	$arrayPenumpang = $model->getPenumpangPerPemberangkatan($tgl, $id_pemberangkatan);
}else{
	$arrayPenumpang = $model->getPenumpang();
}

$setting = $model->getSettingPembelianTiket();
if (!empty($arrayPenumpang)){
	foreach ($arrayPenumpang as $key => $value) {
		$arrayPenumpang[$key]['no'] = $key + 1;
		if ($value['status_batal'] == '1'){
			$button = '<label class="label label-warning">dibatalkan</label>';
		}else{
			$button = "<a class='btn-action' href='".BASE_URL."?m=penumpang&c=viewEdit&a=view&ref_id=".$value['id_detail_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'></a>
			&nbsp;<a class='btn-action' data-nama='".$value['atas_nama']."' target='_blank' href='".BASE_URL."?m=penumpang&c=doPrintTiket&a=do&ref_id=".$value['id_detail_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/print.png' title='cetak tiket'></a>";
			$jarak_waktu_beli = (int) $value['jarak_waktu_beli'];
			if (($value['status_bayar'] == 'belum_lunas' || ($jarak_waktu_beli >= $setting)) || 
				$value['can_cancelled'] == '1'){
				$button .= "&nbsp;<a class='btn-action btn-hapus' data-nama='".$value['atas_nama']."' href='".BASE_URL."?m=penumpang&c=doDelete&a=do&ref_id=".$value['id_detail_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/trash.png' title='batalkan'></a>";
			}else
			if ($value['status_bayar'] == 'lunas'){
				$button .= "&nbsp;<a class='btn-action' data-nama='".$value['atas_nama']."' target='_blank' href='".BASE_URL."?m=penumpang&c=doPrintNota&a=do&ref_id=".$value['id_pembelian_tiket']."'><img src='".BASE_ADDRESS."icon/nota.png' title='cetak nota pembelian'></a>";
			}
		}
		
		$arrayPenumpang[$key]['aksi'] = $button;
	}
}

echo json_encode(array('data' => $arrayPenumpang));
?>