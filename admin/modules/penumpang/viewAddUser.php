<?php
require_once 'modelUser.class.php';
$model = new modelUser();

if (isset($_GET['nama'])){
    $id_role = $_GET['id_role'];
    $nama = $_GET['nama'];
    $alamat = $_GET['alamat'];
    $jenis_kelamin = $_GET['jenis_kelamin'];
    $no_rekening = $_GET['no_rekening'];
    $id_bank = $_GET['id_bank'];
    $no_hp = $_GET['no_hp'];
    $email = $_GET['email'];
    $username = $_GET['username'];
    $pertanyaan = $_GET['pertanyaan'];
    $jawaban = $_GET['jawaban'];
    $status = $_GET['status'];
    $ref_id = (isset($_GET['ref_id'])) ? $_GET['ref_id'] : '';
}else
if (isset($_GET['ref_id'])){
    $p = $model->getUser($_GET['ref_id']);
    if ($p){
        $id_role = $p->id_role;
        $nama = $p->nama;
        $alamat = $p->alamat;
        $jenis_kelamin = $p->jenis_kelamin;
        $no_rekening = $p->no_rekening;
        $id_bank = $p->id_bank;
        $no_hp = $p->no_hp;
        $email = $p->email;
        $username = $p->username;
        $pertanyaan = $p->pertanyaan;
        $jawaban = $p->jawaban;
        $status = $p->status;
    }else{
        $id_role = '';
        $nama = '';
        $alamat = '';
        $jenis_kelamin = '';
        $no_rekening = '';
        $id_bank = '';
        $no_hp = '';
        $email = '';
        $username = '';
        $pertanyaan = '';
        $jawaban = '';
        $status = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $ref_id = '';
    $id_role = '';
    $nama = '';
    $alamat = '';
    $jenis_kelamin = '';
    $no_rekening = '';
    $id_bank = '';
    $no_hp = '';
    $email = '';
    $username = '';
    $pertanyaan = '';
    $jawaban = '';
    $status = '';
}

$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
?>