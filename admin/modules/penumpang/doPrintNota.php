<?php
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
require_once MODULE_PATH.'pembayaran/modelPembayaran.class.php';

Library::load('html2pdf.php', 'html2pdf');
$model = new modelPembayaran();
$refId = isset($_GET['ref_id']) ? $_GET['ref_id'] : die('referensi ID tidak ditemukan');
$detail = $model->getPembeian($refId);
if (empty($detail)){
	die('Data pembelian tiket tidak ditemukan');
}
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding">NOTA PEMBELIAN TIKET</h3>
<hr>
<table>
	<tr>
		<th>Order ID</th><td>: <?php echo $detail[0]['id_pembelian_tiket'];?></td>
	</tr>
	<tr>
		<th>Tanggal Order</th><td>: <?php echo date('d M Y', strtotime($detail[0]['tanggal_beli']));?></td>
	</tr>
	<tr>
		<th>Total Harga</th><td>: <?php echo 'Rp. '.number_format($detail[0]['total_harga']);?></td>
	</tr>
	<tr>
		<th>Total Bayar</th><td>: <?php echo 'Rp. '.number_format($detail[0]['total_harga']);?></td>
	</tr>
	<tr>
		<th>Tanggal Bayar</th><td>: <?php echo date('d M Y', strtotime($detail[0]['tanggal_bayar']));?></td>
	</tr>
	<!-- <tr>
		<th>Metode Pembayaran</th><td>: <?php echo $detail[0]['metode_pembayaran'];?></td>
	</tr> -->
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>Terima kasih atas kepercayaan Anda kepada PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	// $html2pdf = new html2pdf('P', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf = new html2pdf('L', array(101.6, 101.6), 'en', true, 'UTF-8', array(2, 2, 2, 2));
	$html2pdf->pdf->SetTitle('Nota Pembelian Tiket');
	$html2pdf->pdf->SetAuthor('PO Family Ceria');
	$html2pdf->writeHTML($content);
	$html2pdf->Output('nota-'.$refId.'.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>