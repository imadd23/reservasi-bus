<?php
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();
$detailId = isset($_GET['ref_id']) ? $_GET['ref_id'] : 0;
$detailTiket = $model->getdetailTiketForEdit($detailId);
// print_r($detailTiket);die();
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '1'){
	$waktu_pembatalan = $model->getSettingPembelianTiket();
	$message = '<div class="alert alert-success">Data pembelian berhasil disimpan, cek di profil Anda</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '1'){
	$message = '<div class="alert alert-success">Data pembelian berhasil diperbaharui, cek di profil Anda</div>';
}else
if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0'){
	$message = '<div class="alert alert-danger">Data pembelian gagal diperbaharui, pastikan semua kolom yang bertanda bintang diisi</div>';
}

$tanggal_pemberangkatan = isset($detailTiket[0]['tanggal_pemberangkatan']) ? date('d-m-Y', strtotime($detailTiket[0]['tanggal_pemberangkatan'])) : '';
$id_pemberangkatan = isset($detailTiket[0]['id_pemberangkatan']) ? $detailTiket[0]['id_pemberangkatan'] : '';
$id_kota_tujuan = isset($detailTiket[0]['id_kota_tujuan']) ? $detailTiket[0]['id_kota_tujuan'] : '';
$id_kota_asal = isset($detailTiket[0]['id_kota_asal']) ? $detailTiket[0]['id_kota_asal'] : '';
$id_no_kursi = isset($detailTiket[0]['id_no_kursi']) ? $detailTiket[0]['id_no_kursi'] : '';
$no_kursi = isset($detailTiket[0]['no_kursi']) ? $detailTiket[0]['no_kursi'] : '';
$atas_nama = isset($detailTiket[0]['atas_nama']) ? $detailTiket[0]['atas_nama'] : '';
$id_penumpang = isset($detailTiket[0]['id_penumpang']) ? $detailTiket[0]['id_penumpang'] : '';
$sub_total = isset($detailTiket[0]['sub_total']) ? $detailTiket[0]['sub_total'] : '';
$id_detail_pembelian_tiket = isset($detailTiket[0]['id_detail_pembelian_tiket']) ? $detailTiket[0]['id_detail_pembelian_tiket'] : '';
$id_pembelian_tiket = isset($detailTiket[0]['id_pembelian_tiket']) ? $detailTiket[0]['id_pembelian_tiket'] : '';
$nama_armada = isset($detailTiket[0]['nama_armada']) ? $detailTiket[0]['nama_armada'] : '';
$kota_asal = isset($detailTiket[0]['kota_asal']) ? $detailTiket[0]['kota_asal'] : '';
$kota_tujuan = isset($detailTiket[0]['kota_tujuan']) ? $detailTiket[0]['kota_tujuan'] : '';
$id_armada = isset($detailTiket[0]['id_armada']) ? $detailTiket[0]['id_armada'] : '';
$_pemberangkatan = !empty($id_pemberangkatan) ? '<option value="'.$id_pemberangkatan.'" data-id-armada="'.$id_armada.'" selected>'.$nama_armada.' | '.$kota_asal.' - '.$kota_tujuan.'</option>' : '<option>Pilih Pemberangkatan</option>';
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'css/bootstrap-timepicker.css';?>">
<div class="row">
	<div class="col-lg-12">
		<h3 class="text-center">Ubah Data Pemesanan Tiket</h3>
		<?php echo $message;?>
		<form method="post" action="<?php echo BASE_URL.'?m=penumpang&c=doEdit&a=do';?>" class="form-horizontal">
			<div id="form-item-pemesanan">
				<table class="table table-bordered table-stiped form-sub-item-pemesanan">
					<tr>
						<td class="col-sm-2">
							<label class="text-green">Tanggal Pemberangkatan *</label>
						</td>
						<td class="col-sm-4">
							<div class="col-sm-11">
								<input type="hidden" name="id_detail_pembelian_tiket[]" value="<?php echo $id_detail_pembelian_tiket;?>">
								<input type="hidden" name="id_penumpang[]" class="form-control input-sm" value="0" required>
								<input type="hidden" name="id_pembelian_tiket[]" value="<?php echo $id_pembelian_tiket;?>">
								<input type="text" name="tanggal_pemberangkatan[]" class="form-control input-sm datepicker tanggal_pemberangkatan" value="<?php echo $tanggal_pemberangkatan;?>" required>
							</div>
							<img id="imgLoaderTglPemberangkatan" class="imgLoaderTglPemberangkatan pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
						</td>
						<td>
							<label class="text-green">Nomer Kursi *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<div class="input-group">
									<input type="hidden" name="id_no_kursi[]" class="id_no_kursi" value="<?php echo $id_no_kursi;?>">
							      	<input type="text" name="no_kursi[]" class="form-control no_kursi" placeholder="Pilih No Kursi" readonly value="<?php echo $no_kursi;?>">
							      	<span class="input-group-btn">
							        	<button class="btn btn-default btn-cek-no-kursi" type="button" data-toggle="modal" data-target="#myModal">Cek</button>
							      	</span>
							    </div><!-- /input-group -->
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<label class="text-green">Kota Asal *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<select name="id_kota_asal[]" class="form-control input-sm id_kota_asal" required>
									<option value="">Pilih Kota Tujuan</option>
									<?php
									if (!empty($model->getKotaAsal())){
										foreach ($model->getKotaAsal() as $key => $value) {
											if ($value['id_kota'] == $id_kota_asal){
												echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_asal'].'</option>';
											}else{
												echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_asal'].'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</td>
						<td>
							<label class="text-green">Atas Nama *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<input type="text" name="atas_nama[]" class="form-control input-sm atas_nama" value="<?php echo $atas_nama;?>" required>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<label class="text-green">Kota Tujuan *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<select name="id_kota_tujuan[]" class="form-control input-sm id_kota_tujuan" required>
									<option value="">Pilih Kota Tujuan</option>
									<?php
									if (!empty($model->getKotaTujuan())){
										foreach ($model->getKotaTujuan() as $key => $value) {
											if ($value['id_kota'] == $id_kota_tujuan){
												echo '<option value="'.$value['id_kota'].'" selected>'.$value['lokasi_tujuan'].'</option>';
											}else{
												echo '<option value="'.$value['id_kota'].'">'.$value['lokasi_tujuan'].'</option>';
											}
										}
									}
									?>
								</select>
							</div>
						</td>
						<td>
							<label class="text-green">Harga Tiket *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<label class="control-label label_sub_total" id="sub_total"><?php echo $sub_total;?></label>
								<input type="hidden" name="sub_total[]" class="sub_total" class="form-control input-sm" value="<?php echo $sub_total;?>">
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<label class="text-green">Pemberangkatan *</label>
						</td>
						<td>
							<div class="col-sm-11">
								<select name="id_pemberangkatan[]" class="form-control input-sm id_pemberangkatan" required>
									<?php echo $_pemberangkatan;?>
								</select>
							</div>
						</td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</div>
			<!-- <div class="form-group">
				<div class="col-sm-12 col-sm-offset-5">
					<input type="hidden" name="total" class="form-control total" value="0">
					<label class="text-success label-total">Total Bayar: Rp.0,-</label>
				</div>
			</div> -->
			<div class="form-group">
				<div class="col-sm-12 col-sm-offset-5">
					<button type="submit" name="save" class="btn btn-sm btn-primary"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Pilih No. Kursi</h4>
      </div>
      <div class="modal-body" id="container-no-kursi"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
	var formItemPemesanan = $('table.form-sub-item-pemesanan:first').closest('#form-item-pemesanan').html();
	var idNoKursiUsed = [];
	var invoker;

	$(document).on('click', '.btn-add-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$('#form-item-pemesanan').append(formItemPemesanan);
		$('table.form-sub-item-pemesanan:last .container-btn-remove').removeClass('hide');
		init();
	});

	$(document).on('click', '.btn-remove-form', function(e) {
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).closest('table.form-sub-item-pemesanan').remove();
	});

	function NoKursiUsed(){
		$('select.id_no_kursi').each(function(index, value){
			var val = $(this).val();
			if (val != ''){
				idNoKursiUsed.push(val);
			}
		});
		return idNoKursiUsed;
	}

	function init(){
		$('input.tanggal_pemberangkatan').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true,
			onSelect: function(dateText, inst) {
				alert();
			}
		}).on('changeDate', function(ev){
			var selector = $(this);
			var date1 = new Date($(this).datepicker('getDate'));
			var date2 = new Date();
			var diffDays = dateDiffInDays(date2, date1);
			if (diffDays < 0){
				selector.val('');
				$.notify("Tanggal sudah kadaluarsa", "warn");
				selector.closest('table').find('select.id_pemberangkatan').html(opt);
			}else
			if (selector.closest('table').find('.id_kota_asal').val() != '' && selector.closest('table').find('.id_kota_tujuan').val() != ''){
				_getPemberangkatan(selector);
			}
		});

		$('select.id_kota_asal, select.id_kota_tujuan').change(function(){
			if ($(this).val() != ''){
				var selector = $(this);
				_getPemberangkatan(selector);
			}
		});
	}

	// @return void
	function _getPemberangkatan(selector){
		var opt = '<option value="">Pilih Pemberangkatan</option>';
        var imgLoader = selector.closest('table').find('.imgLoaderTglPemberangkatan');
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=penumpang&c=getPemberangkatanJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                tanggal_pemberangkatan : selector.closest('table').find('.tanggal_pemberangkatan').val(),
                id_kota_asal: selector.closest('table').find('.id_kota_asal').val(),
                id_kota_tujuan: selector.closest('table').find('.id_kota_tujuan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                    $.each(response.data, function(i, v){
                        opt += '<option value="'+v['id_pemberangkatan']+'" data-id-armada="'+v['id_armada']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                    });
                    selector.closest('table').find('select.id_pemberangkatan').html(opt);
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data pemberangkatan', 'error');
            }
        });
		return;
	}

	$(document).ready(function(){
		init();
	});

	var _MS_PER_DAY = 1000 * 60 * 60 * 24;

	// a and b are javascript Date objects
	function dateDiffInDays(a, b) {
	  // Discard the time and time-zone information.
	  var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
	  var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());

	  return Math.floor((utc2 - utc1) / _MS_PER_DAY);
	}

	$(document).on('change', 'select.id_pemberangkatan', function(e){
		e.stopImmediatePropagation();
		var selector = $(this);
        var id_pemberangkatan = $(this).val();
        var imgLoader = $(this).closest('table').find('.imgLoaderIdPemberangkatan');
        var optionNoKursi = '<option value="">Pilih Nomer Kursi</option>';
        imgLoader.show();
        $.ajax({
            url : "<?php echo BASE_URL.'?m=penumpang&c=getNoKursiJson&a=do';?>",
            type : 'post', 
            dataType : 'jSON',
            data : {
                ref_id : id_pemberangkatan,
                tanggal_pemberangkatan : selector.closest('table').find('input.tanggal_pemberangkatan').val()
            },
            success : function(response){
                imgLoader.hide();
                if (response.success){
                	// var _noKursiUsed = NoKursiUsed();
                 //    $.each(response.data, function(i, v){
                 //    	if ($.inArray(v['id_no_kursi'], _noKursiUsed) < 0){
                 //    		optionNoKursi += '<option value="'+v['id_no_kursi']+'">'+v['no_kursi']+'</option>';
                 //    	}                        
                 //    });
                 //    selector.closest('table').find('select.id_no_kursi').html(optionNoKursi);
                    selector.closest('table').find('.label_sub_total').html(response.harga);
                    selector.closest('table').find('input.sub_total').val(response.harga);

                    var _total = 0;
                    $('input.sub_total').each(function(i, v){
                    	_total += new Number($(this).val());
                    });
                    $('input.total').val(_total);
                    $('.label-total').html('Total Bayar Rp. '+_total+',-');
                }
            },
            error : function(){
                imgLoader.hide();
                $.notify('Gagal Mengambil data nomer kursi kosong', 'error');
            }
        });
    });

	$('.btn-cek-no-kursi').click(function(){
		if ($(this).closest('table').find('input.tanggal_pemberangkatan').val() == ''){
			$.notify('Pilih Tanggal Pemberangkatan terlebih dulu', 'error');
			return false;
		}else
		if ($(this).closest('table').find('select.id_pemberangkatan').val() == ''){
			$.notify('Pilih Pemberangkatan terlebih dulu', 'error');
			return false;
		}else{
			return true;
		}
		return false;
	});

	$('#myModal').on('shown.bs.modal', function(e) {
		invoker = $(e.relatedTarget);
		var _no_kursi_used = [];
		var _id_kota_asal_all = [];
		var _id_kota_tujuan_all = [];
		var _id_pemberangkatan_all = [];
		var _id_armada_all = [];

		$.each($('input.id_no_kursi'), function(i, v){
			if ($(this).val() != ''){
				_no_kursi_used.push($(this).val());
			}			
		});

		$.each($('.id_kota_asal'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_asal_all.push($(this).val());
			}
		});

		$.each($('.id_kota_tujuan'), function(i, v){
			if ($(this).val() != ''){
				_id_kota_tujuan_all.push($(this).val());
			}
		});

		$.each($('.id_pemberangkatan'), function(i, v){
			if ($(this).val() != ''){
				_id_pemberangkatan_all.push($(this).val());
				_id_armada_all.push($(this).find('option:selected').attr('data-id-armada'));
			}
		});

	    $.ajax({
	    	url : "<?php echo BASE_URL.'?m=penumpang&c=getNoKursi&a=do';?>",
	    	type : "post",
	    	dataType : 'html',
	    	data : {
	    		id_pemberangkatan : invoker.closest('table').find('select.id_pemberangkatan').val(),
	    		tanggal_pemberangkatan : invoker.closest('table').find('input.tanggal_pemberangkatan').val(),
	    		no_kursi_terpakai : _no_kursi_used,
	    		id_kota_asal: invoker.closest('table').find('select.id_kota_asal').val(),
	    		id_kota_tujuan: invoker.closest('table').find('select.id_kota_tujuan').val(),
	    		id_pemberangkatan_all : _id_pemberangkatan_all,
	    		id_kota_asal_all : _id_kota_asal_all,
	    		id_kota_tujuan_all : _id_kota_tujuan_all,
	    		id_armada_all : _id_armada_all
	    	},
	    	success : function(response){
	    		$('#container-no-kursi').html(response);
	    	},
	    	error : function(){
	    		$.notify('Terjadi kesalahan, coba ulangi kembali');
	    		$('#myModal').modal('hide');
	    	}
	    });
	});
</script>