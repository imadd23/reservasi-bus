<?php
require_once 'modelPenumpang.class.php';
$model = new modelPenumpang();

$tanggal_pemberangkatan = $_POST['tanggal_pemberangkatan'];
if (empty($tanggal_pemberangkatan)){
	header('location: '.BASE_URL.'?m=penumpang&c=viewEdit&a=view&statusUpdate=0&ref_id='.$_POST['id_detail_pembelian_tiket'][0]);
}
$update = true;

foreach ($tanggal_pemberangkatan as $key => $value) {
	$p = array(
		'tanggal_pemberangkatan' => date('Y-m-d', strtotime($_POST['tanggal_pemberangkatan'][$key])),
	    'id_pemberangkatan' => $_POST['id_pemberangkatan'][$key],
	    'id_kota_tujuan' => $_POST['id_kota_tujuan'][$key],
	    'id_no_kursi' => $_POST['id_no_kursi'][$key],
	    'atas_nama' => $_POST['atas_nama'][$key],
	    'id_penumpang' => $_POST['id_penumpang'][$key],
	    'sub_total' => $_POST['sub_total'][$key],
	    'id_pembelian_tiket' => $_POST['id_pembelian_tiket'][$key]
	   );
	$id_pelanggan = $_SESSION['user_id'];
	$nama_pelanggan = $_SESSION['username'];
	$no_hp = $_SESSION['no_hp'];
	$status_bayar = 'belum lunas';
	$id_detail_pembelian_tiket = $_POST['id_detail_pembelian_tiket'][$key];

	if (isset($_POST['save'])){

	    if (empty($p['tanggal_pemberangkatan']) || empty($p['id_pemberangkatan']) || empty($p['id_kota_tujuan']) || empty($p['id_no_kursi']) || empty($p['atas_nama']) || empty($p['sub_total'])){
	    	$update = false;
	    }else{
	    	$updateDetail = $model->EditDetailPembelian($p['id_pembelian_tiket'], $p['id_pemberangkatan'], $p['id_kota_tujuan'], $p['id_no_kursi'], $p['tanggal_pemberangkatan'], $p['atas_nama'], $p['id_penumpang'], $p['sub_total'], $id_detail_pembelian_tiket);
	    	$update = $updateDetail && $model->updatePembelian($p['id_pembelian_tiket'], $id_pelanggan, $nama_pelanggan, $no_hp, $status_bayar);
		}
			
	}	# code...
}

if ($update){
	header('location: '.BASE_URL.'?m=penumpang&c=viewPenumpang&a=view&statusUpdate=1');
}else{
	header('location: '.BASE_URL.'?m=penumpang&c=viewEdit&a=view&statusUpdate=0&ref_id='.$_POST['id_detail_pembelian_tiket'][0]);
}
?>