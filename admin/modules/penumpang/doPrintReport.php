<?php
if (!defined('FPDF_FONTPATH'))
	define('FPDF_FONTPATH', Library::getDir('html2pdf/fonts'));
require_once 'modelPenumpang.class.php';
Library::load('html2pdf.php', 'html2pdf');
$model = new modelPenumpang();
$p = array(
	'from' => date('Y-m-d', strtotime($_REQUEST['from'])),
	'to' => date('Y-m-d', strtotime($_REQUEST['to'])),	
	'status_bayar' => $_REQUEST['status_bayar']
   );
$penumpang = $model->getPenumpangForReport($p['from'], $p['to'], $p['status_bayar']);

$judul = ($p['status_bayar'] == 'lunas') ? 'REPORT PEMBELIAN TIKET (LUNAS) ' : 'REPORT PEMBELIAN TIKET (BELUM LUNAS) ';
ob_start();?>
<style type="text/css">
	.center{
		text-align: center;
	}
	.right{
		text-align: right;
	}
	.no-padding{
		padding: 0px;
		margin: 0px;
	}
	.dashed{
		border-style: dashed;
	}
	.col-sm-14{
		width: 100%;
	}
	table.table-control{
		width: 100%;
		border-spacing: 0;
		border-collapse: collapse;
		font-family: Helvetica, Arial, sans-serif;
	}
	table.table-bordered td, table.table-bordered th{
		border: 1px solid #ccc;
		padding: 3px;
	}
</style>
<page>
<h2 class="center">PO FAMILIY RAYA CERIA</h2>
<h3 class="center no-padding"><?php echo $judul.$_REQUEST['from'];?> S.D <?php echo $_REQUEST['to'];?></h3>
<hr class="dashed">
<table class="table-control table-bordered">
	<col style="width: 5%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 10%;">
    <col style="width: 15%;">
    <col style="width: 15%;">
    <col style="width: 15%;">
    <col style="width: 10%;">
	<tr>
		<th class="center">No</th>
        <th>ID Pembelian</th>
        <th>Tanggal Beli</th>
        <th>Pembeli</th>
        <th>No. HP</th>
        <th>Nama Penumpang</th>
        <th>Tujuan</th>
        <th>Armada</th>
        <th class="center">Total Bayar</th>
	</tr>
	<?php
	if (!empty($penumpang)){
		$total = 0;
		foreach ($penumpang as $key => $value) {
			$total += $value['sub_total'];
			echo 
			'<tr>'.
				'<td class="center">'.($key + 1).'</td>'.
				'<td>'.$value['id_pembelian_tiket'].'</td>'.
				'<td>'.date('d M Y', strtotime($value['tanggal_beli'])).'</td>'.
				'<td>'.$value['nama_pelanggan'].'</td>'.
				'<td>'.$value['no_hp'].'</td>'.
				'<td>'.$value['atas_nama'].'</td>'.
				'<td>'.$value['kota_tujuan'].'</td>'.
				'<td>'.$value['nama_armada'].'</td>'.
				'<td class="center">'.$value['sub_total'].'</td>'.
			'</tr>';
		}
		echo 
		'<tr>'.
			'<td class="right" colspan="8"><b>Total</b></td>'.
			'<td class="center"><b>'.$total.'</b></td>'.
		'</tr>';
	}else{
		echo 
		'<tr>'.
			'<td class="center" colspan="9">Data Kosong</td>'.
		'</tr>';
	}
	?>
</table>
<page_footer>
	<div class="col-sm-14 center">
		<hr class="dashed">
		<i>PO. Family Ceria</i>
	</div>
</page_footer>
</page>
<?php $content = ob_get_clean();
try{
	$html2pdf = new html2pdf('L', 'Folio', 'en', true, 'UTF-8'/*, array(1, 2, 1, 3)*/);
	$html2pdf->writeHTML($content);
	$html2pdf->Output('report-pembelian-tiket-'.date('dmy').'.pdf');
}
catch(HTML2PDF_exception $e) {
	echo $e;
	exit;
}
?>