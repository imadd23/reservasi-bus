<?php
$message = '';
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil disimpan</div>';
}else
if (isset($_GET['statusAdd']) && $_GET['statusAdd'] ==  false){
    $message = '<div class="alert alert-danger">Data gagal disimpan</div>';
}

if (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil diubah</div>';
}

if (isset($_GET['statusDelete']) && $_GET['statusDelete'] ==  true){
    $message = '<div class="alert alert-success">Data berhasil dihapus</div>';
}else
if (isset($_GET['statusDelete']) && $_GET['statusDelete'] == false){
    $message = '<div class="alert alert-danger">Data gagal dihapus</div>';
}
?>

<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">

<ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#semua">Semua</a></li>
    <li><a data-toggle="tab" href="#per-pemberangkatan">Per Pemberangkatan</a></li>
</ul>

<div class="tab-content">
    <div id="semua" class="tab-pane fade in active">
        <h3>Daftar Penumpang</h3>
        <?php echo $message;?>
        <a class="btn btn-sm btn-primary" href="<?php echo BASE_URL."?m=penumpang&c=viewAdd&a=view";?>"><img src="<?php echo BASE_ADDRESS."icon/add.png";?>" title='tambah penumpang'> Tambah</a>
        <!-- <a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a> -->
        <br>
        <br>
        <?php 
        require_once 'modelPenumpang.class.php';
        $model = new modelPenumpang();
        $waktu_pembatalan = $model->getSettingPembelianTiket();
        $jamCancelBeforeCancel = 0;
        $jamCancelAfterConfirm = 0;

        if (!empty($waktu_pembatalan)){
          foreach ($waktu_pembatalan as $key => $value) {
              if ($value['id'] == 'BT'){
                  $jamCancelBeforeCancel = $value['set'];
              }
          }
        }
        $info = '<div class="alert alert-info"><ul><li>Tiket tidak bisa dibatalkan setelah dikonfirmasi petugas</li>';
        if (!empty($jamCancelBeforeCancel)){
          $info .= '<li>Pembatalan tiket sebelum dikonfirmasi petugas maksimal '.$jamCancelBeforeCancel.' jam</li>';
        }
        if (!empty($jamCancelAfterConfirm)){
          $info .= '<li>Pembatalan tiket setelah dikonfirmasi petugas maksimal '.$jamCancelAfterConfirm.' jam</li>';
        }
        $info .= '</ul></div>';
        echo $info;
        ?>
        <div class="table-responsive">
            <table id="table-penumpang" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=penumpang&c=getData&a=do';?>">
                <thead>
                    <th class="center col-sm-1" nowrap>No</th>
                    <th class="col-sm-1" nowrap>ID Pembelian</th>
                    <th class="col-sm-1" nowrap>Tanggal Beli</th>
                    <th class="col-sm-1" nowrap>Tanggal Konfirmasi</th>
                    <th class="col-sm-2" nowrap>Pembeli</th>
                    <th class="col-sm-1" nowrap>No. HP</th>
                    <th class="col-sm-2" nowrap>Penumpang</th>
                    <th class="col-sm-1" nowrap>Tujuan</th>
                    <th class="col-sm-1" nowrap>Armada</th>
                    <th class="center col-sm-1" nowrap>Total Bayar</th>
                    <th class="center col-sm-1" nowrap>Aksi</th>
                </thead>
            </table>
        </div>
        <!-- Modal -->
        <form action="<?php echo BASE_URL;?>" method="get" class="form-horizontal">
            <input type="hidden" name="m" value="penumpang">
            <input type="hidden" name="c" value="doPrintReport">
            <input type="hidden" name="a" value="do">
            <div id="myModal" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Cetak Report Pembelian tiket</h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Dari Tanggal</label> 
                            <div class="col-sm-7">
                                <input type="text" name="from" class="input-sm form-control datepicker" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Sampai Tanggal</label> 
                            <div class="col-sm-7">
                                <input type="text" name="to" class="input-sm form-control datepicker" required>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Cetak</button>
                  </div>
                </div>

              </div>
            </div>
        </form>
    </div>
    <div id="per-pemberangkatan" class="tab-pane fade">
        <br>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">Filter</div>
                    <div class="panel-body">
                        <form action="#" id="form-filter" class="form-horizontal">
                            <div class="form-group">
                                <label class="col-sm-3 conttrol-label">Tanggal</label>
                                <div class="col-sm-5">
                                    <input type="text" name="tanggal1" class="input-sm form-control datepicker">
                                </div>
                                <div class="col-sm-1">
                                    <img id="imgLoaderTglPemberangkatan" class="imgLoaderTglPemberangkatan pull-right" style="display:none" src="<?php echo BASE_ADDRESS.'images/ajax-loader.gif';?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 conttrol-label">Pemberangkatan</label>
                                <div class="col-sm-6">
                                    <select name="id_pemberangkatan1" class="form-control input-sm id_pemberangkatan" required>
                                        <option value="">Pilih Pemberangkatan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 conttrol-label"></label>
                                <div class="col-sm-9">
                                    <!-- <a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a> -->
                                    <input type="submit" class="btn btn-sm btn-primary" value="Filter">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="table-penumpang-per-pemberangkatan" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=penumpang&c=getData&a=do';?>">
                <thead>
                    <th class="center col-sm-1" nowrap>No</th>
                    <th class="col-sm-1" nowrap>ID Pembelian</th>
                    <th class="col-sm-1" nowrap>Tanggal Beli</th>
                    <th class="col-sm-1" nowrap>Tanggal Konfirmasi</th>
                    <th class="col-sm-2" nowrap>Pembeli</th>
                    <th class="col-sm-1" nowrap>No. HP</th>
                    <th class="col-sm-2" nowrap>Penumpang</th>
                    <th class="col-sm-1" nowrap>Tujuan</th>
                    <th class="col-sm-1" nowrap>Armada</th>
                    <th class="center col-sm-1" nowrap>Total Bayar</th>
                    <th class="center col-sm-1" nowrap>Aksi</th>
                </thead>
            </table>
        </div>
    </div>
</div> 
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/notify.js';?>"></script>
<script type="text/javascript">
    var table2;

    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoclose : true
        }).on('changeDate', function(ev){
            var selector = $(this);
            var tgl = $(this).datepicker('getDate');
            if (tgl != ''){
                var opt = '<option value="">Pilih Pemberangkatan</option>';
                var imgLoader = selector.closest('table').find('.imgLoaderTglPemberangkatan');
                imgLoader.show();
                $.ajax({
                    url : "<?php echo BASE_URL.'?m=penumpang&c=getPemberangkatanJson&a=do';?>",
                    type : 'post', 
                    dataType : 'jSON',
                    data : {
                        tanggal_pemberangkatan : selector.val(),
                    },
                    success : function(response){
                        imgLoader.hide();
                        if (response.success){
                            $.each(response.data, function(i, v){
                                opt += '<option value="'+v['id_pemberangkatan']+'">'+v['nama_armada']+' | '+v['kota_asal']+' - '+v['kota_tujuan']+'</option>';
                            });
                            selector.closest('form').find('select.id_pemberangkatan').html(opt);
                        }
                    },
                    error : function(){
                        imgLoader.hide();
                        $.notify('Gagal Mengambil data pemberangkatan', 'error');
                    }
                });
            }
        });

        $('#table-penumpang').dataTable({
            ajax : {
                url : $('#table-penumpang').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'id_pembelian_tiket'},
                {data : 'tanggal_beli'},
                {data : 'tanggal_bayar'},
                {data : 'nama_pelanggan'},
                {data : 'no_hp'},
                {data : 'atas_nama'},
                {data : 'kota_tujuan'},
                {data : 'armada_kursi'},
                {data : 'total_harga', class : 'center'},
                {data : 'aksi', class : 'center'}
            ],
            order : [[1, 'asc']]
        });

        table2 = $('#table-penumpang-per-pemberangkatan').dataTable({
            ajax : {
                url : $('#table-penumpang-per-pemberangkatan').attr('data-url'),
                type : 'post',
                dataType : 'json',
                data : function(r){
                    r.tgl = $('input[name=tanggal1]').val();
                    r.id_pemberangkatan = $('select[name=id_pemberangkatan1]').val();
                    // return r;
                }
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'id_pembelian_tiket'},
                {data : 'tanggal_beli'},
                {data : 'tanggal_bayar'},
                {data : 'nama_pelanggan'},
                {data : 'no_hp'},
                {data : 'atas_nama'},
                {data : 'kota_tujuan'},
                {data : 'armada_kursi'},
                {data : 'total_harga', class : 'center'},
                {data : 'aksi', class : 'center'}
            ],
            order : [[1, 'asc']]
        });

        $('#form-filter').submit(function(e){
            e.preventDefault();
            table2.api().ajax.reload();
        });
    });

    $(document).on('click', 'a.btn-hapus', function(){
        if (confirm('Anda yakin akan menghapus data pembelian tiket atas nama '+$(this).attr('data-nama')+' ?')){
            return true;
        }else{
            return false;
        }
        return false;
    });
</script>