<?php
ob_start();
require_once "modelLaporan.php";
$model = new modelLaporan();
$tanggal = !empty($_POST['tanggal']) ? date('Y-m-d', strtotime($_POST['tanggal'])) : date('Y-m-d');
$data = $model->getNoKursi($tanggal);
if (!empty($data)){
	foreach ($data as $key => $value) {
		$data[$key]['no'] = $key + 1;
	}
}
echo json_encode(array('data' => $data));
?>