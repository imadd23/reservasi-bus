<?php
Class modelLaporan{

	public function __construct(){

	}

	public function getNoKursi($tanggal = ''){
		if (empty($tanggal) || $tanggal == ''){
			$tanggal = date('Y-m-d');
		}
		$qPemberangkatan = mysql_query("SELECT nama_armada, '$tanggal' AS tanggal,
		GROUP_CONCAT(DISTINCT no_kursi ORDER BY no_kursi SEPARATOR ', ') AS no_kursi, P.*, 
		CONCAT(A.nama_kota, ' (', C.nama_provinsi, ') - ',B.nama_kota, ' (', D.nama_provinsi, ')') AS pemberangkatan
		FROM pemberangkatan AS P
		LEFT JOIN kota AS A
		    ON A.`id_kota` = P.`id_kota_asal`
		LEFT JOIN provinsi AS C
		    ON C.`id_provinsi` = A.`id_provinsi`
		LEFT JOIN kota AS B
		    ON B.`id_kota` = P.`id_kota_tujuan`
		LEFT JOIN provinsi AS D
		    ON D.`id_provinsi` = B.`id_provinsi`
		LEFT JOIN armada E
		    ON P.`id_armada` = E.`id_armada`
		LEFT JOIN no_kursi AS F
			ON E.`id_armada` = F.`id_armada`
		WHERE F.id_no_kursi NOT IN (
			SELECT id_no_kursi 
			FROM detail_pembelian 
			WHERE
			DATE(tanggal_pemberangkatan) = '$tanggal'
			)
			AND F.no_kursi IS NOT NULL
		GROUP BY E.`id_armada`
		ORDER BY `nama_armada` ASC ");

		$arrayPemberangkatan = array();
		while ($dataPemberangkatan = mysql_fetch_array($qPemberangkatan)) {
		    $arrayPemberangkatan[] = $dataPemberangkatan;
		}

		return $arrayPemberangkatan;
	}
}
?>