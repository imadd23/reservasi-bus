<h3>Laporan Pengiriman Barang (Kurir)</h3>
<a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a>
<br>
<br>
<div class="table-responsive">
    <table id="table-kurir" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=kurir&c=getData&a=do';?>">
        <thead>
            <th class="text-center col-sm-1" nowrap>No</th>
            <th nowrap>ID Pengiriman</th>
            <th nowrap>Paket</th>
            <th nowrap>Nama Pengirim</th>
            <th nowrap>Nama Penerima</th>
            <th>Alamat Penerima</th>
            <th>Alamat Penerima</th>
            <th nowrap>Tanggal Kirim</th>
            <th nowrap>Tanggal Sampai</th>
            <th nowrap>Kota Asal</th>
            <th nowrap>No. telp Pengirim</th>
            <th nowrap>No. telp Tujuan</th>
            <th nowrap>Kota Tujuan</th>
            <th nowrap>Lokasi Terakhir</th>
            <th nowrap>Berat</th>
            <th nowrap>Harga (Kg)</th>
            <th nowrap>Total Biaya</th>
        </thead>
    </table>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">

<!-- form report -->
<form action="<?php echo BASE_URL;?>" method="get" class="form-horizontal">
    <input type="hidden" name="m" value="kurir">
    <input type="hidden" name="c" value="doPrintReport">
    <input type="hidden" name="a" value="do">
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cetak Report Pengiriman Barang</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Dari Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="from" class="input-sm form-control datepicker" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Sampai Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="to" class="input-sm form-control datepicker" required>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Cetak</button>
          </div>
        </div>

      </div>
    </div>
</form>
<!-- end form report -->

<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('#table-kurir').dataTable({
            ajax : {
                url : $('#table-kurir').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'text-center'},
                {data : 'id_pengiriman'},
                {data : 'nama_paket', class : 'text-center'},
                {data : 'nama_pengirim'},
                {data : 'nama_penerima'},
                {data : 'alamat_pengirim'},
                {data : 'alamat_tujuan'},
                {data : 'tanggal_kirim'},
                {data : 'tanggal_sampai'},
                {data : 'kota_asal'},
                {data : 'no_telp_pengirim'},
                {data : 'no_telp_tujuan'},
                {data : 'kota_tujuan'},
                {data : 'posisi_sekarang'},
                {data : 'berat'},
                {data : 'harga'},
                {data : 'total_harga'}
            ],
            fixedColumns: {
                leftColumns : 5
            },
            scrollY : "300px",
            scrollX : true,
            scrollCollapse : true
        });
    });
</script>