<h3>Laporan Penumpang</h3>
<a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a>
<br>
<br>
<div class="table-responsive">
    <table id="table-penumpang" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=penumpang&c=getData&a=do';?>">
        <thead>
            <th class="center col-sm-1" nowrap>No</th>
            <th class="col-sm-1" nowrap>ID Pembelian</th>
            <th class="col-sm-1" nowrap>Tanggal Beli</th>
            <th class="center col-sm-1" nowrap>Status Pembayaran</th>
            <th class="col-sm-2" nowrap>Pembeli</th>
            <th class="col-sm-1" nowrap>No. HP</th>
            <th class="col-sm-2" nowrap>Nama Penumpang</th>
            <th class="col-sm-1" nowrap>Tujuan</th>
            <th class="col-sm-1" nowrap>Armada</th>
            <th class="center col-sm-1" nowrap>Total Bayar</th>
        </thead>
    </table>
</div>
<!-- Modal -->
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">
<form action="<?php echo BASE_URL;?>" method="get" class="form-horizontal">
    <input type="hidden" name="m" value="penumpang">
    <input type="hidden" name="c" value="doPrintReport">
    <input type="hidden" name="a" value="do">
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cetak Report Pembelian tiket</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Dari Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="from" class="input-sm form-control datepicker" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Sampai Tanggal</label> 
                    <div class="col-sm-7">
                        <input type="text" name="to" class="input-sm form-control datepicker" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Status Pembayaran</label> 
                    <div class="col-sm-7">
                        <select name="status_bayar" class="input-sm form-control">
                            <option value="lunas">Lunas</option>
                            <option value="belum lunas">Belum Lunas</option>
                        </select>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Cetak</button>
          </div>
        </div>

      </div>
    </div>
</form>
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        $('#table-penumpang').dataTable({
            ajax : {
                url : $('#table-penumpang').attr('data-url'),
                type : 'post',
                dataType : 'json'
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'id_pembelian_tiket'},
                {data : 'tanggal_beli'},
                {data : 'status_bayar', class : 'center'},
                {data : 'nama_pelanggan'},
                {data : 'no_hp'},
                {data : 'atas_nama'},
                {data : 'kota_tujuan'},
                {data : 'armada_kursi'},
                {data : 'total_harga', class : 'center'}
            ]
        });
    });
</script>