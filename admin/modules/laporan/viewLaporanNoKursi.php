<h3>Laporan Ketersediaan Slot Kursi</h3>
<div class="row">
    <div class="col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">Filter</div>
            <div class="panel-body">
                <form action="#" id="form-filter" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 conttrol-label">Tanggal</label>
                        <div class="col-sm-6">
                            <input type="text" name="tanggal" class="input-sm form-control datepicker">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 conttrol-label"></label>
                        <div class="col-sm-9">
                            <!-- <a class="btn btn-sm btn-warning" href="#" data-toggle="modal" data-target="#myModal"><img src="<?php echo BASE_ADDRESS."icon/print.png";?>" title='Cetak Laporan'> Cetak Report</a> -->
                            <input type="submit" class="btn btn-sm btn-primary" value="Filter">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table id="table-sewaBus" class="table table-hover table-bordered table-striped" width="100%" cell-padding="0" data-url="<?php echo BASE_URL.'?m=laporan&c=getDataNoKursi&a=do';?>">
        <thead>
            <tr>
                <th class="text-center" nowrap>No</th>
                <th nowrap>Tanggal</th>
                <th nowrap>Armada</th>
                <th nowrap>Pemberangkatan</th>
                <th nowrap>No. Kursi Tersedia</th>
            </tr>
        </thead>
    </table>
</div>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/datepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-datetimepicker.css';?>">
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/bootstrap-timepicker.css';?>">
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/bootstrap-datepicker.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.datepicker').datepicker({
            format : 'dd-mm-yyyy',
            autoClose : true
        }).on('changeDate', function(ev){
            $(this).datepicker('hide');
        });

        var table = $('#table-sewaBus').dataTable({
            ajax : {
                url : $('#table-sewaBus').attr('data-url'),
                type : 'post',
                dataType : 'json',
                data : function(d){
                    return $('#form-filter').serialize();
                }
            },
            columns : [
                {data : 'no', class : 'center'},
                {data : 'tanggal'},
                {data : 'nama_armada'},
                {data : 'pemberangkatan'},
                {data : 'no_kursi'}
            ],
            searching : false,
            "bLengthChange": false
        });

        $('#form-filter').submit(function(){
            table.api().ajax.reload();
            return false;
        });
    });
</script>