<?php
ob_start();
$queryPemberangkatan = "SELECT P.*, 
CONCAT(A.nama_kota, ' (', C.nama_provinsi, ')') AS kota_asal,
CONCAT(B.nama_kota, ' (', D.nama_provinsi, ')') AS kota_tujuan, 
nama_paket
FROM harga_pengiriman AS P
LEFT JOIN kota AS A
    ON A.`id_kota` = P.`id_kota_asal`
LEFT JOIN provinsi AS C
    ON C.`id_provinsi` = A.`id_provinsi`
LEFT JOIN kota AS B
    ON B.`id_kota` = P.`id_kota_tujuan`
LEFT JOIN provinsi AS D
    ON D.`id_provinsi` = B.`id_provinsi`
LEFT JOIN paket_pengiriman E
    ON P.`id_paket_pengiriman` = E.`id_paket_pengiriman`
ORDER BY nama_paket ASC";

$qPemberangkatan = mysql_query($queryPemberangkatan);
$arrayPemberangkatan = array();
$i = 0;
while ($dataPemberangkatan = mysql_fetch_array($qPemberangkatan)) {
    $arrayPemberangkatan[] = $dataPemberangkatan;
}

if (!empty($arrayPemberangkatan)){
	foreach ($arrayPemberangkatan as $key => $value) {
		$arrayPemberangkatan[$key]['no'] = $key + 1;
		$arrayPemberangkatan[$key]['aksi'] = "<a class='btn-action' href='".BASE_URL."?m=harga_pengiriman&c=viewAdd&a=view&ref_id=".$value['id_harga_pengiriman']."'><img src='".BASE_ADDRESS."icon/edit.png' title='ubah'></a>\n
            <a class='btn-action btn-hapus' data-nama='".$value['nama_paket']." (".$value['kota_asal']." - ".$value['kota_tujuan'].")' href='".BASE_URL."?m=harga_pengiriman&c=doDelete&a=do&ref_id=".$value['id_harga_pengiriman']."'><img src='".BASE_ADDRESS."icon/trash.png' title='hapus'></a>\n";
	}
}

echo json_encode(array('data' => $arrayPemberangkatan));
?>