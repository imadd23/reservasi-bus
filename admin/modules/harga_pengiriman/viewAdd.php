<?php
$qKota = mysql_query(" SELECT id_kota, CONCAT(nama_kota, ' (', nama_provinsi, ')') AS nama_kota 
FROM kota 
LEFT JOIN provinsi ON kota.id_provinsi = provinsi.id_provinsi
ORDER BY nama_kota ASC");
$dataKota = array();
while ($arrayKota = mysql_fetch_array($qKota)){
    $dataKota[] = $arrayKota;
}

$qPaket = mysql_query("SELECT * FROM paket_pengiriman ORDER BY nama_paket ASC");
$dataPaket = array();
while ($arrayPaket = mysql_fetch_array($qPaket)) {
    $dataPaket[] = $arrayPaket;
}

$dataHari = array('senin', 'selasa', 'rabu', 'kamis', 'jumat', 'sabtu', 'minggu');

if (isset($_GET['harga'])){
    $id_kota_asal = $_GET['id_kota_asal'];
    $id_kota_tujuan = $_GET['id_kota_tujuan'];
    $id_paket_pengiriman = $_GET['id_paket_pengiriman'];
    $harga = $_GET['harga'];
    $estimasi_waktu = $_GET['estimasi_waktu'];
    $ref_id = (isset($_GET['ref_id'])) ? '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">' : '';
}else
if (isset($_GET['ref_id'])){
    $query = mysql_query(" SELECT * FROM harga_pengiriman WHERE id_harga_pengiriman = '".$_GET['ref_id']."'");
    $a = mysql_fetch_object($query);
    if ($a){
        $id_kota_asal = $a->id_kota_asal;
        $id_kota_tujuan = $a->id_kota_tujuan;
        $id_paket_pengiriman = $a->id_paket_pengiriman;
        $estimasi_waktu = $a->estimasi_waktu;
        $harga = $a->harga;
    }else{
        $id_kota_asal = '';
        $id_kota_tujuan = '';
        $id_paket_pengiriman = '';
        $estimasi_waktu = '';
        $harga = '';
    }
    $ref_id = '<input type="hidden" name="ref_id" value="'.$_GET['ref_id'].'">';
}else{
    $id_kota_asal = '';
    $id_kota_tujuan = '';
    $id_paket_pengiriman = '';
    $estimasi_waktu = '';
    $harga = '';
    $ref_id = '';
}

$message = '';
if ((isset($_GET['statusAdd']) && $_GET['statusAdd'] == '0') || (isset($_GET['statusUpdate']) && $_GET['statusUpdate'] == '0')){
    $message = '<div class="alert alert-danger">Data gagal disimpan, pastikan semua kolom yang bertanda bintang diisi</div>';
}
if (isset($_GET['isDuplicated']) && $_GET['isDuplicated'] == '1'){
    $message = '<div class="alert alert-warning">Maaf, Data sudah ada</div>';
}
?>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ADDRESS.'style/jquery.timepicker.css';?>">
<script type="text/javascript" src="<?php echo BASE_ADDRESS.'js/jquery.timepicker.js';?>"></script>
<div class="col-sm-7">
    <h3>Tambah data paket harga pengiriman</h3>
    <?php echo $message;?>
    <form method="post" action="<?php echo BASE_URL.'?m=harga_pengiriman&c=doAdd&a=do';?>" class="form-horizontal">
        <?php echo $ref_id;?>
        <div class="form-group">
            <label class="col-sm-3 control-label">Kota Asal *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_kota_asal" id="id_kota_asal">
                    <option value="">Pilih Kota</option>
                    <?php
                    if (!empty($dataKota)){
                        foreach ($dataKota as $key => $value) {
                            if ($value['id_kota'] == $id_kota_asal){
                                echo '<option value="'.$value['id_kota'].'" selected>'.$value['nama_kota'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_kota'].'">'.$value['nama_kota'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Kota Tujuan *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_kota_tujuan" id="id_kota_tujuan">
                    <option value="">Pilih Kota</option>
                    <?php
                    if (!empty($dataKota)){
                        foreach ($dataKota as $key => $value) {
                            if ($value['id_kota'] == $id_kota_tujuan){
                                echo '<option value="'.$value['id_kota'].'" selected>'.$value['nama_kota'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_kota'].'">'.$value['nama_kota'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Jenis Paket *</label>
            <div class="col-sm-9">
                <select class="input-sm form-control" name="id_paket_pengiriman" id="id_paket_pengiriman">
                    <option value="">Pilih Paket</option>
                    <?php
                    if (!empty($dataPaket)){
                        foreach ($dataPaket as $key => $value) {
                            if ($value['id_paket_pengiriman'] == $id_paket_pengiriman){
                                echo '<option value="'.$value['id_paket_pengiriman'].'" selected>'.$value['nama_paket'].'</option>';
                            }else{
                                echo '<option value="'.$value['id_paket_pengiriman'].'">'.$value['nama_paket'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Estimasi Waktu Pengiriman (hari)*</label>
            <div class="col-sm-3">
                <input type="number" class="input-sm form-control" name="estimasi_waktu" id="estimasi_waktu" value="<?php echo $estimasi_waktu;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Harga per Kg*</label>
            <div class="col-sm-9">
                <input type="text" class="input-sm form-control" name="harga" id="harga" value="<?php echo $harga;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"></label>
            <div class="col-sm-9 right">
                <button type="submit" name="save" class="btn btn-sm btn-primary" id="Simpan"><img src="<?php echo BASE_ADDRESS.'icon/save.png';?>"> Simpan</button>
                <a href="<?php echo BASE_URL.'?m=harga_pengiriman&c=viewHargaPengiriman&a=view';?>" name="view" class="btn btn-sm btn-primary" id="lihat"><img src="<?php echo BASE_ADDRESS.'icon/detail.png';?>"> Lihat Data</a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#jam').timepicker({
            'timeFormat' : 'HH:mm',
            'startTime' : '06:00:00',
            'maxHour' : 23,
            'dropdown' : true,
            'scrollbar' : true,
            'dynamic' : false
        });
    });
</script>